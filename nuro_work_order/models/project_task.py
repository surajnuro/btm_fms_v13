from odoo import api, fields, models, _
from odoo.exceptions import ValidationError
import base64
import xlsxwriter

class ProjectTaks(models.Model):
    _inherit="project.task"
    _description = 'Work Order'

    work_type = fields.Selection(selection_add=[('work_order', 'Work Order')])
    work_order_state = fields.Selection([
        ('draft', 'New'),
        ('pmo_review', 'Under Review'),
        ('sale', 'Work Ordered'),
        ('done', 'Done'),
        ('cancel', 'Cancelled'),
        # ('task_create', 'Task Create')
    ], string='Status', copy=False, index=True, track_visibility='onchange', default='draft')
    desc_work_line = fields.One2many('desc.work.line', 'desc_work_id')
    is_merge = fields.Boolean()
    merged_order_ids = fields.Many2many('project.task', 'merged_order_rel', 'order_id', 'task_id',
                                        string="Merged Sale Order ID")
    work_desc = fields.Char('Description')
    total_skill_amt = fields.Float(compute='get_skill_total', store=1)
    total_un_skill_amt = fields.Float(compute='get_skill_total', store=1)
    top_count = fields.Integer(default=0)
    previous_stage = fields.Char()
    unmerge_reason = fields.Text(track_visibility='onchange')
    client_tic_id = fields.Char(related='ticket_id.client_tic_no', string='Ticket')
    due_date = fields.Datetime('Date Deadline')
    work_categories_ids = fields.Many2many('work.categories')
    build_prop = fields.Char()
    con_start = fields.Date()
    con_end = fields.Date()
    is_work_order = fields.Boolean()
    top_id = fields.Many2one('project.task')

    def _compute_top_count(self):
            self.top_count = 1

    @api.depends('labor_planing_line.name', 'labor_planing_line.amount')
    def get_skill_total(self):
        total_skill = 0.0
        total_unskill = 0.0
        skill_prod = self.env.ref('nuro_product_boq.skill_manpower')
        unskill_prod = self.env.ref('nuro_product_boq.unskill_manpower')
        for top in self:
            for l_line in top.labor_planing_line:
                if l_line.name == unskill_prod:
                    total_unskill = l_line.amount
                if l_line.name == skill_prod:
                    total_skill = l_line.amount
            top.total_skill_amt = total_skill
            top.total_un_skill_amt = total_unskill

    def reset_to_draft_wo(self):
        return self.write({'work_order_state': 'draft'})

    def cancel_wo(self):
        if self.top_id:
            if self.top_id.usw_state == 'draft':
                if self.top_id.merged_order_ids:
                    self.top_id.usw_state = 'cancel'
                    self.top_id.is_unmerge = True
                    self.top_id.is_merge = False
                    for wo in self.top_id.merged_order_ids:
                        wo.write({'work_order_state': 'cancel',
                                  'top_id': False})
            else:
                if self.top_id.usw_state not in ['cancel']:
                    raise ValidationError(('Please cancel TOP which is create from WO'))
        task_ids = self.search([('task_id', '=', self.id)])
        if task_ids:
            task_ids.action_task_cancel()
        return self.write({'work_order_state': 'cancel'})

    @api.model
    def create(self, vals):
        seq = self.env['ir.sequence'].next_by_code('work.order.seq')
        if self.env.context.get('default_work_type') == 'work_order' or vals.get('work_type') == 'work_order':
            vals['name'] = seq or ' '
        res = super(ProjectTaks, self).create(vals)
        return res

    def confirm_work_order(self):
        if self.work_type == 'work_order':
            if not self.material_planing_line:
                raise ValidationError(_('Please Add Material Line!!!'))
            order_line = []
            material_ref = self.env.ref('nuro_product_boq.material_top_product')
            for rec in self.labor_planing_line:
                if rec.manhour > 0.0:
                    order_line.append((0, 0, {'product_id': rec.product_id.id,
                                               'name': rec.product_id.name,
                                               'product_uom': rec.product_id.uom_id.id,
                                               'price_unit': rec.labour_boq_rate or rec.rate,
                                               'product_uom_qty': rec.manhour}))

            order_line.append((0, 0, {'product_id': material_ref.id,
                                       'name': material_ref.name,
                                       'product_uom': material_ref.uom_id.id,
                                       'price_unit': self.total_material_price}))

            return self.write({'work_order_state': 'sale',
                               'task_order_date': str(fields.Datetime.now()),
                               'order_line': order_line
                               })

    def get_task(self):
        task_data = self.search([('task_id', 'in', self.ids)])
        form_view = self.env.ref('nuro_project_task_management.unschedule_task_form_view').id
        tree_view = self.env.ref('nuro_project_task_management.unschedule_task_tree_view').id
        if task_data:
            return {
                'name': _('Unschedule Task'),
                'res_model': 'project.task',
                'views': [
                    (tree_view, 'tree'),
                    (form_view, 'form'),
                ],
                'view_mode': 'tree,form',
                'type': 'ir.actions.act_window',
                'domain': [('task_id', 'in', self.ids)]
            }

    def print_work_order_combine(self):
        return self.env.ref('nuro_work_order.combine_work_order_report_view').report_action(self)

    def print_work_order(self):
        return self.env.ref('nuro_work_order.work_order_report_view').report_action(self)

    def print_wo_combind_report_excel(self):
        f_name = '/tmp/woreport.xlsx'
        workbook = xlsxwriter.Workbook(f_name)
        worksheet = workbook.add_worksheet('Report')
        worksheet.set_column('A:N', 12)
        logo_data = base64.b64decode(self.env.user.company_id.logo)
        file = "/tmp/" + str(self.env.user.company_id.id) + ".png"
        with open(file, "wb") as imgFile:
            imgFile.write(logo_data)

        style = workbook.add_format({
            'bold': 1,
            'border': 1,
            'align': 'center',
            'valign': 'vcenter'
        })
        style.set_font_size(8)

        new_style = workbook.add_format({
            'align': 'right',
            'valign': 'vcenter',
            'bold': 1,
            'border': 1,
        })
        new_style.set_font_size(8)

        left_new_style = workbook.add_format({
            'align': 'left',
            'valign': 'vcenter',
            'bold': 1,
            'border': 1,
        })
        left_new_style.set_font_size(8)

        align_value = workbook.add_format({
            'align': 'left',
            'border': 1,
            'valign': 'vcenter'})
        align_value.set_font_size(8)

        center_align_value = workbook.add_format({
            'align': 'center',
            'border': 1,
            'valign': 'vcenter'})
        center_align_value.set_font_size(8)

        float_align_value = workbook.add_format({
            'align': 'right',
            'border': 1,
            'valign': 'vcenter'})
        float_align_value.set_font_size(8)

        signature_style = workbook.add_format({
            'bold': 1,
            'border': 1,
            'align': 'left',
            'valign': 'vcenter'})
        signature_style.set_font_size(8)
        # Code for printing material_order_line
        index = 1
        row = 17

        worksheet.insert_image('A1:F5', file, {'x_offset': 15, 'y_offset': 5})
        worksheet.merge_range('A6:F6', "Task Order Proposal", style)
        worksheet.merge_range('A7:E7', "Task Order Proposal No", new_style)
        worksheet.write('F7', self.sequence or self.un_to_reference or '', center_align_value)
        worksheet.merge_range('A8:E8', 'Sector', new_style)
        worksheet.write('F8', self.branch_id.name or '', center_align_value)
        worksheet.merge_range('A9:E9', 'Task Location', new_style)
        worksheet.write('F9', self.branch_id.name or '', center_align_value)
        worksheet.merge_range('A10:E10', 'Site', new_style)
        worksheet.write('F10', self.site_id.name or '', center_align_value)
        worksheet.merge_range('A11:E11', 'To Implementation Period', new_style)
        worksheet.write('F11', self.implementation_period or '', center_align_value)
        worksheet.merge_range('A12:E12', 'To Commencement Date', new_style)
        worksheet.write('F12', self.task_order_date or '', center_align_value)
        worksheet.merge_range('A13:E13', 'To Completion Date', new_style)
        worksheet.write('F13', self.completion_date or '', center_align_value)
        worksheet.merge_range('A14:E14', 'Progress Status', new_style)
        worksheet.write('F14', self.progess_status or '', center_align_value)
        worksheet.write('A15', "SCOPE", style)
        worksheet.merge_range('B15:F15', self.project_scope, center_align_value)
        worksheet.write('A16', 'Cont Ref.', style)
        worksheet.write('B16', 'Short description of materials.', style)
        worksheet.write('C16', 'LOCATION', style)
        worksheet.write('D16', 'TICKET NUMBER', style)
        worksheet.write('E16', 'WORK ORDER NUMBER', style)
        worksheet.write('F16', 'WORK ORDER Value(US$)', style)
        # row = row + 1
        worksheet.merge_range('A%s:F%s' % (row, row), 'Work Order Description', style)
        row = row + 1
        for sale in self.merged_order_ids:
            worksheet.write('A%s' % (row), 'A.' + str(index), align_value)
            worksheet.write('B%s' % (row), sale.project_scope or '', align_value)
            worksheet.write('C%s' % (row), sale.ticket_id.location_comp or '', align_value)
            worksheet.write('D%s' % (row), sale.ticket_id.client_tic_no or '', align_value)
            worksheet.write('E%s' % (row), sale.un_to_reference or '', align_value)
            worksheet.write('F%s' % (row), '$' + str(sale.total_task_order_amount) or 0,float_align_value)
            row += 1
            index += 1
        # for printing TOTAL COST OF MATERIALS
        worksheet.merge_range('A%s:E%s' % (row, row), 'TOTAL AMOUNT TASK ORDER', style)
        worksheet.write('F%s' % (row), '$' + str(self.total_material_price), float_align_value)
        row = row + 1
        worksheet.merge_range('A%s:B%s' % (row + 1,row + 1), 'SIGN AND DATE', left_new_style)
        worksheet.merge_range('A%s:B%s' % (row + 2,row + 2), 'PREPARED BY', left_new_style)
        worksheet.merge_range('A%s:B%s' % (row + 3,row + 3), 'DEEQA CONSTRUCTION AUTHORIZED REPRESENTATIVE', left_new_style)
        worksheet.merge_range('A%s:B%s' % (row + 4,row + 4), 'SIGN AND DATE', left_new_style)
        worksheet.merge_range('A%s:B%s' % (row + 5,row + 5), 'APPROVED BY', left_new_style)
        worksheet.merge_range('A%s:B%s' % (row + 6,row + 6), 'UNSOS PM AUTHORIZED REPRESENTATIVE', left_new_style)

        worksheet.merge_range('E%s:F%s' % (row + 1, row + 1), 'SIGN AND DATE', left_new_style)
        worksheet.merge_range('E%s:F%s' % (row + 2, row + 2), 'CHECKED BY', left_new_style)
        worksheet.merge_range('E%s:F%s' % (row + 3, row + 3), 'DEEQA CONSTRUCTION AUTHORIZED REPRESENTATIVE',
                              left_new_style)
        worksheet.merge_range('E%s:F%s' % (row + 4, row + 4), 'SIGN AND DATE', left_new_style)
        worksheet.merge_range('E%s:F%s' % (row + 5, row + 5), 'APPROVED BY', left_new_style)
        worksheet.merge_range('E%s:F%s' % (row + 6, row + 6), 'UNSOS-CHIEF PM AUTHORIZED REPRESENTATIVE', left_new_style)

        workbook.close()
        f = open(f_name, 'rb')
        data = f.read()
        f.close()
        name = 'Combine WO Report'

        out_wizard = self.env['xlsx.output'].create({'name': name + '.xlsx', 'xls_output': base64.b64encode(data)})
        view_id = self.env.ref('nuro_work_order.xlsx_output_form').id
        return {
            'type': 'ir.actions.act_window',
            'res_model': 'xlsx.output',
            'target': 'new',
            'view_mode': 'form',
            'res_id': out_wizard.id,
            'views': [[view_id, 'form']],
        }

class DescWorkLine(models.Model):
    _name='desc.work.line'

    @api.depends('desc_work_id.desc_work_line')
    def _sequence_ref(self):
        for line in self:
            no = 0
            for l in line.desc_work_id.desc_work_line:
                no += 1
                l.number = no

    desc_work_id = fields.Many2one('project.task')
    number = fields.Integer('Item No.', compute="_sequence_ref")
    description = fields.Char('Description')
