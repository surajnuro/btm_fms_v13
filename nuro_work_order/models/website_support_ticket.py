from odoo import api, fields, models, tools, _


class WebsiteSupport(models.Model):
    _inherit = "website.support.ticket"

    work_type = fields.Selection(selection_add=[('work_order', 'Work Order')])
    work_order_ids = fields.Many2many('project.task', 'task_wo_ref', 'website_id', 'task_id', 'Work Orders')
