# -*- coding: utf-8 -*-
# Copyright  Nuro Solution Pvt Ltd
{
    'name': "Work Oder Proposal",
    'version': '13.0.2.0.0',
    'summary': """Work Order""",
    'description': """""",
    'category': 'Business',
    'author': 'Nuro Software',
    'company': 'Nuro Software',
    'website': "https://www.nurosolution.com",
    'depends': ['base','website_support', 'project', 'nuro_project_task_management', 'website_support'],
    'data': [
            'security/ir.model.access.csv',
            'security/security_view.xml',
            'wizard/submit_for_review_view.xml',
            'wizard/assign_task_view.xml',
            'wizard/unmerge_to_wz.xml',
            'wizard/wo_approve_wiz.xml',
            'wizard/create_po_view.xml',
            'wizard/xlsx_output_view.xml',
            'views/sequence.xml',
            'views/website_support_tkt_view.xml',
            'views/sales_work_order_view.xml',
            'views/combine_work_order_report.xml',
            'views/work_order_report.xml',
    ],


    'license': "Other proprietary",
    'installable': True,
    'application': True,
}
