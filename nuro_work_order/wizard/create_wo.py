from odoo import api, fields, models, _
from odoo.exceptions import UserError

class CreateWO(models.TransientModel):
    _name="create.wo"

    wo_number = fields.Integer('WO Number')
    website_ticket_id = fields.Many2one('website.support.ticket', 'Ticket')
    wo_lines = fields.One2many('wo.line', 'create_wo_id')

    @api.model
    def default_get(self, fields):
        result = super(CreateWO, self).default_get(fields)
        context = self._context
        active_id = context.get('active_id')
        ticket_id = self.env['website.support.ticket'].search([('id', '=', active_id)])
        if ticket_id:
            result['website_ticket_id'] = ticket_id.id
            result['wo_number'] = 1
        return result

    @api.onchange('wo_number')
    def onchange_wo_number(self):
        if self.wo_number >= 1:
            wo_lst = []
            # self.passenger_line = [(6, 0, [])]
            self.wo_lines = [(6,0, [])]
            for r in range(self.wo_number):
                wo_lst.append((0,0, {
                    'website_ticket_id': self.website_ticket_id.id,
                    'project_id': self.website_ticket_id.project_id.id,
                    'branch_id': self.website_ticket_id.branch_id.id,
                }))
            if wo_lst:
                self.wo_lines = wo_lst

    def create_wo(self):
        if self.wo_number <= 0:
            raise UserError(('Wo Number Should be greater than 0!!!'))
        if not self.wo_lines:
            raise UserError(('Please add WO Lines!!!'))
        wo_lst = []
        for line in self.wo_lines:
            work_order_id = self.env['project.task'].create({
                'ticket_id': line.website_ticket_id.id,
                'partner_id': line.website_ticket_id.related_person_name_id.id,
                'client_ticket': line.website_ticket_id.client_tic_no,
                'location': line.website_ticket_id.location,
                'tic_team_id': line.website_ticket_id.team_id.id,
                'site_id': line.website_ticket_id.site_id.id,
                'subject': line.website_ticket_id.subject,
                'supervisor_id': line.website_ticket_id.supervisor_id.id,
                'work_type': 'work_order',
                'is_work_order': True,
                'due_date': line.website_ticket_id.date_due,
                'project_id': line.website_ticket_id.project_id.id,
                'branch_id': line.website_ticket_id.branch_id.id,
                'approved_by_id': line.website_ticket_id.approved_user_id.id,
                'work_categories_ids': [(6, 0, line.work_categories_ids.ids)]
            })
            wo_lst.append(work_order_id.id)
            line.website_ticket_id.quotation_id = False
            line.website_ticket_id.work_order_ids = [(4, work_order_id.id)]
        if wo_lst:
            form_view = self.env.ref('nuro_work_order.work_order_form_view').id
            tree_view = self.env.ref('nuro_work_order.work_order_tree_view').id
            return {
                'name': _('Work Order'),
                'res_model': 'project.task',
                'views': [
                    (tree_view, 'tree'),
                    (form_view, 'form'),
                ],
                'view_mode': 'tree,form',
                'type': 'ir.actions.act_window',
                'domain': [('id', 'in', wo_lst)]
            }


class WoLine(models.TransientModel):
    _name = 'wo.line'

    create_wo_id = fields.Many2one('create.wo')
    website_ticket_id = fields.Many2one('website.support.ticket', 'Ticket')
    project_id = fields.Many2one('project.project', 'Project')
    branch_id = fields.Many2one('res.branch', 'Sector')
    work_categories_ids = fields.Many2many('work.categories', 'wo_line_ref', 'categ_id', 'line_id', 'Work Category')