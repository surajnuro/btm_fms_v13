from odoo import models, fields, api

class OrderReportWizard(models.TransientModel):
    _inherit = 'ticket.maintenance'

    maintenance_cat = fields.Selection([('regularmain','Routine Maintenance'),
                                        ('unschedulework','Unschedule Work'),
                                        ('work_order', 'Work Order')], string="Ticket Type")

    def action_ticket_transfer(self):
            ticket = self.env['website.support.ticket'].search([('id','in', self._context.get('active_ids'))])
            current_user_id = self.env.user
            if ticket:
                number = ticket.ticket_number[-4:]
                if self.maintenance_cat == 'regularmain':
                    seq = 'RM' + number
                    ticket.write({
                        'states': 'approved',
                        'work_type': self.maintenance_cat,
                        'supervisor_id': self.supervisor_user_id.id,
                        'project_name': self.project_name_id.id,
                        'team_id': self.team_id.id,
                        'ticket_number': seq,
                        'project_category_id': self.proj_category_id.id,
                        'work_categories_ids': [(6, 0, self.work_categories_ids.ids)]
                    })
                elif self.maintenance_cat == 'unschedulework':
                    seg = 'UW' + ticket.ticket_number[-4:]
                    ticket.write({
                        'states': 'approved',
                        'work_type': self.maintenance_cat,
                        'supervisor_id': self.supervisor_user_id.id,
                        'ticket_number': str(seg),
                        'project_category_id': self.proj_category_id.id,
                        'work_categories_ids': [(6, 0, self.work_categories_ids.ids)]
                    })
                elif self.maintenance_cat == 'work_order':
                    seg = 'WO' + ticket.ticket_number[-4:]
                    ticket.write({
                        'states': 'approved',
                        'work_type': self.maintenance_cat,
                        'supervisor_id': self.supervisor_user_id.id,
                        'ticket_number': str(seg),
                        'project_category_id': self.proj_category_id.id,
                        'project_name': self.project_name_id.id,
                        'work_categories_ids': [(6, 0, self.work_categories_ids.ids)]
                    })
                else:
                    seg = 'RW' + ticket.ticket_number[-4:]
                    ticket.write({
                        'states': 'approved',
                        'supervisor_id': self.supervisor_user_id.id,
                        'work_type': self.maintenance_cat,
                        'ticket_number': str(seg),
                        'project_category_id': self.proj_category_id.id,
                        'work_categories_ids': [(6, 0, self.work_categories_ids.ids)]
                    })

