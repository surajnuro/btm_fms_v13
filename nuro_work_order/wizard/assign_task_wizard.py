from odoo import api, fields, models, _
from odoo.exceptions import UserError

class AssignTaskWO(models.TransientModel):
    _name="assign.task.wo"

    supervisor_name_id = fields.Many2one('res.users','Technician')
    ticket_team_id = fields.Many2one('tkt.team', 'Team')
    project_id = fields.Many2one('project.project','Sub Project')
    branch_id = fields.Many2one('res.branch','Sector')

    @api.model
    def default_get(self, fields):
        res = super(AssignTaskWO, self).default_get(fields)
        task_id = self.env['project.task'].search([('id', 'in', self._context.get('active_ids'))])
        if task_id.project_id:
            res['project_id'] = task_id.project_id.id
            res['branch_id'] = task_id.branch_id.id
        return res

    def submit(self):
        """ method to create task, create location"""

        active_id = self.env.context.get('active_id')
        project_task_id = self.env['project.task'].browse(active_id)
        address = ''
        if project_task_id.ticket_id.location_comp:
            address = address+project_task_id.ticket_id.location_comp + ' '
        if project_task_id.ticket_id.location_phase:
            address = address + project_task_id.ticket_id.location_phase + ' '
        if project_task_id.ticket_id.location_room:
            address = address + project_task_id.ticket_id.location_room
        if not address:
            address = 'work location for Work Order'
        location_id=self.env.ref('nuro_project_task_management.work_consume_location_details')
        if not location_id:
            location = self.env['stock.location']
            location_id = location.create({
                'name': address,
                'usage': 'customer',
                'comapany_id': False
            })
        task_dictonary1 = {
            'user_id':self.supervisor_name_id.id,
            'project_id': self.project_id.id,
            'branch_id': self.branch_id.id,
            'name':'Work Order Task',
            'ticket_id' : project_task_id.ticket_id.id,
            'client_ticket':project_task_id.client_ticket,
            'tic_team_id': self.ticket_team_id.id,
            'location':address,
            'site_id':project_task_id.site_id.id,
            'supervisor_id':project_task_id.supervisor_id.id,
            'consume_location_id':location_id.id,
            'work_type': 'regularmain',
            'planned_hours': project_task_id.total_labour_hours,
            'description':project_task_id.project_scope,
            'date_deadline': project_task_id.due_date or False,
            'task_id': project_task_id.id
        }
        task = self.env['project.task'].create(task_dictonary1)
        for order_line in project_task_id.material_planing_line:
            move_dictonary2 = {
                'product_id': order_line.product_id.id,
                'request_qty':order_line.product_quantity,
                'task_id':task.id,
            }
            self.env['task.material.request'].create(move_dictonary2)
        form_view = self.env.ref('nuro_project_task_management.task_regular_form_view').id
        tree_view = self.env.ref('nuro_project_task_management.task_regular_tree_view').id
        if task:
            return {
                'name': _('Task'),
                'res_model': 'project.task',
                'views': [
                    (tree_view, 'tree'),
                    (form_view, 'form'),
                ],
                'view_mode': 'tree,form',
                'type': 'ir.actions.act_window',
                'domain': [('id', '=', task.id)]
            }

