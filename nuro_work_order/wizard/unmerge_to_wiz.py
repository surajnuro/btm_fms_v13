from odoo import api, fields, models, _
from odoo.exceptions import ValidationError, Warning

class UnmergeTO(models.TransientModel):
    _name="unmerge.to.wz"

    note = fields.Text('Reason')

    def unmerge_to(self):
        active_id = self.env.context.get('active_id')
        if active_id:
            top = self.env['project.task'].browse(active_id)
            if top:
                if top.invoice_ids:
                    for line in top.invoice_ids:
                        if line.state not in ['draft']:
                            raise ValidationError(_('Please Cancel Invoice First!!!'))
                        line.button_cancel()
                if top.merged_order_ids:
                    top.unmerge_reason = self.note
                    top.usw_state = 'cancel'
                    top.is_unmerge = True
                    top.is_merge = False
                    for wo in top.merged_order_ids:
                        wo.write({'work_order_state': 'draft',
                                  'top_id': False})


