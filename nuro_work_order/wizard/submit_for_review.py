from odoo import api, fields, models, _
from odoo.exceptions import ValidationError, Warning

class SubmitReviewWorkOrder(models.TransientModel):
    _name="submit.review.work.wiz"

    user_id = fields.Many2one('res.users','Submit to')

    @api.model
    def default_get(self, fields):
        res = super(SubmitReviewWorkOrder, self).default_get(fields)
        active_id = self.env.context.get('active_id')
        project_task_id = self.env['project.task'].browse(active_id)
        if project_task_id.work_type == 'work_order':
            if not project_task_id.material_planing_line:
                raise Warning(_('Please fill required material or Labour Details before submit for review !'))
            if project_task_id.ticket_applicable == 'tke_apply' and not project_task_id.ticket_attachment_1:
                raise ValidationError("Please Add Ticket Attachment")
            if project_task_id.drawing_applicable == 'drawing_apply' and not project_task_id.drawing_attachment_1:
                raise ValidationError("Please Add Drawing Attachment")
            if project_task_id.other_applicable == 'other_apply' and not project_task_id.other_attachment_1:
                raise ValidationError("Please Add Other Attachment ")
            if not project_task_id.ticket_attachment_1 and not project_task_id.drawing_attachment_1 and not project_task_id.other_attachment_1:
                raise ValidationError(_('You need to attach atleast one document !'))
        res['user_id'] = project_task_id.approved_by_id.id
        return res

    def send_for_review(self):
        ''' send for under review and send mail to how is approve tkt and assign top '''
        active_id = self.env.context.get('active_id')
        task_id = self.env['project.task'].browse(active_id)
        task_id.write({'work_order_state':'pmo_review'})
        return True

class Users(models.Model):
    _inherit = "res.users"

    @api.depends('groups_id')
    def duty_officer_users(self):
        for users in self:
            if users.has_group('website_support.group_ticket_supervisor'):
                users.is_approved_duty_manager = True
            else:
                users.is_approved_duty_manager = False
    is_approved_duty_manager = fields.Boolean('Is Approved', compute='duty_officer_users', store=True)