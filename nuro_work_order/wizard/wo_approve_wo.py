from odoo import fields, models, api, _
from odoo.exceptions import UserError


class MergePurchaseOrder(models.TransientModel):
    _name = 'approve.wo'


    def approve_orders(self):
        sale_orders = self.env['sale.order'].browse(
            self._context.get('active_ids', []))
        if sale_orders:
            for so in sale_orders:
                if not so.work_order_state == 'pmo_review':
                    raise UserError(
                        _('Please select Under Review Work Orders'
                          ))
                so.confirm_work_order()

