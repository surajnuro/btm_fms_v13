# -*- coding: utf-8 -*-

from odoo import fields, models, api, _
from odoo.exceptions import UserError


class MergePurchaseOrder(models.TransientModel):
    _name = 'merge.sale.order'

    merge_type = fields.Selection([('new_cancel',
                'Create new order and cancel all selected  orders')],
            default='new_cancel')
    project_task_id = fields.Many2one('project.task', 'Order')

    @api.onchange('merge_type')
    def onchange_merge_type(self):
        res = {}
        for order in self:
            order.project_task_id = False
            if order.merge_type in ['merge_cancel', 'merge_delete']:
                task_ids = self.env['project.task'].browse(
                    self._context.get('active_ids', []))
                res['domain'] = {
                    'project_task_id':
                    [('id', 'in',
                        [task.id for task in task_ids])]
                }
            return res

    def merge_orders(self):
        task_ids = self.env['project.task'].browse(
            self._context.get('active_ids', []))
        existing_so_line = False
        existing_labour_line = False
        if len(self._context.get('active_ids', [])) < 2:
            raise UserError(
                _('Please select atleast two orders to perform '
                    'the Merge Operation.'))
        if any(order.work_order_state in ('cancel') for order in task_ids):
            raise UserError(
                _('Please select Sale orders which are in Cancel state'
                  'to perform the Merge Operation.'))
        partner = task_ids[0].partner_id.id
        scope = task_ids[0].project_scope
        state = task_ids[0].work_order_state
        sector = task_ids[0].branch_id.id
        project = task_ids[0].project_id.id
        supervisor = task_ids[0].supervisor_id.id
        line_desc = []
        inv_line = []
        material_line = []
        breakdown = []
        for rec in task_ids:
            wk_state = rec.work_order_state
            break

        if any(order.partner_id.id != partner for order in task_ids):
            raise UserError(
                _('Please select orders whose Customers are same to '
                    ' perform the Merge Operation.'))
        if any(order.work_order_state != state for order in task_ids):
            raise UserError(
                _('Please select orders whose state are same to '
                    ' perform the Merge Operation.!!!'))
        for ord in task_ids:
            if ord.top_id:
                raise UserError(
                _('Already Merge Selected work order!!!'))

        if self.merge_type == 'new_cancel':
            count = 0
            for order in task_ids:
                if order.desc_work_line:
                    for line in order.desc_work_line:
                        count += 1
                        line_desc.append((0, 0, {
                            'number': count,
                            'description': line.description,
                        }))
            for order in task_ids:
                if order.material_planing_line:
                    for line in order.material_planing_line:
                        if line.display_type == 'line_section':
                            material_line.append((0, 0, {'display_type': 'line_section',
                                                         'name': line.name,
                                                         }))
                        if line.display_type == False:
                            material_line.append((0, 0, {'product_id': line.product_id.id,
                                                         'name': line.name,
                                                         'product_uom': line.product_uom,
                                                         'product_boq_ref': line.product_boq_ref,
                                                         'product_boq_rate': line.product_boq_rate,
                                                         'product_quantity': line.product_quantity,
                                                         'product_rate': line.product_rate,
                                                         'product_cost': line.product_cost,
                                                         'product_margine_per': line.product_margine_per,
                                                         'product_margine_rate': line.product_margine_rate,
                                                         'total_rate': line.total_rate}))
                if order.labour_breakdown_planing:
                    for l_line in order.labour_breakdown_planing:
                        if l_line.display_type == 'line_section':
                            breakdown.append((0,0,{
                                'name': l_line.name,
                                'display_type': 'line_section'
                            }))
                        if l_line.display_type == False:
                            breakdown.append((0, 0, {
                                'name': l_line.name,
                                'duration': l_line.duration,
                                'skill': l_line.skill,
                                'unskill': l_line.unskill,
                            }))
            top = self.env['project.task'].with_context({
                'trigger_onchange': True,
                'onchange_fields_to_trigger': [partner]
            }).create({'partner_id': partner,
                       'project_scope': scope,
                       'usw_state': 'draft',
                       'work_type': 'unschedulework',
                       'branch_id': sector,
                       'project_id': project,
                       'supervisor_id': supervisor,
                        'is_merge': True,
                       'top_task': True,
                       'top_generated': True,
                       'work_desc': 'Created From Work Order',
                        'previous_stage': wk_state,
                       'material_planing_line': material_line,
                       'labour_breakdown_planing': breakdown,
                       'merged_order_ids': [(6, 0, task_ids.ids)]})
            for order in task_ids:
                order.write({'work_order_state': 'done', 'top_id': top.id})



