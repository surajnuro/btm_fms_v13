# -*- coding: utf-8 -*-
# Copyright  Nuro Solution Pvt Ltd
{
    'name': 'Restrict unpost journal entry',
    'category': 'Custom',
    'summary': 'Restrict unpost journal entry',
    'description': """
        Restrict unpost journal entry
    """,
    'author': 'Nuro Solution Pvt Ltd',
    'website': 'http://www.nurosolution.com',
    'company': 'Nuro Solution Pvt Ltd',
    'license': 'Other proprietary',
    'depends': ['account'],
    'data': [

    ],

    'installable': True,
    'application': True,
}