from odoo import fields,api,models,_
from odoo.exceptions import ValidationError

class Move(models.Model):
    _inherit = 'account.move'

    def button_cancel(self):
        '''check if any ml is reconcile then show warning'''
        if self.line_ids:
            for ml in self.line_ids:
                if ml.reconciled == True:
                    raise ValidationError(('Account entry is reconciled, Please unreconciled first than cancel!!!'))
        return super(Move, self).button_cancel()