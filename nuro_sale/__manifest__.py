# -*- coding: utf-8 -*-
# Copyright  Nuro Solution Pvt Ltd
{
    'name': 'Sale Changes',
    'category': 'Custom',
    'summary': 'Sale changes',
    'description': """
        Sale changes
    """,
    'author': 'Nuro Solution Pvt Ltd',
    'website': 'http://www.nurosolution.com',
    'company': 'Nuro Solution Pvt Ltd',
    'license': 'Other proprietary',
    'depends': ['sale', 'account', 'nuro_sales_cash_credit', 'nuro_sale_order_return'],
    'data': [
        'security/ir.model.access.csv',
        'views/sale_view.xml',
    ],

    'installable': True,
    'application': True,
}