from odoo import fields,models,api,_
from odoo.exceptions import UserError

class Sale(models.Model):
    _inherit = 'sale.order'

    skill_unskill_labor = fields.One2many('skill.unskill.labor', 'sale_id')

    @api.model
    def default_get(self, fields):
        labor_lst = []
        res = super(Sale, self).default_get(fields)
        skill_ref = self.env.ref('nuro_product_boq.skill_manpower')
        unskill_ref = self.env.ref('nuro_product_boq.unskill_manpower')
        labor_lst.append((0,0, {'product_id': skill_ref.id,
                                'rate_hour': skill_ref.lst_price,
                                'skill': True}))
        labor_lst.append((0, 0, {'product_id': unskill_ref.id,
                                 'unskill':True,
                                 'rate_hour': skill_ref.lst_price}))
        res['skill_unskill_labor'] = labor_lst
        return res

    @api.model
    def create(self, vals):
        res = super(Sale, self).create(vals)
        if res.skill_unskill_labor:
            for line in res.skill_unskill_labor:
                # if line.total_men_amt > 0.0:
                    res.order_line = [(0, 0, {'product_id': line.product_id.id,
                                               'name': line.product_id.name,
                                               'unskill_skill': True,
                                               'labor_id': line.id,
                                               'product_uom_qty': 1,
                                               'price_unit': line.total_men_amt})]
        return res

    def write(self, vals):
        res = super(Sale, self).write(vals)
        line_ids = self.order_line.filtered(lambda line: line.unskill_skill == True)
        if line_ids:
            for order in line_ids:
                if self.skill_unskill_labor:
                    for line in self.skill_unskill_labor:
                        if line.id == order.labor_id.id:
                            order.price_unit = line.total_men_amt
        # else:
        #     for skill in self.skill_unskill_labor:
        #         if skill.total_men_amt > 0.0:
        #             self.order_line = [(0, 0, {'product_id': skill.product_id.id,
        #                                        'name': skill.product_id.name,
        #                                        'unskill_skill': True,
        #                                        'labor_id': skill.id,
        #                                        'product_uom_qty': 1,
        #                                        'price_unit': skill.total_men_amt})]
        return res

class OrderLine(models.Model):
    _inherit = 'sale.order.line'

    unskill_skill = fields.Boolean()
    labor_id = fields.Many2one('skill.unskill.labor')

    # def unlink(self):
    #     for rec in self:
    #         if rec.unskill_skill == True:
    #             raise ValidationError(('Sorry you can not delete Skill or Unskill labor line!!!'))
    #     return super(OrderLine, self).unlink()

class SkillLabor(models.Model):
    _name = 'skill.unskill.labor'

    product_id = fields.Many2one('product.product', 'Service')
    sale_id = fields.Many2one('sale.order', ondelete="cascade")
    duration = fields.Float('DURATION(DAYS)', required=0)
    labor = fields.Integer('Labor')
    labor_man_hr = fields.Integer(compute="get_total_labor_hrs", string='MAN HR')
    rate_hour = fields.Float('Rate/Hour')
    total_men_amt = fields.Float(compute="_total_men_amount", store=True)
    skill = fields.Boolean()
    unskill = fields.Boolean()

    @api.depends('rate_hour', 'duration', 'labor')
    def _total_men_amount(self):
        '''calculate total men hour rate'''
        for rec in self:
            rec.total_men_amt = rec.rate_hour * (rec.duration * rec.labor * 8)

    # @api.onchange('total_men_amt', 'labor_man_hr')
    # def onchange_labor_amount(self):
    #     if self.total_men_amt > 0.0:
    #         line_ids = self._origin.sale_id.order_line.filtered(lambda line: line.unskill_skill == True)
    #         print(line_ids)
    #         if line_ids and len(line_ids) == 2:
    #             for line in line_ids:
    #                 if line.labor_id:
    #                     line.price_unit = self.total_men_amt
    #         else:
    #             self._origin.sale_id.order_line = [(0, 0, {'product_id': self.product_id.id,
    #                                               'name': self.product_id.name,
    #                                               'unskill_skill': True,
    #                                                'labor_id': self._origin.id,
    #                                               'product_uom_qty': 1,
    #                                               'price_unit': self.total_men_amt})]

    @api.depends('duration', 'labor')
    def get_total_labor_hrs(self):
        total = 0.0
        for rec in self:
            total = rec.duration * rec.labor * 8
            rec.labor_man_hr = total