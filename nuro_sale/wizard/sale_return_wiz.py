from odoo import fields, models, api,_
from odoo.exceptions import UserError, ValidationError

class SaleReturnWiz(models.TransientModel):
    _inherit = 'sale.return.wiz'

    def create_credit_return_credit_note(self, full_return=False):
        '''override for send branch and project in new invoices'''
        for return_wiz in self:
            if return_wiz.sale_id:
                inv_id = return_wiz.sale_id.invoice_ids.filtered(lambda x: x.type in ('out_invoice') and x.state in ('posted'))
                if inv_id:
                    inv_line = []
                    for return_line in return_wiz.return_lines_ids:

                        if not return_line.product_id.categ_id.property_account_income_categ_id:
                            raise UserError(_('Please configure income account in return product category'))
                        if return_line.price_subtotal > 0.0 and full_return:
                            return_line.sale_order_line.service_return = True
                            inv_line.append((0, 0, {
                                'product_id': return_line.product_id.id,
                                'name': "Return " + return_line.product_id.name,
                                'sale_line_id': return_line.sale_order_line.id,
                                'price_unit': abs(return_line.price_unit),
                                'quantity': full_return and return_line.product_uom_qty,
                                'account_id': return_line.product_id.categ_id.property_account_income_categ_id.id,
                            }))
                        elif return_line.return_quantity > 0.0:
                            return_line.sale_order_line.service_return = True
                            inv_line.append((0, 0, {
                                'product_id': return_line.product_id.id,
                                'name': "Return " + return_line.product_id.name,
                                'sale_line_id': return_line.sale_order_line.id,
                                'price_unit': abs(return_line.price_unit),
                                'quantity': return_line.return_quantity,
                                'account_id': return_line.product_id.categ_id.property_account_income_categ_id.id,
                            }))
                    if inv_line:
                        refund_inv = self.env['account.move'].create({
                            'type': 'out_refund',
                            'partner_id': return_wiz.sale_id.partner_id.id,
                            'invoice_line_ids': inv_line,
                            'branch_id': return_wiz.sale_id.branch_id.id,
                            'project_id': return_wiz.sale_id.project_id.id,
                            'ref':'Refund credit for '+inv_id.ref,
                            'ks_global_discount_type': 'amount',
                            'ks_global_discount_rate': return_wiz.discount_return
                        })
                        refund_inv.action_post()
                        return_wiz.sale_id.invoice_ids = [(4, refund_inv.id)]
                        credit_aml_id = refund_inv.line_ids.filtered(lambda x: x.account_id.user_type_id.type=='receivable')
                        if credit_aml_id and inv_id.state!='posted':
                            inv_id.assign_outstanding_credit(credit_aml_id.id)
                    if refund_inv:
                        # =====for add credit note line in sale invoice line
                        for line in return_wiz.return_lines_ids:
                            for aml in refund_inv.line_ids:
                                if line.sale_order_line == aml.sale_line_id:
                                    sql = (
                                        '''INSERT INTO sale_order_line_invoice_rel (order_line_id,invoice_line_id) VALUES (%s, %s)''')
                                    self._cr.execute(sql, (aml.sale_line_id.id, aml.id))