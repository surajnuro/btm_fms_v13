from odoo import api, fields, models, _
from odoo.exceptions import UserError
import base64
import xlsxwriter
from datetime import datetime
from dateutil.rrule import rrule, DAILY
import calendar

class AttendanceReport(models.TransientModel):
    _name = 'attendance.report.wiz'

    from_date = fields.Date('From Date')
    to_date = fields.Date('To Date')
    department_ids = fields.Many2many('hr.department', 'atten_dept_ref', 'atten_id','dept_id',  'Department')

    def print_attendance_report(self):
        f_name = '/tmp/attendnace_report.xlsx'
        workbook = xlsxwriter.Workbook(f_name)
        worksheet = workbook.add_worksheet('Report')
        worksheet.set_column('A:K', 12)
        emp_lst = []
        style = workbook.add_format({
            'bold': 1,
            'border': 1,
            'align': 'center',
            'valign': 'vcenter'})
        style.set_font_size(8)
        align_value = workbook.add_format({
            'align': 'center',
            'valign': 'vcenter'})
        align_value.set_font_size(8)
        domain = []
        if self.department_ids:
            domain += [('department_id', 'in', self.department_ids.ids)]
        row_r = 3
        col = 5
        worksheet.merge_range('A1:J1', "DEEQA CONSTRUCTION & WATER WELL DRILLING CO. LTD.", style)
        if self.to_date and self.from_date:
            date = datetime.strptime(self.to_date, '%Y-%m-%d')
            worksheet.merge_range('A2:G2', "REPORT FORMAT FOR ATTENDANCE REPORT DATE OF ",style)
            worksheet.merge_range('H2:J2', self.from_date + ' ' + 'To' + ' '+ self.to_date, style)
        else:
            worksheet.merge_range('A2:J2', " REPORT FORMAT FOR ATTENDANCE REPORT", style)
        worksheet.write('A4', "Staff ID", style)
        worksheet.write('B4', "Employee Type", style)
        worksheet.write('C4', "Employee Name", style)
        worksheet.write('D4', "Position", style)
        worksheet.write('E4', "Department", style)
        # worksheet.write('F4', "Project", style)
        # worksheet.write('G4', "Allocation", style)
        start = datetime.strptime(self.from_date, '%Y-%m-%d')
        end = datetime.strptime(self.to_date, '%Y-%m-%d')

        for month_date in rrule(DAILY, dtstart=start, until=end):
            month_day = calendar.day_name[month_date.weekday()]
            date_month = month_date.strftime('%Y-%m-%d')
            worksheet.write(row_r, col, str(date_month)[8:10], style)
            col += 1
        e_row = 5
        e_col = 0
        data_lst = []
        employess = self.env['hr.employee'].search(domain)
        if employess:
            for atten in employess:
                cat_name = ' '
                worksheet.write(e_row, 0, atten.barcode or "-", align_value)
                if atten.category_ids:
                    for cat in atten.category_ids:
                        cat_name = cat.name + ','
                worksheet.write(e_row, 1, cat_name or "-", align_value)
                worksheet.write(e_row, 2, atten.name or '-', align_value)
                worksheet.write(e_row, 3, atten.job_id.name or '-', align_value)
                worksheet.write(e_row, 4, atten.department_id.name or "-", align_value)
                # worksheet.write(e_row, 5, atten.project_category_id.name or '-', align_value)
                e_dt_col = 5
                count = 0
                for month_date in rrule(DAILY, dtstart=start, until=end):
                    hr_attendance_emp = self.env['daily.attendance.line'].sudo().search([
                        ('employe_id', '=', atten.id),
                        ('state', '=', 'validate'),
                        ('check_in', '>=', str(month_date)),
                        ('check_out', '<=', str(month_date.strftime('%Y-%m-%d 23:59:59'))),
                    ], limit=1)
                    if hr_attendance_emp:
                        count += 1
                        emp_lst.append(hr_attendance_emp.employe_id.id)
                        if hr_attendance_emp.is_present:
                            worksheet.write(e_row, e_dt_col, 'P', align_value)
                        if hr_attendance_emp.is_absent:
                            worksheet.write(e_row, e_dt_col, 'A', align_value)
                        if hr_attendance_emp.is_leave:
                            if hr_attendance_emp.leave_id:
                                worksheet.write(e_row, e_dt_col, hr_attendance_emp.leave_id.holiday_status_id.short_code, align_value)
                            else:
                                worksheet.write(e_row, e_dt_col, 'L', align_value)
                    e_dt_col += 1

                    # worksheet.write(e_row, 6, hr_attendance_emp.sale_order.sequence or hr_attendance_emp.sale_order.un_to_reference or "-",
                    #                 align_value)

                e_row += 1

        workbook.close()
        f = open(f_name, 'rb')
        data = f.read()
        f.close()
        name = 'Attendance Report'
        dt = ' ' + 'From' + ' ' + str(self.from_date) + ' ' + 'To' + ' ' + str(self.to_date)
        if self.from_date and self.to_date:
            out_wizard = self.env['xlsx.output'].create({'name': name + dt + '.xlsx',
                                                         'xls_output': base64.encodebytes(data)})
        else:
            out_wizard = self.env['xlsx.output'].create({'name': name + '.xlsx',
                                                         'xls_output': base64.encodebytes(data)})
        view_id = self.env.ref('fms_payroll_report.xlsx_output_form').id
        return {
            'type': 'ir.actions.act_window',
            'name': _(name),
            'res_model': 'xlsx.output',
            'target': 'new',
            'view_mode': 'form',
            'res_id': out_wizard.id,
            'views': [[view_id, 'form']],
        }