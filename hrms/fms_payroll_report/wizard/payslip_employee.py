from odoo import api, fields, models, _

class HrPayslipEmployees(models.TransientModel):
    _inherit = 'hr.payslip.employees'

    @api.model
    def default_get(self, fields):
        domain = []
        rec = super(HrPayslipEmployees, self).default_get(fields)
        context = dict(self._context or {})
        active_ids = context.get('active_ids')
        employees = self.env['hr.employee']
        if active_ids:
            payslip_obj = self.env['hr.payslip.run'].browse(active_ids)
            if payslip_obj:
                if payslip_obj.department_id:
                    domain += [('department_id', '=', payslip_obj.department_id.id)]
                if payslip_obj.type_selection:
                    domain += [('type_selection', '=', payslip_obj.type_selection)]
                if payslip_obj.project_sector_id:
                    domain += [('sector_id', '=', payslip_obj.project_sector_id.id)]
                if domain:
                    emp_ids = employees.search(domain)
                    if emp_ids:
                        rec['employee_ids'] = [(6, 0, emp_ids.ids)]
                    else:
                        rec['employee_ids'] = [(6, 0, [])]
                else:
                    emp_ids = employees.search([])
                    if emp_ids:
                        rec['employee_ids'] = [(6, 0, emp_ids.ids)]
        return rec