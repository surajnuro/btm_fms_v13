from odoo import api, fields, models, _
import base64
import xlsxwriter
from datetime import datetime
import io

class PayslipTransaction(models.TransientModel):
    _name = 'payslip.transaction'

    employee_ids = fields.Many2many('hr.employee','employee_transaction_ref', 'payslip_tran_id', 'employee_id')
    date_from = fields.Date('From Date')
    date_to = fields.Date('From To')
    company_id = fields.Many2one('res.company', string='Company',
                                 default=lambda self: self.env.user.company_id)
    result_selection = fields.Selection([('customer', 'Receivable Accounts'),
                                         ('supplier', 'Payable Accounts'),
                                         ('customer_supplier', 'Receivable and Payable Accounts')
                                         ], string="Account", required=True, default='customer_supplier')
    partner_id = fields.Many2one('res.partner', domain=[('is_company', '=', True)])
    employee_type = fields.Selection([('national', 'National'), ('international', 'International')],default='national', string='Employee Type')


    def get_balance(self, employee,account,date):
        '''
        :param emp:
        :param account:
        :param date:
        :return: balance
        method for calculate balance
        '''
        query = """SELECT sum(debit - credit)
                    FROM account_move_line
                    WHERE employee_id = %s
                    AND state = 'posted'
                    AND account_id IN %s
                    """
        self.env.cr.execute(query, (
            employee.id, tuple(account.ids)))
        contemp = self.env.cr.fetchone()
        if contemp:
            return contemp[0]
        else:
            return 0.0

    def print_payslip_transaction_report(self):
        f_name = '/tmp/report.xlsx'
        workbook = xlsxwriter.Workbook(f_name)
        worksheet = workbook.add_worksheet('Report')
        worksheet.set_column('A:K', 12)
        style = workbook.add_format({
            'bold': 1,
            'border': 1,
            'align': 'center',
            'valign': 'vcenter'})
        style.set_font_size(8)
        align_value = workbook.add_format({
            'align': 'center',
            'valign': 'vcenter'})

        align_value.set_font_size(8)
        row = 4
        new_row = row + 1
        worksheet.merge_range('A1:H1', self.company_id.name, style)
        if self.date_to and self.date_from:
            from_date = datetime.strptime(self.date_from, "%Y-%m-%d").date()
            to_date = datetime.strptime(self.date_to, "%Y-%m-%d").date()
            worksheet.merge_range('A2:H2', str(from_date.day) + ' ' + from_date.strftime("%B") + ' ' + str(
                from_date.year) + ' ' + 'To' + ' '
                                  + str(to_date.day) + ' ' + to_date.strftime("%B") + ' ' + str(to_date.year), style)
        else:
            worksheet.merge_range('A2:H2', "Employee Payslip Transaction in Details", style)

        worksheet.write('A4', "Date", style)
        worksheet.write('B4', "Reference", style)
        worksheet.write('C4', "Debit", style)
        worksheet.write('D4', "Credit", style)
        worksheet.write('E4', "Partner", style)
        worksheet.write('F4', "Employee", style)
        payslip = self.env['hr.payslip']
        domain = []
        if self.date_from:
            domain += [('date', '>=', self.date_from)]
        if self.date_to:
            domain += [('date', '<=', self.date_to)]
        domain += [('state', '=', 'posted')]
        if self.result_selection == 'customer':
            account = self.env['account.account'].search([('user_type_id.name', '=', 'Receivable')])
            domain += [('account_id', 'in', account.ids)]
        if self.result_selection == 'supplier':
            account = self.env['account.account'].search([('user_type_id.name', '=', 'Payable')])
            domain += [('account_id', 'in', account.ids)]
        if self.result_selection == 'customer_supplier':
            account = self.env['account.account'].search([('user_type_id.name', 'in', ['Payable', 'Receivable'])])
            domain += [('account_id', 'in', account.ids)]
        if self.employee_ids:
            employee = self.employee_ids
            domain += [('employee_id', 'in', employee.ids)]
        else:
            employee = self.env['hr.employee'].search([])
            domain += [('employee_id', 'in', employee.ids)]
        if self.partner_id:
            domain += [('partner_id', '=', self.partner_id.id)]
        else:
            partner_ids = self.env['res.partner'].search([('check_company', '=', True)])
            domain += [('partner_id', 'in', partner_ids.ids)]
        total_debit = total_credit = 0.0
        move_lines = self.env['account.move.line'].sudo().search(domain)
        for emp in employee:

            for ml in move_lines:
                if emp == ml.employee_id:
                    date = datetime.strptime(str(ml.date), '%Y-%m-%d')
                    worksheet.write("A%s" % (new_row), str(date.strftime("%d-%m-%Y")) or "Not Filled", align_value)
                    worksheet.write("B%s" % (new_row), ml.name or '/', align_value)
                    worksheet.write("C%s" % (new_row), ml.debit or 0, align_value)
                    worksheet.write("D%s" % (new_row), ml.credit or 0, align_value)
                    worksheet.write("E%s" % (new_row), ml.partner_id.name or '-', align_value)
                    worksheet.write("F%s" % (new_row), ml.employee_id.name or '-', align_value)
                    total_credit += ml.credit
                    total_debit += ml.debit
                    new_row = new_row + 1

            if any(line.employee_id.id == emp.id for line in move_lines):
                # worksheet.merge_range('A%s:B%s' % (new_row, new_row), emp.name, style)
                # new_row = new_row + 1
                balance = self.get_balance(employee = emp, account = account, date = self.date_from or fields.Date.context_today(self))
                worksheet.merge_range('A%s:B%s' % (new_row, new_row), 'Balance', style)
                worksheet.merge_range('C%s:D%s' % (new_row, new_row), balance, style)
                new_row = new_row + 1
                new_row = new_row + 1
        # worksheet.merge_range('A%s:B%s' % (new_row, new_row), "Total", style)
        # worksheet.write("C%s" % (new_row), total_debit or 0, style)
        # worksheet.write("D%s" % (new_row), total_credit or 0, style)
        workbook.close()
        f = open(f_name, 'rb')
        data = f.read()
        f.close()
        name = 'Employee Statement Report'
        dt = ' ' + 'From' + ' ' + str(self.date_from) + ' ' + 'To' + ' ' + str(self.date_to)
        if self.date_from and self.date_to:
            out_wizard = self.env['xlsx.output'].create({'name': name + dt + '.xlsx',
                                                         'xls_output': base64.encodebytes(data)})
        else:
            out_wizard = self.env['xlsx.output'].create({'name': name + '.xlsx',
                                                         'xls_output': base64.encodebytes(data)})
        view_id = self.env.ref('fms_payroll_report.xlsx_output_form').id
        return {
            'type': 'ir.actions.act_window',
            'name': _(name),
            'res_model': 'xlsx.output',
            'target': 'new',
            'view_mode': 'form',
            'res_id': out_wizard.id,
            'views': [[view_id, 'form']],
        }

