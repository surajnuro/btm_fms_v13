from odoo import api, fields, models, _
from datetime import datetime
from odoo.exceptions import UserError

class SalaryPayment(models.TransientModel):
    _inherit = 'salary.payment.wiz'

    payable_account_id = fields.Many2one('account.account', domain=[('user_type_id.name', 'in', ['Payable'])])

    def remaining_slalry_sheet(self, rem_ml_lst, sheet):
        '''create another salary sheet and link with salary sheet'''
        sheet_obj = self.env['salary.sheet']
        sheet_id = sheet_obj.create({
            'salary_line': rem_ml_lst,
            'salary_sheet_id': sheet.id,
            'name': sheet.name,
            'date_from': sheet.date_from,
            'date_to': sheet.date_to,
            'sheet_type': 'remaining',
        })

    def confirm_payment(self):
        ml_lst = []
        rem_ml_lst = []
        move = self.env['account.move']
        active_id = self.env.context.get('active_id')
        sheet = self.env['salary.sheet'].browse(active_id)
        if sheet:
            if sheet.salary_line:
                amount = 0
                total_amt = total_convert_amt = 0.0
                if sheet.currency_id != sheet.company_id.currency_id:
                    for line in sheet.salary_line:
                        if line.select_salary_line == True:
                            amount += line.amount
                            if not line.employee_id.address_home_id:
                                raise UserError(_('Employee accounting details is not configured !'))
                            line.write({'state': 'paid'})
                            total_amt += line.amount
                            total_convert_amt += round((line.amount / sheet.conversion_rate),5)
                            ml_lst.append((0, 0, {
                                'account_id': self.payable_account_id.id,
                                'name': line.salary_id.name + ' ' + 'For' + ' ' + line.employee_id.name,
                                'partner_id': line.employee_id.address_home_id.parent_id.id if line.employee_id.address_home_id.parent_id else line.employee_id.address_home_id.id,
                                'debit': line.amount,
                                # 'credit': 0,
                                'amount_currency': line.amount,
                                'conversion_rate': sheet.conversion_rate,
                                'currency_id': sheet.currency_id.id,
                                'employee_id': line.employee_id.id,
                            }))
                        if line.select_salary_line == False:
                            rem_ml_lst.append((0,0, {
                                'employee_id': line.employee_id.id,
                                'amount': line.amount,
                                'salary': line.salary,
                                'date_from': line.date_from,
                                'date_to': line.date_to,
                                'payment_option_id': line.payment_option_id.id,
                                'select_salary_line': True
                            }))
                    if not ml_lst:
                        raise UserError(_('Please select Atleast one Salary line'))
                    if rem_ml_lst:
                        self.remaining_slalry_sheet(rem_ml_lst, sheet)
                    if not self.journal_id.default_credit_account_id:
                        raise UserError(_('Please configure account of journal !'))
                    ml_lst.append((0, 0, {
                        'account_id': self.journal_id.default_credit_account_id.id,
                        'name': 'Salary Payment',
                        'credit': amount,
                        'amount_currency': -amount,
                        'conversion_rate': sheet.conversion_rate,
                        'currency_id': sheet.currency_id.id,

                    }))
                    move_id = move.create({'date': datetime.now(),
                                           'journal_id': self.journal_id.id,
                                           'ref': 'Salary Sheet',
                                           'line_ids': ml_lst,
                                           'salary_sheet_id': sheet.id
                                           })
                    if move_id:
                        total_debit = 0.0
                        if move_id.line_ids:
                            for ml in move_id.line_ids:
                                if ml.debit > 0.0:
                                    ml._onchange_amount_currency()
                                    total_debit = total_debit + ml.debit
                            for mvline in move_id.line_ids:
                                if mvline.credit > 0.0:
                                    mvline.update({'credit': total_debit})

                        move_id.post()
                        sheet.move_id = move_id.id
                        sheet.state = 'paid'
                else:
                    for line in sheet.salary_line:
                        if line.select_salary_line == True:
                            amount += line.amount
                            if not line.employee_id.address_home_id:
                                raise UserError(_('Employee accounting details is not configured !'))
                            line.write({'state': 'paid'})
                            ml_lst.append((0, 0, {
                                'account_id': line.employee_id.address_home_id.property_account_payable_id.id,
                                'name': line.salary_id.name + ' ' + 'For' + ' ' + line.employee_id.name,
                                'partner_id': line.employee_id.address_home_id.parent_id.id if line.employee_id.address_home_id.parent_id else line.employee_id.address_home_id.id,
                                'debit': line.amount,
                                # 'credit': 0,
                                'currency_id': sheet.currency_id.id,
                                'employee_id': line.employee_id.id,

                            }))
                        if line.select_salary_line == False:
                            rem_ml_lst.append((0,0, {
                                'employee_id': line.employee_id.id,
                                'amount': line.amount,
                                'salary': line.salary,
                                'date_from': line.date_from,
                                'date_to': line.date_to,
                                'payment_option_id': line.payment_option_id.id,
                                'select_salary_line': True
                            }))

                    if not ml_lst:
                        raise UserError(_('Please select Atleast One salary Line'))
                    if rem_ml_lst:
                        self.remaining_slalry_sheet(rem_ml_lst, sheet)
                    if not self.journal_id.default_credit_account_id:
                        raise UserError(_('Please configure account of journal !'))
                    ml_lst.append((0, 0, {
                        'account_id': self.journal_id.default_credit_account_id.id,
                        'name': 'Salary Payment',
                        'credit': amount,
                        'currency_id': sheet.currency_id.id,

                    }))
                    move_id = move.create({'date': datetime.now(),
                                           'journal_id': self.journal_id.id,
                                           'ref': 'Salary Sheet',
                                           'line_ids': ml_lst,
                                           'salary_sheet_id': sheet.id
                                           })
                    if move_id:
                        move_id.post()
                        sheet.move_id = move_id.id
                        sheet.state = 'paid'