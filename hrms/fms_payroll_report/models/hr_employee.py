from odoo import models, fields, api, _
from odoo.exceptions import ValidationError

class HrEmployee(models.Model):
    _inherit = 'hr.employee'

    payment_option_line = fields.One2many('payment.option.line', 'emp_payment_id')
    title_id = fields.Many2one('hr.employee.title', 'Title')
    total_amount = fields.Float(compute='total_amount_compute', store=True)
    wage = fields.Float('Wage', compute='compute_salary', store=True)
    whatsapp_no = fields.Integer('WhatsApp Number')
    passport_issue_date = fields.Date('Passport Issue Date')
    passport_expire_date = fields.Date('Passport Expire Date')
    work_permit_no = fields.Char('Work Permit No.')
    work_permit_issue = fields.Date('Work Permit Issue Date')
    work_expire_issue = fields.Date('Work Permit Expire Date')
    visa_issue_date = fields.Date('Visa Issue Date')
    attachment_ids = fields.Many2many('ir.attachment', 'emp_attch_ref', 'emp_id', 'attch_id', 'Attachments')
    project_id = fields.Many2one('project.project', 'Project')
    location_id = fields.Many2one('stock.location', 'Location')
    branch_id = fields.Many2one('res.branch', 'Sector')

    def name_get(self):
        res = []
        for employee in self:
            name = employee.name
            if employee.barcode:
                name = '[' + employee.barcode + '] ' + name
            res.append((employee.id, name))
        return res

    @api.model
    def name_search(self, name='', args=None, operator='ilike', limit=100):
        if operator not in ('ilike', 'like', '=', '=like', '=ilike'):
            return super(HrEmployee, self).name_search(name, args, operator, limit)
        args = args or []
        domain = ['|', '|', '|', ('barcode', operator, name), ('name', operator, name), ('work_email', operator, name), ('mobile_phone', operator, name)]
        employees = self.search(domain + args, limit=limit)
        return employees.name_get()


    def compute_salary(self):
        for rec in self:
            contract = self.env['hr.contract'].search([('employee_id', '=', rec.id),
                                                       ('state', '=', 'open')], limit=1)
            if contract:
                rec.wage = contract.wage

    @api.depends('payment_option_line.amount')
    def total_amount_compute(self):
        for rec in self:
            tot = 0.0
            for line in rec.payment_option_line:
                tot += line.amount
            rec.total_amount = tot

    @api.model
    def create(self, vals):
        res = super(HrEmployee, self).create(vals)
        contract = self.env['hr.contract'].search([('employee_id', '=', res.id),
                                                   ('state', '=', 'open')], limit=1)
        if contract:
            if res.total_amount != contract.wage:
                raise ValidationError(_('Payment option amount should match employee current salary %s')%contract.wage)
        return res


    @api.constrains('payment_option_line')
    def validation_payment_amt(self):
        if self.payment_option_line:
            contract = self.env['hr.contract'].search([('employee_id', '=', self.id),
                                                           ('state', '=', 'open')], limit=1)
            if contract:
                if self.total_amount != contract.wage:
                    raise ValidationError(_('Payment option amount should match employee salary %s')%str(contract.wage))


class PaymentOptionLine(models.Model):
    _name = 'payment.option.line'
    _order = "preority asc"

    emp_payment_id = fields.Many2one('hr.employee')
    payment_option_id = fields.Many2one('payment.master', 'Payment Option')
    receiver_name = fields.Char('Receiver Name')
    telephone_no = fields.Char('Telephone Number')
    account_no = fields.Char('Account Number')
    account_name = fields.Char('Account Name')
    bank_name = fields.Char('Bank Name')
    branch_name = fields.Char('Branch Name')
    swift_code = fields.Char('Swift Code')
    country_id = fields.Many2one('res.country', 'Country')
    currency_id = fields.Many2one('res.currency', 'Currency')
    mpesa_receiver_name = fields.Char('Receiver Name')
    mpesa_telephone_no = fields.Char('Telephone Number')
    juba_account_no = fields.Char('Account Number')
    juba_account_name = fields.Char('Account Name')
    amount = fields.Float('Amount')
    preority = fields.Integer('Preority', default=1)

    @api.constrains('preority')
    def preority_check(self):
        for rec in self:
            payment_line = rec.search([('preority', '=', rec.preority), ('emp_payment_id', '=', rec.emp_payment_id.id)])
            if len(payment_line) > 1:
                raise ValidationError("Preority Already Exits")

    @api.constrains('payment_option_id')
    def payment_option_check(self):
        for rec in self:
            payment_lines = rec.search([('payment_option_id', '=', rec.payment_option_id.id), ('emp_payment_id', '=', rec.emp_payment_id.id)])
            if len(payment_lines) > 1:
                raise ValidationError("Payment Option Already Exits")

class HrEmployeeFamily(models.Model):
    _inherit = 'hr.employee.family'

    email = fields.Char('Email')
    relation = fields.Selection([('father', 'Father'),
                                 ('mother', 'Mother'),
                                 ('brother', 'Brother'),
                                 ('sister', 'Sister'),
                                 ('daughter', 'Daughter'),
                                 ('son', 'Son'),
                                 ('wife', 'Wife'),
                                 ('husband', 'Husband'),
                                 ('other', 'Other')], string='Relationship', help='Relation with employee')



