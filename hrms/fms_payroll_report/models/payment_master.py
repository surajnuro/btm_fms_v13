from odoo import models, fields, api, _

class PaymentMaster(models.Model):
    _name = 'payment.master'

    name = fields.Char('Name', copy=False, index=True)