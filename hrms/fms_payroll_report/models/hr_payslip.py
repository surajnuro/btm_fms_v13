from odoo import api, fields, models, _, tools
import time
from datetime import datetime
from datetime import time as datetime_time
from dateutil import relativedelta
from odoo.exceptions import ValidationError

import babel

class HrPayroll(models.Model):
    _inherit = 'hr.payslip'

    def action_payslip_done(self):
        res = super(HrPayroll, self).action_payslip_done()
        if self.net_salary <= 0.0:
            raise ValidationError(_('Sorry you can not confirm Because Net salary is neagtive'))
        report_id = self.env.ref('hr_payroll.action_report_payslip')
        template_id = self.env.ref('fms_payroll_report.confirm_payslip_mail_template')
        if template_id:
            # values = template_id.generate_email(self.id)
            template_id.email_from = self.company_id.email
            template_id.email_to = self.employee_id.work_email or ''
            template_id.send_mail(self.id, force_send=True)
        return res


    payslip_payment_line = fields.One2many('payslip.payment.line', 'payslip_id')


    @api.onchange('net_salary')
    def onchnage_emp_payline(self):
        pay_lst = []
        for rec in self:
            salary = rec.net_salary
            rem_sal = 0.0
            count = 0
            preority_lst = []
            if rec.employee_id:
                for line in rec.employee_id.payment_option_line:
                        if rem_sal < 0.0:
                            break
                        if line.amount >= salary and rem_sal <= 0.0:
                            pay_lst.append((0, 0, {
                                'payment_option_id': line.payment_option_id.id,
                                'receiver_name': line.receiver_name,
                                'telephone_no': line.telephone_no,
                                'bank_name': line.bank_name,
                                'branch_name': line.branch_name,
                                'swift_code': line.swift_code,
                                'country_id': line.country_id,
                                'currency_id': line.currency_id,
                                'amount': line.amount,
                                'preority': line.preority,
                                'sal_amount': salary
                            }))
                            break
                        if rem_sal > 0.0:
                            pay_lst.append((0, 0, {
                                'payment_option_id': line.payment_option_id.id,
                                'receiver_name': line.receiver_name,
                                'telephone_no': line.telephone_no,
                                'bank_name': line.bank_name,
                                'branch_name': line.branch_name,
                                'swift_code': line.swift_code,
                                'country_id': line.country_id,
                                'currency_id': line.currency_id,
                                'amount': line.amount,
                                'preority': line.preority,
                                'sal_amount': line.amount if rem_sal > line.amount else rem_sal
                            }))
                            rem_sal = rem_sal - line.amount

                        else:
                            pay_lst.append((0, 0, {
                                'payment_option_id': line.payment_option_id.id,
                                'receiver_name': line.receiver_name,
                                'telephone_no': line.telephone_no,
                                'bank_name': line.bank_name,
                                'branch_name': line.branch_name,
                                'swift_code': line.swift_code,
                                'country_id': line.country_id,
                                'currency_id': line.currency_id,
                                'amount': line.amount,
                                'preority': line.preority,
                                'sal_amount': line.amount if (rec.net_salary - line.amount) > 0 else 0
                            }))
                            rem_sal = rec.net_salary - line.amount



                rec.payslip_payment_line = pay_lst


class PaymentLine(models.Model):
    _name = 'payslip.payment.line'

    payslip_id = fields.Many2one('hr.payslip')
    payment_option_id = fields.Many2one('payment.master', 'Payment Option')
    payment_option = fields.Selection([('cash', 'Cash To Be Paid On Site'),
                                       ('premier_bank', 'Cash Transfer To Primer Bank'),
                                       ('home_bank', 'Cash Transfer To Home Bank'),
                                       ('Salam_bank', 'Cash Transfer to Salam Bank'),
                                       ('transfer_to_evc', 'Cash Transfer to EVC'),
                                       ('mpesa', 'MPESA'),
                                       ('juba', 'JUBA EXPRESS')], defaut='cash', string='Payment Option')
    receiver_name = fields.Char('Receiver Name')
    telephone_no = fields.Char('Telephone Number')
    bank_name = fields.Char('Bank Name')
    branch_name = fields.Char('Branch Name')
    swift_code = fields.Char('Swift Code')
    country_id = fields.Many2one('res.country', 'Country')
    currency_id = fields.Many2one('res.currency', 'Currency')
    amount = fields.Char('Amount')
    account_no = fields.Char('Account Number')
    account_name = fields.Char('Account Name')
    preority = fields.Integer('Preority', default=1)
    # preority = fields.Selection([('1', '1'), ('2', '2'), ('3', '3'),
    #                              ('4', '4'), ('5', '5'), ('6', '6'), ('7', '7')], string='Preority')
    sal_amount = fields.Float('Salary Amount')
