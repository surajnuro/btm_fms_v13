# -- coding: utf-8 --
# Part of Nurosolution Pvt Ltd.

from datetime import datetime, timedelta, date
import time
from odoo import api, fields, models, _
from dateutil import relativedelta
import calendar
from odoo.exceptions import UserError, ValidationError

class PayrollCalculation(models.Model):
    _inherit = 'payroll.calculation'

    project_id = fields.Many2one('project.project', 'Project')
    branch_id = fields.Many2one('res.branch', 'Sector')

    def calculate_employee_data(self):
        '''method fill employee and claulate leave, attendance, ded, allowance,ded'''
        domain = []
        if not self.type_selection:
            raise ValidationError(('Please fill employee type!!!'))
        emp_lst = []
        employee = self.env['hr.employee']
        if self.payroll_calculation_lines:
            self.update({'payroll_calculation_lines': False})
        if self.department_id:
            domain += [('department_id', '=', self.department_id.id)]
        if self.type_selection:
            domain += [('type_selection', '=', self.type_selection)]
        if self.project_id:
            domain += [('project_id', '=', self.project_id.id)]
        if domain:
            d1 = datetime.strptime(str(self.date_from), '%Y-%m-%d')
            d2 = datetime.strptime(str(self.date_to), '%Y-%m-%d')
            daysDiff = (d2 - d1).days
            emp_ids = employee.search(domain)
            if emp_ids:
                for employee in emp_ids:
                    search_wage = self.env['hr.contract'].search(
                        [('employee_id', '=', employee.id), ('state', '=', 'open')], limit=1)
                    if not search_wage:
                        raise ValidationError(('Please define contract for %s') % employee.name)
                    wage = search_wage.wage
                    total_present = 0.0
                    total_absent = 0.0
                    datetime_from = datetime.strptime(str(self.date_from), '%Y-%m-%d').replace(hour=0, minute=0,
                                                                                               second=1)
                    datetime_to = datetime.strptime(str(self.date_to), '%Y-%m-%d').replace(hour=23, minute=59,
                                                                                           second=59)
                    for x in range(0, daysDiff + 1):
                        date = (d1 + timedelta(days=x)).date()
                        total_day = self._calculate_attendance_days(date=date, employee=employee)
                        if total_day == 1:
                            total_present += total_day
                        else:
                            total_absent += 1
                    leave_attendance = self.env['hr.leave'].search([('employee_id', '=', employee.id),
                                                                    ('state', '=', 'validate'),
                                                                    ('date_from', '>=', str(datetime_from)),
                                                                    ('date_to', '<=', str(datetime_to))])

                    public_holiday = self.env['hr.holidays.public.line'].search([('date', '>=', str(self.date_from)),
                                                                                 ('date', '<=', str(self.date_to))])
                    weeklst = []
                    for rec in search_wage.resource_calendar_id.attendance_ids:
                        weeklst.append(rec.dayofweek)
                    holiday_days = 0
                    if public_holiday:
                        for ph in public_holiday:
                            ph_date = datetime.strptime(ph.date, '%Y-%m-%d')
                            if str(ph_date.weekday()) in weeklst:
                                holiday_days += 1
                    total_paid_leave = unpaid_leaves = 0
                    if leave_attendance:
                        for lv in leave_attendance:
                            if lv.holiday_status_id.leave_type == 'paid':
                                total_paid_leave += lv.number_of_days
                            if lv.holiday_status_id.leave_type == 'unpaid':
                                unpaid_leaves += lv.number_of_days
                    weekend = self._get_weekend(d1.year, d1.month)
                    if employee.attendance_calculation == 'on_attendance':
                        self.env['payroll.calculation.line'].create({
                            'payroll_cal_id': self.id,
                            'employee_id': employee.id,
                            'total_present': total_present,
                            'total_absent': total_absent - weekend - total_paid_leave - unpaid_leaves,
                            'paid_leaves': total_paid_leave,
                            'unpaid_leaves': unpaid_leaves,
                            'weekend_days': weekend,
                            'working_days_paid': total_present + weekend + total_paid_leave,
                            'wage': wage,
                            'total_days': daysDiff + 1,
                        })
            else:
                self.update({'payroll_calculation_lines': False})
