from odoo import models, fields, api, _

class HrContract(models.Model):
    _inherit = 'hr.contract'
    _description = 'Employee Contract'

    release_date = fields.Date('Release date', track_visibility='onchange')