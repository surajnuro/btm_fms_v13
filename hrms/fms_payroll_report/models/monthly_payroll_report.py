from odoo import api, fields, models, _, tools
import babel
import time
import base64
import xlsxwriter
from datetime import datetime, timedelta
from odoo.exceptions import ValidationError
import io

class HrPayrollBatch(models.Model):
    _inherit = 'hr.payslip.run'

    project_sector_id = fields.Many2one('project.project', 'Sector')
    type_selection = fields.Selection([('national', 'National'), ('international', 'International')], default='national', string='Type')
    journal_id = fields.Many2one('account.journal', string='Salary Journal',domain=[('name', 'ilike', 'Salary')], default=lambda self: self.env['account.journal'].search([('name', 'ilike', 'Salary')], limit=1))

    def _get_advance(self, emp_id, date_from, date_to):
        advance = self.env['hr.loan.line'].search([('employee_id', '=', emp_id.id),
                                              ('loan_id.state', '=', 'paid_to_employee'),
                                              ('date', '>=', date_from),
                                              ('date', '<=', date_to)], limit=1)
        return advance

    def approve(self):
        payment_type = ['cash','premier_bank','home_bank','mpesa','juba','Salam_bank', 'transfer_to_evc']
        payment_option = self.env['payment.master'].search([])
        salary_sheet = self.env['salary.sheet']
        sheet_lst = []
        if self.slip_ids:
            for rec in self.slip_ids:
                rec.compute_sheet()
                rec.action_payslip_done()
        if self.slip_ids:
            user = self.user_id
            if payment_option:
                for pay in payment_option:
                    for line in self.slip_ids:
                        if not line.payslip_payment_line:
                            raise ValidationError(('Please configure payment option for %s' % (line.employee_id.name)))
                        if line.payslip_payment_line:
                            for rec in line.payslip_payment_line:

                                if rec.payment_option_id.id == pay.id:
                                    date_from = line.date_from
                                    date_to = line.date_to
                                    sheet_lst.append((0,0,{
                                        'employee_id': line.employee_id.id,
                                        'date_from': date_from,
                                        'date_to': date_to,
                                        'amount': rec.sal_amount,
                                        'salary': line.net_salary,
                                        'payment_option_id': rec.payment_option_id.id
                                    }))
                                    name = 'Salary Sheet for' + ' ' + str(rec.payment_option_id.name) + ' ' + 'of' + ' '

                    if sheet_lst:
                        ttyme = datetime.fromtimestamp(time.mktime(time.strptime(str(date_from), "%Y-%m-%d")))
                        locale = self.env.context.get('lang', 'en_US')
                        sheet_id = salary_sheet.create({'name': str(str(name) + tools.ustr(
                            babel.dates.format_date(date=ttyme, format='MMMM-y',
                                                    locale=locale))),
                                                        'date_from': date_from,
                                                        'date_to': date_to,
                                                        'batch_pay_id': self.id,
                                                        'sheet_type': 'full',
                                                        # 'bank_id': bnk.id,
                                                        'user_id': user.id or False,
                                                        # 'company_id': self.company_id.id,
                                                        'salary_line': sheet_lst})
                        sheet_lst = []
        self.write({'state': 'approved', 'approved_by': self.env.user.id, 'approve_date': datetime.now()})

    def print_excel(self):
        f_name = '/tmp/monthly_payroll_report.xlsx'
        workbook = xlsxwriter.Workbook(f_name)
        sheet2 = 'pie'
        sheet3 = 'bar'
        worksheet = workbook.add_worksheet('Report')
        worksheet.set_column('A:K', 12)
        style = workbook.add_format({
            'bold': 1,
            'border': 1,
            'align': 'center',
            'valign': 'vcenter'})
        style.set_font_size(8)
        align_value = workbook.add_format({
            'align': 'center',
            'valign': 'vcenter',
            })
        align_value.set_font_size(8)

        row = 7
        col = 21
        new_row = row + 1
        payment_options = self.env['payment.master'].search([])
        worksheet.merge_range('A4:J4', "DEEQA CONSTRUCTION & WATER WELL DRILLING CO. LTD.", style)
        date = 'ab'
        if self.date_end and self.date_start:
            from_date = datetime.strptime(self.date_start, "%Y-%m-%d").date()
            to_date = datetime.strptime(self.date_end, "%Y-%m-%d").date()
            worksheet.merge_range('A5:G5', self.name, style)
            worksheet.merge_range('H5:J5',str(from_date.day) + ' ' + from_date.strftime("%B") + ' ' + str(from_date.year) + ' ' + 'To' + ' '
                                  + str(to_date.day) + ' ' + to_date.strftime("%B") + ' ' + str(to_date.year), style)
        else:
            worksheet.merge_range('A5:J5', " REPORT FORMAT FOR SALARY SHEET", style)

        buf_image = io.BytesIO(base64.b64decode(self.company_id.logo))
        worksheet.insert_image('C1', "any_name.png", {'image_data': buf_image, 'x_scale': 0.6, 'y_scale': 0.5})

        worksheet.merge_range('A7:Z7', "PAYSLIPS DETAILS", style)
        worksheet.write('A8', "Staff Name", style)
        worksheet.write('B8', "Position", style)
        worksheet.write('C8', "Department", style)
        worksheet.write('D8', 'ID NO.', style)
        worksheet.write('E8', 'Title', style)
        worksheet.write('F8', 'Work Station.', style)
        worksheet.write('G8', 'Nationality', style)
        worksheet.write('H8', 'Monthly Salary', style)
        worksheet.write('I8', 'No Of Days In Month', style)
        worksheet.write('J8', 'No Of Worked days', style)
        worksheet.write('K8', 'Gross Salary', style)
        worksheet.write('L8', 'Tax', style)
        worksheet.write('M8', 'Net Salary After Tax', style)
        worksheet.write('N8', 'Employee Bonus or other Salary', style)
        worksheet.write('O8', 'Cash Advance Opening Balance', style)
        worksheet.write('P8', 'Monthly Deduction', style)
        worksheet.write('Q8', 'Cash Advance Closing Bal', style)
        worksheet.write('R8', 'Unpaid Leave Days', style)
        worksheet.write('S8', 'Other Staff Deduction', style)
        worksheet.write('T8', 'Total Deduction', style)
        worksheet.write('U8', 'Net Pay After Deduction', style)
        payment_type = []
        if self.slip_ids:
            for line in self.slip_ids:
                if line.payslip_payment_line:
                    for pl in line.payslip_payment_line:
                        if not pl.payment_option_id in payment_type:
                            payment_type.append(pl.payment_option_id)
            if payment_type:
                for pay in payment_type:
                    worksheet.write(row,col, pay.name, style)
                    col = col + 1
        col = 21
        new_row = 9
        tax = gross = tot_ded = net = month_sal = pay_after_ded  = workday = bonus = adv_open = total_paid_day = month_ded = adv_close = unpaid_lev = staff_ded = 0
        cash = prem_bank = home = mpesa = juba = 0
        data_dict = {}
        amt = 0.0
        for line in self.slip_ids:
            worksheet.write("A%s" % (new_row), line.employee_id.name or '-', align_value)
            worksheet.write("B%s" % (new_row), line.job_id.name or '-', align_value)
            worksheet.write("C%s" % (new_row), line.department_id.name or '-', align_value)
            worksheet.write("D%s" % (new_row), line.employee_id.identification_id or '-', align_value)
            worksheet.write("E%s" % (new_row), line.employee_id.title_id.name or '-', align_value)
            worksheet.write("F%s" % (new_row), line.employee_id.work_location or '-', align_value)
            worksheet.write("G%s" % (new_row),  line.employee_id.country_id.name or '-', align_value)
            worksheet.write("H%s" % (new_row), line.contract_id.wage or '-', align_value)
            month_sal += line.contract_id.wage
            worksheet.write("I%s" % (new_row), line.total_work_day , align_value)
            worksheet.write("J%s" % (new_row), abs(line.total_paid_day), align_value)
            total_paid_day += abs(line.total_paid_day)
            workday += line.total_work_day
            worksheet.write("K%s" % (new_row), line.gross_salary, align_value)
            gross += line.gross_salary
            worksheet.write("L%s" % (new_row), line.tax_deduction, align_value)
            tax += line.tax_deduction
            worksheet.write("M%s" % (new_row), line.contract_id.wage - (-line.tax_deduction), align_value)
            net += (line.contract_id.wage - (-line.tax_deduction))
            worksheet.write("N%s" % (new_row),  0, align_value)
            bonus += 0
            loan = self._get_advance(emp_id = line.employee_id, date_from = line.date_from, date_to = line.date_to)
            if loan:
                amt = 0.0
                rem = 0.0
                for ln in loan.loan_id.loan_lines:
                    if ln.paid:
                        amt += ln.amount
                    if not ln.paid:
                        rem += ln.amount
                worksheet.write("O%s" % (new_row), rem, align_value)
                adv_open += rem
                worksheet.write("Q%s" % (new_row),  (rem) - (-line.advance_loan_deduction), align_value)
                adv_close += ((rem) - (-line.advance_loan_deduction))
            else:
                worksheet.write("O%s" % (new_row), 0, align_value)
                worksheet.write("Q%s" % (new_row), 0, align_value)

            worksheet.write("P%s" % (new_row), line.advance_loan_deduction, align_value)
            month_ded += line.advance_loan_deduction
            if self.slip_ids:
                for rec in self.slip_ids:
                    unpaid = rec.worked_days_line_ids.filtered(lambda x: x.code == 'UNPAID')
                    if unpaid:
                        worksheet.write("R%s" % (new_row), unpaid.number_of_days, align_value)
                        unpaid_lev += unpaid.number_of_days
                        start_date = datetime.strptime(self.date_start, "%Y-%m-%d")
                        end_date = datetime.strptime(self.date_end, "%Y-%m-%d")
                        tot_day = abs((end_date - start_date).days)
                        deduct_sal = (line.contract_id.wage / tot_day) * unpaid.number_of_days
                        worksheet.write("T%s" % (new_row),
                                        (deduct_sal) + (line.advance_loan_deduction) + (line.tax_deduction), align_value)
                        tot_ded += ((deduct_sal) + (line.advance_loan_deduction) + (line.tax_deduction))
                    else:
                        worksheet.write("R%s" % (new_row), 0, align_value)
                        worksheet.write("T%s" % (new_row),
                                         (line.advance_loan_deduction) + (line.tax_deduction), align_value)
                        tot_ded += ((line.advance_loan_deduction) + (line.tax_deduction))
            worksheet.write("S%s" % (new_row), 0, align_value)
            staff_ded += 0
            worksheet.write("U%s" % (new_row), line.net_salary, align_value)
            pay_after_ded += line.net_salary
            if line.payslip_payment_line:
                col = 21
                for pay in line.payslip_payment_line:
                    for pl in payment_type:
                        if pl.id == pay.payment_option_id.id:
                            ind = payment_type.index(pl)
                            if line.net_salary <= 0.0:
                                continue
                            worksheet.write(new_row - 1,col + ind,pay.sal_amount,align_value)
                            data_dict[pl.name] = data_dict.get(pl.name, []) + [pay.sal_amount]

                    # if pay.payment_option == 'cash':
                    #     worksheet.write("V%s" % (new_row), (float(pay.amount) / line.contract_id.wage) * float(line.net_salary),
                    #                     align_value)
                    #     cash += ((float(pay.amount) / line.contract_id.wage) * float(line.net_salary))

            new_row += 1

        worksheet.merge_range("A%s:G%s" % (new_row, new_row), 'TOTAL', style)
        worksheet.write("H%s" % (new_row), month_sal or 0, style)
        worksheet.write("I%s" % (new_row), workday or 0, style)
        worksheet.write("J%s" % (new_row), total_paid_day or 0, style)
        worksheet.write("K%s" % (new_row), gross or 0, style)
        worksheet.write("L%s" % (new_row), tax or 0, style)
        worksheet.write("M%s" % (new_row), net or 0, style)
        worksheet.write("N%s" % (new_row), bonus or 0, style)
        worksheet.write("O%s" % (new_row), adv_open or 0, style)
        worksheet.write("P%s" % (new_row), month_ded or 0, style)
        worksheet.write("Q%s" % (new_row), adv_close or 0, style)
        worksheet.write("R%s" % (new_row), unpaid_lev or 0, style)
        worksheet.write("S%s" % (new_row), staff_ded or 0, style)
        worksheet.write("T%s" % (new_row), tot_ded or 0, style)
        worksheet.write("U%s" % (new_row),  pay_after_ded or 0, style)
        if data_dict:
            col = 21
            for key, value in data_dict.items():
                amt = 0.0
                for pay in payment_type:
                    for rec in value:
                        if pay.name == key:
                            amt = amt + rec
                worksheet.write(new_row - 1,col, amt, style)
                col = col + 1
        workbook.close()
        f = open(f_name, 'rb')
        data = f.read()
        f.close()
        name = 'Monthly Payroll Report'
        dt = ' ' + 'From' + ' ' + str(self.date_start) + ' ' + 'To' + ' ' + str(self.date_end)
        if self.date_start and self.date_end:
            out_wizard = self.env['xlsx.output'].create({'name': name + dt + '.xlsx',
                                                         'xls_output': base64.encodebytes(data)})
        else:
            out_wizard = self.env['xlsx.output'].create({'name': name + '.xlsx',
                                                         'xls_output': base64.encodebytes(data)})
        view_id = self.env.ref('nuro_salary_sheet.xlsx_output_form').id
        return {
            'type': 'ir.actions.act_window',
            'name': _(name),
            'res_model': 'xlsx.output',
            'target': 'new',
            'view_mode': 'form',
            'res_id': out_wizard.id,
            'views': [[view_id, 'form']],
        }