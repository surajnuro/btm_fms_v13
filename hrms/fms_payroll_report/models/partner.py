from odoo import api, fields, models, _, tools

class ResPartner(models.Model):
    _inherit = 'res.partner'
    _description = 'check Company salary option'

    check_company = fields.Boolean('Company Salary')