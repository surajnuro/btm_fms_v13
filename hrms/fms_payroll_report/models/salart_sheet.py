from odoo import api, fields, models, _
import base64
import xlsxwriter
from datetime import datetime
import io

class SalarySheet(models.Model):
    _inherit = 'salary.sheet'

    salary_line = fields.One2many('salary.line', 'salary_id', 'Salary Lines')
    company_id = fields.Many2one('res.company', 'Company',  index=True,
                                 default=lambda self: self.env.user.company_id.id)
    currency_id = fields.Many2one('res.currency', string='Currency',  default=lambda self: self.env.user.company_id.currency_id.id)
    conversion_rate = fields.Float('Conversion Rate')
    state = fields.Selection(selection_add=[("remaining", "Remaining")])
    remaining_amt = fields.Float(compute='_remaining_amount', store=True)
    salary_sheet_id = fields.Many2one('salary.sheet')

    @api.onchange('currency_id', 'date_to')
    def onchange_currency_rate(self):
        if self.currency_id:
            if self.currency_id != self.company_id.currency_id:
                currency_rate = self.currency_id.with_context(
                    date=self.date_to or fields.Date.context_today(self)).rate
                if currency_rate:
                    self.conversion_rate = currency_rate
            else:
                self.conversion_rate = 0.0

    @api.depends('salary_line.select_salary_line')
    def _remaining_amount(self):
        for rec in self:
            total = 0
            for sl in rec.salary_line:
                if sl.select_salary_line != True:
                    total += sl.amount
            rec.remaining_amt = total

    @api.depends('payslip_ids')
    def compute_total(self):
        for rec in self:
            net = gross = tax = adv = 0
            for line in rec.salary_line:
                net += line.amount
                # gross += line.gross_salary
                # tax += line.tax_deduction
                # adv += line.advance_loan_deduction
            rec.total_net = abs(net)
            # rec.total_gross = abs(gross)
            # rec.total_tax_deduction = abs(tax)
            # rec.total_advance = abs(adv)

    def print_excel(self):
        f_name = '/tmp/salary_sheet_report.xlsx'
        workbook = xlsxwriter.Workbook(f_name)
        sheet2 = 'pie'
        sheet3 = 'bar'
        worksheet = workbook.add_worksheet('Report')
        worksheet.set_column('A:K', 12)
        style = workbook.add_format({
            'bold': 1,
            'border': 1,
            'align': 'center',
            'valign': 'vcenter'})
        style.set_font_size(8)
        align_value = workbook.add_format({
            'align': 'center',
            'valign': 'vcenter'})

        align_value.set_font_size(8)
        row = 4
        new_row = row + 1
        worksheet.merge_range('A4:H4', "DEEQA CONSTRUCTION & WATER WELL DRILLING CO. LTD.", style)
        date = 'ab'
        if self.date_to and self.date_from:

            from_date = datetime.strptime(self.date_from, "%Y-%m-%d").date()
            to_date = datetime.strptime(self.date_to, "%Y-%m-%d").date()
            worksheet.merge_range('A5:E5', self.name, style)
            worksheet.merge_range('F5:H5', str(from_date.day) + ' ' + from_date.strftime("%B") + ' ' + str(from_date.year) + ' ' + 'To' + ' '
                                  + str(to_date.day) + ' ' + to_date.strftime("%B") + ' ' + str(to_date.year), style)
        else:
            worksheet.merge_range('A4:I4', " REPORT FORMAT FOR SALARY SHEET", style)

        worksheet.merge_range('A7:B7', "Cheque Number", style)
        worksheet.merge_range('A9:D9', "Salary Details", style)
        # worksheet.write('A8', "Refernece", style)
        worksheet.write('A10', "Employee", style)
        worksheet.write('B10', 'Date from', style)
        worksheet.write('C10', 'Date To', style)
        worksheet.write('D10', 'Amount', style)
        worksheet.write("C7", self.cheque_number or "Not Filled", align_value)

        new_row = 11
        total = 0

        buf_image = io.BytesIO(base64.b64decode(self.company_id.logo))
        worksheet.insert_image('C1', "any_name.png", {'image_data': buf_image, 'x_scale': 0.6, 'y_scale': 0.5})

        for line in self.salary_line:
            from_date = datetime.strptime(line.date_from, "%Y-%m-%d").date()
            to_date = datetime.strptime(line.date_to, "%Y-%m-%d").date()

            worksheet.write("A%s" % (new_row), line.employee_id.name or '-', align_value)
            worksheet.write("B%s" % (new_row), str(from_date.day) + ' ' + from_date.strftime("%B") + ' ' + str(from_date.year) or '-', align_value)
            worksheet.write("C%s" % (new_row), str(to_date.day) + ' ' + to_date.strftime("%B") + ' ' + str(to_date.year) or '-', align_value)
            worksheet.write("D%s" % (new_row), line.amount or 0, align_value)
            total += line.amount
            new_row += 1
        worksheet.merge_range("A%s:C%s" % (new_row, new_row), 'TOTAL', style)
        worksheet.write("D%s" % (new_row), total or 0, style)

        worksheet.write("A%s" % (new_row + 2), 'Signature', style)
        worksheet.write("A%s" % (new_row + 3), 'Name', style)

        workbook.close()
        f = open(f_name, 'rb')
        data = f.read()
        f.close()
        name = 'Salary Sheet Report'
        dt = ' ' + 'From' + ' ' + str(self.date_from) + ' ' + 'To' + ' ' + str(self.date_to)
        if self.date_from and self.date_to:
            out_wizard = self.env['xlsx.output'].create({'name': name + dt + '.xlsx',
                                                         'xls_output': base64.encodebytes(data)})
        else:
            out_wizard = self.env['xlsx.output'].create({'name': name + '.xlsx',
                                                         'xls_output': base64.encodebytes(data)})
        view_id = self.env.ref('nuro_salary_sheet.xlsx_output_form').id
        return {
            'type': 'ir.actions.act_window',
            'name': _(name),
            'res_model': 'xlsx.output',
            'target': 'new',
            'view_mode': 'form',
            'res_id': out_wizard.id,
            'views': [[view_id, 'form']],
        }


class SalaryLine(models.Model):
    _name = 'salary.line'
    _order = "payment_option_id asc"

    salary_id = fields.Many2one('salary.sheet')
    employee_id = fields.Many2one('hr.employee')
    amount = fields.Float('Amount')
    salary = fields.Float('Salary')
    date_from = fields.Date('Date From')
    date_to = fields.Date('Date To')
    payment_option_id = fields.Many2one('payment.master', 'Payment Option')
    payment_option = fields.Selection([('cash', 'Cash To Be Paid On Site'),
                                       ('premier_bank', 'Cash Transfer To Primer Bank'),
                                       ('home_bank', 'Cash Transfer To Home Bank'),
                                       ('Salam_bank', 'Cash Transfer to Salam Bank'),
                                       ('transfer_to_evc', 'Cash Transfer to EVC'),
                                       ('mpesa', 'MPESA'),
                                       ('juba', 'JUBA EXPRESS')], string='Payment Option')
    select_salary_line = fields.Boolean('Select')
