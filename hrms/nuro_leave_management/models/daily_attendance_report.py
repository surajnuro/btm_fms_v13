from odoo import models, fields, api, _
from datetime import timedelta, datetime, date


class DailyAttandanceReport(models.Model):
    _name = 'daily.attandance.report'
    _inherit = ['resource.resource']

    employee_id = fields.Many2one('hr.employee', string='Employee', required=True)
    department_id = fields.Many2one('hr.department', string='Department', related='employee_id.department_id')
    date = fields.Date('Date', default=datetime.now().strftime('%Y-%m-%d'))
    total_houre = fields.Float('Total Houre')
    day_count = fields.Integer('Day Count')

    def update_daily_attandence_report(self):
        current_date = datetime.now().date()
        hr_employee = self.env['hr.employee'].search([])
        yesterday = datetime.now().date() - timedelta(days=1)
        for emp in hr_employee:
            attendance_obj = self.env['hr.attendance'].read_group([('check_in', '>=', str(yesterday)),
                                                               ('check_out', '<=', str(yesterday))],
                                                                  ['check_in', 'check_out'],
                                                                  'employee_id')

            existing_attendance = self.env['daily.attandance.report'].search([('employee_id', '=', emp.id),
                                                              ('date', '=', str(yesterday))])

            