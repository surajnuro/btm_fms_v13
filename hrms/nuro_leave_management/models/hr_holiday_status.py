from odoo import models, fields, api, _

class DailyAttandanceReport(models.Model):
    _inherit = 'hr.leave.type'

    leave_type = fields.Selection([('paid', 'Paid'), ('unpaid', 'Unpaid')], default='paid')
