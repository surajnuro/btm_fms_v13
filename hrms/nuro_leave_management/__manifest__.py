# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.


{
    'name': 'Daily Attendance Report',
    'version': '13.0',
    'category': 'Attendance',
    'description': """
    Allow to fetch the details report for employee Attendance.
    """,
    'license': 'AGPL-3',
    'author': "Nuro Solution Pvt. LTD",
    'website': 'http://www.nurosolution.com',
    'depends': ['hr', 'hr_attendance'],
    'data': [
            'views/daily_attandance.xml',
            'views/holiday_status.xml',
            'data/daily_attandance_report.xml',
             ],
    'installable': True,
    'application': True,
    'auto_install': False,
}
