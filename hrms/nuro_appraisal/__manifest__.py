# -*- coding: utf-8 -*-
# Copyright  Nuro Solution Pvt Ltd
{
    'name': 'FMS Employee Appraisal Management',
    'category': 'Custom',
    'summary': 'FMS Employee Appraisal Management',
    'description': """
        FMS Employee Appraisal Management
    """,
    'author': 'Nuro Solution Pvt Ltd',
    'website': 'http://www.nurosolution.com',
    'company': 'Nuro Solution Pvt Ltd',
    'license': 'Other proprietary',
    'depends': ['hr', 'nuro_fms_employees'],
    'data': [
            'security/ir.model.access.csv',
            'security/security_view.xml',
            'wizard/send_approval_view.xml',
            'views/employee_year_appraisal.xml',
            'views/employee_appraisal.xml',
            'views/appraisal_report.xml',
            'views/hr_employee.xml',
            'report/90_days_report.xml',
            'report/year_appraisal_view.xml',
            'menu/menu.xml',
    ],

    'installable': True,
    'application': True,
}