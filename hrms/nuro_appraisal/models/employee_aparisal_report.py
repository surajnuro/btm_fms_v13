from odoo import api, fields, models,_,tools
from datetime import date

class EmployeeAprisalReport(models.Model):
    _name = 'appraisal.report'
    _rec_name = 'employee_id'
    _auto = False

    id = fields.Integer()
    employee_id = fields.Many2one('hr.employee', 'Employee')
    appraisal_date = fields.Date('Appraisal Date')
    next_apraisal_date = fields.Date('Next Appraisal Date')
    staff_id = fields.Char('Employee ID')
    department_id = fields.Many2one('hr.department', 'Department')
    type = fields.Selection([('day', '90 Day Appraisal'),('year','Yearly Appraisal')], string='Appraisal Type')

    def init(self):
        tools.drop_view_if_exists(self._cr, 'appraisal_report')
        self._cr.execute("""
            CREATE or REPLACE view appraisal_report as (
                select id as id, employee_id as employee_id, appraisal_date as appraisal_date,  next_appraisal as next_apraisal_date,
                type as type, staff_id as staff_id, department_id as department_id
                from emp_appraisal where state = 'confirm'
            )""")

