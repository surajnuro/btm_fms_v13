from  odoo import fields,models,api,_

class Employee(models.Model):
    _inherit = 'hr.employee'

    appraisal_count = fields.Integer(compute='compute_appraisal')

    def compute_appraisal(self):
        for rec in self:
            appraisals = self.env['emp.appraisal'].search([('employee_id', '=', rec.id)])
            if appraisals:
                rec.appraisal_count = len(appraisals)
            else:
                rec.appraisal_count = 0

    def open_appraisal(self):
        form_view = self.env.ref('nuro_appraisal.year_appraisal_form_view').id
        tree_view = self.env.ref('nuro_appraisal.year_appraisal_tree_view').id
        appraisals = self.env['emp.appraisal'].search([('employee_id', '=', self.id)])
        return {
            'name': _('appraisal'),
            'res_model': 'emp.appraisal',
            'views': [
                (tree_view, 'tree'),
                (form_view, 'form'),
            ],
            'view_mode': 'tree,form',
            'type': 'ir.actions.act_window',
            'domain': [('id', 'in', appraisals.ids)]
        }