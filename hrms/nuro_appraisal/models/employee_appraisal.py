from odoo import fields,models,api,_
from datetime import datetime,timedelta

class YearAppraisal(models.Model):
    _name = 'emp.appraisal'
    _rec_name = 'employee_id'

    employee_id = fields.Many2one('hr.employee', 'Employee name', default=lambda self: self.env['hr.employee'].search([('user_id', '=', self.env.uid)], limit=1))
    job_id = fields.Many2one('hr.job', 'Job')
    department_id = fields.Many2one('hr.department', 'Department')
    empl_date = fields.Date('Date of Employment')
    employment_date = fields.Date('Date of Employment')
    evaluated_date = fields.Date('Evaluated Date')
    appraisal_date = fields.Date('Appraisal Date', default=fields.Date.today())
    next_appraisal = fields.Date()
    staff_id = fields.Char('Staff ID')
    work_station = fields.Char('Work Station')
    work_duration = fields.Char('Work Duration')
    type = fields.Selection([('day', 'Day'),('year','Year')], default='year')
    appraisal_category = fields.Selection([('office_staff', 'Office Staff'),
                                           ('staff_supervisor', 'Office Staff-Supervisor'),
                                           ('site_supervisor', 'Site Supervisor/Site Engineer'),
                                           ('civil_engineer', 'Civil Engineer/Manager/HOD')], string='Appraisal Category')
    line_manager_id = fields.Many2one('res.users', 'Line Manager')
    supervisor_id = fields.Many2one('res.users', 'Immediate Supervisor')
    project_manager_id = fields.Many2one('res.users', 'Project Manager')
    hr_manager_id = fields.Many2one('res.users', 'HR Manager')
    request_date = fields.Date('Requested Date')

    summarize_job_roles = fields.Text('')
    assessment_performance_a = fields.Boolean('Well Ahead of Standard')
    assessment_performance_b = fields.Boolean('More than satisfactory-slightly above job requirements')
    assessment_performance_c = fields.Boolean('Less than satisfactory-Needs slight improvement')
    assessment_performance_d = fields.Boolean('Unsatisfactory-Below expected standard')
    state = fields.Selection([('draft', 'Draft'),
                              ('waiting', 'Waiting'),
                              ('confirm', 'Confirm'),
                              ('cancel', 'Cancel')], default='draft')
    quality_rating = fields.Integer('')
    quality_exceptionally_high = fields.Boolean()
    quality_usually = fields.Boolean()
    quality_occasionally = fields.Boolean()
    quality_insufficienty = fields.Boolean()

    quantity_rating = fields.Integer('')
    quantity_outstanding = fields.Boolean()
    quantity_meets_expectations = fields.Boolean()
    quantity_improvement = fields.Boolean()
    quantity_unsatisfactory = fields.Boolean()

    job_rating = fields.Integer('')
    job_exceptionally = fields.Boolean()
    job_good_knowledge = fields.Boolean()
    job_lack_of_job = fields.Boolean()
    job_inadequate = fields.Boolean()

    safety_rating = fields.Integer()
    safety_highly_motivated = fields.Boolean()
    safety_attitude = fields.Boolean()
    safety_sometimes = fields.Boolean()
    safety_disregards = fields.Boolean()

    working_rating = fields.Integer()
    working_outstanding = fields.Boolean()
    working_meets = fields.Boolean()
    working_needs = fields.Boolean()
    working_unsatisfactory = fields.Boolean()

    skills_rating = fields.Integer()
    skill_outstanding = fields.Boolean()
    skills_meets = fields.Boolean()
    skill_improvement = fields.Boolean()
    skill_unsatisfactory = fields.Boolean()

    dependability_rating = fields.Integer()
    dependability_always = fields.Boolean()
    dependability_supervision = fields.Boolean()
    dependability_requires = fields.Boolean()
    dependability_constant = fields.Boolean()

    teamwork_rating = fields.Integer()
    teamwork_extremely = fields.Boolean()
    teamwork_operative = fields.Boolean()
    teamwork_usually = fields.Boolean()
    teamwork_resists = fields.Boolean()

    attendance_rating = fields.Integer()
    attendance_exceptionally = fields.Boolean()
    attendance_levels = fields.Boolean()
    attendance_absence = fields.Boolean()
    attendance_frequently = fields.Boolean()

    planing_rating = fields.Integer()
    planing_displays = fields.Boolean()
    planing_organises = fields.Boolean()
    planing_improve = fields.Boolean()
    planing_effectively = fields.Boolean()

    communication_rating = fields.Integer()
    communication_exceptionally = fields.Boolean()
    communication_good = fields.Boolean()
    communication_difficulties = fields.Boolean()
    communication_not_good = fields.Boolean()

    overall_rating = fields.Integer()
    overall_well_ahead = fields.Boolean()
    overall_satisfactory = fields.Boolean()
    overall_needs_improvement = fields.Boolean()
    overall_not_satisfactory = fields.Boolean()

    line_manager = fields.Text()
    supervisor_manager = fields.Text()
    action_plan_employee = fields.Text()
    action_plan_deeqa = fields.Text()
    approve = fields.Boolean('Approve')
    not_approve  = fields.Boolean('Not Approve')

    job_understanding_coment = fields.Text('')
    job_performance = fields.Text('')
    job_productivity = fields.Text('')
    dependability = fields.Text('')
    teamwork = fields.Text('')
    judgment = fields.Text('')
    client_service = fields.Text('')
    initiative = fields.Text('')
    personal_attributes = fields.Text('Personal attributes')
    overall_assessment = fields.Text('')

    job_understanding_rating = fields.Char('')
    job_performance_rating = fields.Char('')
    job_productivity_rating = fields.Char('')
    dependability_rating = fields.Char('')
    teamwork_rating = fields.Char('')
    judgment_rating = fields.Char('')
    client_service_rating = fields.Char('')
    initiative_rating = fields.Char('')
    personal_attributes_rating = fields.Char('Personal attributes')
    overall_assessment_rating = fields.Char('')

    comment_supervisor = fields.Text()
    comment_project_manager = fields.Text()
    comment_hrd = fields.Text()
    next_review = fields.Date('')
    line_manager_id = fields.Many2one('res.users', 'Line Manager')
    supervisor_id = fields.Many2one('res.users', 'Immediate Supervisor')
    project_manager_id = fields.Many2one('res.users', 'Project Manager')
    hr_manager_id = fields.Many2one('res.users', 'HR Manager')


    def action_confirm(self):
        appraisal_date = fields.Date.today()
        next_apraisal_date = datetime.strptime(appraisal_date, '%Y-%m-%d').date() + timedelta(days=365)
        self.write({'state': 'confirm',
                    'next_appraisal': next_apraisal_date,
                    'appraisal_date': fields.Date.today()})

    def action_cancel(self):
        self.write({'state': 'cancel'})

    def reset_to_draft(self):
        self.write({'state': 'draft'})

    @api.onchange('employee_id')
    def onchange_emp(self):
        if self.employee_id:
            self.job_id = self.employee_id.job_id.id
            self.department_id = self.employee_id.department_id.id
            self.staff_id = self.employee_id.barcode

    def print_days_report(self):
        return self.env.ref('nuro_appraisal.days_employee_report_id').report_action(self)

    def print_year_report(self):
        return self.env.ref('nuro_appraisal.year_employee_report_id').report_action(self)









