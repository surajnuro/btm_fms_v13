from odoo import fields,models,api, _
from datetime import date

class SendApproval(models.TransientModel):
    _name = 'send.approval'

    line_manager_id = fields.Many2one('res.users', 'Line Manager')
    supervisor_id = fields.Many2one('res.users', 'Immediate Supervisor')
    project_manager_id = fields.Many2one('res.users', 'Project Manager')
    hr_manager_id = fields.Many2one('res.users', 'HR Manager')
    appraisal_id = fields.Many2one('emp.appraisal')
    boolean = fields.Boolean()

    @api.model
    def default_get(self, fields):
        res = super(SendApproval, self).default_get(fields)
        appraisal = self.env['emp.appraisal'].search([('id', 'in', self._context.get('active_ids'))])
        if appraisal:
            if appraisal.type == 'day':
                res['appraisal_id'] = appraisal.id
                res['boolean'] = True
            if appraisal.type == 'year':
                res['appraisal_id'] = appraisal.id
                res['boolean'] = True
        return res

    @api.onchange('appraisal_id')
    def onchange_appraisal(self):
        hr_manager_groups = self.env.ref('hr.group_hr_manager')
        hr_user_lst = []
        for user in hr_manager_groups.sudo().users:
            hr_user_lst.append(user.id)
        return {'domain': {'hr_manager_id': [('id', 'in', hr_user_lst)]}}

    @api.onchange('boolean')
    def onchange_appraisal_year(self):
        project_manager_groups = self.env.ref('project.group_project_manager')
        project_user_lst = []
        for user in project_manager_groups.sudo().users:
            project_user_lst.append(user.id)
        return {'domain': {'project_manager_id': [('id', 'in', project_user_lst)]}}

    @api.onchange('boolean')
    def onchange_line_manager(self):
        line_manager_groups = self.env.ref('nuro_appraisal.group_line_manager')
        line_user_lst = []
        for user in line_manager_groups.sudo().users:
            line_user_lst.append(user.id)
        return {'domain': {'line_manager_id': [('id', 'in', line_user_lst)]}}

    @api.onchange('boolean')
    def onchange_supervisor(self):
        supervisor_manager_groups = self.env.ref('nuro_appraisal.group_immediate_supervisor')
        super_user_lst = []
        for user in supervisor_manager_groups.sudo().users:
            super_user_lst.append(user.id)
        return {'domain': {'supervisor_id': [('id', 'in', super_user_lst)]}}

    def send_for_approval(self):
        appraisal = self.env['emp.appraisal'].search([('id', 'in', self._context.get('active_ids'))])
        if appraisal:
            if appraisal.type == 'day':
                appraisal.write({'line_manager_id': self.line_manager_id.id,
                                  'supervisor_id': self.supervisor_id.id,
                                  'project_manager_id': self.project_manager_id.id,
                                  'hr_manager_id': self.hr_manager_id.id,
                                  'request_date': date.today(),
                                  'state': 'waiting'})
            if appraisal.type == 'year':
                appraisal.write({'line_manager_id': self.line_manager_id.id,
                                  'supervisor_id': self.supervisor_id.id,
                                  'project_manager_id': self.project_manager_id.id,
                                  'hr_manager_id': self.hr_manager_id.id,
                                  'request_date': date.today(),
                                  'state': 'waiting'})
