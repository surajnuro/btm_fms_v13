from odoo  import fields,models,api,_
import datetime
from datetime import date
from odoo.exceptions import ValidationError

class Approval(models.TransientModel):
    _name = 'send.approval.resign'

    user_id = fields.Many2one('res.users', 'User')
    resign_id = fields.Many2one('hr.resignation')
    boolean = fields.Boolean()

    @api.model
    def default_get(self, fields):
        res = super(Approval, self).default_get(fields)
        resign = self.env['hr.resignation'].search([('id', '=', self._context.get('active_ids'))])
        if resign:
            res['resign_id'] = resign.id

        return res

    @api.onchange('resign_id')
    def onchange_boolean(self):
        line_manager_groups = self.env.ref('hr_resignation.group_resign')
        user_lst = []
        for user in line_manager_groups.sudo().users:
            user_lst.append(user.id)
        return {'domain': {'user_id': [('id', 'in', user_lst)]}}

    def send_for_approval(self):
        resign = self.env['hr.resignation'].search([('id', '=', self._context.get('active_ids'))])
        if resign:
            for rec in resign:
                if rec.create_uid.id == self.user_id.id:
                    raise ValidationError(_('Sorry You can not approve own resignation  !!'))
                rec.state = 'waiting'
                rec.approval_user_id = self.user_id.id
                rec.resign_confirm_date = date.today()