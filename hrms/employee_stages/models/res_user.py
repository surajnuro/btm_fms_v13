from odoo import api,models,fields
from odoo.exceptions import ValidationError

class ResUser(models.Model):
    _inherit = 'res.users'

    categories_ids = fields.Many2many('hr.employee.category', 'emp_category_reference', 'empl_id', 'category_id',
                                      'Categories')
    allowed_category = fields.Boolean()


    def write(self, vals):
        self.clear_caches()
        res =  super(ResUser, self).write(vals)
        allow_category_group = self.has_group('employee_stages.group_allow_emplouyee_category')
        emp_officer = self.has_group('hr.group_hr_user')
        emp_manager = self.has_group('hr.group_hr_manager')
        if allow_category_group == True:
            if not self.categories_ids:
                raise ValidationError(('Please add Employee Category!!!'))
        else:
            if self.categories_ids:
                raise ValidationError(('Please Remove Employee Category if user have not Allow category group!!!'))
        if emp_manager == False and emp_officer == True:
            if not self.categories_ids:
                raise ValidationError(('Please add Employee Category!!!'))
        return res


class ResPartner(models.Model):
    _inherit = 'res.partner'

    def write(self, vals):
        self.clear_caches()
        return super(ResPartner, self).write(vals)

class EmpCategory(models.Model):
    _inherit = 'hr.employee.category'

    def write(self, vals):
        self.clear_caches()
        return super(EmpCategory, self).write(vals)
