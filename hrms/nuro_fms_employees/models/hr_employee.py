from odoo import api,models,fields,_
# from lxml import etree
# from odoo.osv.orm import setup_modifiers
from odoo.exceptions import ValidationError

class HrEmp(models.Model):
    _inherit = 'hr.employee'

    is_invisible = fields.Boolean(compute='_find_user_emp_group')
    type_selection = fields.Selection([('national', 'National'), ('international', 'International')], string='Employee Type')

    @api.constrains('category_ids')
    def _check_employee_category(self):
        if self.category_ids:
            if len(self.category_ids) > 1:
                raise ValidationError('Please add only one Employee Category!!!')

    @api.model
    def create(self, vals):
        res = super(HrEmp, self).create(vals)
        if res.category_ids:
            if len(res.category_ids) > 1:
                raise ValidationError('Please add only one Employee Category!!!')
        return res

    def _find_user_emp_group(self):
        '''cumputed field for check if login employee can see own emp data or not and
            employee have not manager access then can see only public data of another employee'''
        self.ensure_one()
        current_user = self.env.user.id # find current user
        for rec in self:
            # find login user access
            payroll_user = rec.env.user.has_group('hr_payroll.group_hr_payroll_user')
            payroll_manager = rec.env.user.has_group('hr_payroll.group_hr_payroll_manager')
            hr_manager = rec.env.user.has_group('hr.group_hr_manager')
            #-----------------------------------------------------------------------------
            current_emp = rec.search([('user_id', '=', current_user)], limit=1)
            if payroll_manager == True or payroll_user == True:
                rec.is_invisible = True
                return
            if hr_manager == False:
                if current_emp.id == rec.id: # login emp can see own all emp data
                    rec.is_invisible = True
                else:
                    rec.is_invisible = False
            else:
                rec.is_invisible = True