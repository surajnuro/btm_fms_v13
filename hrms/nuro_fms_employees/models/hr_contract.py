from odoo import api,models,fields,_
from odoo.exceptions import ValidationError

class Contract(models.Model):
    _inherit = 'hr.contract'

    @api.model
    def create(self, vals):
        res = super(Contract, self).create(vals)
        contracts = self.search([('state', '=', 'open'), ('employee_id', '=', res.employee_id.id)])
        if contracts:
            if len(contracts) > 1:
                raise  ValidationError(('%s Contract Already Runing')% res.employee_id.name)
        return res

    def write(self, vals):
        res = super(Contract, self).write(vals)
        contracts = self.search([('state', '=', 'open'), ('employee_id', '=', self.employee_id.id)])
        if contracts:
            if len(contracts) > 1:
                raise  ValidationError(('%s Contract Already Runing')% self.employee_id.name)
        return res