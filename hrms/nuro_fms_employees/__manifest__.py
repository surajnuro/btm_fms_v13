# -*- coding: utf-8 -*-
{
    'name': "FMS Employee Changes",
    'summary': """
        Employee Segregation
        """,
    'description': """
        Does the Employee Segregation as per national
    """,
    'author': "NuroSolution Pvt Ltd",
    'website': "https://www.nurosolution.com",
    'category': 'Employee',
    'version': '13.0.1',
    'depends': ['hr','hr_recruitment','hr_attendance','hr_payroll','hr_timesheet', 'hr_contract'],
    'data': [
        'security/security_view.xml',
        'data/employee_tag.xml',
        'views/fms_employee.xml',
        # 'views/hr_contract.xml',
    ],
    'demo': [
        'demo/demo.xml',
    ],
}