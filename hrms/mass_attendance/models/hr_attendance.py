from odoo import models, fields, api, _
from datetime import datetime

class HrAttendance(models.Model):
    _inherit = 'hr.attendance'

    check_date = fields.Date(compute='_Checkin_date', store=True)

    @api.depends('check_in')
    def _Checkin_date(self):
        for rec in self:
            if rec.check_in:
                rec.check_date = rec.check_in.date()
            else:
                rec.check_date = False
