# -*- coding: utf-8 -*-
# See LICENSE file for full copyright and licensing details.

from odoo import models, fields, api, _
from datetime import datetime, timedelta, date
import time
import calendar
from odoo.exceptions import ValidationError
import math

class DailyAttendance(models.Model):
    _name = 'daily.attendance'
    _description = 'Daily Attendance'
    _order = 'id desc'
    _rec_name = 'date'

    @api.depends('employee_ids')
    def _compute_total(self):
        for rec in self:
            rec.total_employee = len(rec.employee_ids) or 0

    @api.depends('employee_ids')
    def _compute_present(self):
        for rec in self:
            count = 0
            if rec.employee_ids:
                for att in rec.employee_ids:
                    if att.is_present:
                        count += 1
                rec.total_presence = count
            else:
                rec.total_presence = count

    @api.depends('employee_ids')
    def _compute_absent(self):
        for rec in self:
            count_fail = 0
            if rec.employee_ids:
                for att in rec.employee_ids:
                    if att.is_absent:
                        count_fail += 1
                rec.total_absent = count_fail
            else:
                rec.total_absent = count_fail

    @api.depends('employee_ids')
    def _compute_leave(self):
        for rec in self:
            count_fail = 0
            if rec.employee_ids:
                for att in rec.employee_ids:
                    if att.is_leave:
                        count_fail += 1
                rec.total_leave = count_fail
            else:
                rec.total_leave = count_fail

    user_id = fields.Many2one('res.users', 'Created By', default=lambda self: self.env.user.id)
    type_selection = fields.Selection([('national', 'National'), ('international', 'International')],default='national', string='Employee Type')
    department_id = fields.Many2one('hr.department', 'Department')
    date = fields.Date("Date", help="Current Date", default=lambda *a: time.strftime('%Y-%m-%d'))
    state = fields.Selection([('draft', 'Draft'), ('validate', 'Validate')], 'Status', readonly=True, default='draft')
    employee_ids = fields.One2many('daily.attendance.line', 'emp_id_mor','Employee', ondelete="cascade",)
    total_employee = fields.Integer(compute="_compute_total", store=True, help="Total Employees", string='Total Employees')
    total_presence = fields.Integer(compute="_compute_present", store=True, string='Present Employees', help="Present Employees")
    total_absent = fields.Integer(compute="_compute_absent", store=True, string='Absent Employees', help="Absent Employees")
    total_leave = fields.Integer(compute="_compute_leave", store=True, string='Total Leaves',)
    date_time_from = fields.Date(string='Check In', default=fields.Date.context_today)
    date_time_to = fields.Date(string='Check Out',)
    present_all = fields.Boolean()
    absent_all = fields.Boolean()
    current_datetime = fields.Datetime(compute='_compute_datetime')
    time_check = fields.Selection([('morning', 'Morning Attendance'), ('afternon', 'Afternoon Attendance')],
                                  string='Attendance Time')
    type = fields.Selection([('mass', 'Mass'),
                             ('individual', 'Individual')])
    start_date = fields.Date(compute='_compute_date', store=True)

    @api.depends('date_time_from')
    def _compute_date(self):
        for rec in self:
            date = rec.date_time_from
            rec.start_date = date

    @api.constrains('date_time_from')
    def check_date_cons(self):
        if self.type == 'mass':
            attendances = self.search([('date_time_from', '=', self.date_time_from),
                                   ('type', '=', 'mass'),
                                    ('department_id', '=', self.department_id.id),
                                   ('id', '!=', self.id)])
            if attendances:
                raise ValidationError(_('Already Attendance has done from this date!!'))
        if self.type == 'individual':
            attendances = self.search([('date_time_from', '=', self.date_time_from),
                                       ('type', '=', 'individual'),
                                       ('id', '!=', self.id)])
            if attendances:
                raise ValidationError(_('Already Attendance has done from this date!!'))

    @api.depends('date')
    def _compute_datetime(self):
        self.current_datetime = datetime.now()

    def unlink(self):
        for rec in self:
            if rec.state == 'validate':
                raise ValidationError(_('Sorry you can not delete Validate Record!!'))
        res = super(DailyAttendance, self).unlink()
        return res


    @api.onchange('department_id', 'date_time_from','type_selection')
    def onchange_department(self):
        emp_lst = []
        if self.department_id and self.type_selection:
            emps = self.env['hr.employee'].search([('department_id', '=', self.department_id.id), ('type_selection', '=', self.type_selection)])
        if self.type_selection:
            emps = self.env['hr.employee'].sudo().search([('type_selection', '=', self.type_selection)])
            status = ''
            leave_id = False
            if emps:
                if self.employee_ids:
                    self.update({'employee_ids': False})
                check_in_from = self.date_time_from
                check_out_from = self.date_time_to
                for emp in emps:
                    leave = self.env['hr.leave'].search([('employee_id', '=', emp.id),
                                                            ('date_from', '<=', check_in_from),
                                                            ('date_to', '>=', check_in_from),
                                                            ('state', '=', 'validate')], limit=1)
                    if leave:
                        if leave.holiday_status_id.name == 'Unpaid':
                            status = 'unpaid_leave'
                        else:
                            status = 'paid_leave'
                        leave_id = leave.id
                        emp_lst.append((0, 0, {'employe_id': emp.id,
                                               'project_id': False,
                                               'check_in': check_in_from,
                                               'check_out': check_out_from,
                                               'leave_type': status,
                                               'leave_id': leave_id,
                                               'leave_status_id': leave.holiday_status_id.id,
                                               'is_present': False,
                                               'is_absent': False,
                                               'is_leave': True}))
                    else:
                        emp_lst.append((0, 0, {'employe_id': emp.id,
                                               'project_id': False,
                                               'check_in': check_in_from,
                                               'check_out': check_out_from,
                                               'is_present': True}))
            else:
                self.update({'employee_ids': False})
                # self.employee_ids = emp_lst
        print(emp_lst)
        self.employee_ids = emp_lst
        # self.update({'employee_ids': False})
        return {
            'type': 'ir.actions.client',
            'tag': 'reload',
        }

    @api.onchange('type_selection')
    def onchange_type_selection(self):
        if self.env.context.get('default_type') == 'individual':
            self.update({'employee_ids': False})

    def attendance_validate(self):
        for emp in self.employee_ids:
            attd = self.env['hr.attendance']
            attendance = self.env['hr.attendance'].search([('employee_id', '=', emp.employe_id.id),
                                                           ('check_date', '=', emp.check_out)
                                                           ])
            if attendance:
                raise ValidationError(_('Already Attendance done for %s You need to remove from line' % attendance.employee_id.name))
            if emp.is_present == True:
                attd_crete_id = attd.create({'employee_id': emp.employe_id.id,
                                               'check_in': emp.check_in,
                                               'check_out': emp.check_in,
                                            })
                self.write({
                    'state': 'validate',
                })
            else:
                self.write({
                    'state': 'validate',
                })
        return True


class DailyAttendanceLine(models.Model):
    _description = 'Daily Attendance Line'
    _name = 'daily.attendance.line'

    @api.onchange('employe_id')
    def onchange_empl_id(self):
        emp_lst = []
        if self.employe_id:
            status = ''
            leave_id = False
            check_in_from = self.emp_id_mor.date_time_from
            check_out_from = self.emp_id_mor.date_time_to
            leave = self.env['hr.leave'].search([('employee_id', '=', self.employe_id.id),
                                                    ('date_from', '<=', check_in_from),
                                                    ('date_to', '>=', check_in_from),
                                                    ('state', '=', 'validate')], limit=1)
            if leave:
                if leave.holiday_status_id.name == 'Unpaid':
                    status = 'unpaid_leave'
                else:
                    status = 'paid_leave'
                leave_id = leave.id
                self.leave_type = status
                self.leave_id = leave_id
                self.leave_status_id = leave.holiday_status_id.id
                self.is_present = False
                self.is_absent = False
                self.is_leave = True
            else:
                self.is_present = True

    type_selection = fields.Selection(related='emp_id_mor.type_selection', store=True)
    emp_id_mor = fields.Many2one('daily.attendance', 'Employee', ondelete="cascade")
    emp_id_eve = fields.Many2one('daily.attendance', 'Employee', ondelete="cascade")
    is_present = fields.Boolean('Present', help="Check if EMP is present")
    is_absent = fields.Boolean('Absent', help="Check if EMP is absent")
    is_leave = fields.Boolean('Leave', help="Check if Emp is Leave")
    employe_id = fields.Many2one('hr.employee', 'Employee Name')
    project_id = fields.Many2one('project.project',string='Project')
    allocation = fields.Char('Allocation')
    check_in = fields.Date(related='emp_id_mor.date_time_from',string='Check In', readonly=True)
    check_out = fields.Date(related='emp_id_mor.date_time_to',string='Check Out', readonly=True)
    leave_type = fields.Selection([('unpaid_leave', 'Unpaid Leave'),
                                   ('paid_leave', 'Paid Leave')], string='Leave Status')
    remarks = fields.Char('Remarks')
    leave_id = fields.Many2one('hr.leave','Leave')
    leave_status_id = fields.Many2one('hr.leave.type', 'Leave Type')
    state = fields.Selection(related='emp_id_mor.state', store=True)
    work_type = fields.Selection([
        ('usw', 'Unsechdule Work'),
        ('routine', 'Routine Maintenance'),
        ('repair', 'Repair'),
    ], string='Work Type')
    sale_order = fields.Many2one('sale.order', 'TOP/WO NO.')
    project_name = fields.Char(related='project_id.name', store=True)
    timesheet_cost = fields.Float('TimeSheet Cost', compute='_compute_salary', store=True)
    salary = fields.Monetary('Monthly Salary', digits=(16, 2), help="Employee's monthly gross wage.", compute='_compute_salary', store=True)
    company_id = fields.Many2one('res.company', default=lambda self: self.env.user.company_id)
    currency_id = fields.Many2one(string="Currency", related='company_id.currency_id', readonly=True)

    @api.depends('employe_id')
    def _compute_salary(self):
        for rec in self:
            contract = self.env['hr.contract'].search([('employee_id', '=', rec.employe_id.id),
                                                       ('state', '=', 'open')], limit=1)
            if contract:
                rec.salary = contract.wage
                if rec.check_in:
                    date = rec.check_in
                    number_of_days = calendar.monthrange(date.year, date.month)[1]
                    if contract.wage > 0.0:
                        rec.timesheet_cost = contract.wage / number_of_days
                        rec.salary = contract.wage
            else:
                rec.timesheet_cost = 0.0
                rec.salary = 0.0


    @api.onchange('is_present')
    def onchange_attendance(self):
        '''Method to make absent false when student is present'''
        if self.is_present:
            self.is_absent = False
            self.is_leave = False

    @api.onchange('is_absent')
    def onchange_absent(self):
        '''Method to make present false when student is absent'''
        if self.is_absent:
            self.is_present = False
            self.is_leave = False

    @api.onchange('is_leave')
    def onchange_leave(self):
        '''Method to make present false when student is absent'''
        if self.is_leave:
            self.is_present = False
            self.is_absent = False

    @api.constrains('is_present', 'is_absent', 'is_leave')
    def check_present_absent(self):
        if not self.is_present and not self.is_absent and not self.is_leave:
            raise ValidationError(_('Check Present or Absent for all Employees!!'))


class HRHolidays(models.Model):
    _description = 'HR Holidays'
    _inherit = 'hr.leave'

    holiday_status_id = fields.Many2one("hr.leave.type", string="Leave Type", required=True, readonly=True,
                                        states={'draft': [('readonly', False)], 'confirm': [('readonly', False)]},
                                        default=lambda self: self.env['hr.leave.type'].search([('name', '=', 'Unpaid')]))
    approval_id = fields.Many2one('res.users')
    emp_id = fields.Char('Employee ID', related='employee_id.barcode', store=True)
    date_from = fields.Datetime('Start Date', readonly=True, index=True, copy=False,default= lambda *a: time.strftime('%Y-%m-%d 01:30:00'),
                                states={'draft': [('readonly', False)], 'confirm': [('readonly', False)]},
                                track_visibility='onchange')
    date_to = fields.Datetime('End Date', readonly=True, copy=False, default= lambda *a: time.strftime('%Y-%m-%d 13:30:00'),
                              states={'draft': [('readonly', False)], 'confirm': [('readonly', False)]},
                              track_visibility='onchange')
    state = fields.Selection([
        ('draft', 'To Submit'),
        ('waiting', 'Waiting'),
        ('cancel', 'Cancelled'),
        ('confirm', 'To Approve'),
        ('refuse', 'Refused'),
        ('validate1', 'Second Approval'),
        ('validate', 'Approved')
    ], string='Status', readonly=True, track_visibility='onchange', copy=False, default='draft',
        help="The status is set to 'To Submit', when a leave request is created." +
             "\nThe status is 'To Approve', when leave request is confirmed by user." +
             "\nThe status is 'Refused', when leave request is refused by manager." +
             "\nThe status is 'Approved', when leave request is approved by manager.")

class HolidayStatus(models.Model):
    _inherit = 'hr.leave.type'

    short_code = fields.Char('Short Code')
