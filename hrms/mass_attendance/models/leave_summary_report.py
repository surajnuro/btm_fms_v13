from odoo import fields,models,api,_

class LeaveReport(models.Model):
    _name = 'leave.summary.report'

    employee_id = fields.Many2one('hr.employee', 'Employee')
    department_id = fields.Many2one('hr.department', 'Department')
    staff_id = fields.Char('Employee ID')
    leave_type_id = fields.Many2one('hr.leave.type', 'Leave Type')
    allowed_leave = fields.Float('Allowed Leaves')
    remaining_leave = fields.Float('Remaining Leaves')
    taken_leave = fields.Float('Taken Leaves')

    def leave_summary_data(self):
        record_ids = self.search([])
        if record_ids:
            record_ids.unlink()
        leave_type = self.env['hr.leave.type'].search([])
        emp_lst = []
        leave_allocation = self.env['hr.leave'].search([('type', '=', 'add')])
        if leave_allocation:
            for la in leave_allocation:
                if la.employee_id not in emp_lst:
                    emp_lst.append(la.employee_id)
        if emp_lst:
            for lt in leave_type:
                for emp in emp_lst:
                    days = 0.0
                    allow_days = 0.0
                    add_holidays = self.env['hr.leave'].search([('holiday_status_id', '=', lt.id),
                                                               ('employee_id', '=', emp.id),
                                                               ('type', '=', 'add')])
                    if add_holidays:
                        for holiday in add_holidays:
                            allow_days = allow_days + holiday.number_of_days
                    remove_holidays = self.env['hr.leave'].search([('holiday_status_id', '=', lt.id),
                                                                   ('employee_id', '=', emp.id),
                                                                   ('type', '=', 'remove')])
                    if remove_holidays:
                        for rm in remove_holidays:
                            days = days + rm.number_of_days
                    self.create({'employee_id': emp.id,
                                 'department_id': emp.department_id.id,
                                 'staff_id': emp.barcode,
                                 'leave_type_id': lt.id,
                                 'allowed_leave': allow_days,
                                 'taken_leave': days,
                                 'remaining_leave': allow_days - (-days)})
        tree_view_id = self.env.ref('mass_attendance.leave_summary_tree').id
        value = {
            'name': _('Leave Summary Report'),
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'leave.summary.report',
            'view_id': False,
            'create': False,
            'views': [[tree_view_id, 'list']],
            'search_default_leave_type_id': 1,
            'type': 'ir.actions.act_window',
        }
        return value



