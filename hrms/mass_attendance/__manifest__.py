# -*- coding: utf-8 -*-
{
    'name': "Mass Attendance",
    'summary': """
        Mass Attendance for employees""",
    'description': """
        Takes Mass Attendance for all Employess listed in HR Module. 
        Once validated, present employess will be listed in Attendance menu and Absent 
        Employees will be listed in HR Leaves.
    """,
    'author': "Supreeth",
    'website': "http://www.autochip.in",
    'category': 'Attendance',
    'version': '13.0.1.0.0',
    'depends': ['hr', 'hr_attendance', 'hr_holidays', 'website_support', 'nuro_appraisal','account'],
    'data': [
        'security/ir.model.access.csv',
        'views/mass_attendance_view.xml',
        'wizard/send_approval_view.xml',
        'views/hr_holiday_view.xml',
        'views/leave_summary_report.xml',
        'wizard/attendance_report_view.xml',
    ],
    'demo': [
        'demo/demo.xml',
    ],
}