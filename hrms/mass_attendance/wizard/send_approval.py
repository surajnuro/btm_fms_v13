from odoo import api,models,fields,_

class SendApproval(models.TransientModel):
    _name = 'approval.leave.req'

    user_id = fields.Many2one('res.users', 'User')
    holiday_id = fields.Many2one('hr.leave')

    @api.model
    def default_get(self, fields):
        res = super(SendApproval, self).default_get(fields)
        holiday = self.env['hr.leave'].search([('id', '=', self._context.get('active_ids'))])
        if holiday:
            res['holiday_id'] = holiday.id
            if holiday.employee_id.line_manager_id:
                res['user_id'] = holiday.employee_id.line_manager_id.id
        return res

    @api.onchange('holiday_id')
    def onchange_holidays(self):
        if not self.holiday_id.employee_id.line_manager_id:
            line_manager_groups = self.env.ref('nuro_appraisal.group_line_manager')
            user_lst = []
            for user in line_manager_groups.sudo().users:
                user_lst.append(user.id)
            return {'domain': {'user_id': [('id', 'in', user_lst)]}}

    def send_for_approval(self):
        holiday = self.env['hr.leave'].search([('id', '=', self._context.get('active_ids'))])
        if holiday:
            holiday.write({'state': 'confirm',
                           'approval_id': self.user_id.id})
