from odoo import models, fields, api, _
import base64
import xlsxwriter
from datetime import datetime, timedelta
from dateutil.rrule import rrule, DAILY
import calendar
from odoo.exceptions import ValidationError

class ExcelDownload(models.TransientModel):
    _name = 'attendance.wizard'

    file_name = fields.Binary('File Name')
    name = fields.Char('Data')
    date_start = fields.Date('Start Date')
    date_end = fields.Date('End Date')
    department_id = fields.Many2one('hr.department', string='Department')

    @api.onchange('date_start', 'date_end')
    def on_change_date_start(self):
        current_date = datetime.today().date()
        if self.date_start:
            st_date = datetime.strptime(self.date_start, '%Y-%m-%d').date()
            if st_date >= current_date:
                raise ValidationError('Start date can not be more than current date')
        if self.date_end:
            st_end = datetime.strptime(self.date_end, '%Y-%m-%d').date()
            if st_end < st_date or st_end > current_date:
                raise ValidationError('End date can not be less than start date and greater than current date')

    def remove_html_tags(self, text):
        """Remove html tags from a string"""
        import re
        clean = re.compile('<.*?>')
        return re.sub(clean, '', text)

    # Function for printing excel report
    def print_ticket_report_excel(self):
        f_name = '/tmp/mass_attendance_report.xlsx'
        workbook = xlsxwriter.Workbook(f_name)
        worksheet = workbook.add_worksheet('Report')
        worksheet.set_column('A:A', 22)
        style = workbook.add_format({
            'bold': 1,
            'border': 1,
            'align': 'center',
            'valign': 'vcenter'})
        style.set_font_size(8)
        align_value = workbook.add_format({
            'align': 'center',
            'valign': 'vcenter'})
        align_value.set_font_size(8)


        start = datetime.strptime(self.date_start, '%Y-%m-%d')
        end = datetime.strptime(self.date_end, '%Y-%m-%d')

        row_r, col = 2, 2
        worksheet.merge_range('A1:M1', "DEEQA CONSTRUCTION & WATER WELL DRILLING CO. LTD.", style)
        worksheet.merge_range('A2:M2', "EMPLOYEE ATTENDANCE REPORT FROM "+self.date_start+" TO "+self.date_end, style)
        worksheet.write('A3', "Name:", style)
        worksheet.write('B3', "#ID:", style)

        for month_date in rrule(DAILY, dtstart=start, until=end):
            month_day = calendar.day_name[month_date.weekday()]

            date_month = month_date.strftime('%d')
            worksheet.write(row_r, col, date_month, align_value)
            worksheet.write(row_r + 1, col, month_day[:3], align_value)
            col += 1
            worksheet.write(row_r + 1, col, 'Days Absent(unpaid)', align_value)
            worksheet.write(row_r + 1, col+1, 'No of days worked', align_value)

        e_row = 5
        e_col = 0

        domain = ([('active', '=', True)])
        # hr_attendance = self.env['hr.employee'].sudo().search([('active', '=', True)])
        if self.department_id:
            domain.append(('department_id', '=', self.department_id.id))
        hr_attendance = self.env['hr.employee'].sudo().search(domain)
        if hr_attendance:
            for attendance in hr_attendance:
                worksheet.write(e_row, e_col, attendance.name, align_value)
                worksheet.write(e_row, e_col + 1, attendance.barcode, align_value)
                e_dt_col = 2
                total_present = 0
                total_absent = 0
                for month_date in rrule(DAILY, dtstart=start, until=end):
                    hr_attendance_emp = self.env['hr.attendance'].sudo().search([
                        ('employee_id', '=', attendance.id),
                        ('check_in', '>=', str(month_date)),
                        ('check_out', '<=', str(month_date.strftime('%Y-%m-%d 23:59:59'))),
                        ])
                    if hr_attendance_emp:
                        if len(hr_attendance_emp) >= 2:
                            worksheet.write(e_row, e_dt_col, 'P', align_value)
                            total_present += 1
                        else:
                            worksheet.write(e_row, e_dt_col, 'A', align_value)
                            total_absent += 1
                    else:
                        worksheet.write(e_row, e_dt_col, 'A', align_value)
                        total_absent += 1
                    e_dt_col += 1
                    worksheet.write(e_row, e_dt_col, total_absent, align_value)
                    worksheet.write(e_row, e_dt_col+1, total_present, align_value)
                e_row += 1

        workbook.close()
        f = open(f_name, 'rb')
        data = f.read()
        f.close()
        name = 'Attendance Report'

        out_wizard = self.env['attendance.wizard'].create({'name': name + '.xlsx', 'file_name': base64.b64encode(data)})
        view_id = self.env.ref('mass_attendance.gts_attendance_report_wizard_sheet_xls').id
        return {
            'type': 'ir.actions.act_window',
            'res_model': 'attendance.wizard',
            'target': 'new',
            'view_mode': 'form',
            'res_id': out_wizard.id,
            'views': [[view_id, 'form']],
        }