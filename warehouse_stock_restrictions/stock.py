# -*- coding: utf-8 -*-

from odoo import models, fields, api, _
from odoo.exceptions import Warning

class ResUsers(models.Model):
    _inherit = 'res.users'

    restrict_locations = fields.Boolean('Restrict Location')

    stock_location_ids = fields.Many2many(
        'stock.location',
        'location_security_stock_location_users',
        'user_id',
        'location_id',
        'Stock Locations')

    default_picking_type_ids = fields.Many2many(
        'stock.picking.type', 'stock_picking_type_users_rel',
        'user_id', 'picking_type_id', string='Default Warehouse Operations')
    location_id = fields.Many2one('stock.location', string="Location To Order", domain="[('usage', '=', 'internal')]")
    warehouse_req_id = fields.Many2one('stock.picking.type', domain=[('code', '=', 'internal')])

class stock_move(models.Model):
    _inherit = 'stock.move'

    @api.constrains('state', 'location_id', 'location_dest_id')
    def check_user_location_rights(self):
        if self.state == 'draft':
            return True
        user_locations = self.env.user.stock_location_ids
        if self.env.user.restrict_locations:
            purchase_user = self.env.user.has_group('purchase.group_purchase_user')
            inventory_user = self.env.user.has_group('stock.group_stock_user')
            internal_transfer = self.env.user.has_group('internal_material_request.group_material_transfer_user')
            message = _(
                'Invalid Location. You cannot process this move since you do'
                'not control the location "%s". '
                'Please contact your Adminstrator.')
            if not purchase_user and not inventory_user and not internal_transfer:
                if self.location_id not in user_locations:
                    raise Warning(message % self.location_id.name)
                elif self.location_dest_id not in user_locations:
                    raise Warning(message % self.location_dest_id.name)

class ProductChangeQuantity(models.TransientModel):
    _inherit = "stock.change.product.qty"

    # location_id = fields.Many2one('stock.location', 'Location', required=True, domain="[('usage', '=', 'internal')]")
    user_id = fields.Many2one('res.users', default=lambda self: self.env.user, readonly=True)
    stock_location_ids = fields.Many2many(
        'stock.location',
        'location_security_stock_location_users_rel',
        'location_id',
        'user_id',
        'Stock Locations')

    @api.onchange('user_id')
    def onchange_location_ids(self):
        # for line in self
        self.stock_location_ids = self.user_id.stock_location_ids
        if self.stock_location_ids:
            self.location_id = self.stock_location_ids[0].id