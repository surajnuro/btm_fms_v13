# -*- coding: utf-8 -*-
# Copyright 2016 Eficent Business and IT Consulting Services S.L.
# License LGPL-3.0 or later (http://www.gnu.org/licenses/lgpl-3.0).

{
    "name": "Material Request",
    "author": "NuroSolution Pvt Ltd",
    "version": "13.0.1.1.0",
    "summary": "Use this module to have notification of requirements of "
               "materials and/or external services and keep track of such "
               "requirements.",
    "category": "Material Management",
    "depends": [
        'stock',
        'stock_account',
    ],
    "data": [
        "security/material_request.xml",
        "security/ir.model.access.csv",
        "data/material_request_sequence.xml",
        # "data/material_request_data.xml",
        "views/material_request_view.xml",
        "views/picking_operation_qweb.xml",
        # "views/product_category_view.xml",
    ],
    'demo': [],
    "license": 'Other proprietary',
    'installable': True
}
