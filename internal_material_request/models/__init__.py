# -*- coding: utf-8 -*-
# Copyright 2018 geo Technosoft Pvt Ltd.
# License LGPL-3.0 or later (http://www.gnu.org/licenses/lgpl-3.0).

from . import material_request
from . import res_partner