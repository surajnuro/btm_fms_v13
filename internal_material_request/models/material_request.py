# -*- coding: utf-8 -*-
# Copyright 2016 Eficent Business and IT Consulting Services S.L.
# License LGPL-3.0 or later (http://www.gnu.org/licenses/lgpl-3.0).

from odoo import api, fields, models, _
from odoo.exceptions import UserError, Warning, ValidationError
import odoo.addons.decimal_precision as dp
from odoo import SUPERUSER_ID

_STATES = [
    ('draft', 'Draft'),
    ('to_approve', 'To be approved'),
    ('approved', 'Approved'),
    ('rejected', 'Rejected'),
    ('done', 'Done')
]


class MaterialRequest(models.Model):

    _name = 'material.request'
    _description = 'Material Request'
    _inherit = ['mail.thread']
    _order = 'date_start desc'

    @api.model
    def _company_get(self):
        company_id = self.env['res.company']._company_default_get(self._name)
        return self.env['res.company'].browse(company_id.id)

    @api.model
    def _get_default_requested_by(self):
        return self.env['res.users'].browse(self.env.uid)

    # @api.model
    # def _get_default_name(self):
    #     return self.env['ir.sequence'].next_by_code('material.request')

    @api.depends('state')
    def _compute_is_editable(self):
        for rec in self:
            if rec.state in ('to_approve', 'approved', 'rejected','done'):
                rec.is_editable = False
            else:
                rec.is_editable = True



    name = fields.Char('Request Reference', size=32,
                       track_visibility='onchange')
    origin = fields.Char('Source Document', size=32)
    date_start = fields.Date('Creation date',
                             help="Date when the user initiated the "
                                  "request.",
                             default=fields.Date.context_today,
                             track_visibility='onchange')
    requested_by = fields.Many2one('res.users',
                                   'Requested by',
                                   required=True,
                                   track_visibility='onchange',
                                   default=_get_default_requested_by)
    assigned_to = fields.Many2one('res.users', 'Approver', required=1)
    description = fields.Text('Description', required=1)
    company_id = fields.Many2one('res.company', 'Company',
                                 required=True,
                                 default=_company_get,
                                 track_visibility='onchange')
    line_ids = fields.One2many('material.request.line', 'request_id',
                               'Products to Purchase',
                               readonly=False,
                               copy=True,
                               track_visibility='onchange')
    state = fields.Selection(selection=_STATES,
                             string='Status',
                             index=True,
                             track_visibility='onchange',
                             required=True,
                             copy=False,
                             default='draft')
    is_editable = fields.Boolean(string="Is editable",
                                 compute="_compute_is_editable",
                                 readonly=True)
    to_approve_allowed = fields.Boolean(
        compute='_compute_to_approve_allowed')
    warehouse_id = fields.Many2one('stock.warehouse',
                                      'Warehouse')
    picking_type_id = fields.Many2one('stock.picking.type',
                                      'Picking Type')
    consume_location_id = fields.Many2one('stock.location', domain=[('usage','=', 'internal')])
    sector_id = fields.Many2one('project.project')
    internal_transfer = fields.Boolean('Inter-Sector Transfer', default=True)
    stock_picking_number = fields.Char('Stock Picking Number', readonly=True)
    dest_location_id = fields.Many2one('stock.location', String='Dest Location', store=True, domain=[('usage','=', 'internal')])
    warehouse_req_id = fields.Many2one('stock.picking.type', compute="current_user", string="Source Warehouse", readonly=True,store=True)
    internal_consume = fields.Boolean('Consume Internally')
    picking_id = fields.Many2one('stock.picking', 'Picking')
    all_return = fields.Boolean()
    consume_dest_loc_id = fields.Many2one('stock.location', 'Destination Location')


    def button_cancel_internal(self):
        if self.state == 'done':
            if self.stock_picking_number:
                picking = self.env['stock.picking'].search([('name', '=', self.stock_picking_number)], limit=1)
                if picking:
                    if picking.state != 'done':
                        picking.action_cancel()
                        self.write({'state': 'rejected'})
                    else:
                        raise UserError(_('Picking has been done !!!'))


    def return_all_product(self):
        '''create picking for return material '''
        line_lst = []
        picking_dict = {}
        if self.all_return == True:
            raise UserError(_('All Quantity has been returned!!!'))
        if not self.line_ids:
            raise UserError(_('Please create Material Line!!!'))
        for rec in self.line_ids:
            if not rec.transfer_qty:
                raise UserError(_('Material line not confirm !!!'))
            if rec.qty_returned == rec.transfer_qty:
                continue
            if rec.return_qty > rec.transfer_qty:
                raise UserError(_('Sorry you can not take more return qty from transfer qty !!'))
            rec.returen_qty = rec.product_qty
            line_lst.append((0, 0, {
                'product_id': rec.product_id.id,
                'product_uom': rec.product_id.uom_id.id,
                'product_uom_qty': rec.product_qty,
                'name': 'Task Material Request'
            }))
        if self.internal_transfer == True:
            warehouse_details_id = self.sudo().warehouse_req_id.default_location_src_id.id
            picking_type_details_id = self.sudo().warehouse_req_id.id
            location_destination = self.dest_location_id.id
        else:
            warehouse_details_id = self.consume_location_id.id
            picking_type_details_id = self.picking_type_id.id
            location_destination = self.consume_dest_loc_id.id
        picking_dict = {
            'picking_type_id': picking_type_details_id,
            'location_id': location_destination,
            'location_dest_id': warehouse_details_id,
            'material_request_id': self.id,
            'client_ticket': self.name,
            'origin': self.name,
            'move_lines': line_lst,
            'analytic_account_id': self.sector_id.analytic_account_id and self.sector_id.analytic_account_id.id,
        }
        internal_transfer = self.env['stock.picking'].sudo().create(picking_dict)
        self.all_return = True
        for line in self.line_ids:
            line.qty_returned = line.product_qty

    def return_particial_product(self):
        '''create picking for return material '''
        line_lst = []
        picking_dict = {}
        if self.all_return == True:
            raise UserError(_('All Quantity has been returned!!!'))
        if not self.line_ids:
            raise UserError(_('Please create Material Line!!!'))
        for rec in self.line_ids:
            if not rec.transfer_qty:
                raise UserError(_('Material line not confirm !!!'))
            if rec.qty_returned == rec.transfer_qty:
                continue
            if rec.qty_returned == rec.transfer_qty:
                if rec.return_qty:
                    raise UserError(_('Quantity has been returned !!!'))
            if not rec.return_qty:
                raise UserError(_('Please fill Quantity for return !!!'))

            if rec.return_qty > rec.product_qty:
                raise UserError(_('Sorry you can not take more return qty from transfer qty !!'))
            if rec.qty_returned > 0:
                if rec.return_qty > rec.remaining_qty:
                    raise UserError(_('Sorry you can not take more return qty from transfer qty !!'))
            rec.returen_qty = rec.product_qty
            line_lst.append((0, 0, {
                'product_id': rec.product_id.id,
                'product_uom': rec.product_id.uom_id.id,
                'product_uom_qty': rec.return_qty,
                'name': 'Task Material Request'
            }))
        if self.internal_transfer == True:
            warehouse_details_id = self.sudo().warehouse_req_id.default_location_src_id.id
            picking_type_details_id = self.sudo().warehouse_req_id.id
            location_destination = self.dest_location_id.id
        else:
            warehouse_details_id = self.consume_location_id.id
            picking_type_details_id = self.picking_type_id.id
            location_destination = self.consume_dest_loc_id.id
        picking_dict = {
            'picking_type_id': picking_type_details_id,
            'location_id': location_destination,
            'location_dest_id': warehouse_details_id,
            'material_request_id': self.id,
            'client_ticket': self.name,
            'origin': self.name,
            'move_lines': line_lst,
            'analytic_account_id': self.sector_id.analytic_account_id and self.sector_id.analytic_account_id.id,
        }
        internal_transfer = self.env['stock.picking'].sudo().create(picking_dict)
        for line in self.line_ids:
            line.qty_returned += line.return_qty

    # @api.depends('internal_transfer')
    # def current_user(self):
    #     login_user = self.env.user
    #     for rec in self:
    #         if rec.internal_transfer == True and login_user.id != SUPERUSER_ID:
    #             rec.dest_location_id = login_user.location_id.id
    #             rec.warehouse_req_id = login_user.warehouse_req_id.id

    @api.constrains('line_ids', 'assigned_to')
    def line_ids_validation(self):
        if not self.line_ids:
            raise UserError(_('Product Line can not be empty'))

    @api.onchange('assigned_to')
    def get_user(self):
        material_request_manager_group = self.env.ref('internal_material_request.group_material_request_manager')
        return {'domain': {'assigned_to': [('id', 'in', material_request_manager_group.users.ids)]}}

    # @api.onchange('sector_id', 'warehouse_id')
    # def _default_picking_type(self):
    #     for val in self:
    #         types = self.env['stock.picking.type'].search([('code', '=', 'internal'),('warehouse_id', '=', val.warehouse_id.id)], limit=1)
    #         if val.internal_consume == False:
    #             val.warehouse_id = self.sector_id.warehouse_id.id
    #             val.picking_type_id = types.id
    #         else:
    #             val.picking_type_id = types.id

    # @api.onchange('internal_consume', 'warehouse_id')
    # def default_consume_location(self):
    #     if self.internal_consume == True:
    #         if self.internal_consume == True and self.warehouse_id.is_consume_warehouse == True:
    #             self.consume_location_id = self.warehouse_id.consume_location_id.id or False
    #         else:
    #             self.consume_location_id = self.consume_location_id.id

    @api.onchange('internal_transfer')
    def onchange_internal_transfer(self):
        if self.internal_transfer == True:
            self.internal_consume == False
            self.internal_transfer == False
            self.picking_type_id == False
        if self.internal_transfer == False:
            self.update({'internal_consume': True})

    @api.onchange('internal_consume')
    def onchange_internal_consume(self):
        if self.internal_consume == True:
            self.internal_transfer == False
        if self.internal_consume == False:
            self.update({'internal_transfer': True})


    @api.onchange('consume_location_id')
    def onchange_dest_loc_id(self):
        # if self.internal_transfer == True:
        if self.consume_location_id:
            wh_id = self.env['stock.warehouse'].sudo().search([('lot_stock_id', '=', self.consume_location_id.id)], limit=1)
            if not wh_id:
                raise Warning(_('Please select warehouse internal transfer for source location'))
            self.warehouse_id = wh_id.id
            picking_type = self.env['stock.picking.type'].sudo().search([('warehouse_id', '=', wh_id.id),
                                                                  ('code', '=', 'internal')], limit=1)
            if picking_type:
                self.picking_type_id = picking_type.id

    @api.constrains('internal_transfer', 'internal_consume')
    def check_boolean(self):
        if not self.internal_consume and not self.internal_transfer:
            raise ValidationError(_('Check Internal Transfer or Consume Internally!!'))

    def create_material_request(self):
        rfq_env = self.env['stock.picking']
        lst = []
        for rec in self.line_ids:
            if not rec.product_qty:
                raise UserError("There is not  request qty for inventory !")

            lst.append((0, 0, {
                'product_id': rec.product_id.id,
                'product_uom': rec.product_id.uom_id.id,
                'product_uom_qty': rec.product_qty,
                'name': 'Task Material Request'
            }))

        if self.internal_transfer == True:
            if self.consume_location_id == self.dest_location_id:
                raise Warning(_('Source and Destination Location can not be same !!!'))
            warehouse_details_id = self.sudo().warehouse_req_id.default_location_src_id.id
            if self.dest_location_id:
                wh_id = self.env['stock.warehouse'].sudo().search([('lot_stock_id', '=', self.dest_location_id.id)], limit=1)
                if not wh_id:
                    raise Warning(_('Please select warehouse internal transfer for destination location'))
                # self.warehouse_id = wh_id.id
            if self.consume_location_id:
                source_wh_id = self.env['stock.warehouse'].sudo().search([('lot_stock_id', '=', self.consume_location_id.id)], limit=1)
                if not source_wh_id:
                    raise Warning(_('Please select warehouse internal transfer for source location'))
                self.warehouse_id = source_wh_id.id
                picking_type = self.env['stock.picking.type'].sudo().search([('warehouse_id', '=', source_wh_id.id),
                                                                             ('code', '=', 'internal')], limit=1)
                if picking_type:
                    self.picking_type_id = picking_type.id
            picking_type_details_id = self.picking_type_id.id
            location_destination = self.dest_location_id.id
            source_loc_id = self.consume_location_id.id
            warehouse_id = source_wh_id.id or False
        if self.internal_consume == True:
            if self.consume_dest_loc_id == self.consume_location_id:
                raise Warning(_('Source and Destination Location can not be same !!!'))
            source_loc_id = self.consume_location_id.id
            picking_type_details_id = self.picking_type_id.id
            location_destination = self.consume_dest_loc_id.id
            warehouse_id = self.warehouse_id.id or False
        picking_dict = {
            'picking_type_id': picking_type_details_id,
            'location_id': source_loc_id,
            'location_dest_id': location_destination,
            'warehouse_id': warehouse_id,
            'material_request_id': self.id,
            'client_ticket': self.name,
            'origin': self.name,
            'move_lines': lst,
            'analytic_account_id': self.sale_order_id.analytic_account_id.id,
        }
        internal_transfer = self.env['stock.picking'].sudo().create(picking_dict)
        self.state = 'done'
        self.stock_picking_number = internal_transfer.sudo().name
        self.picking_id = internal_transfer.id
        for line in self.line_ids:
            line.transfer_qty = line.product_qty
        return self.env.ref('internal_material_request.action_report_picking_operation_record').report_action(self)


    @api.depends(
        'state',
        'line_ids.product_qty',
        'line_ids.cancelled',
    )
    def _compute_to_approve_allowed(self):
        for rec in self:
            rec.to_approve_allowed = (
                rec.state == 'draft' and
                any([
                    not line.cancelled and line.product_qty
                    for line in rec.line_ids
                ])
            )

    def copy(self, default=None):
        default = dict(default or {})
        self.ensure_one()
        if self.internal_transfer:
            sequence = self.env['ir.sequence'].next_by_code('material.request.ins')
        else:
            sequence = self.env['ir.sequence'].next_by_code('material.request.mr')
        default.update({
            'state': 'draft',
            'name': sequence
        })
        return super(MaterialRequest, self).copy(default)

    # to create the sequence in name with ir.sequence.
    @api.model
    def create(self, vals):
        if vals.get('internal_transfer'):
            sequence = self.env['ir.sequence'].next_by_code('material.request.ins')
            vals['name'] = sequence
        else:
            sequence = self.env['ir.sequence'].next_by_code('material.request.mr')
            vals['name'] = sequence
        request = super(MaterialRequest, self).create(vals)
        # if vals.get('assigned_to'):
            # request.message_subscribe_users(user_ids=[request.assigned_to.id])
        return request

    def write(self, vals):
        res = super(MaterialRequest, self).write(vals)
        for request in self:
            if vals.get('assigned_to'):
                self.message_subscribe_users(user_ids=[request.assigned_to.id])
        return res

    def button_draft(self):
        return self.write({'state': 'draft'})

    def button_to_approve(self):
        if self.internal_transfer == True:
            if self.consume_location_id == self.dest_location_id:
                raise Warning(_('Source and Destination Location can not be same !!!'))
        if self.internal_consume == True:
            if self.consume_dest_loc_id == self.consume_location_id:
                raise Warning(_('Source and Destination Location can not be same !!!'))
        return self.write({'state': 'to_approve'})

    def button_approved(self):
        if self.env.uid != self.assigned_to.id:
            raise Warning(_('You you are not authorise to approve this material request'))
        return self.write({'state': 'approved'})

    def button_rejected(self):
        return self.write({'state': 'rejected'})

    def button_reset(self):
        return self.write({'state': 'draft'})

    def button_done(self):
        return self.write({'state': 'done'})

    def to_approve_allowed_check(self):
        for rec in self:
            if not rec.to_approve_allowed:
                raise UserError(
                    _("You can't request an approval for a material request "
                      "which is empty. (%s)") % rec.name)


class MaterialRequestLine(models.Model):

    _name = "material.request.line"
    _description = "Material Request Line"
    _inherit = ['mail.thread']

    @api.depends('product_id', 'name', 'product_uom_id', 'product_qty',
                 'analytic_account_id', 'date_required', 'specifications')
    def _compute_is_editable(self):
        for rec in self:
            if rec.request_id.state in ('to_approve', 'approved', 'rejected',
                                        'done'):
                rec.is_editable = False
            else:
                rec.is_editable = True

    state = fields.Selection(related='request_id.state', store=True)
    product_id = fields.Many2one(
        'product.product', 'Product',
        track_visibility='onchange')
    name = fields.Char('Description', size=256,
                       track_visibility='onchange')
    product_uom_id = fields.Many2one('uom.uom', 'Product Unit of Measure',
                                     track_visibility='onchange')
    product_qty = fields.Float('Quantity', track_visibility='onchange',
                               digits=dp.get_precision(
                                   'Product Unit of Measure'))
    request_id = fields.Many2one('material.request',
                                 'Material Request',
                                 ondelete='cascade', readonly=True)
    company_id = fields.Many2one('res.company',
                                 related='request_id.company_id',
                                 string='Company',
                                 store=True, readonly=True)
    analytic_account_id = fields.Many2one('account.analytic.account',
                                          'Analytic Account',
                                          track_visibility='onchange')
    requested_by = fields.Many2one('res.users',
                                   related='request_id.requested_by',
                                   string='Requested by',
                                   store=True, readonly=True)
    assigned_to = fields.Many2one('res.users',
                                  related='request_id.assigned_to',
                                  string='Assigned to', readonly=True)
    date_start = fields.Date(related='request_id.date_start',
                             string='Request Date', readonly=True,
                             store=True)
    description = fields.Text(related='request_id.description',
                              string='Description', readonly=True,
                              store=True)
    origin = fields.Char(related='request_id.origin',
                         size=32, string='Source Document', readonly=True,
                         store=True)
    date_required = fields.Date(string='Request Date', required=True,
                                track_visibility='onchange',
                                default=fields.Date.context_today)
    is_editable = fields.Boolean(string='Is editable',
                                 compute="_compute_is_editable",
                                 readonly=True)
    specifications = fields.Text(string='Specifications')
    request_state = fields.Selection(string='Request state',
                                     readonly=True,
                                     related='request_id.state',
                                     selection=_STATES,
                                     store=True)
    cancelled = fields.Boolean(
        string="Cancelled", readonly=True, default=False, copy=False)
    transfer_qty = fields.Float('Transfer Qty')
    return_qty = fields.Float('Return Qty')
    qty_returned = fields.Float('Returned Qty')
    done_qty = fields.Float('Done Qty')
    remaining_qty = fields.Float(compute='remaining_qty_compute', string='Remaining Qty', store=True)

    @api.depends('qty_returned')
    def remaining_qty_compute(self):
        for rec in self:
            rec.remaining_qty = rec.product_qty - rec.qty_returned



    @api.onchange('product_id')
    def onchange_product_id(self):
        if self.product_id:
            name = self.product_id.name
            if self.product_id.code:
                name = '[%s] %s' % (name, self.product_id.code)
            if self.product_id.description_purchase:
                name += '\n' + self.product_id.description_purchase
            self.product_uom_id = self.product_id.uom_id.id
            self.product_qty = 1
            self.name = name


class StockPicking(models.Model):
    _inherit = 'stock.picking'

    material_request_id = fields.Many2one('material.request')

# class ProductUoM(models.Model):
#     _inherit = 'product.uom'
#
#     @api.constrains('name')
#     def _check_name_agains(self):
#         if self.name:
#             self._cr.execute("select name from product_uom where name ilike %s", (self.name,), )
#             result = self._cr.fetchall()
#             if len(result) > 1:
#                 raise ValidationError(_('Unit Of Measure Cannot Be Duplicated'))