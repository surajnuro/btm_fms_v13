from odoo import api, fields, models, _


class Partner(models.Model):
    _inherit = 'res.partner'

    name = fields.Char(index=True, track_visibility='onchange')
    email = fields.Char(track_visibility='onchange')
    phone = fields.Char(track_visibility='onchange')
    mobile = fields.Char(track_visibility='onchange')



