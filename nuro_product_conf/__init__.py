from . import models
from odoo import api, SUPERUSER_ID


def pre_init_hook(cr):
    '''
    update default property value for product category
    :param cr: db cursur
    :return:
    '''
    cr.execute("update ir_property set value_text='real_time' where name='Valuation Property'")