# -*- encoding: utf-8 -*-
{
    'name': 'BTM Product Configuration',
    'version': '13.0',
    'author': 'Nuro Solution Pvt.Ltd',
    'website': 'nurosolution.com',
    'category': 'Generic Modules',
    'summary': 'Default product costing as average',
    'description': """
        This module set auto product type as stockable and it's costing as average.
    """,
    'depends': ['base', 'stock', 'stock_account'],
    'data': [],
    'auto_install': False,
    'installable': True,
    'pre_init_hook': 'pre_init_hook',

}
