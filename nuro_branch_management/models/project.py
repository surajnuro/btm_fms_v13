from odoo import api, fields, models, _

class Project(models.Model):
    _inherit = 'project.project'

    branch_ids = fields.Many2many('res.branch', 'project_branch_ref', 'project_id', 'branch_id', 'Branch')

    @api.onchange('company_id')
    def _get_default_branch(self):
        branch_lst = []
        if self.env.user.branch_ids:
            if len(self.env.user.branch_ids) == 1:
                self.branch_id = self.env.user.branch_ids[0]
            for branch in self.env.user.branch_ids:
                branch_lst.append(branch.id)
        else:
            for b in self.env['res.branch'].search([]):
                branch_lst.append(b.id)
        return {'domain': {'branch_id': [('id', 'in', branch_lst)]}}

class ProjectTask(models.Model):
    _inherit = 'project.task'

    branch_id = fields.Many2one('res.branch', 'Branch')

    @api.onchange('company_id')
    def _get_default_branch(self):
        branch_lst = []
        if self.env.user.branch_ids:
            if len(self.env.user.branch_ids) == 1:
                self.branch_id = self.env.user.branch_ids[0]
            for branch in self.env.user.branch_ids:
                branch_lst.append(branch.id)
        else:
            for b in self.env['res.branch'].search([]):
                branch_lst.append(b.id)
        return {'domain': {'branch_id': [('id', 'in', branch_lst)]}}
