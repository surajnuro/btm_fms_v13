from odoo import api, fields, models, _
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT, float_compare
from odoo.exceptions import UserError


class SaleOrder(models.Model):
    _inherit = 'sale.order'

    @api.onchange('user_id')
    def _get_default_branch(self):
        branch_lst = []
        if self.env.user.branch_ids:
            if len(self.env.user.branch_ids) == 1:
                self.branch_id = self.env.user.branch_ids[0]
            for branch in self.env.user.branch_ids:
                branch_lst.append(branch.id)
        else:
            for b in self.env['res.branch'].search([]):
                branch_lst.append(b.id)
        return {'domain': {'branch_id': [('id', 'in', branch_lst)]}}

    def write(self, vals):
        self.clear_caches()
        return super(SaleOrder, self).write(vals)

    @api.model
    def _default_warehouse_id(self):
        '''
        overwrite to fill  warehouse if only one allowed branch allocated
        :return:
        '''

        if len(self.env.user.branch_ids)>1:
            return False
        warehouse_ids = self.env['stock.warehouse'].search([('branch_id', '=', self.env.user.branch_id.id)], limit=1)
        return warehouse_ids


    warehouse_id = fields.Many2one(
        'stock.warehouse', string='Warehouse',
        required=True, readonly=True, states={'draft': [('readonly', False)], 'sent': [('readonly', False)]},
        default=_default_warehouse_id)
    branch_id = fields.Many2one('res.branch', 'Branch', readonly=True, required=True,
                                states={'draft': [('readonly', False)], 'sent': [('readonly', False)]}, tracking=True)

    def action_confirm(self):
        res = super(SaleOrder, self).action_confirm()
        for sale in self:
            for inv in sale.invoice_ids:
                inv.branch_id = sale.branch_id and sale.branch_id.id
            sale.picking_ids.sudo().write({'branch_id': sale.branch_id.id})
            sale.picking_ids.move_ids_without_package.sudo().write({'branch_id': sale.branch_id.id})
        return res

    def _prepare_invoice(self):
        """
        Prepare the dict of values to create the new invoice for a sales order. This method may be
        overridden to implement custom invoice generation (making sure to call super() to establish
        a clean extension chain).
        """
        res = super(SaleOrder, self)._prepare_invoice()
        res.update({'branch_id':self.branch_id.id})
        return res

    @api.onchange('branch_id')
    def _onchange_branch_id(self):
        warehouse_obj = self.env['stock.warehouse']
        if self.branch_id:
            warehouse_id = warehouse_obj.search([('branch_id', '=',self.branch_id.id)], limit=1)
            if warehouse_id:
                self.warehouse_id = warehouse_id.id
            else:
                raise UserError(
                    _("No Warehouse has the branch same as the one selected "
                      "in the Sale Order")
                )



class StockPicking(models.Model):
    _inherit = 'stock.picking'

    branch_id = fields.Many2one('res.branch', 'Branch', tracking=True)
    user_id = fields.Many2one('res.users', default=lambda self: self.env.user)

    def write(self, vals):
        self.clear_caches()
        return super(StockPicking, self).write(vals)

    @api.onchange('user_id')
    def _get_default_branch(self):
        branch_lst = []
        if self.env.user.branch_ids:
            if len(self.env.user.branch_ids) == 1:
                self.branch_id = self.env.user.branch_ids[0]
            for branch in self.env.user.branch_ids:
                branch_lst.append(branch.id)
        else:
            for b in self.env['res.branch'].search([]):
                branch_lst.append(b.id)
        return {'domain': {'branch_id': [('id', 'in', branch_lst)]}}


class StockMove(models.Model):
    _inherit = 'stock.move'

    branch_id = fields.Many2one('res.branch', 'Branch', related='picking_id.branch_id', store=True)

class SaleOrderLine(models.Model):
    _inherit = 'sale.order.line'

    branch_id = fields.Many2one('res.branch', related='order_id.branch_id', store=True)
    user_id = fields.Many2one('res.users', default=lambda self: self.env.user)

    def write(self, vals):
        self.clear_caches()
        return super(SaleOrderLine, self).write(vals)

    @api.onchange('user_id')
    def onchange_product_branch(self):
        prod = []
        if self.order_id.branch_id:
            for product in self.env['product.product'].search([('branch_ids', 'in', self.order_id.branch_id.id)]):
                prod.append(product.id)
        else:
            for product in self.env['product.product'].search([]):
                prod.append(product.id)
        return {'domain': {'product_id': [('id', 'in', prod)]}}

