from odoo import api, fields, models, _

class Product(models.Model):
    _inherit = 'product.template'

    branch_ids = fields.Many2many('res.branch', 'product_branchs_ref', 'partner_id', 'branch_id')
    user_id = fields.Many2one('res.users', default=lambda self: self.env.user)

    def write(self, vals):
        self.clear_caches()
        return super(Product, self).write(vals)

    # @api.onchange('categ_id')
    # def _get_default_branch(self):
    #     branch_lst = []
    #     if self.env.user.branch_ids:
    #         if len(self.env.user.branch_ids) == 1:
    #             self.branch_ids = [(6, 0, [self.env.user.branch_ids[0]])]
    #         for branch in self.env.user.branch_ids:
    #             branch_lst.append(branch.id)
    #     else:
    #         for b in self.env['res.branch'].search([]):
    #             branch_lst.append(b.id)
    #     return {'domain': {'branch_ids': [('id', 'in', branch_lst)]}}