from odoo import api, fields, models, _

class Partner(models.Model):
    _inherit = 'res.partner'

    branch_ids = fields.Many2many('res.branch', 'partners_ref_branch', 'partner_id', 'branch_id')
    user_id = fields.Many2one('res.users', default=lambda self: self.env.user)

    @api.onchange('user_id')
    def _get_default_branch(self):
        branch_lst = []
        if self.env.user.branch_ids:
            for branch in self.env.user.branch_ids:
                branch_lst.append(branch.id)
        else:
            for b in self.env['res.branch'].search([]):
                branch_lst.append(b.id)
        return {'domain': {'branch_ids': [('id', 'in', branch_lst)]}}

    def write(self, vals):
        self.clear_caches()
        return super(Partner, self).write(vals)


