from odoo import api, fields, models, _
from odoo.exceptions import UserError


class AccountJournal(models.Model):
    _inherit = 'account.journal'

    branch_id = fields.Many2one('res.branch', 'Branch', tracking=True)

    def write(self, vals):
        self.clear_caches()
        return super(AccountJournal, self).write(vals)

class AccountMove(models.Model):
    _inherit = 'account.move'

    @api.onchange('invoice_user_id')
    def _get_default_branch(self):
        branch_lst = []
        if not self.purchase_id:
            if self.env.user.branch_ids:
                if len(self.env.user.branch_ids) == 1:
                    self.branch_id = self.env.user.branch_ids[0]
                for branch in self.env.user.branch_ids:
                    branch_lst.append(branch.id)
            else:
                for b in self.env['res.branch'].search([]):
                    branch_lst.append(b.id)
            return {'domain': {'branch_id': [('id', 'in', branch_lst)]}}

    branch_id = fields.Many2one('res.branch', 'Branch',  tracking=True)

    @api.model
    def create(self, vals):
        self.clear_caches()
        res = super(AccountMove, self).create(vals)
        if res.stock_move_id:
            res.branch_id = res.stock_move_id.branch_id
        return res

    def write(self, vals):
        self.clear_caches()
        return super(AccountMove, self).write(vals)

    @api.onchange('purchase_vendor_bill_id', 'purchase_id')
    def _onchange_purchase_auto_complete(self):
        ''' Load from either an old purchase order, either an old vendor bill.

        When setting a 'purchase.bill.union' in 'purchase_vendor_bill_id':
        * If it's a vendor bill, 'invoice_vendor_bill_id' is set and the loading is done by '_onchange_invoice_vendor_bill'.
        * If it's a purchase order, 'purchase_id' is set and this method will load lines.

        /!\ All this not-stored fields must be empty at the end of this function.
        '''
        if self.purchase_vendor_bill_id.vendor_bill_id:
            self.invoice_vendor_bill_id = self.purchase_vendor_bill_id.vendor_bill_id
            self._onchange_invoice_vendor_bill()
        elif self.purchase_vendor_bill_id.purchase_order_id:
            self.purchase_id = self.purchase_vendor_bill_id.purchase_order_id
        self.purchase_vendor_bill_id = False

        if not self.purchase_id:
            return

        # Copy partner.
        self.partner_id = self.purchase_id.partner_id
        self.fiscal_position_id = self.purchase_id.fiscal_position_id
        self.invoice_payment_term_id = self.purchase_id.payment_term_id
        self.currency_id = self.purchase_id.currency_id

        # Copy purchase lines.
        po_lines = self.purchase_id.order_line - self.line_ids.mapped('purchase_line_id')
        new_lines = self.env['account.move.line']
        for line in po_lines.filtered(lambda l: not l.display_type):
            new_line = new_lines.new(line._prepare_account_move_line(self))
            new_line.account_id = new_line._get_computed_account()
            new_line._onchange_price_subtotal()
            new_lines += new_line
        new_lines._onchange_mark_recompute_taxes()

        # Compute invoice_origin.
        origins = set(self.line_ids.mapped('purchase_line_id.order_id.name'))
        self.invoice_origin = ','.join(list(origins))

        # Compute ref.
        refs = set(self.line_ids.mapped('purchase_line_id.order_id.partner_ref'))
        refs = [ref for ref in refs if ref]
        self.ref = ','.join(refs)

        # Compute _invoice_payment_ref.
        if len(refs) == 1:
            self._invoice_payment_ref = refs[0]

        # self.purchase_id = False
        self._onchange_currency()
        self.branch_id = self.purchase_id.branch_id.id
        self.invoice_partner_bank_id = self.bank_partner_id.bank_ids and self.bank_partner_id.bank_ids[0]

class AccountMoveLine(models.Model):
    _inherit = 'account.move.line'

    branch_id = fields.Many2one('res.branch', 'Branch', related='move_id.branch_id', store=True)

    def write(self, vals):
        self.clear_caches()
        return super(AccountMoveLine, self).write(vals)
