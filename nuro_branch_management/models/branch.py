from odoo import api, fields, models, _
from odoo.tools import pycompat
from odoo.tools.float_utils import float_compare
from odoo.exceptions import UserError


class ResBranch(models.Model):
    _name = 'res.branch'


    name = fields.Char('Name', required=True)
    address = fields.Text('Address', size=252)
    telephone_no = fields.Char("Telephone No.")
    company_id = fields.Many2one('res.company', 'Company', required=True)
    branch_code = fields.Char('Branch Code')
    state_id = fields.Many2one('res.country.state', 'State')
    warehouse_id = fields.Many2one('stock.warehouse', 'Warehouse')
    prefix = fields.Char('Prefix')
    # active = fields.Boolean('Active', default=True)


class StockWarehouse(models.Model):
    _inherit = 'stock.warehouse'

    branch_id = fields.Many2one('res.branch', 'Branch')
    user_id = fields.Many2one('res.users', default=lambda self: self.env.user)

    @api.onchange('company_id')
    def _get_default_branch(self):
        branch_lst = []
        if self.env.user.branch_ids:
            if len(self.env.user.branch_ids) == 1:
                self.branch_id = self.env.user.branch_ids[0]
            for branch in self.env.user.branch_ids:
                branch_lst.append(branch.id)
        else:
            for b in self.env['res.branch'].search([]):
                branch_lst.append(b.id)
        return {'domain': {'branch_id': [('id', 'in', branch_lst)]}}



class StockLocation(models.Model):
    _inherit = 'stock.location'

    branch_id = fields.Many2one('res.branch', 'Branch')

    @api.constrains('branch_id')
    def _check_branch(self):
        for location in self:
            warehouse_obj = self.env['stock.warehouse']
            warehouse_id = warehouse_obj.search(
                ['|', '|', ('wh_input_stock_loc_id', '=', location.id),
                 ('lot_stock_id', '=', location.id),
                 ('wh_output_stock_loc_id', '=', location.id)])
            for warehouse in warehouse_id:
                if location.branch_id != warehouse.branch_id:
                    raise UserError(_(
                        'Configuration error\nYou  \
                        must select same branch on a location as \
                        assigned on a warehouse configuration.'
                    ))


class ResUsers(models.Model):
    _inherit = 'res.users'

    branch_id = fields.Many2one('res.branch', 'Branch', required=False)
    branch_ids = fields.Many2many(
        'res.branch', id1='user_id',
        id2='branch_id',string='Branch'
    )

    def write(self, vals):
        self.clear_caches()
        return super(ResUsers, self).write(vals)

class PurchaseOrderLine(models.Model):
    _inherit = 'purchase.order.line'

    branch_id = fields.Many2one('res.branch', related='order_id.branch_id', store=True)
    user_id = fields.Many2one('res.users', default=lambda self: self.env.user)

    def write(self, vals):
        self.clear_caches()
        return super(PurchaseOrderLine, self).write(vals)

    @api.onchange('product_qty')
    def onchange_product_branch(self):
        prod = []
        if self.order_id.branch_id:
            for product in self.env['product.product'].search([('branch_ids', 'in', self.order_id.branch_id.id)]):
                prod.append(product.id)
        else:
            for product in self.env['product.product'].search([]):
                prod.append(product.id)
        return {'domain': {'product_id': [('id', 'in', prod)]}}

    def _prepare_stock_moves(self, picking):
        """ Prepare the stock moves data for one order line. This function returns a list of
        dictionary ready to be used in stock.move's create()
        """
        self.ensure_one()
        res = []
        if self.product_id.type not in ['product', 'consu']:
            return res
        qty = 0.0
        price_unit = self._get_stock_move_price_unit()
        for move in self.move_ids.filtered(lambda x: x.state != 'cancel' and not x.location_dest_id.usage == "supplier"):
            qty += move.product_qty
        template = {
            'name': self.name or '',
            'product_id': self.product_id.id,
            'product_uom': self.product_uom.id,
            'date': self.order_id.date_order,
            'date_expected': self.date_planned,
            'location_id': self.order_id.partner_id.property_stock_supplier.id,
            'location_dest_id': self.order_id._get_destination_location(),
            'picking_id': picking.id,
            'partner_id': self.order_id.dest_address_id.id,
            'move_dest_ids': [(4, x) for x in self.move_dest_ids.ids],
            'state': 'draft',
            'purchase_line_id': self.id,
            'company_id': self.order_id.company_id.id,
            'price_unit': price_unit,
            'picking_type_id': self.order_id.picking_type_id.id,
            'group_id': self.order_id.group_id.id,
            'origin': self.order_id.name,
            'route_ids': self.order_id.picking_type_id.warehouse_id and [(6, 0, [x.id for x in self.order_id.picking_type_id.warehouse_id.route_ids])] or [],
            'warehouse_id': self.order_id.picking_type_id.warehouse_id.id,
            'branch_id':self.order_id.branch_id.id
        }
        diff_quantity = self.product_qty - qty
        if float_compare(diff_quantity, 0.0,  precision_rounding=self.product_uom.rounding) > 0:
            template['product_uom_qty'] = diff_quantity
            res.append(template)
        return res


class PurchaseOrder(models.Model):
    _inherit = 'purchase.order'

    def write(self, vals):
        self.clear_caches()
        return super(PurchaseOrder, self).write(vals)

    @api.onchange('user_id')
    def _get_default_branch(self):
        branch_lst = []
        if self.env.user.branch_ids:
            if len(self.env.user.branch_ids) == 1:
                self.branch_id = self.env.user.branch_ids[0]
            for branch in self.env.user.branch_ids:
                branch_lst.append(branch.id)
        else:
            for b in self.env['res.branch'].search([]):
                branch_lst.append(b.id)
        return {'domain': {'branch_id': [('id', 'in', branch_lst)]}}

    @api.onchange('branch_id')
    def _onchange_branch_id(self):
        type_obj = self.env['stock.picking.type']
        if self.branch_id:
            if self.branch_id:
                picking_type = type_obj.search([('code', '=', 'incoming'),
                                         ('warehouse_id.branch_id', '=',
                                          self.branch_id.id)])
                if picking_type:
                    self.picking_type_id = picking_type[0]


    @api.onchange('picking_type_id')
    def _onchange_picking_type_id(self):
        if self.picking_type_id and self.picking_type_id.warehouse_id:
            if self.picking_type_id.warehouse_id.branch_id:
                self.branch_id = self.picking_type_id.warehouse_id.branch_id.id


    branch_id = fields.Many2one('res.branch', 'Branch')

    @api.model
    def _prepare_picking(self):
        """
        Prepare the dict of values to create the new picking for a Purchase order. This method may be
        """
        res = super(PurchaseOrder, self)._prepare_picking()
        res.update({'branch_id':self.branch_id.id})
        return res


class AccountPayment(models.Model):
    _inherit = "account.payment"
    _order = 'create_date desc'

    @api.onchange('partner_type')
    def _get_default_branch(self):
        branch_lst = []
        if self.env.user.branch_ids:
            if len(self.env.user.branch_ids) == 1:
                self.branch_id = self.env.user.branch_ids[0]
            for branch in self.env.user.branch_ids:
                branch_lst.append(branch.id)
        else:
            for b in self.env['res.branch'].search([]):
                branch_lst.append(b.id)
        return {'domain': {'branch_id': [('id', 'in', branch_lst)]}}

    def write(self, vals):
        self.clear_caches()
        return super(AccountPayment, self).write(vals)

    branch_id = fields.Many2one('res.branch', string='Branch',)
    user_id = fields.Many2one('res.users', default=lambda self: self.env.user)