
{
    'name': "Ticket Reassign",
    'version': '11.0.2.0.0',
    'summary': """Ticket Re-assign""",
    'description': """""",
    'category': 'Business',
    'author': 'Nuro Software',
    'company': 'Nuro Software',
    'website': "https://www.nurosolution.com",
    'depends': ['website_support'],
    'data': [
        'wizard/reassign_tkt_wiz.xml',
        'wizard/reopen_wiz_view.xml',
        'views/support_ticket_view.xml',
    ],
    # 'qweb': ["static/src/xml/ticket_dashboard.xml"],
    # 'images': ["static/description/banner.gif"],
    'license': "AGPL-3",
    'installable': True,
    'application': True,
}
