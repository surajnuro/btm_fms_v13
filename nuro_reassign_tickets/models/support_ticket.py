from odoo import api, fields, models, _

class SupportTicket(models.Model):
    _inherit = 'website.support.ticket'

    note = fields.Text('Re assign Reason', track_visibility='onchange')
    reopen_note = fields.Text(' Reopen Ticket Reason', track_visibility='onchange')

