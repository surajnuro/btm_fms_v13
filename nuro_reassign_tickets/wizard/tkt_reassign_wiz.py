from odoo import api, fields, models, _

class Reassigntktwiz(models.TransientModel):
    _name = 'reassign.tkt.wiz'


    user_id = fields.Many2one('res.users', string='Technician')
    branch_id = fields.Many2one('res.branch', string='Sector')
    note = fields.Text('Reason for Reassign')

    def update_reassign_data(self):
        active_id = self.env.context.get('active_id')
        if active_id:
            tkt = self.env['website.support.ticket'].browse(active_id)
            if tkt:
                tkt.branch_id = self.branch_id.id
                tkt.note = self.note
                tkt.supervisor_id = self.user_id.id
                task = self.env['project.task'].search([('ticket_id', '=', tkt.id)])
                if task:
                    task.branch_id = self.branch_id.id
                    task.user_id = self.user_id.id
                if tkt.quotation_id:
                    tkt.quotation_id.branch_id = self.branch_id.id
                    tkt.quotation_id.supervisor_id = self.user_id.id
                    task_data = self.env['project.task'].search([('quotation_id', '=', tkt.quotation_id.id)])
                    if task_data:
                        task_data.branch_id = self.branch_id.id
                        task_data.user_id = self.user_id.id





