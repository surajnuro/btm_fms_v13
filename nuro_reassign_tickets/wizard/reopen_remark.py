from odoo import api, fields, models, _

class Reopen(models.TransientModel):
    _name = 'reopen.tkt.wiz'

    note = fields.Text('Reason for Reopen')

    def reopen_task(self):
        active_id = self.env.context.get('active_id')
        if active_id:
            tkt = self.env['website.support.ticket'].browse(active_id)
            if tkt:
                tkt.reopen_note = self.note
                tkt.states = 'inprogress'








