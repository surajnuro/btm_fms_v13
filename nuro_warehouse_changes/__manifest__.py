# -*- coding: utf-8 -*-
# Copyright 2016 Eficent Business and IT Consulting Services S.L.
# License LGPL-3.0 or later (http://www.gnu.org/licenses/lgpl-3.0).

{
    "name": "Task Material request warehouse Changes",
    "author": "Geo Technoosft Pvt Ltd",
    "version": "10.0.1.1.0",
    "summary": "By Using this module the warehouse can be changes post requesting the material",
    "category": "Material Management",
    "depends": [
        'nuro_project_task_management',
        'website_support',
        'warehouse_stock_restrictions',
        'stock',
        'stock_account',
    ],
    "data": [
        "security/security_view.xml",
        "views/stock_picking_request.xml",
    ],
    'demo': [],
    "license": 'LGPL-3',
    'installable': True
}
