from odoo import api, fields, models, _
from odoo.exceptions import UserError, Warning, ValidationError

class StockPicking(models.Model):
    _inherit = 'stock.picking'


    warehouse_id = fields.Many2one('stock.warehouse', 'Warehouse To Request', domain=[('is_un_warehouse', '=', True)])
    warehouse_transfer = fields.Boolean()
    stock_move_check = fields.Boolean()
    pre_picking_check = fields.Boolean()
    picking_number_id = fields.Many2one('stock.picking','New Picking Number', readonly=True)
    pre_picking_id = fields.Many2one('stock.picking','Previous Picking Number', readonly=True)

    # function to create new picking and update the same into the task with different warehouse id
    def picking_operation_change(self):
        lst = []
        for rec in self.move_lines:
            if self.warehouse_id and rec.state != 'cancel':
                lst.append((0, 0, {
                    'product_id': rec.product_id.id,
                    'product_uom': rec.product_uom.id,
                    'product_uom_qty': rec.product_uom_qty,
                    'name': 'Task Material Request',
                    'material_request_id': rec.material_request_id.id
                }))
        int_type = self.env['stock.picking.type'].search(
            [('warehouse_id', '=', self.warehouse_id.id), ('code', '=', 'internal')], limit=1)
        picking_dict = {
            'picking_type_id': int_type.id,
            'location_id': int_type.default_location_src_id.id,
            'location_dest_id': self.location_dest_id.id,
            'task_id': self.task_id.id,
            'client_ticket': self.client_ticket,
            'ticket_id': self.ticket_id.id,
            'site_id': self.site_id.id,
            'location': self.location,
            'origin': self.origin,
            'move_ids_without_package': lst,
            'branch_id': self.branch_id.id,
            'project_id': self.project_id.id,
            'analytic_account_id': self.analytic_account_id.id,
        }
        if self.picking_type_id.warehouse_id != self.warehouse_id:
            internal_transfer = self.create(picking_dict)
        else:
            raise UserError(_('You can not create another picking for for the same Same Location'))
        if internal_transfer:
            self.action_cancel()
            self.task_id.stock_picking_number = internal_transfer.name
            self.picking_number_id = internal_transfer.id
            internal_transfer.pre_picking_check = True
            internal_transfer.pre_picking_id = self.id
            internal_transfer.write({'state': 'draft'})
            return internal_transfer.do_print_material_request()

    # function to create new picking and update the same into the task with different warehouse id
    def picking_operation_change_line(self):
        lst = []
        for rec in self.move_lines:
            if rec.warehouse_id and rec.state != 'cancel':
                lst.append((0, 0, {
                    'product_id': rec.product_id.id,
                    'product_uom': rec.product_uom.id,
                    'product_uom_qty': rec.product_uom_qty,
                    'name': 'Task Material Request',
                    'material_request_id': rec.material_request_id.id
                }))
                rec._action_cancel()
        for move in self.move_lines:
            if move.warehouse_id:
                int_type = self.env['stock.picking.type'].search(
                    [('warehouse_id', '=', move.warehouse_id.id), ('code', '=', 'internal')], limit=1)
        picking_dict = {
            'picking_type_id': int_type.id,
            'location_id': int_type.default_location_src_id.id,
            'location_dest_id': self.location_dest_id.id,
            'task_id': self.task_id.id,
            'client_ticket': self.client_ticket,
            'ticket_id': self.ticket_id.id,
            'site_id': self.site_id.id,
            'location': self.location,
            'origin': self.origin,
            'move_ids_without_package': lst,
            'branch_id': self.branch_id.id,
            'project_id': self.project_id.id,
            'analytic_account_id': self.analytic_account_id.id,
        }
        internal_transfer = self.create(picking_dict)
        if internal_transfer:
            for task in self.task_id:
                task.stock_picking_number = internal_transfer.name
            self.picking_number_id = internal_transfer.id
            internal_transfer.pre_picking_check = True
            internal_transfer.pre_picking_id = self.id
            internal_transfer.write({'state': 'draft'})
            return internal_transfer.do_print_material_request()


class StockMove(models.Model):
    _inherit = 'stock.move'

    warehouse_id = fields.Many2one('stock.warehouse', 'Warehouse', domain=[('is_un_warehouse', '=', True)])
    stock_move_check = fields.Boolean(related='picking_id.stock_move_check', store=True)

class StockMove(models.Model):
    _inherit = 'stock.warehouse'

    is_un_warehouse = fields.Boolean('Un Warehouse')
    is_consume_warehouse = fields.Boolean('Consume Warehouse')
    consume_location_id = fields.Many2one('stock.location', 'Consume Location', domain=[('usage', '=', 'customer')])
