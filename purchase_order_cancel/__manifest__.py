{
    'name': 'Purchase Order Cancel',
    'version': '13.0.0.1',
    'category': 'Purchase Order',
    'sequence': 1,
    'summary': 'Cancel Done Purchase Order and related Moves',
    'description': """
        This module helps in reset stock move or cancel stock move or cancel stock picking.
        cancel Delivery order or cancel Receipt. Delivery order cancel / reverse .
        cancel done delivery order. 
        This module helps to reverse the done picking, allow to cancel picking and 
        set it to draft stage.
    """,
    'author': "Nurosolution Pvt ltd",
    'website': "https://www.nurosolution.com",
    'depends': ['stock','purchase','account','stock_account','nuro_stock_picking_cancel'],
    'data': [
        'security/security_view.xml',
        'views/purchase_order_inherit.xml'
    ],
    'images': ['static/description/icon.png'],
    'license': 'Other proprietary',
    'installable': True,
    'application': True,
}
