from odoo import api, models, _
from odoo.exceptions import ValidationError, UserError, Warning

class PurchaseOrder(models.Model):
    _inherit = "purchase.order"

    def action_purchase_order_cancel(self):
        for order in self.picking_ids:
            if order.state != 'cancel':
                order.action_picking_cancel()
                order.write({'is_locked': True})
        for rec in self.invoice_ids:
            if rec.state == 'posted':
                raise UserError(_('Please Unreconcile the Paid Invoices to cancel the Order'))
            if rec.state not in ('cancel'):
                rec.button_cancel()
        self.write({'state': 'cancel'})
        return True