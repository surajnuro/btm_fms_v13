from odoo import models, fields, api, _
from odoo.exceptions import UserError, Warning, ValidationError
from datetime import datetime, timedelta
from dateutil.relativedelta import relativedelta
from math import ceil


class NuroBudget(models.Model):
    _name = 'nuro.budget'

    name = fields.Char('Name')
    project_id = fields.Many2one('project.project', 'Project')
    branch_id = fields.Many2one('res.branch', 'Branch')
    budget_duration = fields.Selection([('monthly', 'Monthly'),
                                        ('quarterly', 'Quarterly'),
                                        ('half_yearly', 'Half Yearly'),
                                        ('anually', 'Anually')], string='Budget Frequency')
    date_start = fields.Date('Start Date')
    date_end = fields.Date('End Date')
    state = fields.Selection([('draft', 'Draft'), ('submit', 'Submit'), ('approved', 'Approved'), ('cancel', 'Cancel')],
                             default='draft')
    budget_line_income = fields.One2many('nuro.budget.line', 'budget_income_id', string='Budget Line', auto_join=True)
    budget_line_expense = fields.One2many('nuro.budget.line', 'budget_expense_id', string='Budget Line', auto_join=True)
    total_budget_income = fields.Float('Total Budget Income', compute='_compute_budget_line')
    total_budget_expense = fields.Float('Total Budget Expense', compute='_compute_budget_line')
    income = fields.Float('Income', compute='_compute_budget_line')
    expense = fields.Float('Expense', compute='_compute_budget_line')
    month = fields.Selection(
        [('0', '0'), ('1', '1'), ('2', '2'), ('3', '3'), ('4', '4'), ('5', '5'), ('6', '6'), ('7', '7'),
         ('8', '8'), ('9', '9'), ('10', '10'), ('11', '11'), ('12', '12'), ('13', '13'), ('14', '14'),
         ('15', '15'), ('16', '16'), ('17', '17'), ('18', '18'), ('19', '19'), ('20', '20'), ('21', '21'),
         ('22', '22'), ('23', '23'), ('24', '24'), ('25', '25'), ('26', '26'), ('27', '27'), ('28', '28'),
         ('29', '29'), ('30', '30'), ('31', '31'), ('32', '32'), ('33', '33'), ('34', '34'), ('35', '35'),
         ('36', '36')], string='Month')
    first_month = fields.Char('First Month')
    last_month = fields.Char('Last Month')

    @api.depends('budget_line_income', 'budget_line_income.total', 'budget_line_income.income',
                 'budget_line_income.expense', 'budget_line_expense', 'budget_line_expense.total',
                 'budget_line_expense.income', 'budget_line_expense.expense', )
    def _compute_budget_line(self):
        budget_income = 0.0
        budget_expense = 0.0
        incm = 0.0
        expns = 0.0
        for inc in self.budget_line_income:
            budget_income += inc.total
            incm += inc.income - inc.expense
        for exp in self.budget_line_expense:
            budget_expense += exp.total
            expns += exp.expense - exp.income
        self.total_budget_income = budget_income
        self.total_budget_expense = budget_expense
        self.income = incm
        self.expense = expns

    def date_constrains(self):
        if self.date_start and self.date_end:
            start = datetime.strptime(str(self.date_start), '%Y-%m-%d')
            end = datetime.strptime(str(self.date_end), '%Y-%m-%d')
            date_diff = ((end.year - start.year) * 12 + end.month - start.month) + 1
            if date_diff > 36:
                self.date_start = False
                self.date_end = False
                raise UserError(_('You are not allowed to create budget more than 36 Month'))
            elif date_diff < 1:
                self.date_start = False
                self.date_end = False
                raise UserError(_('You are not allowed to create budget more than 36 Month'))

    @api.model
    def create(self, vals):
        res = super(NuroBudget, self).create(vals)
        res.date_constrains()
        return res

    def write(self, vals):
        self.date_constrains()
        return super(NuroBudget, self).write(vals)

    @api.onchange('date_start', 'date_end')
    def onchange_budget_date(self):
        if self.date_start and self.date_end:
            start = datetime.strptime(str(self.date_start), '%Y-%m-%d')
            end = datetime.strptime(str(self.date_end), '%Y-%m-%d')
            date_diff = ((end.year - start.year) * 12 + end.month - start.month) + 1
            if date_diff > 36:
                self.date_start = False
                self.date_end = False
                raise UserError(_('You are not allowed to create budget more than 36 Month'))
            elif date_diff < 1:
                self.date_start = False
                self.date_end = False
                raise UserError(_('You are not allowed to create budget more than 36 Month'))
            self.month = str(date_diff)
            self.first_month = str(start.strftime("%b")) + '-' + str(start.year)
            self.last_month = str(end.strftime("%b")) + '-' + str(end.year)

    def button_submit(self):
        for inc in self.budget_line_income:
            inc._check_budget_line()
        for exp in self.budget_line_expense:
            exp._check_budget_line()
        self.state = 'submit'

    def button_approve(self):
        for inc in self.budget_line_income:
            inc._check_budget_line()
        for exp in self.budget_line_expense:
            exp._check_budget_line()
        self.state = 'approved'

    def button_cancel(self):
        self.state = 'cancel'


class NuroBudgetLine(models.Model):
    _name = 'nuro.budget.line'

    date_start = fields.Date('Start Date')
    date_end = fields.Date('End Date')
    income_project_id = fields.Many2one('project.project', 'Project', related='budget_income_id.project_id', store=True)
    income_branch_id = fields.Many2one('res.branch', 'Sector', related='budget_income_id.branch_id', store=True)
    exp_project_id = fields.Many2one('project.project', 'Project',
                                                 related='budget_expense_id.project_id', store=True)
    exp_branch_id = fields.Many2one('res.branch', 'Sector', related='budget_expense_id.branch_id', store=True)
    budget_income_id = fields.Many2one('nuro.budget', ondelete='cascade', index=True,)
    budget_expense_id = fields.Many2one('nuro.budget', ondelete='cascade', index=True,)
    account_id = fields.Many2one('account.account', 'Account')
    budget_frequency_line = fields.One2many('nuro.budget.frequency', 'budget_frequency_id', string="Budget Lines")
    budget_duration = fields.Selection([('monthly', 'Monthly'),
                                        ('quarterly', 'Quarterly'),
                                        ('half_yearly', 'Half Yearly'),
                                        ('anually', 'Anually')], string='Budget Frequency')
    income = fields.Float('Income', compute='_compute_income_expense')
    expense = fields.Float('Expense', compute='_compute_income_expense')
    total = fields.Float('Total', compute='_compute_total', store=True)
    date_start = fields.Date('Start Date')
    date_end = fields.Date('End Date')
    panel_type = fields.Selection([('income', 'Income'), ('expense', 'Expense')])

    @api.model
    def create(self, vals):
        res = super(NuroBudgetLine, self).create(vals)
        if res.budget_income_id:
            res.date_start = res.budget_income_id.date_start
            res.date_end = res.budget_income_id.date_end
        if res.budget_expense_id:
            res.date_start = res.budget_expense_id.date_start
            res.date_end = res.budget_expense_id.date_end
        return res

    @api.onchange('panel_type')
    def _onchange_panel_type(self):
        if self.panel_type == 'income':
            return {'domain': {'account_id': [('user_type_id.name', '=', 'Income')]}}
        if self.panel_type == 'expense':
            return {'domain': {'account_id': [('user_type_id.name', '=', 'Expenses')]}}

    def _check_budget_line(self):
        domain = ([('account_id', '=', self.account_id.id),('id', '!=', self.id)])
        if self.budget_income_id:
            domain.append(('budget_income_id.date_start', '>=', self.budget_income_id.date_start))
            domain.append(('budget_income_id.state', 'in', ('submit', 'approved')))
            domain.append(('income_project_id', '=', self.budget_income_id.project_id.id))
            domain.append(('income_branch_id', '=', self.budget_income_id.branch_id.id))
        if self.budget_expense_id:
            domain.append(('budget_expense_id.date_end', '<=', self.budget_expense_id.date_end))
            domain.append(('budget_expense_id.state', 'in', ('submit', 'approved')))
            domain.append(
                ('exp_project_id', '=', self.budget_expense_id.project_id.id))
            domain.append(('exp_branch_id', '=', self.budget_expense_id.branch_id.id))
        search_budget_line = self.env['nuro.budget.line'].search(domain)
        if search_budget_line:
            raise UserError(_('Duplicate Line found for the account %s') %(self.account_id.name))

    @api.depends('account_id', 'budget_income_id', 'budget_expense_id', 'budget_income_id.date_start',
                 'budget_income_id.date_end', 'budget_expense_id.date_start', 'budget_expense_id.date_end')
    def _compute_income_expense(self):
        for acc in self:
            if acc.account_id:
                query_income = '''select sum(credit) from account_move_line where account_id = %s and date >= '%s' 
                                    and date <= '%s' ''' % (
                acc.account_id.id, acc.date_start or acc.date_start, acc.date_end or acc.date_end)
                self.env.cr.execute(query_income)
                result_income = self.env.cr.fetchone()
                acc.income = result_income and result_income[0] or 0.0
                query_expense = '''select sum(debit) from account_move_line where account_id = %s and date >= '%s' 
                                    and date <= '%s' ''' % (
                acc.account_id.id, acc.date_start or acc.date_start, acc.date_end or acc.date_end)
                self.env.cr.execute(query_income)
                self.env.cr.execute(query_expense)
                result_expense = self.env.cr.fetchone()
                acc.expense = result_expense and result_expense[0] or 0.0


    @api.depends('budget_frequency_line', 'budget_frequency_line.amount')
    def _compute_total(self):
        for record in self:
            total_amt = 0.0
            for rec in record.budget_frequency_line:
                total_amt += rec.amount
            record.total = total_amt

    def compute_month(self):
        start = datetime.strptime(str(self.date_start), '%Y-%m-%d')
        end = datetime.strptime(str(self.date_end), '%Y-%m-%d')
        date_diff = ((end.year - start.year) * 12 + end.month - start.month) + 1
        if self.budget_duration == 'monthly':
            for i in range(0, date_diff):
                update_date = start
                month = str(start.strftime("%b")) + '-' + str(start.year)
                self.budget_frequency_line.create({
                    'name': month,
                    'budget_frequency_id': self.id,
                    'date': update_date,
                })
                update_date += relativedelta(months=1)
                start += relativedelta(months=1)
        if self.budget_duration == 'quarterly':
            qtr_diff = date_diff/3
            for i in range(0, ceil(qtr_diff)):
                month_number = start.month
                if month_number in (1,2,3):
                    quarter = '1st Quarter'
                elif month_number in (4,5,6):
                    quarter = '2nd Quarter'
                elif month_number in (7,8,9):
                    quarter = '3rd Quarter'
                elif month_number in (10,11,12):
                    quarter = '4th Quarter'
                self.budget_frequency_line.create({
                    'name': quarter +'-'+ str(start.year),
                    'budget_frequency_id': self.id,
                })
                start += relativedelta(months=3)
        if self.budget_duration == 'half_yearly':
            qtr_diff = date_diff/6
            for i in range(0, ceil(qtr_diff)):
                month_number = start.month
                print(month_number)
                if month_number in (1,2,3,4,5,6):
                    half = '1st Half'
                elif month_number in (7,8,9,10,11,12):
                    half = '2nd Half'
                self.budget_frequency_line.create({
                    'name': half +'-'+ str(start.year),
                    'budget_frequency_id': self.id,
                })
                start += relativedelta(months=6)
        if self.budget_duration == 'anually':
            qtr_diff = date_diff/12
            print(ceil(qtr_diff))
            for i in range(0, ceil(qtr_diff)):
                # month_number = start.month
                self.budget_frequency_line.create({
                    'name': str(start.year),
                    'budget_frequency_id': self.id,
                })
                start += relativedelta(months=12)

class NuroBudgetAmountLines(models.Model):
    _name = 'nuro.budget.frequency'

    budget_frequency_id = fields.Many2one('nuro.budget.line', ondelete='cascade')
    name = fields.Char('Name')
    amount = fields.Float('Amount')
    date = fields.Date('Date')
