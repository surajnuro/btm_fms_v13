# -*- coding: utf-8 -*-
{
    'name': "Nuro Budget",
    'summary': """
        This is the module to fetch the income and expense and also assign the budget""",
    'description': """
       This is the module to fetch the income and expense and also assign the budget
    """,

    'author': "NUROSOLUTION PRIVATE LIMITED",
    'website': "http://www.nurosolution.com",

    'category': 'BUDGET',
    'depends': ['base', 'stock', 'project', 'nuro_branch_management'],
    'data': [
        'security/security.xml',
        'security/ir.model.access.csv',
        'views/nuro_budget.xml',
    ],
}