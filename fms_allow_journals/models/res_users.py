from odoo import api, fields, models

class ResUsers(models.Model):
    _inherit = 'res.users'

    journal_ids = fields.Many2many('account.journal', 'user_journal_ref', 'user_id', 'journal_id', 'Allow Journals',
    default = lambda self: self.env['account.journal'].search([('type', 'in', ('sale','purchase'))]))

    # @api.model
    # def default_get(self, fields):
    #     res = super(ResUsers, self).default_get(fields)
    #     print(self)
    #
    #     return res