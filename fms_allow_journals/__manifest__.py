
{
    'name': "FMS Allow Journals",
    'version': '13.0.2.0.0',
    'summary': """FMS Allow Journals""",
    'description': """""",
    'category': 'Business',
    'author': 'Nuro Software',
    'company': 'Nuro Software',
    'website': "https://www.nurosolution.com",
    'depends': ['base', 'account'],
    'data': [

        'security/security_view.xml',
        'views/res_users_view.xml',
    ],
    'license': "Other proprietary",
    'installable': True,
    'application': True,
}
