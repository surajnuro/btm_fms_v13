# -*- coding: utf-8 -*-
# Copyright  Nuro Solution Pvt Ltd
{
    'name': "FMS Ticket and TO Advance Report",
    'version': '13.0.2.0.0',
    'summary': """FMS Ticket and TO Advance Report""",
    'description': """""",
    'category': 'Business',
    'author': 'Nuro Software',
    'company': 'Nuro Software',
    'website': "https://www.nurosolution.com",
    'depends': ['base','website_support', 'project', 'nuro_project_task_management', 'nuro_ticket_summary_report', 'nuro_stock_xlsx_report'],
    'data': [
        'wizard/additional_material_view.xml',
        'wizard/material_compare_view.xml',
        'wizard/return_material_view.xml',
        'wizard/top_combind_view.xml',
    ],


    'license': "Other proprietary",
    'installable': True,
    'application': True,
}
