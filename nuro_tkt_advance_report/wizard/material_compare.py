from odoo import models, fields, api, _
import base64
import xlsxwriter
from datetime import datetime
from odoo.exceptions import ValidationError

class MaterialCompare(models.TransientModel):
    _name = 'material.compare.wiz'
    _description = 'Material Compare Report'

    top_id = fields.Many2one('project.task', 'TOP')
    from_date = fields.Date('From')
    to_date = fields.Date('To')
    project_id = fields.Many2one('project.project', 'Project')
    branch_id = fields.Many2one('res.branch', 'Branch')

    @api.constrains('from_date', 'to_date')
    def on_change_date_start(self):
        current_date = datetime.today().date()
        if self.from_date:
            st_date = self.from_date
            if st_date >= current_date:
                raise ValidationError('Start date can not be more than current date')
        if self.to_date:
            st_end = self.to_date
            if st_end < st_date or st_end > current_date:
                raise ValidationError('End date can not be less than start date and greater than current date')

    def print_material_compare(self):
        f_name = '/tmp/material_report.xlsx'
        workbook = xlsxwriter.Workbook(f_name)
        worksheet = workbook.add_worksheet('Report')
        worksheet.set_column('A:N', 12)
        style = workbook.add_format({
            'bold': 1,
            'border': 1,
            'align': 'center',
            'valign': 'vcenter'})
        style.set_font_size(8)
        align_value = workbook.add_format({
            'align': 'center',
            'valign': 'vcenter'})
        align_value.set_font_size(8)

        domain = [('work_type', '=', 'unschedulework'),('top_task', '=', False)]

        if self.from_date:
            domain += [('task_create_date', '>=', self.from_date)]
        if self.to_date:
            domain.append(('task_create_date', '<=', self.to_date))
        if self.project_id:
            domain.append(('project_id', '=', self.project_id.id))
        if self.top_id:
            domain.append(('task_id', '=', self.top_id.id))
        if self.branch_id:
            domain.append(('branch_id', '=', self.branch_id.id))
        top_ids = self.env['project.task'].sudo().search(domain)

        row = 4
        new_row = row + 1
        worksheet.merge_range('A1:G1', "DEEQA CONSTRUCTION & WATER WELL DRILLING CO. LTD.", style)
        if self.from_date and self.project_id:
            worksheet.merge_range('A2:G2', "Consumption reports comparison AS OF:" + str(
                self.to_date) + ' for ' + str(
                self.project_id.name), align_value)
        else:
            worksheet.merge_range('A2:G2', "ALL Consumption reports comparison", align_value)
        # worksheet.write('A4', "Date", style)
        worksheet.write('A4', "TOP", style)
        worksheet.write('B4', "Product", style)
        # worksheet.write('D4', "Warehouse", style)
        # worksheet.write('E4', "Location", style)
        worksheet.write('C4', "Planned Material", style)
        worksheet.write('D4', "Actual Consumption", style)
        worksheet.write('E4', "Variance", style)
        worksheet.write('F4', "Status", style)
        if not top_ids:
            return True
        for top in top_ids:

            for line in top.material_line:
                plan_qty = transfer_qty = 0.0
                plan_qty += line.request_qty
                if top.additional_material_line:
                    sql = '''
                            SELECT sum(request_qty)
                            FROM  task_material_request
                            WHERE additional_task_id = %s
                            AND product_id = %s
                        ''' % (top.id, line.product_id.id)
                    self.env.cr.execute(sql)
                    plan_qty_data = self.env.cr.fetchone()
                    plan_qty += plan_qty_data[0] if plan_qty_data else 0.0

                    sql = '''
                            SELECT sum(transfered_qty - returned_qty)
                            FROM  task_material_request
                            WHERE additional_task_id = %s
                            AND product_id = %s
                        ''' % (top.id, line.product_id.id)
                    self.env.cr.execute(sql)
                    trans_qty_data = self.env.cr.fetchone()
                    transfer_qty += trans_qty_data[0] if trans_qty_data else 0.0
                worksheet.write('A%s' % (new_row), top.task_id.name or 'Not Filled', align_value)
                worksheet.write('B%s' % (new_row), line.product_id.name or 'Not Filled', align_value)
                worksheet.write('C%s' % (new_row), plan_qty or 0, align_value)
                worksheet.write('D%s' % (new_row), transfer_qty or 0, align_value)
                new_row += 1

        workbook.close()
        f = open(f_name, 'rb')
        data = f.read()
        f.close()
        name = 'Consumption Compare  Report'
        out_wizard = self.env['xlsx.output'].create({'name': name + '.xlsx',
                                                     'xls_output': base64.encodebytes(data)})
        view_id = self.env.ref('nuro_ticket_summary_report.xlsx_output_form').id
        return {
            'type': 'ir.actions.act_window',
            'name': _(name),
            'res_model': 'xlsx.output',
            'target': 'new',
            'view_mode': 'form',
            'res_id': out_wizard.id,
            'views': [[view_id, 'form']],
        }

