from odoo import models, fields, api, _
import base64
import xlsxwriter
from datetime import datetime
from odoo.exceptions import ValidationError

class StockMove(models.Model):
    _inherit = 'stock.move'

    move_date = fields.Date(compute='_change_datetime_to_date', store=True)
    location_usage = fields.Selection([
        ('supplier', 'Vendor Location'),
        ('view', 'View'),
        ('internal', 'Internal Location'),
        ('customer', 'Customer Location'),
        ('inventory', 'Inventory Loss'),
        ('production', 'Production'),
        ('transit', 'Transit Location')], string='Location Type', compute='_find_location_type', store=True)
    location_dest_usage = fields.Selection([
        ('supplier', 'Vendor Location'),
        ('view', 'View'),
        ('internal', 'Internal Location'),
        ('customer', 'Customer Location'),
        ('inventory', 'Inventory Loss'),
        ('production', 'Production'),
        ('transit', 'Transit Location')], string='Location Type', compute='_find_location_type', store=True)

    def _find_location_type(self):
        '''fetch location type'''
        for move in self:
            if move.location_dest_id:
                move.location_dest_usage = move.location_dest_id.usage
            if move.location_id:
                move.location_usage = move.location_id.usage

    @api.depends('date')
    def _change_datetime_to_date(self):
        '''change move datetime to date'''
        for move in self:
            if move.date:
                move.move_date = move.date.date()
            else:
                move.move_date = False

class ExcelDownload(models.TransientModel):
    _inherit = 'inventory.wizard'

    def print_ticket_report_excel(self):
        f_name = '/tmp/inventory_consumption_report.xlsx'
        workbook = xlsxwriter.Workbook(f_name)
        worksheet = workbook.add_worksheet('Report')
        worksheet.set_column('A:K', 12)
        style = workbook.add_format({
            'bold': 1,
            'border': 1,
            'align': 'center',
            'valign': 'vcenter'})
        style.set_font_size(8)
        align_value = workbook.add_format({
            'align': 'center',
            'valign': 'vcenter'})
        align_value.set_font_size(8)

        end_date = self.date_end
        if not self.date_end:
            end_date = fields.Datetime.now()

        domain = ([('stock_move_id.state', '=', 'done'),
                   ('stock_move_id.inventory_id', '=', False),
                   ('stock_move_id.location_dest_id.usage', '!=', 'supplier'),
                   ('stock_move_id.location_id.usage', '!=', 'supplier')])
        if self.date_start:
            domain.append(('stock_move_id.picking_id.date_done', '>=', self.date_start))
        if self.date_end:
            domain.append(('stock_move_id.picking_id.date_done', '<=', end_date))
        if self.warehouse_id:
            domain.append(('stock_move_id.picking_id.picking_type_id.warehouse_id', '=', self.warehouse_id.id))
        if self.location_id:
            domain.append(('stock_move_id.picking_id.location_id', '=', self.location_id.id))
        layer_ids = self.env['stock.valuation.layer'].sudo().search(domain)

        total = 0.0
        total_qty = 0.0
        unit_price = return_qty = 0.0
        row = 4
        new_row = row + 1
        worksheet.merge_range('A1:M1', self.company_id.name, style)
        if self.date_start:
            date_ = datetime.strptime(str(self.date_start), '%Y-%m-%d').strftime('%B %Y')
            worksheet.merge_range('A2:M2', "MATERIALS CONSUMPTION REPORT FOR THE MONTH OF " + str(date_) or 'All',
                                  style)
        else:
            worksheet.merge_range('A2:M2', "MATERIALS CONSUMPTION REPORT FOR THE MONTH FOR All",
                                  style)
        worksheet.write('A3', "Location:", style)
        worksheet.merge_range('B3:M3', self.warehouse_id.name, style)
        worksheet.write('B4', "DATE OF ISSUE", style)
        worksheet.write('C4', "DESCRIPTION OF MATERIALS", style)
        worksheet.write('D4', "SKU NUMBER", style)
        worksheet.write('E4', "UNIT", style)
        worksheet.write('F4', "QTY ISSUED", style)
        worksheet.write('G4', "Return", style)
        worksheet.write('H4', 'Unit Price', style)
        worksheet.write('I4', 'Total Price', style)
        worksheet.write('J4', "Location", style)
        worksheet.write('K4', "Consumed Location", style)
        prod = {}
        for layer in layer_ids:
            prod[layer.product_id] = prod.get(layer.product_id, []) + [layer]
        for key, value in prod.items():
            value_price = price = qty = 0
            for line in value:
                stock = line[0].stock_move_id
                value_price += line.value
                price += line.unit_cost
                qty += line.stock_move_id.quantity_done
            if stock.location_id.usage == 'internal' and stock.location_dest_id.usage == 'internal':
                continue
            worksheet.write("A%s" % (new_row), stock.reference or "Not Filled", align_value)
            worksheet.write("B%s" % (new_row), str(stock.picking_id.date_done) or "Not Filled", align_value)
            worksheet.write("C%s" % (new_row), key.name or "Not Filled", align_value)
            worksheet.write("D%s" % (new_row), key.default_code or "Not Filled", align_value)
            worksheet.write("E%s" % (new_row), stock.product_uom.name or "Not Filled", align_value)
            worksheet.write("K%s" % (new_row), stock.picking_id.location_id.name or "Not Filled",
                            align_value)
            worksheet.write("J%s" % (new_row), stock.picking_id.location_dest_id.name or "Not Filled",
                            align_value)
            if stock.location_dest_id.usage in ['customer', 'inventory'] and stock.location_id.usage == 'internal':
                worksheet.write("F%s" % (new_row), -qty or "Not Filled", align_value)
                worksheet.write("G%s" % (new_row), 0, align_value)
                total_qty += qty
                unit_price += price
                return_qty += 0
                worksheet.write("H%s" % (new_row), -price or 0.0, align_value)
                worksheet.write("I%s" % (new_row), value_price or 0.0, align_value)
                total -= value_price

            if stock.location_id.usage in ['customer', 'inventory'] and stock.location_dest_id.usage == 'internal':
                worksheet.write("F%s" % (new_row), 0.0, align_value)
                worksheet.write("G%s" % (new_row), qty, align_value)
                total_qty -= qty
                unit_price += price
                return_qty += stock.quantity_done
                worksheet.write("H%s" % (new_row), price or 0.0, align_value)
                worksheet.write("I%s" % (new_row),
                                value_price or 0.0, align_value)
                total -= value_price
            new_row += 1
        row_total = new_row
        worksheet.merge_range('A%s:E%s' % (row_total, row_total), 'Total ', style)
        worksheet.write('F%s' % (row_total), total_qty, style)
        worksheet.write('G%s' % (row_total), return_qty, style)
        worksheet.write('H%s' % (row_total), unit_price, style)
        worksheet.write('I%s' % (row_total), total, style)

        workbook.close()
        f = open(f_name, 'rb')
        data = f.read()
        f.close()
        name = 'Inventory Consumption Report'

        out_wizard = self.env['inventory.wizard'].create({'name': name + '.xlsx', 'file_name': base64.b64encode(data)})
        view_id = self.env.ref('nuro_stock_xlsx_report.inventory_consumption_report_sheet_xls').id
        return {
            'type': 'ir.actions.act_window',
            'res_model': 'inventory.wizard',
            'target': 'new',
            'view_mode': 'form',
            'res_id': out_wizard.id,
            'views': [[view_id, 'form']],
        }
