from odoo import models, fields, api, _
import base64
import xlsxwriter
from datetime import datetime
from odoo.exceptions import ValidationError

class TOPcombined_report(models.TransientModel):
    _name = 'combined.report.wiz'
    _description = 'TOP Combined Report'

    top_id = fields.Many2one('project.task', 'TOP')
    from_date = fields.Date('From')
    to_date = fields.Date('To')
    project_id = fields.Many2one('project.project', 'Project')
    branch_id = fields.Many2one('res.branch', 'Branch')

    @api.constrains('from_date', 'to_date')
    def on_change_date_start(self):
        current_date = datetime.today().date()
        if self.from_date:
            st_date = self.from_date
            if st_date >= current_date:
                raise ValidationError('Start date can not be more than current date')
        if self.to_date:
            st_end = self.to_date
            if st_end < st_date or st_end > current_date:
                raise ValidationError('End date can not be less than start date and greater than current date')

    def print_top_combined_report(self):
        f_name = '/tmp/combined_report.xlsx'
        workbook = xlsxwriter.Workbook(f_name)
        worksheet = workbook.add_worksheet('Report')
        worksheet.set_column('A:N', 12)
        style = workbook.add_format({
            'bold': 1,
            'border': 1,
            'align': 'center',
            'valign': 'vcenter'})
        style.set_font_size(8)
        align_value = workbook.add_format({
            'align': 'center',
            'border': 1,
            'valign': 'vcenter'})
        align_value.set_font_size(8)

        domain = [('work_type', '=', 'unschedulework'),('top_task', '=', False),('task_id', '!=', False)]

        if self.from_date:
            domain += [('task_create_date', '>=', self.from_date)]
        if self.to_date:
            domain.append(('task_create_date', '<=', self.to_date))
        if self.project_id:
            domain.append(('project_id', '=', self.project_id.id))
        if self.top_id:
            domain.append(('task_id', '=', self.top_id.id))
        if self.branch_id:
            domain.append(('branch_id', '=', self.branch_id.id))
        top_ids = self.env['project.task'].sudo().search(domain)

        row = 4
        new_row = row + 1
        worksheet.merge_range('A1:G1', "DEEQA CONSTRUCTION & WATER WELL DRILLING CO. LTD.", style)
        if self.from_date and self.project_id:
            worksheet.merge_range('A2:G2', "TOP COMBINED REPORT AS OF:" + str(
                self.to_date) + ' for ' + str(
                self.project_id.name), align_value)
        else:
            worksheet.merge_range('A2:G2', "ALL TOP COMBINED REPORT", align_value)
        worksheet.write('A4', "TOP", style)
        worksheet.write('B4', "Invoiced Amount", style)
        worksheet.write('C4', "Product", style)
        worksheet.write('D4', "Material Proposed", style)
        worksheet.write('E4', "Actual Material Used", style)
        worksheet.write('F4', "Additional Material", style)
        worksheet.write('G4', "Material Consume ", style)

        if not top_ids:
            return True
        for top in top_ids:
            prod = {}
            prod_lst = []
            inv_amt = 0
            inv_ids = self.env['account.move'].search([('task_id', '=', top.id),
                                        ('type', '=', 'out_invoice')])
            if inv_ids:
                for inv in inv_ids:
                    inv_amt += inv.amount_total
            worksheet.write('A%s' % (new_row), top.task_id.name or 'Not Filled', align_value)
            worksheet.write('B%s' % (new_row), inv_amt or 0, align_value)
            if top.material_line:
                for line in top.material_line:
                    prod[line.product_id] = prod.get(line.product_id, []) + [line]
                for key, value in prod.items():
                    prod_lst.append(key.id)
                    qty = req_qty = add_transfer = add_request = 0.0
                    for line in value:
                        qty += line.request_qty
                        req_qty += line.transfered_qty
                    worksheet.write('C%s' % (new_row), key.name or 'Not Filled', align_value)
                    worksheet.write('D%s' % (new_row), req_qty or 0, align_value)
                    worksheet.write('E%s' % (new_row), qty or 0, align_value)
                    worksheet.write('F%s' % (new_row), 0, align_value)
                    worksheet.write('G%s' % (new_row),  0, align_value)
                    if top.additional_material_line:
                        for adline in top.additional_material_line:
                            if adline.product_id.id == key.id:
                                add_transfer += adline.transfered_qty
                                add_request += adline.request_qty
                        if add_request > 0.0 or add_transfer > 0.0:
                            worksheet.write('F%s' % (new_row), add_request, align_value)
                            worksheet.write('G%s' % (new_row), add_transfer, align_value)
                        add_transfer = add_request = 0.0
                    new_row += 1
            if top.additional_material_line:
                for aline in top.additional_material_line:
                    if prod_lst:
                        if aline.product_id.id not in prod_lst:
                            worksheet.write('D%s' % (new_row), 0, align_value)
                            worksheet.write('E%s' % (new_row), 0, align_value)
                            worksheet.write('C%s' % (new_row), aline.product_id.name or 'Not Filled', align_value)
                            worksheet.write('F%s' % (new_row), aline.request_qty or 0, align_value)
                            worksheet.write('G%s' % (new_row), aline.transfered_qty or 0, align_value)
                            new_row += 1
                    else:
                        worksheet.write('D%s' % (new_row), 0, align_value)
                        worksheet.write('E%s' % (new_row), 0, align_value)
                        worksheet.write('C%s' % (new_row), aline.product_id.name or 'Not Filled', align_value)
                        worksheet.write('F%s' % (new_row), aline.request_qty or 0, align_value)
                        worksheet.write('G%s' % (new_row), aline.transfered_qty or 0, align_value)
                        new_row += 1

            new_row += 1


        workbook.close()
        f = open(f_name, 'rb')
        data = f.read()
        f.close()
        name = 'TOP Combined  Report'
        out_wizard = self.env['xlsx.output'].create({'name': name + '.xlsx',
                                                     'xls_output': base64.encodebytes(data)})
        view_id = self.env.ref('nuro_ticket_summary_report.xlsx_output_form').id
        return {
            'type': 'ir.actions.act_window',
            'name': _(name),
            'res_model': 'xlsx.output',
            'target': 'new',
            'view_mode': 'form',
            'res_id': out_wizard.id,
            'views': [[view_id, 'form']],
        }