from odoo import models, fields, api, _
import base64
import xlsxwriter
from datetime import datetime
from odoo.exceptions import ValidationError

class ReturnReport(models.TransientModel):
    _name = 'return.report.wiz'
    _description = 'Return Material Report'

    top_id = fields.Many2one('project.task', 'TOP')
    from_date = fields.Date('From')
    to_date = fields.Date('To')
    project_id = fields.Many2one('project.project', 'Project')
    branch_id = fields.Many2one('res.branch', 'Branch')
    warehouse_id = fields.Many2one('stock.warehouse', 'Warehouse')

    @api.constrains('from_date', 'to_date')
    def on_change_date_start(self):
        current_date = datetime.today().date()
        if self.from_date:
            st_date = self.from_date
            if st_date >= current_date:
                raise ValidationError('Start date can not be more than current date')
        if self.to_date:
            st_end = self.to_date
            if st_end < st_date or st_end > current_date:
                raise ValidationError('End date can not be less than start date and greater than current date')

    def print_return_report(self):
        f_name = '/tmp/return_report.xlsx'
        workbook = xlsxwriter.Workbook(f_name)
        worksheet = workbook.add_worksheet('Return Validated By WH')
        worksheet2 = workbook.add_worksheet('Pending Validated By WH')
        worksheet.set_column('A:N', 12)
        style = workbook.add_format({
            'bold': 1,
            'border': 1,
            'align': 'center',
            'valign': 'vcenter'})
        style.set_font_size(8)
        align_value = workbook.add_format({
            'align': 'center',
            'border': 1,
            'valign': 'vcenter'})
        align_value.set_font_size(8)

        domain = [('work_type', '=', 'unschedulework'),('top_task', '=', False)]

        if self.from_date:
            domain += [('task_create_date', '>=', self.from_date)]
        if self.to_date:
            domain.append(('task_create_date', '<=', self.to_date))
        if self.project_id:
            domain.append(('project_id', '=', self.project_id.id))
        if self.branch_id:
            domain.append(('branch_id', '=', self.branch_id.id))
        if self.top_id:
            domain.append(('task_id', '=', self.top_id.id))
        top_ids = self.env['project.task'].sudo().search(domain)

        row = 4
        new_row = row + 1
        pending_row = 5
        worksheet.merge_range('A1:G1', "DEEQA CONSTRUCTION & WATER WELL DRILLING CO. LTD.", style)
        worksheet2.merge_range('A1:G1', "DEEQA CONSTRUCTION & WATER WELL DRILLING CO. LTD.", style)
        if self.from_date and self.project_id:
            worksheet.merge_range('A2:G2', "SUMMARY OF TASK ORDERS RETURN MATERIAL AS OF:" + str(self.to_date) + ' for ' + str(
                self.project_id.name), align_value)
            worksheet2.merge_range('A2:G2', "SUMMARY OF TASK ORDERS RETURN MATERIAL AS OF:" + str(
                self.to_date) + ' for ' + str(
                self.project_id.name), align_value)
        else:
            worksheet.merge_range('A2:G2', "ALL SUMMARY OF TASK ORDERS RETURN MATERIAL",  align_value)
            worksheet2.merge_range('A2:G2', "ALL SUMMARY OF TASK ORDERS RETURN MATERIAL",  align_value)
        worksheet.write('A4', "Date", style)
        worksheet.write('B4', "Picking Number", style)
        worksheet.write('C4', "TOP", style)
        worksheet.write('D4', "Product", style)
        worksheet.write('E4', "Warehouse", style)
        worksheet.write('F4', "Validated Return", style)

        worksheet2.write('A4', "Date", style)
        worksheet2.write('B4', "Picking Number", style)
        worksheet2.write('C4', "TOP", style)
        worksheet2.write('D4', "Product", style)
        worksheet2.write('E4', "Warehouse", style)
        worksheet2.write('F4', "Pending Return", style)

        if not top_ids:
            return True
        for top in top_ids:
                    for line in top.return_material_line:
                        picking_id = self.env['stock.move'].search([('material_request_id', '=', line.id)], limit=1).picking_id
                        if self.warehouse_id:
                            move_id = self.env['stock.move'].search([('material_request_id', '=', line.id),
                                                                     ('picking_id.warehouse_id', '=', self.warehouse_id.id)])
                        else:
                            move_id = self.env['stock.move'].search([('material_request_id', '=', line.id)])
                        if move_id and move_id.state == 'done':
                            worksheet.write('A%s' % (new_row), str(top.task_create_date) or 'Not Filled', align_value)
                            worksheet.write('B%s' % (new_row), picking_id.name or 'Not Filled', align_value)
                            worksheet.write('C%s' % (new_row), top.task_id.name or 'Not Filled', align_value)
                            worksheet.write('D%s' % (new_row), line.product_id.name or 'Not Filled', align_value)
                            worksheet.write('E%s' % (new_row), move_id.picking_id.warehouse_id.name or 'Not Filled', align_value)
                            worksheet.write('F%s' % (new_row), move_id.product_uom_qty or 0, align_value)
                            new_row += 1
                        if move_id and move_id.state not in ['done', 'cancel']:
                            worksheet2.write('A%s' % (pending_row), str(top.task_create_date) or 'Not Filled', align_value)
                            worksheet2.write('B%s' % (pending_row), picking_id.name or 'Not Filled', align_value)
                            worksheet2.write('C%s' % (pending_row), top.task_id.name or 'Not Filled', align_value)
                            worksheet2.write('D%s' % (pending_row), line.product_id.name or 'Not Filled', align_value)
                            worksheet2.write('E%s' % (pending_row), move_id.picking_id.warehouse_id.name or 'Not Filled', align_value)
                            worksheet2.write('F%s' % (pending_row), move_id.product_uom_qty or 0, align_value)
                            pending_row += 1

        workbook.close()
        f = open(f_name, 'rb')
        data = f.read()
        f.close()
        name = 'Return Material Report'
        out_wizard = self.env['xlsx.output'].create({'name': name + '.xlsx',
                                                     'xls_output': base64.encodebytes(data)})
        view_id = self.env.ref('nuro_ticket_summary_report.xlsx_output_form').id
        return {
            'type': 'ir.actions.act_window',
            'name': _(name),
            'res_model': 'xlsx.output',
            'target': 'new',
            'view_mode': 'form',
            'res_id': out_wizard.id,
            'views': [[view_id, 'form']],
        }
