from odoo import api, fields, models,_
from odoo.exceptions import ValidationError

class IrAttachment(models.Model):
    _inherit = 'ir.attachment'

    def unlink(self):
        delete_group = self.user_has_groups('nuro_del_restrict.group_user_access_delete')
        if self.env.user.id == 1:
            res = super(IrAttachment, self).unlink()
            return res
        if delete_group == True:
            res = super(IrAttachment, self).unlink()
            return res
        # if not (self.create_uid.id == self.env.user.id):
        #     raise ValidationError(_('Sorry Can not delete'))
        res = super(IrAttachment, self).unlink()
        return res