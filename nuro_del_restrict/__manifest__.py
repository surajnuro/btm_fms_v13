# -*- coding: utf-8 -*-
##############################################################################
#
#    Tech-Receptives Solutions Pvt. Ltd.
#    Copyright (C) 2009-TODAY Tech Receptives(<http://www.techreceptives.com>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Lesser General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Lesser General Public License for more details.
#
#    You should have received a copy of the GNU Lesser General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

{
    'name': 'Geo Restrict Delete',
    'license': 'LGPL-3',
    'summary': 'Restrict delete access for all users except admin',
    'complexity': "easy",
    'author': 'Geo Technosoft Pvt Ltd',
    'website': 'http://www.geotechnosoft.org',
    'depends': ['base'],
    'data': [
        'security/delete_secuity.xml',
    ],
    'demo': [],
    'installable': True,
    'auto_install': False,
    'application': True,
}
