from odoo import http,_
from odoo.http import request
import odoo.addons.web.controllers.main as main
from odoo.exceptions import ValidationError

class MyDataSet(main.DataSet):
	
	@http.route(['/web/dataset/call_kw', '/web/dataset/call_kw/<path:path>'], type='json', auth="user")
	def call_kw(self, model, method, args, kwargs, path=None):
		delete_access = request.env.user.has_group('nuro_del_restrict.group_user_access_delete')
		if method == 'unlink' and not delete_access:
			raise ValidationError(_("You have not access to delete record."))
		return super(MyDataSet, self).call_kw(model, method, args, kwargs, path)

