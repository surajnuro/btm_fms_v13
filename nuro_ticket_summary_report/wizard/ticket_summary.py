from odoo import models, fields, api, _
import base64
import xlsxwriter
from datetime import datetime
import pandas as pd

class TicketSummary(models.TransientModel):
    _name = 'ticket.summary.wiz'

    from_date = fields.Date('From Date')
    to_date = fields.Date('To Date')
    team_id = fields.Many2one('tkt.team', 'Team')
    ticket_type = fields.Selection([('checker', 'Checker Team'),
                                    ('unticket', 'UN Ticket')], string="Ticket Type")
    work_type = fields.Selection([('regularmain', 'Routine Maintenance'),
                                  ('unschedulework', 'Unschedule Work'),
                                  ('work_order', 'Work Order')], string="Work Type", copy=False)
    branch_id = fields.Many2one('res.branch', 'Sector')
    project_id = fields.Many2one('project.project', 'Project')

    def print_tkt_summary_report(self):
        f_name = '/tmp/ticket_summary_report.xlsx'
        workbook = xlsxwriter.Workbook(f_name)
        sheet1 = 'report'
        sheet2 = 'pie'
        sheet3 = 'bar'
        worksheet = workbook.add_worksheet(sheet1)
        worksheet2 = workbook.add_worksheet(sheet2)
        worksheet3 = workbook.add_worksheet(sheet3)
        writer = pd.ExcelWriter(f_name, engine='xlsxwriter',  date_format='mmmm dd yyyy')


        domain = []
        if self.team_id:
            domain += [('team_id', '=', self.team_id.id)]
        if self.work_type:
            domain += [('work_type', '=', self.work_type)]
        if self.from_date:
            domain += [('receive_date', '>=', self.from_date)]
        if self.to_date:
            domain += [('receive_date', '<=', self.to_date)]
        if self.ticket_type:
            domain += [('ticket_type', '=', self.ticket_type)]
        if self.branch_id:
            domain += [('branch_id', '=', self.branch_id.id)]
        if self.project_id:
            domain += [('project_id', '=', self.project_id.id)]
        tickets = self.env['website.support.ticket'].sudo().search(domain)
        row = 4
        new_row = row + 1

        column = ['Ticket', 'Ticket Description', 'Site', 'Location', 'Team', 'Received Date', 'Work Category',
                  'Start Date',
                  'Finish Date', 'Ticket Status', 'TOP No.', 'Remark']
        column = []
        df1 = pd.DataFrame(column)
        df1.to_excel(writer, "Report", startrow=new_row, startcol=1, index=False)
        worksheet = writer.sheets["Report"]
        worksheet.set_column('A:K', 12)
        style = workbook.add_format({
            'bold': 1,
            'border': 1,
            'align': 'center',
            'valign': 'vcenter'})
        style.set_font_size(8)
        align_value = workbook.add_format({
            'align': 'center',
            'valign': 'vcenter'})
        align_value.set_font_size(8)
        worksheet.merge_range('A1:J1', "DEEQA CONSTRUCTION & WATER WELL DRILLING CO. LTD.", style)
        if self.to_date and self.from_date:
            date = datetime.strptime(self.to_date, '%Y-%m-%d')
            worksheet.merge_range('A2:G2', "REPORT FORMAT FOR Ticket Summary Report for Date OF ",style)
            worksheet.merge_range('H2:J2', self.from_date + ' ' + 'To' + ' '+ self.to_date, style)
        else:
            worksheet.merge_range('A2:J2', " REPORT FORMAT FOR TICKET SUMMARY REPORT", style)
        worksheet.write('A4', "Ticket", style)
        worksheet.write('B4', "Ticket Description", style)
        worksheet.write('C4', "Site", style)
        worksheet.write('D4', "Location", style)
        worksheet.write('E4', "Team", style)
        worksheet.write('F4', "Received Date", style)
        worksheet.write('G4', "Work Category", style)
        worksheet.write('H4', 'Start Date', style)
        worksheet.write('I4', 'Finish Date', style)
        worksheet.write('J4', 'Ticket Status', style)
        worksheet.write('K4', 'TOP No.', style)
        worksheet.write('L4', 'Remark', style)

        for tkt in tickets:
            if tkt.states == 'rejected':
                worksheet.write("J%s" % (new_row), 'Canceled' or "Not Filled", align_value)
            if tkt.states == 'customer_close':
                worksheet.write("J%s" % (new_row), 'Completed' or "Not Filled", align_value)
            if tkt.states not in ['customer_close', 'rejected']:
                worksheet.write("J%s" % (new_row), 'In Progress' or "Not Filled", align_value)
            worksheet.write("A%s" % (new_row), tkt.client_tic_no or "Not Filled", align_value)
            worksheet.write("B%s" % (new_row), tkt.subject or "Not Filled", align_value)
            worksheet.write("C%s" % (new_row), tkt.site_id.name or "Not Filled", align_value)
            worksheet.write("D%s" % (new_row), tkt.location_comp or '-', align_value)
            worksheet.write("E%s" % (new_row), tkt.team_id.name or '-', align_value)
            worksheet.write("F%s" % (new_row), str(tkt.receive_date) or 0, align_value)
            worksheet.write("G%s" % (new_row), tkt.request_type.request_name or '-' , align_value)
            if tkt.start_date:
                # start_date = datetime.strptime(str(tkt.start_date), "%Y-%m-%d %H:%M:%S").date()
                # seconds = (int(tkt.start_date) - 25569) * 86400.0
                # date = datetime.utcfromtimestamp(seconds)
                # row_date = date.strftime('%Y/%m/%d')
                worksheet.write("H%s" % (new_row), str(tkt.start_date.date()) or "-", align_value)
            else:
                worksheet.write("H%s" % (new_row), str(tkt.receive_date) or "-", align_value)
            worksheet.write("I%s" % (new_row), str(tkt.close_date) or "-", align_value)
            worksheet.write("K%s" % (new_row), tkt.quotation_id.name or ' ', align_value)
            new_row += 1

        domain2 = [('states', 'not in', ('customer_close','rejected'))]
        if self.work_type:
            domain2 += [('work_type', '=', self.work_type)]
        if self.from_date:
            domain2 += [('receive_date', '>=', self.from_date)]
        if self.to_date:
            domain2 += [('receive_date', '<=', self.to_date)]
        if self.branch_id:
            domain2 += [('branch_id', '=', self.branch_id.id)]
        if self.project_id:
            domain2 += [('project_id', '=', self.project_id.id)]
        if self.ticket_type:
            domain2 += [('ticket_type', '=', self.ticket_type)]
        #
        domain3 = [('states', '=', 'customer_close')]
        if self.work_type:
            domain3 += [('work_type', '=', self.work_type)]
        if self.from_date:
            domain3 += [('receive_date', '>=', self.from_date)]
        if self.to_date:
            domain3 += [('receive_date', '<=', self.to_date)]
        if self.branch_id:
            domain2 += [('branch_id', '=', self.branch_id.id)]
        if self.project_id:
            domain2 += [('project_id', '=', self.project_id.id)]
        if self.ticket_type:
            domain3 += [('ticket_type', '=', self.ticket_type)]
        #
        #
        progress_tickets = self.env['website.support.ticket'].sudo().search(domain2)
        total_compleate = self.env['website.support.ticket'].search(domain3)
        # if len(total_tickets) > 1:
        #     percentage = float(len(progress_tickets) / len(total_tickets)) * 100
        row = 0
        col = 0
        farm_1 = {'Count of Progress': len(progress_tickets), 'Count of Completed': len(total_compleate)}
        df = pd.DataFrame([farm_1], index=['Farm 1'])
        df.to_excel(writer, sheet_name=sheet2)
        workbook = writer.book
        worksheet2 = writer.sheets[sheet2]
        worksheet2.merge_range(row, col, row + 1, col + 3, " ")
        row += 2
        for item in range(0, 1):
            worksheet2.merge_range(row, col, row, col + 1, 'Count of Progress')
            col += 2
            worksheet2.merge_range(row, col, row, col + 1, len(progress_tickets))
            col = 0
            row += 1
            break
        for item in range(0, 1):
            worksheet2.merge_range(row, col, row, col + 1, 'Count of Completed')
            col += 2
            worksheet2.merge_range(row, col, row, col + 1, len(total_compleate))
            col = 0
            row += 1
            break
        # ==============================pie chart===================


        chart = workbook.add_chart({'type': 'pie'})
        chart.add_series({
            'name': 'Ticket Completion %',
            'marker': {'type': 'diamond', 'size': 7},
            'data_labels': {'position': 'inside_end','series_name': True, 'leader_lines': True,
                            'percentage': True},
            'categories': '=' + sheet2 + '!$B$1:$C$1',
                    'values': '=' + sheet2 + '!$B$2:$C$2',
                    'points': [
                        {'fill': {'color': '#294704'}},
                        {'fill': {'color': '#bfab13'}},
                    ],
        })
        chart.set_chartarea({
            'border': {'none': True},
            'fill': {'color': '#787878'}
        })
        # chart.set_rotation(50)
        chart.set_style(10)

        worksheet2.insert_chart('A10', chart, {'x_offset': 25, 'y_offset': 10})
        # ==================================bar chart========================

        chart = workbook.add_chart({'type': 'bar'})
        chart.add_series({
            'name': 'Ticket Status Count',
            'categories': '=' + sheet2 + '!$B$1:$C$1',
            'values': '=' + sheet2 + '!$B$2:$C$2',
            # 'data_labels': {'position': 'inside_end','series_name': True, 'leader_lines': True,
            #                 'percentage': True},
            'points': [
                {'fill': {'color': '#294704'}},
                {'fill': {'color': '#bfab13'}},
            ],
        })
        chart.set_style(10)
        chart.set_chartarea({
            'border': {'none': True},
            'fill': {'color': '#787878'}
        })
        worksheet2.insert_chart('K10', chart, {'x_offset': 25, 'y_offset': 10})

        # writer.save()
        col = 0
        row = 0
        row = 4
        new_row = row + 1
        column = ['Sector Name', 'Inprogress States', 'Customer Close States']

        df3 = pd.DataFrame(column)
        df3.to_excel(writer, "Pivot", startrow=new_row, startcol=1, index=False)
        worksheet3 = writer.sheets["Pivot"]
        worksheet3.set_column('A:K', 12)
        worksheet3.merge_range('A1:D1', "DEEQA CONSTRUCTION & WATER WELL DRILLING CO. LTD.", style)
        worksheet3.write('A4', "Sector Name", style)
        # worksheet3.write('B4', "New States", style)
        # worksheet3.write('C4', "Waiting States", style)
        # worksheet3.write('D4', "Approved States", style)
        worksheet3.write('B4', "Inprogress States", style)
        # worksheet3.write('F4', "Staff close States", style)
        worksheet3.write('C4', "Customer Close States", style)
        # worksheet3.write('H4', 'Reject States', style)
        # worksheet3.write('I4', 'Total Tickets', style)
        # sectors = self.env['project.project'].search([('project_category_id.name', '=', 'FMS')])
        # total_progress = total_compleate = 0
        # if sectors:
        #     for sec in sectors:
        #         new = waiting = inprogress = total = staff = complete = cancel = approved = 0
        #         for tkt in tickets:
        #             if tkt.project_name.id == sec.id:
        #                 if tkt.states == 'rejected':
        #                     continue
        #                 if tkt.states == 'customer_close':
        #                     complete = complete + 1
        #                     total_compleate = total_compleate + 1
        #                 if tkt.states != 'customer_close':
        #                     inprogress = inprogress + 1
        #                     total_progress = total_progress + 1
        #         worksheet3.write("A%s" % (new_row), sec.name or '-', align_value)
        #         # worksheet3.write("B%s" % (new_row), new or '-', align_value)
        #         # worksheet3.write("C%s" % (new_row), waiting or '-', align_value)
        #         # worksheet3.write("D%s" % (new_row), approved or '-', align_value)
        #         worksheet3.write("B%s" % (new_row), inprogress or '-', align_value)
        #         # worksheet3.write("F%s" % (new_row), staff or '-', align_value)
        #         worksheet3.write("C%s" % (new_row), complete or '-', align_value)
        #         # worksheet3.write("H%s" % (new_row), cancel or '-', align_value)
        #         # worksheet3.write("I%s" % (new_row), total or '-', align_value)
        #         new_row += 1
        #         # new = waiting = inprogress = total = staff = customer = cancel = approved = 0
        #     worksheet3.write("A%s" % (new_row), 'Total', style)
        #     worksheet3.write("B%s" % (new_row), total_progress or 0, style)
        #     worksheet3.write("C%s" % (new_row), total_compleate or 0, style)

        workbook.close()
        f = open(f_name, 'rb')
        data = f.read()
        f.close()
        name = 'Ticket Summary Report'
        dt = ' '+ 'From' + ' ' + str(self.from_date) + ' ' + 'To' + ' ' + str(self.to_date)
        if self.from_date and self.to_date:
            out_wizard = self.env['xlsx.output'].create({'name': name + dt + '.xlsx',
                                                         'xls_output': base64.encodebytes(data)})
        else:
            out_wizard = self.env['xlsx.output'].create({'name': name + '.xlsx',
                                                         'xls_output': base64.encodebytes(data)})
        view_id = self.env.ref('nuro_ticket_summary_report.xlsx_output_form').id
        return {
            'type': 'ir.actions.act_window',
            'name': _(name),
            'res_model': 'xlsx.output',
            'target': 'new',
            'view_mode': 'form',
            'res_id': out_wizard.id,
            'views': [[view_id, 'form']],
        }