
{
    'name': "Ticket Summary Report",
    'version': '11.0.2.0.0',
    'summary': """Ticket Report""",
    'description': """""",
    'category': 'Business',
    'author': 'Nuro Software',
    'company': 'Nuro Software',
    'website': "https://www.nurosolution.com",
    'depends': ['website_support', 'sale','project'],
    'data': [
       'wizard/ticket_summary_view.xml',
       'wizard/xlsx_output_view.xml',
    ],
    'license': "AGPL-3",
    'installable': True,
    'application': True,
}
