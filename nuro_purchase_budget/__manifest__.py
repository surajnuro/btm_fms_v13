# -*- coding: utf-8 -*-
# Copyright  Nuro Solution Pvt Ltd
{
    'name': 'Purchase Budget',
    'version': '13.0',
    'author': 'Nuro Solution Pvt.Ltd',
    'website': 'nurosolution.com',
    'category': 'Generic Modules',
    'summary': 'Purchase Budget',
    'description': """
    """,
    'depends': ['base', 'purchase', 'purchase_order_cancel', 'project','nuro_project_task_management'],
    'data': [
        'security/ir.model.access.csv',
        'security/security_view.xml',
        'wizard/submit_approval_view.xml',
        'wizard/po_approval_wiz_view.xml',
        'wizard/amount_revise_view.xml',
        'views/purchase_budget_view.xml',
        'views/purchase_order_view.xml',
        'views/res_sector_view.xml',
    ],
    'auto_install': False,
    'installable': True,


}
