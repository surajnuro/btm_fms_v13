from odoo import api, fields, models, _
from odoo.exceptions import ValidationError, Warning
from datetime import datetime

class SubmitApproval(models.TransientModel):
    _name="submit.approval.wiz"

    user_id = fields.Many2one('res.users','Submit to')
    budget_id = fields.Many2one('purchase.budget')
    check_boolean = fields.Boolean()

    def send_for_approval_budget(self):
        active_id = self.env.context.get('active_id')
        budget = self.env['purchase.budget'].browse(active_id)
        if budget:
            budget.write({'state':'waiting',
                              'requested_by': self.env.user.id,
                              'requested_to': self.user_id.id,
                              'request_date': datetime.now()})
        return True

    @api.model
    def default_get(self, fields):
        res = super(SubmitApproval, self).default_get(fields)
        budget_id = self.env['purchase.budget'].search([('id', 'in', self._context.get('active_ids'))])
        if budget_id:
            res['budget_id'] = budget_id.id
        return res

    @api.onchange('budget_id')
    def onchange_budget(self):
        groups = self.env.ref('nuro_purchase_budget.group_approve_budget')
        user_lst = []
        for user in groups.sudo().users:
            user_lst.append(user.id)
        return {'domain': {'user_id': [('id', 'in', user_lst)]}}









