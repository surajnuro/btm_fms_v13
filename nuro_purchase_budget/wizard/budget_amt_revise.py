from odoo import api, fields, models, _

class AmountRevise(models.TransientModel):
    _name="amount.revise.wz"

    amount = fields.Float('AMount')

    def confirm(self):
        active_id = self.env.context.get('active_id')
        budget = self.env['purchase.budget'].browse(active_id)
        if budget:
            budget.write({'old_budget_amt': budget.amount,
                'revise_budget_amt': self.amount})
            budget.amount = self.amount
        return True