from odoo import api, fields, models, _
from odoo.exceptions import ValidationError, Warning
from datetime import datetime

class SubmitApprovalPO(models.TransientModel):
    _name="submit.approval.po"

    user_id = fields.Many2one('res.users','Submit to')

    def send_for_approval(self):
        active_id = self.env.context.get('active_id')
        po = self.env['purchase.order'].browse(active_id)
        if po:
            po.write({'state':'waiting',
                          'is_over_amt': True})
        return True


# class Users(models.Model):
#     _inherit = "res.users"
#
#     @api.depends('groups_id')
#     def check_approval(self):
#         for users in self:
#             if users.has_group('purchase.group_purchase_manager'):
#                 users.is_approval = True
#             else:
#                 users.is_approval = False
#     is_purchase_approval= fields.Boolean('Is Approved', compute='check_approval', store=True)
