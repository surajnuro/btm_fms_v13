from odoo import api, fields, models, _
from datetime import datetime

class ResSector(models.Model):
    _name = 'res.sector'

    name = fields.Char('Name')