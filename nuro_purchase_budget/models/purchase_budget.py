from odoo import api, fields, models, _
from datetime import datetime
from odoo.exceptions import ValidationError

class PurchaseBudget(models.Model):
    _name = 'purchase.budget'
    _inherit = ['mail.thread', 'mail.activity.mixin', 'portal.mixin']

    name = fields.Char('Name', track_visibility='onchange')
    date_to = fields.Date('Date To', track_visibility='onchange')
    date_from = fields.Date('Date From', track_visibility='onchange')
    state = fields.Selection([('draft', 'Draft'),
                              ('waiting', 'Waiting for Approval'),
                              ('approved', 'Approved'),
                              ('cancel', 'Cancel')], default='draft', track_visibility='onchange')
    amount = fields.Float('Budgeted Amount')
    requested_by = fields.Many2one('res.users', 'Requested By')
    requested_to = fields.Many2one('res.users', 'Requested To')
    request_date = fields.Date('Requested Date')
    approved_date = fields.Date('Approved Date')
    remaining_amt = fields.Float('Remaining Amt', compute='calculate_budget_Amt', store=True)
    over_budget = fields.Float('Over Budget Amt', compute='calculate_over_budget', store=True)
    achived_amt = fields.Float('PO Amount')
    purchase_ids = fields.Many2many('purchase.order', 'budget_po_ref', 'budget_id', 'po_id', 'Purchase Lines')
    res_sector_id = fields.Many2one('res.sector', 'Sector', track_visibility='onchange')
    project_id = fields.Many2one('project.project', 'Project', track_visibility='onchange')
    purchase_type = fields.Selection([('construction', 'Construction FM'), ('maintenance', 'Regular maintenance'),
                                      ('life_support', 'Life support'), ('it', 'IT'), ('project', 'Other Projects'),
                                      ('task_order', 'Task Order'), ('vehicle_maintenance', 'Vehicle Maintenance'),
                                      ('warehouse_re_stocking', 'Warehouse Re-Stocking')], string="Purchase Type", track_visibility='onchange')
    revise_budget_amt = fields.Float('Revise Budget Amount', track_visibility='onchange')
    old_budget_amt = fields.Float('Old Budget Amount', track_visibility='onchange')

    @api.depends('achived_amt', 'amount')
    def calculate_budget_Amt(self):
        for rec in self:
            if rec.amount < rec.achived_amt:
                rec.remaining_amt = 0.0
            else:
                rec.remaining_amt = rec.amount - rec.achived_amt

    @api.depends('achived_amt','amount')
    def calculate_over_budget(self):
        for rec in self:
            if rec.amount < rec.achived_amt:
                rec.over_budget = rec.amount - rec.achived_amt
            else:
                rec.over_budget = 0

    def approved(self):
        self.write({'state': 'approved',
                    'approved_date': datetime.now()})

    def cancel(self):
        self.write({'state': 'cancel',
                    })

    @api.constrains('date_to', 'date_from', 'res_sector_id', 'purchase_type', 'project_id')
    def _check_date(self):
        for rec in self:
            query = """
                            SELECT id from purchase_budget where (date_from BETWEEN  %s and  %s
                            or date_to BETWEEN  %s and %s or date_from <= %s and date_to >= %s) and  
                            res_sector_id = %s and project_id = %s and  purchase_type = %s
                        """
            self._cr.execute(query, (rec.date_from, rec.date_to,
                                     rec.date_from, rec.date_to,
                                     rec.date_from,
                                     rec.date_to, rec.res_sector_id.id, rec.project_id.id, rec.purchase_type))
            data = self._cr.fetchall()
            if len(data) > 1:
                raise ValidationError(_('The budget is already created from give date range !!!'))


