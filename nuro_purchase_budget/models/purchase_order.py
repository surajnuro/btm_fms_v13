from odoo import api, fields, models, _
from datetime import datetime

class PurchaseOrder(models.Model):
    _inherit = 'purchase.order'

    budget_amt = fields.Float('Total Budget', compute='budget_amount_find', store=True)
    budget_achieved = fields.Float('Total Budget Utilize', compute='budget_amount_find', store=True)
    remaining_budget = fields.Float('Remaining Budget Amt.', compute='budget_amount_find', store=True)
    budget_id = fields.Many2one('purchase.budget', compute='budget_amount_find', store=True)
    state = fields.Selection(selection_add=[('waiting', 'Waiting For Approval')])
    res_sector_id = fields.Many2one('res.sector', 'Sector')
    is_over_amt = fields.Boolean(default=True)

    @api.onchange('amount_total')
    def onchange_over_amt(self):
        if self.amount_total:
            if self.amount_total > self.budget_amt:
                self.is_over_amt = False
            else:
                self.is_over_amt = True


    @api.depends('project_id', 'date_order', 'purchase_type','res_sector_id')
    def budget_amount_find(self):
        for rec in self:
            # date_order = datetime.strptime(rec.date_order, "%Y-%m-%d %H:%M:%S").date()
            budget = self.env['purchase.budget'].sudo().search([('date_from', '<=', str(rec.date_order.date())),
                                                         ('date_to', '>=', str(rec.date_order.date())),
                                                         ('state', '=', 'approved'),
                                                         ('res_sector_id', '=', rec.res_sector_id.id),
                                                         ('project_id', '=', rec.project_id.id),
                                                         ('purchase_type', '=', rec.purchase_type),
                                                         ], limit=1)
            print(budget)
            if budget:
                rec.budget_amt = budget.amount
                rec.remaining_budget = budget.remaining_amt
                rec.budget_achieved = budget.achived_amt
                rec.budget_id = budget.id



    # def button_confirm(self):
    #     res = super(PurchaseOrder, self).button_confirm()
    #     if self.budget_id:
    #         self.budget_id.achived_amt += self.amount_untaxed
    #         sql = ('''INSERT INTO budget_po_ref (budget_id,po_id) VALUES (%s, %s)''')
    #         self._cr.execute(sql, (self.budget_id.id, self.id))
    #     return res

    def button_cancel(self):
        res = super(PurchaseOrder, self).button_cancel()
        if self.budget_id:
            cut_id = 0
            self.budget_id.achived_amt -= self.amount_untaxed
            if self.budget_id.purchase_ids:
                for rec in self.budget_id.purchase_ids:
                    if rec.id == self.id:
                        sql = ('''delete from budget_po_ref where budget_id = %s and po_id = %s''' % (self.budget_id.id, self.id))
                        self._cr.execute(sql)
        return res

    def button_confirm(self):
        res = super(PurchaseOrder, self).button_confirm()
        for order in self:
            if order.state == 'waiting':
                if self.budget_id:
                    self.budget_id.achived_amt += self.amount_untaxed
                    sql = ('''INSERT INTO budget_po_ref (budget_id,po_id) VALUES (%s, %s)''')
                    self._cr.execute(sql, (self.budget_id.id, self.id))
                order._add_supplier_to_product()
                # Deal with double validation process
                if order.company_id.po_double_validation == 'one_step' \
                        or (order.company_id.po_double_validation == 'two_step' \
                            and order.amount_total < self.env.user.company_id.currency_id.compute(
                            order.company_id.po_double_validation_amount, order.currency_id)) \
                        or order.user_has_groups('purchase.group_purchase_manager'):
                    order.button_approve()
                else:

                    order.write({'state': 'to approve'})
            else:
                if self.budget_id:
                    self.budget_id.achived_amt += self.amount_untaxed
                    sql = ('''INSERT INTO budget_po_ref (budget_id,po_id) VALUES (%s, %s)''')
                    self._cr.execute(sql, (self.budget_id.id, self.id))
        return res

    def approve(self):
        self.button_confirm()


