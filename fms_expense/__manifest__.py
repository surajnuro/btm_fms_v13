# -*- coding: utf-8 -*-
# Copyright  Nuro Solution Pvt Ltd
{
    'name': 'FMS Expense',
    'category': 'Expense',
    'summary': 'This module handel expense according to sector and project',
    'description': """
        This module handel expense according to sector and project
    """,
    'author': 'Nuro Solution Pvt Ltd',
    'website': 'http://www.nurosolution.com',
    'company': 'Nuro Solution Pvt Ltd',
    'license': 'OPL-1',
    'depends': ['hr_expense', 'account','project', 'nuro_expense_management', 'project', 'website_support' , 'nuro_project_task_management'],
    'data': [
        'security/ir.model.access.csv',
        'views/hr_expense_sheet.xml',
        'views/hr_expense_view.xml',

    ],

    'installable': True,
    'application': True,
}