from odoo import api, fields, models,_
from odoo.exceptions import UserError, ValidationError
from datetime import datetime

class ExpenseSheet(models.Model):
    _inherit = 'hr.expense.sheet'

    branch_id = fields.Many2one('project.project', 'Sector')
    project_id = fields.Many2one('project.project', 'Project')
    expense_line_ids = fields.One2many('hr.expense', 'sheet_id', string='Expense Lines', states={'approve': [('readonly', False)], 'done': [('readonly', True)], 'post': [('readonly', True)]}, copy=False)
    document_line = fields.One2many('document.line', 'exp_sheet_id')
    bill_number = fields.Char('Bill Number')
    vendor_reference = fields.Char('Vendor Reference')
    req_date = fields.Date('Requested Date')
    reference = fields.Char('Reference')
    partner_id = fields.Many2one('res.partner', 'Partner')
    employee_id = fields.Many2one('hr.employee', string="Employee", required=False, readonly=True,
                                  states={'draft': [('readonly', False)], 'refused': [('readonly', False)]})
    doc = fields.Binary()
    file_name = fields.Char('File Name')
    attachment_ids = fields.Many2many('ir.attachment', 'exp_attach_ref', 'exp_id', 'attach_id', 'Document')


    @api.constrains('expense_line_ids', 'employee_id')
    def _check_employee(self):
        return True
        # employee_ids = self.expense_line_ids.mapped('employee_id')
        # if len(employee_ids) > 1 or (len(employee_ids) == 1 and employee_ids != self.employee_id):
        #     raise ValidationError(_('You cannot add expense lines of another employee.'))

    def check_consistency(self):
        for rec in self:
            expense_lines = rec.expense_line_ids
            if not expense_lines:
                continue
            if any(expense.payment_mode != expense_lines[0].payment_mode for expense in expense_lines):
                raise UserError(_("Expenses must have been paid by the same entity (Company or employee)"))

    def action_sheet_bill_create(self):
        invoice = self.env['account.move']
        inv_lst = []
        if self.expense_line_ids:
            for rec in self.expense_line_ids:
                if not rec.account_id:
                    raise UserError(_("Please select Account in Expense Line"))
                inv_lst.append((0, 0, {
                    'name': rec.name,
                    'account_id': rec.account_id.id,
                    'analytic_account_id': rec.analytic_account_id.id or False,
                    'price_unit': rec.total_amount
                }))
        inv_id = invoice.create({
            'partner_id': self.vendor_id.id,
            'ref': self.expense_ref,
            'invoice_line_ids': inv_lst,
            'expense_id': self.id,
            'type': 'in_invoice',
            'branch_id': self.branch_id.id,
            'project_id': self.project_id.id,
        })
        inv_id.action_post()
        # self.date = datetime.now()
        if inv_id:
            self.invoice_id = inv_id.id
            self.write({'state': 'post'})

    def action_sheet_journal_entry(self):
        #  Method inherit for send project and sector from expense
        ''' method to create account.move and move.line for expense'''

        account_move = self.env['account.move']
        ml_lst = []

        if any(sheet.state != 'approve' for sheet in self):
            raise UserError(_("You can only generate accounting entry for approved expense(s)."))

        if any(not sheet.journal_id for sheet in self):
            raise UserError(_("Expenses must have an expense journal specified to generate accounting entries."))
        if self.expense_line_ids:
            total_amt = 0.0
            for exp in self.expense_line_ids:
                if not exp.account_id:
                    raise UserError(_("Please select Account in Expense Line"))
                company_currency = exp.company_id.currency_id
                # total, total_currency, move_lines = exp._compute_expense_totals(company_currency, move_lines,

                total_amt += exp.unit_amount
                ml_lst.append((0, 0, {
                    'name': exp.name,
                    'debit': exp.unit_amount,
                    'credit': 0,
                    'account_id': exp.account_id.id,
                    'branch_id': exp.branch_id.id,
                    'project_id': exp.project_id.id,
                    'currency_id': exp.currency_id.id,
                    'amount_currency': exp.unit_amount,
                    'analytic_account_id': exp.analytic_account_id.id,
                    'expense_id': exp.id}))
            ml_lst.append((0, 0, {
                'name': exp.name,
                'debit': 0,
                'credit': total_amt,
                'account_id': self.bank_journal_id.default_credit_account_id.id,
                'analytic_account_id': exp.analytic_account_id.id,
                'branch_id': exp.branch_id.id,
                'amount_currency': exp.unit_amount,
                'project_id': exp.project_id.id,
                # 'amount_currency': line['price'] > 0 and abs(line.get('amount_currency')) or - abs(line.get('amount_currency')),
                'currency_id': exp.currency_id.id,
                'expense_id': exp.id}))
        if ml_lst:
            move_id = account_move.create({'date': datetime.now().date(),
                                           'journal_id': self.bank_journal_id.id,
                                           'ref': self.name,
                                           'line_ids': ml_lst,
                                           })
            if move_id:
                move_id.post()
                self.account_move_id = move_id.id
        if not self.accounting_date:
            self.accounting_date = self.account_move_id.date
        self.write({'state': 'done'})

class DocumentLine(models.Model):
    _name = 'document.line'
    _rec_name = 'name'

    exp_sheet_id = fields.Many2one('hr.expense.sheet', ondelete='cascade')
    name = fields.Char('Document Name')
    doc_attach = fields.Binary('Attachment')
    file_name = fields.Char('File Name')

