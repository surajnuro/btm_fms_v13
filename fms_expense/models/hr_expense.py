from odoo import api, fields, models,_
from datetime import datetime
from odoo.exceptions import UserError

class Expense(models.Model):
    _inherit = 'hr.expense'

    branch_id = fields.Many2one('res.branch', string='Sector')
    project_id = fields.Many2one('project.project', string='Project')
    account_id = fields.Many2one('account.account', string='Account',
                                 states={'post': [('readonly', True)], 'done': [('readonly', True)]}, default=False,
                                 help="An expense account is expected", groups='nuro_expense_management.expense_post_account,account.group_account_manager')
    sheet_state = fields.Selection(related='sheet_id.state', store=True)
    employee_id = fields.Many2one('hr.employee', string="Employee", required=False, readonly=True,
                                  states={'draft': [('readonly', False)], 'refused': [('readonly', False)]})
    task_id = fields.Many2one('project.task')


