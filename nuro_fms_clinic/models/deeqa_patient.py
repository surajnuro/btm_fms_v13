from odoo import api, fields, models, _
from odoo.osv import expression

class FmsPatient(models.Model):
    _name = 'fms.patient'

    number = fields.Char('Name', copy=False, index=True)
    name = fields.Char('Name')
    mobile = fields.Char('Mobile')
    email = fields.Char('Email')
    is_emp = fields.Boolean('Is Employee')
    is_guest = fields.Boolean('Is Guest')
    is_temporary_staff = fields.Boolean('Is Temporary')
    guest_id = fields.Many2one('res.guest', 'Guest')
    temporary_staff_id = fields.Many2one('temporary.staff', 'Temporary Staff')
    employee_id = fields.Many2one('hr.employee', 'Employee')
    street1 = fields.Char('Permanent Address')
    street2 = fields.Char('Street2')
    city = fields.Char('City')
    state_id = fields.Many2one('res.country.state')
    country_id = fields.Many2one('res.country')
    zip = fields.Char('Zip')
    appointment_count = fields.Integer(compute='_count_record', string='Picking')
    lab_request_count = fields.Integer(compute='_count_record', string='Picking')
    precription_count = fields.Integer(compute='_count_record', string='Picking')

    def _count_record(self):
        count = 0
        picking = self.env['stock.picking']
        lab_test = self.env['lab.test.request']
        appointment = self.env['oeh.medical.appointment']
        for rec in self:
            picking_ids = picking.search([('appointment_id.patient_id', '=', rec.id)])
            rec.precription_count = len(picking_ids)
            lab_ids = lab_test.search([('patient_id', '=', self.id)])
            rec.lab_request_count = len(lab_ids)
            appoint_ids = appointment.search([('patient_id', '=', self.id)])
            rec.appointment_count = len(appoint_ids)

    def action_open_appointment(self):
        appointment = self.env['oeh.medical.appointment']
        appoint_ids = appointment.search([('patient_id', '=', self.id)])
        form_view = self.env.ref('nuro_fms_clinic.appointment_form_view').id
        tree_view = self.env.ref('nuro_fms_clinic.appointment_tree_view').id
        return {
            'name': _('Appointment'),
            'res_model': 'oeh.medical.appointment',
            'views': [
                (tree_view, 'tree'),
                (form_view, 'form'),
            ],
            'view_mode': 'tree,form',
            'type': 'ir.actions.act_window',
            'domain': [('id', 'in', appoint_ids.ids)]
        }

    def action_open_lab_request(self):
        lab = self.env['lab.test.request']
        lab_ids = lab.search([('patient_id', '=', self.id)])
        form_view = self.env.ref('nuro_fms_clinic.lab_test_request_form').id
        tree_view = self.env.ref('nuro_fms_clinic.lab_test_request_tree').id
        return {
            'name': _('Lab'),
            'res_model': 'lab.test.request',
            'views': [
                (tree_view, 'tree'),
                (form_view, 'form'),
            ],
            'view_mode': 'tree,form',
            'type': 'ir.actions.act_window',
            'domain': [('id', 'in', lab_ids.ids)]
        }

    def action_open_prescription(self):
        picking = self.env['stock.picking']
        picking_ids = picking.search([('appointment_id.patient_id', '=', self.id)])
        form_view = self.env.ref('stock.view_picking_form').id
        tree_view = self.env.ref('stock.vpicktree').id
        return {
            'name': _('picking'),
            'res_model': 'stock.picking',
            'views': [
                (tree_view, 'tree'),
                (form_view, 'form'),
            ],
            'view_mode': 'tree,form',
            'type': 'ir.actions.act_window',
            'domain': [('id', 'in', picking_ids.ids)]
        }

    @api.model
    def create(self, values):
        sequence = self.env['ir.sequence'].next_by_code('patient.sequence')
        if sequence:
            values['number'] = sequence
        return super(FmsPatient, self).create(values)

