from odoo import api, fields, models, _
from odoo.exceptions import ValidationError
import datetime
import time

class OeHealthAppointment(models.Model):
    _name = 'oeh.medical.appointment'
    _description = 'Appointment'
    _inherit = ['mail.thread', 'mail.activity.mixin', 'portal.mixin']
    _order = 'name desc'

    # @api.depends('prescription_lines.picking_id')
    def _picking_count(self):
        count = 0
        picking = self.env['stock.picking']
        for rec in self:
            picking_ids = picking.search([('appointment_id', '=', rec.id)])
            rec.picking_count = len(picking_ids)

    name = fields.Char('Name', index=True, copy=False)
    project_id = fields.Many2one('project.project', 'Project')
    branch_id = fields.Many2one('res.branch', 'Sector')
    patient_id = fields.Many2one('fms.patient', 'Patient')
    doctor_id = fields.Many2one('oeh.medical.physician', 'Doctor')
    date = fields.Date('Appointment Date', default=fields.Datetime.now)
    department_id = fields.Many2one('hr.department', 'Department')
    # project_id = fields.Many2one('project.category', 'Project')
    # sector_id = fields.Many2one('project.project', 'Sector')
    is_temporary_staff = fields.Boolean('Is Temporary Staff')
    temporary_staff_id = fields.Many2one('temporary.staff', 'Temporary Staff')
    labtest_type_ids = fields.Many2many('oeh.medical.labtest.types', 'labtest_app_rel', 'lab_id',
                                         'app_id', 'Lab Test Line')
    sex = fields.Selection([('male', 'Male'),
                            ('female', 'Female')], string='Gender')
    comments = fields.Text('Comments')
    state = fields.Selection([('draft', 'Draft'),
                              ('inpatient', 'In Patient'),
                              ('confirm', 'Confirm')], default='draft')
    appointment_imging_ids = fields.Many2many('oeh.medical.imaging', 'appointment_img_ref', 'appointment_id', 'img_id', 'Imaging Lines')
    imaging_lines = fields.One2many('imaging.lines', 'appointment_id')
    alcohol1 = fields.Selection([('Yes', 'Yes'), ('No', 'No')], string='Alcohol')
    smoker = fields.Selection([('Yes', 'Yes'), ('No', 'No')], string='Smoker')
    snuffer = fields.Selection([('Yes', 'Yes'), ('No', 'No')], string='Snuffer')
    iv_drug = fields.Selection([('Yes', 'Yes'), ('No', 'No')], string='IV Drug User ')
    dm = fields.Selection([('Yes', 'Yes'), ('No', 'No')], string='DM')
    htn = fields.Selection([('Yes', 'Yes'), ('No', 'No')], string='HTN')
    ihd = fields.Selection([('Yes', 'Yes'), ('No', 'No')], string='IHD')
    epilepsy = fields.Selection([('Yes', 'Yes'), ('No', 'No')], string='Epilepsy')
    asthma = fields.Selection([('Yes', 'Yes'), ('No', 'No')], string='Asthma')
    surgery = fields.Selection([('Yes', 'Yes'), ('No', 'No')], string='Surgery')
    illness = fields.Selection([('Yes', 'Yes'), ('No', 'No')], string='Illness')
    patient_type = fields.Selection([('in_patient', 'In Patent'),('out_patient','Out Patient')], default='in_patient', string='Patient Type')

    patient_name = fields.Char('Patient Name')
    age = fields.Char('Age')
    gender = fields.Char('Gender')
    country_id = fields.Many2one('res.country', 'Nationality')
    job_id = fields.Many2one('hr.job', 'Job title')
    id_no = fields.Char('ID No')

    jaundice = fields.Selection([('Yes', 'Yes'), ('No', 'No')], string='Jaundice')
    anemia = fields.Selection([('Yes', 'Yes'), ('No', 'No')], string='Anemia')
    dm = fields.Selection([('Yes', 'Yes'), ('No', 'No')], string='DM')
    epilepsy = fields.Selection([('Yes', 'Yes'), ('No', 'No')], string='Epilepsy')
    mi = fields.Selection([('Yes', 'Yes'), ('No', 'No')], string='MI')
    angina = fields.Selection([('Yes', 'Yes'), ('No', 'No')], string='Angina')
    rheumatic_fever = fields.Selection([('Yes', 'Yes'), ('No', 'No')], string='Rheumatic fever')
    cholesterol = fields.Selection([('Yes', 'Yes'), ('No', 'No')], string='Cholesterol')
    hypertension = fields.Selection([('Yes', 'Yes'), ('No', 'No')], string='Hypertension')
    tb = fields.Selection([('Yes', 'Yes'), ('No', 'No')], string='TB')
    asthma = fields.Selection([('Yes', 'Yes'), ('No', 'No')], string='Asthma')
    bronchitis = fields.Selection([('Yes', 'Yes'), ('No', 'No')], string='Bronchitis')
    aditional_phmx = fields.Text('Additional PHMx')

    phsx = fields.Selection([('Yes', 'Yes'), ('No', 'No')], string='PHSx')
    allergy = fields.Selection([('Yes', 'Yes'), ('No', 'No')], string='ALLERGY')
    mhx = fields.Selection([('Yes', 'Yes'), ('No', 'No')], string='MHx')
    mhx_edit = fields.Text('MHx')
    surgery_type = fields.Char('Type Of Surgery')
    allergy_type = fields.Char('Type Of Allergy')
    allergy_to = fields.Char('Allergy To')
    investigation = fields.Text('Investigation')

    marital_status = fields.Selection([('Yes', 'Yes'), ('No', 'No')], string='Marital Status')
    marital_edit = fields.Text('Marital')

    smoking_hx = fields.Selection([('Yes', 'Yes'), ('No', 'No')], string='Smoking HX')
    alcohol_hx = fields.Selection([('Yes', 'Yes'), ('No', 'No')], string='Alcohol Hx')
    illicit_drug_use_hx = fields.Selection([('Yes', 'Yes'), ('No', 'No')], string='Illicit drug use Hx')
    smoking_hx_edit = fields.Text('Smoking Hx')
    illicit_drug_use_edit = fields.Text('Illicit drug use')
    alcohol_hx_edit = fields.Text('Alcohol Hx')

    fhx = fields.Selection([('Yes', 'Yes'), ('No', 'No')], string='FHx')
    fhx_edit = fields.Text('FHx')
    pe_edit = fields.Text('PE')
    test_perform = fields.Text('Test Perform')
    ddx_edit = fields.Text('DDX')
    dx = fields.Text('DX')
    tx_plan = fields.Text('TX Plan')
    recommendations = fields.Text('Recommendations')
    recommendations_case = fields.Text('Recommendations Case')

    cefixime = fields.Text('Cefixime 400mg OD PO')
    doxycycline = fields.Text('Doxycycline 100mg BID PO')
    metronidazole = fields.Text('Metronidazole 400mg BID PO')

    vital_hr = fields.Char('HR')
    vital_so = fields.Char('SO2')
    vital_bp = fields.Char('BP')
    vital_t = fields.Char('T')
    vital_rr = fields.Char('RR')
    vital_h = fields.Char('H')
    vital_w = fields.Char('W')
    vital_bmi = fields.Char('BMI')
    presenting = fields.Text('Presenting Compliant')

    hpi_site = fields.Text('Site')
    hpi_onset = fields.Text('Onset')
    hpi_radiating = fields.Text('Radiating')
    hpi_factors = fields.Text('Alleviating Factors')
    hpi_time = fields.Text('Time')
    hpi_exacerbating = fields.Text('Exacerbating factors')
    hpi_severity = fields.Text('Severity')
    hpi_character = fields.Text('Character')
    complaints = fields.Text('Complaints')
    assessment = fields.Text('Assessment ')
    diagnoses = fields.Text('Diagnosis ')
    prescription_lines = fields.One2many('prescription.line', 'appointment_id')
    extra_prescription_lines = fields.One2many('extra.prescription.line', 'appointment_id')
    company_id = fields.Many2one('res.company', 'Company', default=lambda self: self.env.user.company_id.id)
    picking_type_id = fields.Many2one('stock.picking.type', 'Picking Type', domain=[('code', '=', 'internal'),('warehouse_id.name', 'ilike', 'Clinic')],default=lambda self: self.env['stock.picking.type'].search([('code', '=', 'internal'),
                                                                                                                                        ('warehouse_id.name', '=', 'Clinic Warehouse')], limit=1))
    picking_count = fields.Integer(compute='_picking_count', string='Picking')
    is_emp = fields.Boolean('Is Employee', default=True)
    is_guest = fields.Boolean('Is Guest')
    guest_id = fields.Many2one('res.guest', 'Guest')
    employee_id = fields.Many2one('hr.employee', 'Employee')
    dest_location_id = fields.Many2one('stock.location', 'Consume Location', domain=[('name', 'ilike', 'clinic')], default=lambda self: self.env['stock.location'].search([('name', 'ilike', 'clinic')], limit=1))
    attachment_number = fields.Integer(compute='_compute_attachment_number', string='Number of Attachments')
    request_ids = fields.Many2many('lab.test.request', 'appointment_lab_ref', 'appint_id', 'req_id',compute='calculate_lab_requested')

    def action_print_lab_request(self):
        return self.env.ref('nuro_fms_clinic.report_labtest').report_action(self)

    def calculate_lab_requested(self):
        lst = []
        for rec in self:
            lab_request = self.env['lab.test.request'].search([('appointment_id', '=', rec.id)])
            if lab_request:
                for lab in lab_request:
                    lst.append(lab.id)
            rec.request_ids = [(6,0, lst)]

    @api.onchange('employee_id', 'guest_id')
    def onchange_guest_emp(self):

        if self.employee_id:
            self.patient_name = self.employee_id.sudo().name
            self.gender = self.employee_id.sudo().gender
            self.country_id = self.employee_id.sudo().country_id.id
            self.job_id = self.employee_id.sudo().job_id.id
            self.id_no = self.employee_id.sudo().barcode
            self.department_id = self.employee_id.sudo().department_id.id
            self.project_id = False
            self.branch_id = False
        if self.guest_id:
            self.patient_name = self.guest_id.name
            self.country_id = self.guest_id.nationality.id
            self.gender = self.guest_id.gender
            self.job_id = False
            self.id_no = False
        if self.temporary_staff_id:
            self.patient_name = self.temporary_staff_id.name
            self.country_id = self.temporary_staff_id.nationality.id
            self.gender = self.temporary_staff_id.gender
            self.job_id = False
            self.id_no = False

    def action_get_attachment_view(self):
        self.ensure_one()
        res = self.env['ir.actions.act_window'].for_xml_id('base', 'action_attachment')
        res['domain'] = [('res_model', '=', 'oeh.medical.appointment'), ('res_id', 'in', self.ids)]
        res['context'] = {'default_res_model': 'oeh.medical.appointment', 'default_res_id': self.id}
        return res

    def _compute_attachment_number(self):
        attachment_data = self.env['ir.attachment'].read_group(
            [('res_model', '=', 'oeh.medical.appointment'), ('res_id', 'in', self.ids)], ['res_id'], ['res_id'])
        attachment = dict((data['res_id'], data['res_id_count']) for data in attachment_data)
        for expense in self:
            expense.attachment_number = attachment.get(expense.id, 0)

    @api.onchange('is_guest')
    def onchange_guest(self):
        if self.is_guest:
            self.is_emp = False
            self.employee_id = False
            self.is_temporary_staff = False
            self.temporary_staff_id = False

    @api.onchange('is_emp')
    def onchange_emp(self):
        if self.is_emp:
            self.is_guest = False
            self.guest_id = False
            self.is_temporary_staff = False
            self.temporary_staff_id = False

    @api.onchange('is_temporary_staff')
    def onchange_temporary_staff(self):
        if self.is_temporary_staff:
            self.is_guest = False
            self.is_emp = False
            self.guest_id = False
            self.employee_id = False

    @api.constrains('is_emp', 'is_guest', 'is_temporary_staff')
    def check_emp_guest(self):
        if not self.is_emp and not self.is_guest and not self.is_temporary_staff:
            raise ValidationError(_('Check Employee or Guest or Temporary Staff!!'))

    def action_in_patient(self):
        patient_ref = self.env['fms.patient']
        if self.employee_id:
            patients = patient_ref.search([('employee_id', '=', self.employee_id.sudo().id)
                                                   ], limit=1)
        if self.guest_id:
            patients = patient_ref.search([('guest_id', '=', self.guest_id.id)
                                           ], limit=1)
        if self.temporary_staff_id:
            patients = patient_ref.search([('temporary_staff_id', '=', self.temporary_staff_id.id)
                                           ], limit=1)
        if patients:
            self.patient_id = patients.id
        else:
            patient_id = patient_ref.create({'mobile': self.employee_id.sudo().mobile_phone or self.guest_id.mobile or self.temporary_staff_id.mobile,
                                'email': self.employee_id.sudo().work_email or self.guest_id.email or self.temporary_staff_id.email,
                                'employee_id': self.employee_id.sudo().id,
                                'guest_id': self.guest_id.id,
                                'is_emp': self.is_emp,
                                'is_temporary_staff': self.is_temporary_staff,
                                'is_guest': self.is_guest,
                                'temporary_staff_id': self.temporary_staff_id.id,
                                'name': self.employee_id.sudo().name or self.guest_id.name or self.temporary_staff_id.name})
            if patient_id:
                self.patient_id = patient_id.id
        self.write({'state': 'inpatient'})

    def action_confirm(self):
        self.write({'state': 'confirm'})

    def action_open_picking(self):
        picking = self.env['stock.picking']
        picking_ids = picking.search([('appointment_id', '=', self.id)])
        form_view = self.env.ref('stock.view_picking_form').id
        tree_view = self.env.ref('stock.vpicktree').id
        return {
            'name': _('picking'),
            'res_model': 'stock.picking',
            'views': [
                (tree_view, 'tree'),
                (form_view, 'form'),
            ],
            'view_mode': 'tree,form',
            'type': 'ir.actions.act_window',
            'domain': [('id', 'in', picking_ids.ids)]
        }

    def print_imaging_report(self):
        imaging = self.env['oeh.medical.imaging']
        if self.imaging_lines:
            for line in self.imaging_lines:
                imaging_request = imaging.search(
                    [('appointment_id', '=', self.id)])
                if imaging_request:
                    continue
                img_id = imaging.create({'patient': self.patient_id.id,
                                         'appointment_id': self.id,
                                'imaging_department': line.imag_department_id.id,
                                'test_type': line.test_type_id.id,
                                })
                img_id.write({'state': 'progress'})

    def print_labtest_report(self):
        lab_req = self.env['lab.test.request']
        lst = []
        if self.labtest_type_ids:
            for line in self.labtest_type_ids:
                lab_request = self.env['lab.test.request'].search([('appointment_id', '=', self.id),('test_name', '=', line.name)])
                if lab_request:
                    continue
                if line.lab_criteria:
                    for lb in line.lab_criteria:
                        lst.append((0,0,{'name': lb.name,
                                         'normal_range': lb.normal_range,
                                         'is_master': lb.is_master,
                                         'range_result_ids': [(6,0, lb.range_result_ids.ids)],
                                         'sequence': lb.sequence}))
                lab_req.create({'patient_id': self.patient_id.id,
                                'doctor_id': self.doctor_id.id,
                                'appointment_id': self.id,
                                'code': line.code,
                                'test_name': line.name,
                                'lab_department': line.lab_department.id,
                                'test_charge': line.test_charge,

                                'lab_test_result_line': lst})
                lst = []
        # return self.env.ref('nuro_fms_clinic.report_labtest').report_action(self)

    def out_patient_medical_report(self):
        return self.env.ref('nuro_fms_clinic.report_medical_out_patinet').report_action(self)

    def in_patient_medical_report(self):
        return self.env.ref('nuro_fms_clinic.report_medical').report_action(self)

    def print_extra_prescription(self):
        return self.env.ref('nuro_fms_clinic.extra_report_prescription').report_action(self)

    def print_prescription_report(self):
        if self.state == 'draft':
            raise ValidationError(_('Please click on In patient  button then you can transfer prescription!!'))
        data_lst = []
        picking = self.env['stock.picking']
        if not self.picking_type_id:
            raise ValidationError(_('Please select picking Type !!'))
        if not self.prescription_lines:
            raise ValidationError(_('Please Add Prescription Details !!'))
        if not self.dest_location_id:
            raise ValidationError(_('Please Add Consume Location !!'))
        for line in self.prescription_lines:
            if line.state == 'draft':
                data_lst.append((0,0,{
                    'product_id': line.medicine_id.product_id.id,
                    'product_uom_qty': line.quantity,
                    'name': line.medicine_id.name,
                    'product_uom': line.medicine_id.uom_id.id,
                    'quantity_done': line.quantity,
                }))
                line.state = 'confirm'
        if not data_lst:
            raise ValidationError(_('Please Add Prescription Details !!'))
        pick_id = picking.create({'picking_type_id': self.picking_type_id.id,
                        'location_id': self.picking_type_id.default_location_src_id.id,
                        'location_dest_id': self.dest_location_id.id,
                        'appointment_id': self.id,
                        'receiver_name': self.patient_id.name,
                        'move_lines': data_lst})
        if pick_id:
            pick_id.button_validate()
        return self.env.ref('nuro_fms_clinic.report_prescription').report_action(self)

    @api.model
    def create(self, vals):
        sequence = self.env['ir.sequence'].next_by_code('oeh.medical.appointment')
        vals['name'] = sequence
        health_appointment = super(OeHealthAppointment, self).create(vals)
        return health_appointment

class PrescriptionLine(models.Model):
    _name = 'prescription.line'

    appointment_id = fields.Many2one('oeh.medical.appointment')
    medicine_id = fields.Many2one('oeh.medical.medicines', 'Medicine')
    meal_dose = fields.Selection(string="", selection=[('after_breakfast', 'After Breakfast'),
                                                       ('before_breakfast', 'Before Breakfast'),
                                                       ('after_meal', 'After Meal'),
                                                       ('before_meal', 'Before Meal'),
                                                       ('after_meal_night', 'After Meal at Night'),
                                                       ('before_meal_night', 'Before Meal at Night'),
                                                       ])
    duration = fields.Float('Duration')
    duration_unit = fields.Selection([
                                        ('Days', 'Days'),
                                        ('Months', 'Months'),
                                        ('Years', 'Years'),
                                        ('Indefinite', 'Indefinite'),
                                    ])
    state = fields.Selection([('draft', 'Draft'),
                              ('confirm', 'Confirm')], default='draft')
    dosage_id = fields.Many2one('oeh.medical.dosage', 'Frequency')
    company_id = fields.Many2one('res.company', 'Company', default=lambda self: self.env.user.company_id.id)
    picking_id = fields.Many2one('stock.picking', 'Stock')
    routes_id = fields.Many2one('clinic.route', 'Route')
    dosage = fields.Char('Dosage')
    quantity = fields.Float('Quantity', default=1)

class OeHealthDosage (models.Model):
    _name = "oeh.medical.dosage"
    _description = "Medicines Dosage"

    name = fields.Char(string='Frequency', size=256, help='Common dosage frequency')
    code = fields.Char(string='Code', size=64, help='Dosage Code, such as SNOMED, 229798009 = 3 times per day')
    abbreviation = fields.Char(string='Abbreviation', size=64, help='Dosage abbreviation, such as tid in the US or tds in the UK')
    company_id = fields.Many2one('res.company', 'Company', default=lambda self: self.env.user.company_id.id)

class ImagingLine(models.Model):
    _name = 'imaging.lines'

    appointment_id = fields.Many2one('oeh.medical.appointment')
    imag_department_id = fields.Many2one('oeh.medical.imagingtest.department', 'Department')
    test_type_id = fields.Many2one('oeh.medical.imaging.test.type', 'Test')
    remark = fields.Text('Remarks')
    company_id = fields.Many2one('res.company', 'Company', default=lambda self: self.env.user.company_id.id)

class ExtraPrescription(models.Model):
    _name = 'extra.prescription.line'

    appointment_id = fields.Many2one('oeh.medical.appointment')
    medicine_id = fields.Many2one('oeh.medical.medicines', 'Medicine')
    meal_dose = fields.Selection(string="", selection=[('after_breakfast', 'After Breakfast'),
                                                       ('before_breakfast', 'Before Breakfast'),
                                                       ('after_meal', 'After Meal'),
                                                       ('before_meal', 'Before Meal'),
                                                       ('after_meal_night', 'After Meal at Night'),
                                                       ('before_meal_night', 'Before Meal at Night'),
                                                       ])
    duration = fields.Float('Duration')
    duration_unit = fields.Selection([
        ('Days', 'Days'),
        ('Months', 'Months'),
        ('Years', 'Years'),
        ('Indefinite', 'Indefinite'),
    ])
    state = fields.Selection([('draft', 'Draft'),
                              ('confirm', 'Confirm')], default='draft')
    dosage_id = fields.Many2one('oeh.medical.dosage', 'Frequency')
    company_id = fields.Many2one('res.company', 'Company', default=lambda self: self.env.user.company_id.id)
    picking_id = fields.Many2one('stock.picking', 'Stock')
    routes_id = fields.Many2one('clinic.route', 'Route')
    dosage = fields.Char('Dosage')
    quantity = fields.Float('Quantity', default=1)

