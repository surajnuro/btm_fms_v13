from odoo import api, fields, models,_

class Route(models.Model):
    _name = 'clinic.route'

    name = fields.Char('Name', copy=False)