from odoo import api, fields, models, _

class HrEmployee(models.Model):
    _inherit = 'hr.employee'

    @api.model
    def name_search(self, name='', args=None, operator='ilike', limit=100):
        if operator not in ('ilike', 'like', '=', '=like', '=ilike'):
            return super(HrEmployee, self).name_search(name, args, operator, limit)
        args = args or []
        domain = ['|', '|', ('barcode', operator, name), ('name', operator, name), ('work_email', operator, name)]
        employees = self.search(domain + args, limit=limit)
        return employees.name_get()