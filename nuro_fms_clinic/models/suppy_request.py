from odoo import fields,models,api,_
from odoo.exceptions import UserError, Warning

class SupplyConsumption(models.Model):
    _name = 'supply.consumption'
    _order = "id desc"

    name = fields.Char('Name', readonly=True)
    user_id = fields.Many2one('res.users', 'User',
                              default=lambda self: self.env.user,
                              store=True,
                              readonly=True)
    location_ids = fields.Many2many('stock.location',
                                    'prescription_rel',
                                    'prescription_location_id',
                                    string='Location IDS')
    location_id = fields.Many2one('stock.location', domain=[('usage', '=', 'internal')])
    consume_location_id = fields.Many2one('stock.location', readonly=True)
    supply_consume_line = fields.One2many('supply.consume.line', 'supply_consume_id', string='Supply Line',
                                          states={'draft': [('readonly', False)]}, readonly=True)
    date = fields.Date('Date', default=fields.Datetime.now, readonly=True)
    state = fields.Selection([('draft', 'Draft'), ('confirm', 'Confirm'), ('cancel', 'Cancelled')], default='draft')
    stock_picking_id = fields.Many2one('stock.picking', string='Delivery Request')
    description = fields.Text('Description')

    @api.onchange('user_id')
    def onchange_location_ids(self):
        self.location_ids = self.user_id.stock_consume_loc_ids
        self.consume_location_id = self.user_id.consume_loc_id
        if self.location_ids:
            self.location_id = self.location_ids[0].id
        return {'domain': {'location_id': [('id', 'in', self.location_ids.ids)]}}


    def create_supply_sequence(self):
        for rec in self:
            seq = self.env['ir.sequence'].next_by_code('supply.consume')
            rec.name = seq

    @api.model
    def create(self, vals):
        res = super(SupplyConsumption, self).create(vals)
        res.create_supply_sequence()
        return res

    def cancel_supply_consume(self):
        self.state = 'cancel'

    def create_material_request(self):
        lst = []
        picking_search = self.env['stock.picking.type'].search([('code', '=', 'internal'),
                                                                ('default_location_src_id', '=', self.location_id.id)],
                                                               limit=1)
        if not self.consume_location_id.id:
            raise UserError(_('Please select Receipt Location !'))
        for rec in self.sudo().supply_consume_line:
            if not rec.qty_requested:
                raise UserError("There is not request qty for inventory !")
            lst.append((0, 0, {
                'product_id': rec.product_id.product_id.id,
                'product_uom': rec.product_id.product_id.uom_id.id,
                'product_uom_qty': rec.qty_requested + rec.qty_scrapped,
                'name': 'Supply Consumption',
            }))
        picking_dict = {
            'picking_type_id': picking_search.id,
            'location_id': self.location_id.id,
            'location_dest_id': self.consume_location_id.id,
            'supply_consume_id': self.id,
            'supply_consumption': True,
            # 'client_ticket': self.name,
            'origin': self.name,
            'move_lines': lst,
        }
        if self.supply_consume_line:
            consume_transfer = self.env['stock.picking'].create(picking_dict)
            self.stock_picking_id = consume_transfer.id
            self.state = 'confirm'
            consume_transfer.action_assign()
            consume_transfer.action_confirm()
            for mv_line in consume_transfer.move_lines:
                if mv_line.move_line_ids:
                    for move_line_id in mv_line.move_line_ids:
                        move_line_id.update({'qty_done': move_line_id.product_uom_qty})
                else:
                    mv_line.update({'quantity_done': mv_line.product_uom_qty})
            consume_transfer.action_done()
            # return consume_transfer.suply_request_picking()

class SupplyConsumeLine(models.Model):
    _name = 'supply.consume.line'

    product_id = fields.Many2one('oeh.medical.medicines', required=True)
    available_qty = fields.Float('Available Quantity', compute='_qty_available_location', store=True)
    qty_requested = fields.Float('Quantity Consumed')
    qty_scrapped = fields.Float('Quantity Damaged')
    supply_consume_id = fields.Many2one('supply.consumption')

    @api.depends('product_id')
    def _qty_available_location(self):
        """
        @api.depends() should contain all fields that will be used in the calculations.
        """
        for rec in self:
            domain = ([('product_id', '=', rec.product_id.product_id.id), ('location_id.usage', '=', 'internal')])
            if rec.supply_consume_id.user_id.stock_consume_loc_ids:
                domain.append(('location_id', 'in', (rec.supply_consume_id.user_id.stock_consume_loc_ids.ids)))
            quant_obj = self.env['stock.quant'].sudo().search(domain)
            quantity_avlbl = 0.0
            for quant in quant_obj:
                quantity_avlbl += quant.quantity
            rec.available_qty = quantity_avlbl

    # @api.onchange('product_id')
    # def _qty_available_user_location(self):
    #     user_location_ids = self.env.user.stock_consume_loc_ids.ids
    #     search_quants = self.env['stock.quant'].search([('location_id', 'in', user_location_ids)])
    #     product_ids = []
    #     for rec in search_quants:
    #         if rec.product_id.id not in product_ids:
    #             product_ids.append(rec.product_id.id)
    #     return {'domain': {'product_id': [('id', 'in', product_ids)]}}


class StockPicking(models.Model):
    _inherit = 'stock.picking'

    supply_consume_id = fields.Many2one('supply.consumption')
    supply_consumption = fields.Boolean()