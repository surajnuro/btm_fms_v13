from odoo import fields,models,api,_

class TemporaryStaff(models.Model):
    _name = 'temporary.staff'
    _description = 'Temporary Staff'

    name = fields.Char('Name')
    email = fields.Char('Email')
    mobile = fields.Char('Phone')
    desc = fields.Text('Description')
    nationality = fields.Many2one('res.country', 'Nationality')
    gender = fields.Selection([('male', 'Male'), ('female', 'Female')], string='Gender')