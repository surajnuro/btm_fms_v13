from odoo import api, fields, models,_

class OeHealthPhysician(models.Model):
    _name = "oeh.medical.physician"
    _description = "Information about the doctor"

    CONSULTATION_TYPE = [
        ('Residential', 'Residential'),
        ('Visiting', 'Visiting'),
        ('Other', 'Other'),
    ]

    APPOINTMENT_TYPE = [
        ('Not on Weekly Schedule', 'Not on Weekly Schedule'),
        ('On Weekly Schedule', 'On Weekly Schedule'),
    ]

    def _app_count(self):
        oe_apps = self.env['oeh.medical.appointment']
        for pa in self:
            domain = [('doctor', '=', pa.id)]
            app_ids = oe_apps.search(domain)
            apps = oe_apps.browse(app_ids)
            app_count = 0
            for ap in apps:
                app_count+=1
            pa.app_count = app_count
        return True

    def _prescription_count(self):
        oe_pres = self.env['oeh.medical.prescription']
        for pa in self:
            domain = [('doctor', '=', pa.id)]
            pres_ids = oe_pres.search(domain)
            pres = oe_pres.browse(pres_ids)
            pres_count = 0
            for pr in pres:
                pres_count+=1
            pa.prescription_count = pres_count
        return True

    employee_id = fields.Many2one('hr.employee', string='Related Employee', required=False, ondelete='cascade', help='Employee-related data of the physician')
    # institution = fields.Many2one('oeh.medical.health.center', string='Institution', help="Institution where doctor works")
    code = fields.Char(string='Licence ID', size=128, help="Doctor's License ID")
    speciality = fields.Many2one('oeh.medical.speciality', string='Speciality', help="Speciality Code")
    consultancy_type = fields.Selection(CONSULTATION_TYPE, string='Consultancy Type', help="Type of Doctor's Consultancy", default=lambda *a: 'Residential')
    consultancy_price = fields.Integer(string='Consultancy Charge', help="Doctor's Consultancy price")
    available_lines = fields.One2many('oeh.medical.physician.line', 'physician_id', string='Doctor Availability')
    degree_id = fields.Many2many('oeh.medical.degrees', id1='physician_id', id2='degree_id', string='Degrees')
    app_count = fields.Integer(compute=_app_count, string="Appointments")
    prescription_count = fields.Integer(compute=_prescription_count, string="Prescriptions")
    is_pharmacist = fields.Boolean(string='Pharmacist?', default=lambda *a: False)
    oeh_user_id = fields.Many2one('res.users', string='Responsible Odoo User')
    appointment_type = fields.Selection(APPOINTMENT_TYPE, string='Allow Appointment on?', default=lambda *a: 'Not on Weekly Schedule')
    notes = fields.Text('Notes')
    mobile_phone = fields.Char('Mobile')
    work_email = fields.Char('Email')
    image = fields.Binary('Image', attachment=True)
    name = fields.Char('Name')
    walkin_schedule_lines = fields.One2many('oeh.medical.physician.walkin.schedule', 'physician_id', string='Walkin Schedule')
    complementary_days = fields.Integer('Complementary Days')
    commission_type = fields.Selection(string="Commission Type", required=False,
                                       selection=[('Amount', 'Amount'), ('Percentage', 'Percentage'), ])
    commission_rate = fields.Float(string="Commission Rate")
    end_date = fields.Date('End Date')
    commission_master_ids = fields.One2many(comodel_name="hms.doctor.commission.master", inverse_name="doctor_id",
                                            string="Commission Lines", required=False, )
    is_surgeon = fields.Boolean(string='Surgeon?', default=lambda *a: False)
    surgery = fields.Many2many('hms.surgery.master', string='Surgery Specialist')
    _sql_constraints = [
        ('code_oeh_physician_userid_uniq', 'unique(oeh_user_id)', "Selected 'Responsible' user is already assigned to another physician !")
    ]

class HmsSurgeryMaster(models.Model):
    _name = 'hms.surgery.master'


    name = fields.Char('Surgery Name')
    charges = fields.Float('Surgery Charges')

class OeHealthPhysicianWalkinSchedule(models.Model):
    _name = "oeh.medical.physician.walkin.schedule"
    _description = "Information about walkin schedule"

    name = fields.Date(string='Start Date', required=True)
    end_date = fields.Date(string='End Date', required=True)
    physician_id = fields.Many2one('oeh.medical.physician', string='Doctor', index=True, ondelete='cascade')

    _order = 'name desc'

class HMSDoctorCommissionMaster(models.Model):
    _name = 'hms.doctor.commission.master'

    name = fields.Selection(string="Panel", selection=[('Appointment', 'Appointment'),
                                                       ('Lab', 'Lab'),
                                                       ('Imaging', 'Imaging'),
                                                       ('Pharmacy', 'Pharmacy'),
                                                       ('Surgery', 'Surgery')
                                                       ], required=False, )
    commission_type = fields.Selection(string="Commission Type", required=False,
                                       selection=[('Amount', 'Amount'), ('Percentage', 'Percentage'), ])
    commission_rate = fields.Float(string="Commission Rate")
    doctor_id = fields.Many2one(comodel_name="oeh.medical.physician", string="Doctor", required=False, )

    _sql_constraints = [
        ('name_doctor_uniq', 'unique (name,doctor_id)', "Doctor with commission panel already exists !"),
    ]

class OeHealthPhysicianLine(models.Model):

    # Array containing different days name
    PHY_DAY = [
        ('Monday', 'Monday'),
        ('Tuesday', 'Tuesday'),
        ('Wednesday', 'Wednesday'),
        ('Thursday', 'Thursday'),
        ('Friday', 'Friday'),
        ('Saturday', 'Saturday'),
        ('Sunday', 'Sunday'),
    ]

    _name = "oeh.medical.physician.line"
    _description = "Information about doctor availability"

    name = fields.Selection(PHY_DAY, string='Available Day(s)', required=True)
    start_time = fields.Float(string='Start Time (24h format)')
    end_time = fields.Float(string='End Time (24h format)')
    physician_id = fields.Many2one('oeh.medical.physician', string='Doctor', index=True, ondelete='cascade')