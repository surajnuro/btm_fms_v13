from odoo import fields,api,models,_

class ResUsers(models.Model):
    _inherit = 'res.users'

    stock_consume_loc_ids = fields.Many2many('stock.location', 'user_consum_ref', 'user_id', 'loc_id', 'Supply Stock Location',
                                             domain=[('usage', '=', 'internal')])
    consume_loc_id = fields.Many2one('stock.location', 'Consume Location')
