from odoo import api, fields, models, _

class StockPicking(models.Model):
    _inherit = 'stock.picking'

    appointment_id = fields.Many2one('oeh.medical.appointment', 'Appointment')
