from odoo import api, fields, models,_

class OeHealthPhysicianSpeciality(models.Model):
    _name = "oeh.medical.speciality"
    _description = "Physician Speciality"

    name = fields.Char(string='Description', size=128, help="ie, Addiction Psychiatry", required=True)
    code = fields.Char(string='Code', size=128, help="ie, ADP")

    _order = 'name'
    _sql_constraints = [
        ('code_uniq', 'unique (name)', 'The Medical Speciality code must be unique')]


class OeHealthPhysicianDegree(models.Model):
    _name = "oeh.medical.degrees"
    _description = "Physicians Degrees"

    name = fields.Char(string='Degree', size=128, required=True)
    full_name = fields.Char(string='Full Name', size=128)
    physician_ids = fields.Many2many('oeh.medical.physician', id1='degree_id', id2='physician_id', string='Physicians')

    _sql_constraints = [
        ('full_name_uniq', 'unique (name)', 'The Medical Degree must be unique')]