from odoo import api, fields, models,_
import time
import datetime

class OeHealthImagingTypeManagement(models.Model):
    _name = 'oeh.medical.imaging'
    _description = 'Imaging Test Management'

    IMAGING_STATE = [
        ('Draft', 'Draft'),
        ('Test In Progress', 'Test In Progress'),
        ('Completed', 'Completed'),

    ]

    appointment_id = fields.Many2one('oeh.medical.appointment', 'Appointment')
    name = fields.Char(string='Test #', size=16, required=False, readonly=True, default=lambda *a: '/')
    patient = fields.Many2one('fms.patient', string='Patient', help="Patient Name", required=True)
    imaging_department = fields.Many2one('oeh.medical.imagingtest.department', string='Department')
    test_type = fields.Many2one('oeh.medical.imaging.test.type', string='Test Type', domain="[('imaging_department', '=', imaging_department)]", required=True, help="Imaging Test type")
    requestor = fields.Many2one('oeh.medical.physician', string='Doctor who requested the test', domain=[('is_pharmacist','=',False)], help="Doctor who requested the test")
    analysis = fields.Text(string='Analysis')
    conclusion = fields.Text(string='Conclusion')
    date_requested = fields.Date(string='Date requested', readonly=True, default=lambda *a: time.strftime('%Y-%m-%d %H:%M:%S'))
    date_analysis = fields.Datetime(string='Date of the Analysis')
    image1 = fields.Binary(string="Image 1", attachment=True)
    image2 = fields.Binary(string="Image 2", attachment=True)
    image3 = fields.Binary(string="Image 3", attachment=True)
    image4 = fields.Binary(string="Image 4", attachment=True)
    image5 = fields.Binary(string="Image 5", attachment=True)
    image6 = fields.Binary(string="Image 6", attachment=True)
    company_id = fields.Many2one('res.company',
                                 string='Company',
                                 change_default=True,
                                 required=True,
                                 readonly=True,
                                 states={'Draft': [('readonly', False)]},
                                 default=lambda self:
                                 self.env['res.company']._company_default_get('oeh.medical.lab.test'))
    state = fields.Selection([('draft', 'Draft'),
                              ('progress', 'Progress'),
                              ('complete','Completed')], default='draft')

    @api.model
    def create(self, vals):
        ids = self.search([('appointment_id', '=', vals.get('appointment_id'))], limit=1)
        if not ids:
            sequence = self.env['ir.sequence'].next_by_code('imaging.test.seq')
            vals['name'] = sequence
        else:
            vals['name'] = ids.name
        res = super(OeHealthImagingTypeManagement, self).create(vals)
        return res

    def print_report(self):
        return self.env.ref('nuro_fms_clinic.imaging_receipt').report_action(self)

    def set_to_test_start(self):
        return self.write({'state': 'progress'})

    def set_to_test_complete(self):
        return self.write({'state': 'complete'})

class OeHealthImagingTestDepartment(models.Model):
    _name = 'oeh.medical.imagingtest.department'
    _description = 'Imaging Test Departments'

    name = fields.Char(string='Name', size=128, required=True)

class OeHealthImagingTestType(models.Model):
    _name = 'oeh.medical.imaging.test.type'
    _description = 'Imaging Test Type Configuration'

    name = fields.Char(string='Name', size=128, required=True)
    code = fields.Char(string='Code', size=25, required=True)
    test_charge = fields.Float(string='Test Charge', required=True, default=lambda *a: 0.0)
    imaging_department = fields.Many2one('oeh.medical.imagingtest.department', string='Department')

    _sql_constraints = [('name_uniq', 'unique(name)', 'The Imaging test type name must be unique')]