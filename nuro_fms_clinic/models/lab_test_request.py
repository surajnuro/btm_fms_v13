from odoo import api, fields, models, _

class LabTestRequest(models.Model):
    _name = 'lab.test.request'
    _order = 'id desc'

    name = fields.Char('Name', copy=False, index=True)
    patient_id = fields.Many2one('fms.patient', 'Patient')
    doctor_id = fields.Many2one('oeh.medical.physician', 'Doctor')
    date = fields.Date('Lab Test Request Date', default=fields.Datetime.now)
    state = fields.Selection([('draft', 'Draft'),
                              ('confirm', 'Completed')], default='draft')
    labtest_type_ids = fields.Many2many('oeh.medical.labtest.types', 'lab_req_ref', 'lab_req_id',
                                        'lab_id', 'Lab Test Line')
    appointment_id = fields.Many2one('oeh.medical.appointment', 'Appointment')
    lab_test_result_line = fields.One2many('lab.test.result', 'request_id')
    result = fields.Char('Lab Test Result')
    test_name = fields.Char(string='Lab Test Name', size=128, required=True, help="Test type, eg X-Ray, Hemogram, Biopsy...")
    code = fields.Char(string='Code', size=128, help="Short code for the test")
    test_charge = fields.Float(string='Test Charge', default=lambda *a: 0.0)
    lab_department = fields.Many2one('oeh.medical.labtest.department', string='Department')

    def print_report(self):
        return self.env.ref('nuro_fms_clinic.report_result').report_action(self)


    @api.model
    def create(self, vals):
        ids = self.search([('appointment_id', '=', vals.get('appointment_id'))], limit=1)
        if not ids:
            sequence = self.env['ir.sequence'].next_by_code('lab.test.seq')
            vals['name'] = sequence
        else:
            vals['name'] = ids.name
        res = super(LabTestRequest, self).create(vals)
        return res

    def confirm(self):
        self.write({'state': 'confirm'})


class OeHealthLabTestTypesMaster(models.Model):
    _name = 'labtest.types.result'

    name = fields.Char(string="Name", required=True)
    remark = fields.Char(string="Remark", required=False)

class LabTestResult(models.Model):
    _name = 'lab.test.result'

    is_master = fields.Boolean(string="Is Master?", )
    request_id = fields.Many2one('lab.test.request')
    name = fields.Char(string='Tests', size=128)
    normal_range = fields.Text(string='Normal Range')
    result = fields.Char('Result')
    units = fields.Many2one('oeh.medical.lab.units', string='Units')
    sequence = fields.Integer(string='Sequence')
    remark = fields.Text('Remark')
    normal_range_id = fields.Many2one('labtest.types.result')
    range_result_ids = fields.Many2many('labtest.types.result', 'result_labtest_relation', 'result_id', 'lab_id', 'Test Type Master')
