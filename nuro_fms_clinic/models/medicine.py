from odoo import api, fields, models, _
import time
import datetime

class OeHealthMedicines(models.Model):
    _name = 'oeh.medical.medicines'
    _description = "Information about the medicines"

    _inherits={
        'product.product': 'product_id',
    }

    MEDICAMENT_TYPE = [
        ('Medicine', 'Medicine'),
        ('Vaccine', 'Vaccine'),
    ]

    product_id = fields.Many2one('product.product', string='Related Product', required=False,ondelete='cascade', help='Product-related data of the medicines')
    therapeutic_action = fields.Char(string='Therapeutic effect', size=128, help="Therapeutic action")
    composition = fields.Text(string='Composition',help="Components")
    indications = fields.Text(string='Indication',help="Indications")
    dosage = fields.Text(string='Dosage Instructions',help="Dosage / Indications")
    overdosage = fields.Text(string='Overdosage',help="Overdosage")
    pregnancy_warning = fields.Boolean(string='Pregnancy Warning', help="Check when the drug can not be taken during pregnancy or lactancy")
    pregnancy = fields.Text(string='Pregnancy and Lactancy',help="Warnings for Pregnant Women")
    adverse_reaction = fields.Text(string='Adverse Reactions')
    storage = fields.Text(string='Storage Conditions')
    info = fields.Text(string='Extra Info')
    medicament_type = fields.Selection(MEDICAMENT_TYPE, string='Medicament Type')