from odoo import models, fields, api, _
import base64
import xlsxwriter
from datetime import datetime
from odoo.exceptions import ValidationError

class SupplyConsumption(models.TransientModel):
    _name = 'supply.consumption.wizard'

    file_name = fields.Binary('File Name')
    name = fields.Char('Data')
    date_start = fields.Date('Start Date')
    date_end =fields.Date('End Date')
    location_id  = fields.Many2one('stock.location', 'Consumption Location', domain=[('usage', '=', 'customer')])

    def remove_html_tags(self, text):
        """Remove html tags from a string"""
        import re
        clean = re.compile('<.*?>')
        return re.sub(clean, '', text)


    # Function for printing excel report
    def print_ticket_report_excel(self):
        f_name = '/tmp/report_consumption.xlsx'
        workbook = xlsxwriter.Workbook(f_name)
        worksheet = workbook.add_worksheet('Report')
        worksheet.set_column('A:B', 12)
        worksheet.set_column('C:C', 15)
        worksheet.set_column('D:D', 20)
        worksheet.set_column('E:I', 12)
        style = workbook.add_format({
            'bold': 1,
            'border': 1,
            'align': 'center',
            'valign': 'vcenter'})
        style.set_font_size(8)
        align_value = workbook.add_format({
            'align': 'center',
            'valign': 'vcenter',
            'border': 1})
        align_value.set_font_size(8)

        domain = ([('supply_consume_id.state', '=', 'confirm')])
        if self.date_start:
            domain.append(('supply_consume_id.date', '>=', self.date_start))
        if self.date_end:
            domain.append(('supply_consume_id.date', '<=', self.date_end))
        if self.location_id:
            domain.append(('supply_consume_id.consume_location_id', '=', self.location_id.id))

        consume_line = self.env['supply.consume.line'].sudo().search(domain)
        total = 0.0
        row = 3
        new_row = row + 1
        worksheet.merge_range('A1:I1', "DEEQA CONSTRUCTION & WATER WELL DRILLING CO. LTD", style)
        if self.date_start and self.date_end:
            worksheet.merge_range('A2:I2', "MATERIALS CONSUMPTION REPORT FROM " + str(self.date_start) + " TO " + str(self.date_end),  style)
        if self.date_start and not self.date_end:
            worksheet.merge_range('A2:I2', "MATERIALS CONSUMPTION REPORT FROM " + str(self.date_start) + " TILL TODAY ", style)
        if self.date_end and not self.date_start:
            worksheet.merge_range('A2:I2', "MATERIALS CONSUMPTION REPORT FROM BEGINING TILL " + str(self.date_end), style)
        worksheet.write('A3', "SUPPLY REQ NO.:", style)
        worksheet.write('B3', "DATE OF ISSUE", style)
        worksheet.write('C3', "PRODUCT NAME", style)
        worksheet.write('D3', "PRODUCT REF", style)
        worksheet.write('E3', "QTY EXPIRE", style)
        worksheet.write('F3', "QTY LOSS/DAMAGED", style)
        worksheet.write('G3', 'COST PER UNIT', style)
        worksheet.write('H3', 'TOTAL COST', style)
        worksheet.write('I3', "LOCATION/ AREA", style)
        for stock in consume_line:
            worksheet.write("A%s" % (new_row), stock.supply_consume_id.name, align_value)
            worksheet.write("B%s" % (new_row), stock.supply_consume_id.date, align_value)
            worksheet.write("C%s" % (new_row), stock.product_id.name, align_value)
            worksheet.write("D%s" % (new_row), stock.product_id.default_code, align_value)
            worksheet.write("E%s" % (new_row), stock.qty_requested, align_value)
            worksheet.write("F%s" % (new_row), stock.qty_scrapped, align_value)
            for move in stock.supply_consume_id.stock_picking_id.move_lines:
                if move.product_id.id == stock.product_id.id:
                    worksheet.write("G%s" % (new_row), -(move.price_unit), align_value)
                    worksheet.write("H%s" % (new_row), -(move.value), align_value)
                    total += -(move.value)
            worksheet.write("I%s" % (new_row), stock.supply_consume_id.location_id.name, align_value)
            new_row += 1
        row_total = new_row
        worksheet.merge_range('A%s:G%s' % (row_total, row_total), 'Total ', style)
        worksheet.write('H%s' % (row_total), total, align_value)
        worksheet.write('I%s' % (row_total), '', align_value)

        workbook.close()
        f = open(f_name, 'rb')
        data = f.read()
        f.close()
        name = 'Supply Consumption Report'

        out_wizard = self.env['supply.consumption.wizard'].create({'name': name + '.xlsx', 'file_name': base64.b64encode(data)})
        view_id = self.env.ref('nuro_fms_clinic.supply_consumption_report_sheet_xls_clinic').id
        return {
            'type': 'ir.actions.act_window',
            'res_model': 'supply.consumption.wizard',
            'target': 'new',
            'view_mode': 'form',
            'res_id': out_wizard.id,
            'views': [[view_id, 'form']],
        }

    # Function for printing excel report
    # @api.multi
    # def print_stock_supply_report_summery_excel(self):
    #     f_name = '/tmp/report7.xlsx'
    #     workbook = xlsxwriter.Workbook(f_name)
    #     worksheet = workbook.add_worksheet('Report')
    #     worksheet.set_column('A:A', 20)
    #     worksheet.set_column('B:E', 12)
    #     # worksheet.set_column('G:G', 20)
    #     style = workbook.add_format({
    #         'bold': 1,
    #         'border': 1,
    #         'align': 'center',
    #         'valign': 'vcenter'})
    #     style.set_font_size(8)
    #     align_value = workbook.add_format({
    #         'align': 'center',
    #         'valign': 'vcenter'})
    #     align_value.set_font_size(8)
    #
    #     end_date = self.date_end
    #     if not self.date_end:
    #         end_date = fields.Datetime.now()
    #
    #     domain = ([('state', '=', 'done'),
    #                ('picking_id.picking_type_id.code', '=', 'internal'),
    #                ('picking_id.supply_consumption', '=', True)
    #                ])
    #     if self.date_start:
    #         domain.append(('picking_id.date_done', '>=', self.date_start))
    #     if self.date_end:
    #         domain.append(('picking_id.date_done', '<=', end_date))
    #
    #     stock_move_obj = self.env['stock.move'].sudo().read_group(domain,
    #                                                               ['product_id', 'product_uom', 'product_uom_qty',
    #                                                                'value'], ['product_id'])
    #
    #     row = 3
    #     new_row = row + 1
    #     worksheet.merge_range('A1:E1', "Kalkaal Speciality Hospital", style)
    #     if self.date_start and self.date_end:
    #         worksheet.merge_range('A2:E2', "MATERIALS CONSUMPTION REPORT FROM " + str(self.date_start) + " TO " + str(
    #             self.date_end), style)
    #     if self.date_start and not self.date_end:
    #         worksheet.merge_range('A2:E2', "MATERIALS CONSUMPTION REPORT FROM " + str(self.date_start) + " TILL TODAY ",
    #                               style)
    #     if self.date_end and not self.date_start:
    #         worksheet.merge_range('A2:E2', "MATERIALS CONSUMPTION REPORT FROM BEGINING TILL " + str(self.date_end),
    #                               style)
    #     worksheet.write('A3', "PRODUCT NAME:", style)
    #     worksheet.write('B3', "REF", style)
    #     worksheet.write('C3', "QUANTITY", style)
    #     worksheet.write('D3', "UNIT PRICE", style)
    #     worksheet.write('E3', "TOTAL VALUE", style)
    #     for stock in stock_move_obj:
    #         product_details = self.env['product.product'].browse(stock['product_id'][0])
    #         worksheet.write("A%s" % (new_row), product_details.name, align_value)
    #         worksheet.write("B%s" % (new_row), product_details.default_code or '', align_value)
    #         worksheet.write("C%s" % (new_row), stock['product_uom_qty'] or " ", align_value)
    #         worksheet.write("D%s" % (new_row), -stock['value'] / stock['product_uom_qty'] or " ", align_value)
    #         worksheet.write("E%s" % (new_row), -stock['value'] or " ", align_value)
    #         new_row += 1
    #
    #     workbook.close()
    #     f = open(f_name, 'rb')
    #     data = f.read()
    #     f.close()
    #     name = 'Material Supply Summery'
    #
    #     out_wizard = self.env['supply.consumption.wizard'].create(
    #         {'name': name + '.xlsx', 'file_name': base64.b64encode(data)})
    #     view_id = self.env.ref('internal_material_request.supply_consumption_report_sheet_xls').id
    #     return {
    #         'type': 'ir.actions.act_window',
    #         'res_model': 'supply.consumption.wizard',
    #         'target': 'new',
    #         'view_mode': 'form',
    #         'res_id': out_wizard.id,
    #         'views': [[view_id, 'form']],
    #     }