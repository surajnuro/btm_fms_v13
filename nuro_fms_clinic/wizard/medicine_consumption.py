from odoo import models, fields, api, _
import base64
import xlsxwriter
from datetime import datetime
from odoo.exceptions import ValidationError

class MedicineWiz(models.TransientModel):
    _name = 'medicine.report.wiz'

    date_start = fields.Date('Start Date')
    date_end = fields.Date('Start End')
    patient_id = fields.Many2one('fms.patient', 'Patient')

    @api.onchange('date_start', 'date_end')
    def on_change_date_start(self):
        current_date = datetime.today().date()
        if self.date_start:
            st_date = datetime.strptime(self.date_start, '%Y-%m-%d').date()
            if st_date >= current_date:
                raise ValidationError('Start date can not be more than current date')
        if self.date_end:
            st_end = datetime.strptime(self.date_end, '%Y-%m-%d').date()
            if st_end < st_date or st_end > current_date:
                raise ValidationError('End date can not be less than start date and greater than current date')

    # Function for printing excel report
    def print_medicine_report_excel(self):
        f_name = '/tmp/medicine_consumtion_report.xlsx'
        workbook = xlsxwriter.Workbook(f_name)
        worksheet = workbook.add_worksheet('Report')
        worksheet.set_column('A:K', 12)
        style = workbook.add_format({
            'bold': 1,
            'border': 1,
            'align': 'center',
            'valign': 'vcenter'})
        style.set_font_size(8)
        align_value = workbook.add_format({
            'align': 'center',
            'valign': 'vcenter'})
        align_value.set_font_size(8)

        end_date = self.date_end
        if not self.date_end:
            end_date = fields.Datetime.now()

        domain = ([
                   ('picking_id.picking_type_id.code', '=', 'internal'),
                   ('picking_id.appointment_id', '!=', False),
                   ('picking_id.picking_type_id.warehouse_id.name', 'ilike', 'clinic'),
                   ])
        if self.date_start:
            domain.append(('date', '>=', self.date_start))
        if self.date_end:
            domain.append(('date', '<=', end_date))
        if self.patient_id:
            domain.append(('picking_id.appointment_id.patient_id', '=', self.patient_id.id))
        stock_move = self.env['stock.move'].sudo().search(domain)

        total = 0.0
        total_qty = 0.0
        row = 4
        new_row = row + 1
        worksheet.merge_range('A1:H1', "DEEQA CONSTRUCTION & WATER WELL DRILLING CO. LTD.", style)
        if self.date_start:
            date_ = datetime.strptime(self.date_start, '%Y-%m-%d').strftime('%B %Y')
            worksheet.merge_range('A2:H2', "MATERIALS CONSUMPTION REPORT FOR THE MONTH OF " + str(date_) or 'All',
                                  style)
        else:
            worksheet.merge_range('A2:H2', "MATERIALS CONSUMPTION REPORT",
                                  style)
        worksheet.write('A4', "Reference", style)
        worksheet.write('B4', "Appointment", style)
        worksheet.write('C4', "Patient Name", style)
        worksheet.write('D4', "Location", style)
        worksheet.write('E4', "Date", style)
        worksheet.write('F4', "Prescription", style)
        worksheet.write('G4', "Quantity", style)
        worksheet.write('H4', 'Value', style)
        total = 0
        if stock_move:
            for stock in stock_move:
                worksheet.write("A%s" % (new_row), stock.picking_id.name or "Not Filled", align_value)
                worksheet.write("B%s" % (new_row), stock.picking_id.appointment_id.name or "Not Filled", align_value)
                worksheet.write("C%s" % (new_row), stock.picking_id.appointment_id.patient_id.name or "Not Filled", align_value)
                worksheet.write("D%s" % (new_row), 'Clinic' + ' ' + stock.location_id.name or "Not Filled", align_value)
                worksheet.write("E%s" % (new_row), stock.date or "Not Filled", align_value)
                worksheet.write("F%s" % (new_row), stock.product_id.name or "Not Filled", align_value)
                worksheet.write("G%s" % (new_row), stock.product_uom_qty or "0", align_value)
                worksheet.write("H%s" % (new_row), stock.value or "0", align_value)
                total += stock.value
                new_row += 1
            worksheet.merge_range('A%s:G%s' % (new_row, new_row), 'Total ', style)
            worksheet.write('H%s' % (new_row), total, style)

        workbook.close()
        f = open(f_name, 'rb')
        data = f.read()
        f.close()
        name = 'Consumption Report'
        dt = ' ' + 'From' + ' ' + str(self.date_start) + ' ' + 'To' + ' ' + str(self.date_end)
        if self.date_start and self.date_end:
            out_wizard = self.env['xlsx.output'].create({'name': name + dt + '.xlsx',
                                                         'xls_output': base64.encodebytes(data)})
        else:
            out_wizard = self.env['xlsx.output'].create({'name': name + '.xlsx',
                                                         'xls_output': base64.encodebytes(data)})
        view_id = self.env.ref('nuro_fms_clinic.xlsx_output_form').id
        return {
            'type': 'ir.actions.act_window',
            'res_model': 'xlsx.output',
            'target': 'new',
            'view_mode': 'form',
            'res_id': out_wizard.id,
            'views': [[view_id, 'form']],
        }
