# -*- coding: utf-8 -*-
# Copyright  Nuro Solution Pvt Ltd
{
    'name': 'FMS Project Dashboard',
    'category': 'Project',
    'author': '',
    'summary': 'FMS Project Dashboard',
    "description":
        """
            FMS Project Dashboard
        """,
    'depends': [
        'base',
        'website_support',
        'project',
        'stock',
        'analytic',
        'website_support',
        'nuro_branch_management',
        'hr_timesheet',
        'nuro_product_boq',
        'account',
        'nuro_project_task_management',
    ],
    'data': [
        'views/project_view.xml'
    ],
    'qweb': [],
    'demo': [],
    "website": "https://www.nurosolution.com",
    "author": "Nuro Solution Pvt Ltd",
    'license': 'Other proprietary',
    'installable': True,
    'auto_install': True,
}
