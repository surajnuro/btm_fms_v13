# -*- coding: utf-8 -*-
# Copyright  Nuro Solution Pvt Ltd
from odoo import fields,models,api,_
from odoo.exceptions import ValidationError, UserError, Warning

class Project(models.Model):
    _inherit = 'project.project'

    total_top = fields.Integer(compute='compute_total_top')
    all_task = fields.Integer(compute='compute_all_task')
    all_invoices_amt = fields.Float(compute='compute_all_invoices_amt')
    all_bill_amt = fields.Float(compute='compute_all_bill_amt')
    total_po = fields.Float(compute='compute_all_po')

    def compute_total_top(self):
        ''' top count'''
        for rec in self:
            top_ids = self.env['project.task'].search_count([('project_id', '=', rec.id),
                                                             ('work_type', '=', 'unschedulework'),
                                                             ('top_task', '=', True)])
            if top_ids:
                rec.total_top = top_ids
            else:
                rec.total_top = 0

    def compute_all_task(self):
        '''compute all task'''
        for rec in self:
            rec.all_task = 0

    def compute_all_invoices_amt(self):
        ''' compute all invoices amount'''
        for rec in self:
            total = 0
            inv_ids = self.env['account.move'].search([('project_id', '=', rec.id), ('type', '=', 'out_invoice')])
            for inv in inv_ids:
                total += inv.amount_total
            rec.all_invoices_amt = total

    def compute_all_bill_amt(self):
        ''' compute bill amount'''
        for rec in self:
            total = 0
            inv_ids = self.env['account.move'].search([('project_id', '=', rec.id), ('type', '=', 'in_invoice')])
            for inv in inv_ids:
                total += inv.amount_total
            rec.all_bill_amt = total

    def compute_all_po(self):
        ''' compute bill amount'''
        for rec in self:
            total = 0
            po_ids = self.env['purchase.order'].search_count([('project_id', '=', rec.id)])
            if po_ids:
                rec.total_po = po_ids
            else:
                rec.total_po = po_ids

    def get_action_all_top(self):
        '''open all top'''
        task_ids = self.env['project.task'].search([('project_id', '=', self.id),('work_type', '=', 'unschedulework'),
                                                             ('top_task', '=', True)])
        form_view = self.env.ref('nuro_project_task_management.task_top_work_form_view').id
        tree_view = self.env.ref('nuro_project_task_management.task_top_tree_view').id
        return {'type': 'ir.actions.act_window',
                'name': 'TOP',
                'views': [
                    (tree_view, 'tree'),
                    (form_view, 'form'),
                ],
                'res_model': 'project.task',
                'context': {'default_project_id': self.id, 'create': False},
                'domain': [('id', 'in', task_ids.ids)],
                }

    def get_action_all_task(self):
        ''' open all task'''
        task_ids = self.env['project.task'].search([('project_id', '=', self.id), ('work_type', '=', 'unschedulework'),
                                                    ])
        form_view = self.env.ref('nuro_project_task_management.unschedule_task_form_view').id
        tree_view = self.env.ref('nuro_project_task_management.unschedule_task_tree_view').id
        return {'type': 'ir.actions.act_window',
                'name': 'Task',
                'views': [
                    (tree_view, 'tree'),
                    (form_view, 'form'),
                ],
                'res_model': 'project.task',
                'context': {'default_create': False, 'create': False},
                'domain': [('id', 'in', task_ids.ids)],
                }

    def get_action_invoices(self):
        ''' get all invoices'''
        inv_ids = self.env['account.move'].search([('project_id', '=', self.id), ('type', '=', 'out_invoice')])
        return {'type': 'ir.actions.act_window',
                'name': 'Invoices',
                'view_mode': 'tree',
                'res_model': 'account.move',
                'context': {'default_create': False},
                'domain': [('id', 'in', inv_ids.ids)],
                }

    def get_action_bill_invoices(self):
        '''get all bills'''
        bill_ids = self.env['account.move'].search([('project_id', '=', self.id), ('type', '=', 'in_invoice')])
        return {'type': 'ir.actions.act_window',
                'name': 'Invoices',
                'res_model': 'account.move',
                'view_mode': 'tree',
                'context': {'default_create': False},
                'domain': [('id', 'in', bill_ids.ids)],
                }

    def get_action_po(self):
        '''get all po'''
        po_ids = self.env['purchase.order'].search([('project_id', '=', self.id)])
        return {'type': 'ir.actions.act_window',
                'name': 'PO',
                'res_model': 'purchase.order',
                'view_mode': 'tree',
                'context': {'default_create': False},
                'domain': [('id', 'in', po_ids.ids)],
                }

    def action_create_top(self):
        ''' open top form'''
        if self.top_apply == False:
            raise ValidationError(('Can not create TOP if project have not top apply'))
        form_view = self.env.ref('nuro_project_task_management.task_top_work_form_view').id
        tree_view = self.env.ref('nuro_project_task_management.task_top_tree_view').id
        return {'type': 'ir.actions.act_window',
                'name': 'TOP',
                'views': [
                    (form_view, 'form'),
                ],
                'res_model': 'project.task',
                'context': {'default_project_id': self.id, 'default_work_type': 'unschedulework', 'deafult_top_task': True},
                }

    def action_create_hardwall_task(self):
        '''create hardwall task if projectnot have top apply'''
        if self.top_apply == True:
            raise ValidationError(('Can not create task if project have TOP Apply'))
        form_view = self.env.ref('nuro_project_task_management.task_hardwall_form_view').id
        return {'type': 'ir.actions.act_window',
                'name': 'Hardwall task',
                'views': [
                    (form_view, 'form'),
                ],
                'res_model': 'project.task',
                'context': {'default_project_id': self.id, 'default_hardwall_task': True},
                }