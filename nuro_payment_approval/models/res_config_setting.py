from odoo import api, fields, models


class ResConfigSettings(models.TransientModel):
    _inherit = 'res.config.settings'

    vendor_payment_approval = fields.Boolean('Vendor Payment Approval')
    vendor_amount = fields.Monetary('Amount')
    approval_level = fields.Selection([('direct', 'Direct'),
                                       ('single', 'Single Level'),
                                       ('double', 'Double Level')], default='direct')

    @api.onchange('approval_level')
    def onchange_level_amt(self):
        if self.approval_level:
            if self.approval_level == 'direct':
                self.vendor_amount = 0.0

    def get_values(self):
        vendor_approval_res = super(ResConfigSettings, self).get_values()
        vendor_approval_res.update(
            vendor_payment_approval=self.env['ir.config_parameter'].get_param('vendor_payment_approval'),
            approval_level=self.env['ir.config_parameter'].get_param('approval_level'),
            vendor_amount=float(self.env['ir.config_parameter'].get_param('vendor_amount')),
        )
        return vendor_approval_res

    def set_values(self):
        super(ResConfigSettings, self).set_values()
        self.env['ir.config_parameter'].set_param('vendor_payment_approval', self.vendor_payment_approval)
        self.env['ir.config_parameter'].set_param('approval_level', self.approval_level)
        if self.vendor_payment_approval:
            self.env['ir.config_parameter'].set_param('vendor_amount', self.vendor_amount)

