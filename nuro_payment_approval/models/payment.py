from odoo import api, fields, models,_
from odoo.exceptions import UserError, AccessError, ValidationError
from datetime import datetime

class AccountPayment(models.Model):
    _inherit = 'account.payment'


    name = fields.Char(readonly=True, copy=False, default=False) # The name is attributed upon post()
    state = fields.Selection(selection_add=[('under_review', 'Under Review'),('waiting_approval', 'Waiting Approval'),
                                            ('approved', 'Approved')], track_visibility="onchange")
    bill_exp_type = fields.Selection([('bill', 'Bill Payment'),
                                      ('expense', 'Expense')], default='bill', string='Bill-Expense Type')
    review_date = fields.Datetime('Review Date')
    review_to = fields.Many2one('res.users', 'Review To')
    requested_by = fields.Many2one('res.users', track_visibility="onchange")
    requested_to = fields.Many2one('res.users', track_visibility="onchange")
    requested_date = fields.Datetime('Requested Date')
    approved_by = fields.Many2one('res.users', track_visibility="onchange")
    approve_date = fields.Datetime('Approved Date')
    rejected_by = fields.Many2one('res.users', track_visibility="onchange")
    vendor_payment_approval = fields.Boolean('Vendor Payment Approval', compute='vendor_payment_Approval')
    vendor_amount = fields.Text('Amount', compute='vendor_payment_Approval')
    is_approvals = fields.Boolean(compute='is_payment_approved', store=True)
    is_double_approval = fields.Boolean(compute='is_payment_approved', store=True)
    approval_level = fields.Selection([('direct', 'Direct'),
                                       ('single', 'Single Level'),
                                       ('double', 'Double Level')], compute='is_payment_approved', store=True)
    total_due_amt = fields.Float(compute='_compute_total_due_amt', string='Due Amount', store=True)
    total_due_amt_customer = fields.Float(compute='_compute_total_due_amt_customer', string='Due Amount', store=True)


    @api.depends('partner_id')
    def _compute_total_due_amt_customer(self):
        for rec in self:
            total = 0.0
            invoices = self.env['account.move'].search([('partner_id', '=', rec.partner_id.id),
                                                           ('type', '=', 'out_invoice'), ('state','=', 'open')])
            if invoices:
                for inv in invoices:
                    total += inv.residual
            rec.total_due_amt_customer = total

    @api.depends('partner_id')
    def _compute_total_due_amt(self):
        for rec in self:
            total = 0.0
            invoices = self.env['account.move'].search([('partner_id', '=', rec.partner_id.id),
                                                           ('type', '=', 'in_invoice'), ('state','=', 'open')])
            if invoices:
                for inv in invoices:
                    total += inv.residual
            rec.total_due_amt = total

    @api.depends('amount', 'partner_id')
    def is_payment_approved(self):
        for rec in self:
            level = rec.env['ir.config_parameter'].get_param('approval_level')
            amount = rec.vendor_amount = rec.env['ir.config_parameter'].get_param('vendor_amount')
            rec.approval_level = level
            rec.is_double_approval = False
            rec.is_approvals = False
            if level == 'direct':
                rec.is_double_approval = False
                rec.is_approvals = False
            if level == 'single':
                if rec.amount >= float(amount):
                    rec.is_approvals = True
            if level == 'double':
                if rec.amount >= float(amount):
                    rec.is_double_approval = True

    def vendor_payment_Approval(self):
        for rec in self:
            rec.vendor_payment_approval = rec.env['ir.config_parameter'].get_param('vendor_payment_approval')
            rec.vendor_amount = rec.env['ir.config_parameter'].get_param('vendor_amount')
            rec.approval_level = rec.env['ir.config_parameter'].get_param('approval_level')

    def approved(self):
        self.write({'state': 'approved',
                    'approved_by': self.env.user.id,
                    'approve_date': datetime.now()})

    def post(self):
        """ Create the journal items for the payment and update the payment's state to 'posted'.
            A journal entry is created containing an item in the source liquidity account (selected journal's default_debit or default_credit)
            and another in the destination reconcilable account (see _compute_destination_account_id).
            If invoice_ids is not empty, there will be one reconcilable move line per invoice to reconcile with.
            If the payment is a transfer, a second journal entry is created in the destination journal to receive money from the transfer account.
        """
        # override for remove draft payment restriction
        AccountMove = self.env['account.move'].with_context(default_type='entry')
        for rec in self:

            # if rec.state != 'draft':
            #     raise UserError(_("Only a draft payment can be posted."))

            if any(inv.state != 'posted' for inv in rec.invoice_ids):
                raise ValidationError(_("The payment cannot be processed because the invoice is not open!"))

            # keep the name in case of a payment reset to draft
            if not rec.name:
                # Use the right sequence to set the name
                if rec.payment_type == 'transfer':
                    sequence_code = 'account.payment.transfer'
                else:
                    if rec.partner_type == 'customer':
                        if rec.payment_type == 'inbound':
                            sequence_code = 'account.payment.customer.invoice'
                        if rec.payment_type == 'outbound':
                            sequence_code = 'account.payment.customer.refund'
                    if rec.partner_type == 'supplier':
                        if rec.payment_type == 'inbound':
                            sequence_code = 'account.payment.supplier.refund'
                        if rec.payment_type == 'outbound':
                            sequence_code = 'account.payment.supplier.invoice'
                rec.name = self.env['ir.sequence'].next_by_code(sequence_code, sequence_date=rec.payment_date)
                if not rec.name and rec.payment_type != 'transfer':
                    raise UserError(_("You have to define a sequence for %s in your company.") % (sequence_code,))

            moves = AccountMove.create(rec._prepare_payment_moves())
            moves.filtered(lambda move: move.journal_id.post_at != 'bank_rec').post()

            # Update the state / move before performing any reconciliation.
            move_name = self._get_move_name_transfer_separator().join(moves.mapped('name'))
            rec.write({'state': 'posted', 'move_name': move_name})

            if rec.payment_type in ('inbound', 'outbound'):
                # ==== 'inbound' / 'outbound' ====
                if rec.invoice_ids:
                    (moves[0] + rec.invoice_ids).line_ids \
                        .filtered(lambda line: not line.reconciled and line.account_id == rec.destination_account_id) \
                        .reconcile()
            elif rec.payment_type == 'transfer':
                # ==== 'transfer' ====
                moves.mapped('line_ids') \
                    .filtered(lambda line: line.account_id == rec.company_id.transfer_account_id) \
                    .reconcile()

        return True

