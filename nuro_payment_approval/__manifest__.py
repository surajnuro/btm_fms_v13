
{
    'name': "Payment Approval Management",
    'version': '11.0.2.0.0',
    'summary': """Payyment ApprovalPa""",
    'description': """""",
    'category': 'Business',
    'author': 'Nuro Software',
    'company': 'Nuro Software',
    'website': "https://www.nurosolution.com",
    'depends': ['account', 'payment', 'account_pdc'],
    'data': [
        'security/security_view.xml',
        'wizard/submit_approval_view.xml',
        'wizard/submit_for_review_view.xml',
        # 'views/invoice_view.xml',
        'views/payment_view.xml',
        'views/customer_payment_view.xml',
        'views/res_config_setting_view.xml',
    ],
    'license': "AGPL-3",
    'installable': True,
    'application': True,
}
