from odoo import models, fields, api, _
from odoo.exceptions import UserError, ValidationError


class account_register_payments(models.TransientModel):
    _inherit = "account.register.payments"

    def create_payments(self):
        '''Create payments according to the invoices.
        Having invoices with different commercial_partner_id or different type (Vendor bills with customer invoices)
        leads to multiple payments.
        In case of all the invoices are related to the same commercial_partner_id and have the same type,
        only one payment will be created.

        :return: The ir.actions.act_window to show created payments.
        '''
        if not self.env.user.has_group('nuro_payment_approval.group_approved_payment'):
            raise ValidationError(_('Sorry You Have Not Access To Register Payment!!!'))

        Payment = self.env['account.payment']
        payments = Payment
        for payment_vals in self.get_payments_vals():
            payments += Payment.create(payment_vals)
        payments.post()
        return {
            'name': _('Payments'),
            'domain': [('id', 'in', payments.ids), ('state', '=', 'posted')],
            # 'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'account.payment',
            'view_id': False,
            'type': 'ir.actions.act_window',
        }