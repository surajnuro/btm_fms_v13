from odoo import api, fields, models, _
from odoo.exceptions import ValidationError, Warning
from datetime import datetime

class SubmitPayment(models.TransientModel):
    _name="submit.payment.wiz"

    user_id = fields.Many2one('res.users','Submit to')
    payment_id = fields.Many2one('account.payment')

    def send_for_approval(self):
        active_id = self.env.context.get('active_id')
        payment = self.env['account.payment'].browse(active_id)
        payment.write({'state':'waiting_approval',
                          'requested_by': self.env.user.id,
                          'requested_to': self.user_id.id,
                          'requested_date': datetime.now()})
        return True

    @api.model
    def default_get(self, fields):
        res = super(SubmitPayment, self).default_get(fields)
        payment_id = self.env['account.payment'].search([('id', 'in', self._context.get('active_ids'))])
        if payment_id:
            res['payment_id'] = payment_id.id
        return res

    @api.onchange('payment_id')
    def onchange_payment(self):
        groups = self.env.ref('nuro_payment_approval.group_approved_payment')
        user_lst = []
        for user in groups.sudo().users:
            user_lst.append(user.id)
        return {'domain': {'user_id': [('id', 'in', user_lst)]}}



