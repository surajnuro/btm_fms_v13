from odoo import api, fields, models, _
from odoo.exceptions import ValidationError, Warning
from datetime import datetime

class SubmitReview(models.TransientModel):
    _name="submit.review"

    user_id = fields.Many2one('res.users','Submit to')

    def send_for_review(self):
        active_id = self.env.context.get('active_id')
        payment = self.env['account.payment'].browse(active_id)
        payment.write({'state':'under_review',
                          'review_to': self.user_id.id,
                          'review_date': datetime.now()})
        return True


class Users(models.Model):
    _inherit = "res.users"

    @api.depends('groups_id')
    def check_payment_review(self):
        for users in self:
            if users.has_group('nuro_payment_approval.group_review_payment'):
                users.is_review = True
            else:
                users.is_review = False

    is_review= fields.Boolean('Is PAyment Review', compute='check_payment_review', store=True)
