# -*- coding: utf-8 -*-
# Copyright  Nuro Solution Pvt Ltd
{
    'name': 'Product BOQ Rate Configuration',
    'category': 'Product',
    'author': '',
    'summary': 'Product BOQ Rate Configuration',
    "description":
        """
            Product BOQ Rate Configuration
        """,
    'depends': [
        'base',
        'product',
    ],
    'data': [
        'security/ir.model.access.csv',
        'views/product_view.xml',
        'data/data_view.xml',
    ],
    'qweb': [],
    'demo': [],
    "website": "https://www.nurosolution.com",
    "author": "Nuro Solution Pvt Ltd",
    'license': 'Other proprietary',
    'installable': True,
    'auto_install': True,
}
