from odoo import fields,models,_, api

class ProductTemplate(models.Model):
    _inherit = 'product.template'

    boq_line = fields.One2many('product.boq.line', 'product_id')
    product_boq_ref = fields.Char('BOQ Ref')

class BoqLine(models.Model):
    _name = 'product.boq.line'

    product_id = fields.Many2one('product.template')
    partner_id = fields.Many2one('res.partner', 'Vendor')
    amount = fields.Float('Amount')