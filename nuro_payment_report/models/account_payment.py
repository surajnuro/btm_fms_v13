from odoo import models, fields, api, _

class Accountpayment(models.Model):
    _inherit = 'account.payment'

    posted_id = fields.Many2one('res.users', 'Posted')

    def post(self):
        res = super(Accountpayment, self).post()
        for rec in self:
            rec.write({'posted_id': rec.env.user.id})
        return res
