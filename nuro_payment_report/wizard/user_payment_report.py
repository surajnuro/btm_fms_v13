from odoo import models, fields, api, _
import base64
import xlsxwriter
from datetime import datetime

class XlsxOutput(models.TransientModel):
    _name = 'user.payment.report'

    from_date = fields.Date('From Date')
    to_date = fields.Date('To Date')
    user_id = fields.Many2one('res.users', 'User')
    journal_id = fields.Many2one('account.journal', 'Journal')

    def print_payment_report(self):
        f_name = '/tmp/payment_report.xlsx'
        workbook = xlsxwriter.Workbook(f_name)
        worksheet = workbook.add_worksheet('Report')
        worksheet.set_column('A:K', 12)
        style = workbook.add_format({
            'bold': 1,
            'border': 1,
            'align': 'center',
            'valign': 'vcenter'})
        style.set_font_size(8)
        align_value = workbook.add_format({
            'align': 'center',
            'valign': 'vcenter'})
        align_value.set_font_size(8)

        domain = [('payment_type', '=', 'outbound'),('state', '=', 'posted')]
        if self.user_id:
            domain += [('posted_id', '=', self.user_id.id)]
        if self.from_date:
            domain += [('payment_date', '>=', self.from_date)]
        if self.to_date:
            domain += [('payment_date', '<=', self.to_date)]
        if self.journal_id:
            domain += [('journal_id', '=', self.journal_id.id)]
        payments = self.env['account.payment'].sudo().search(domain)
        row = 4
        new_row = row + 1
        worksheet.merge_range('A1:J1', "DEEQA CONSTRUCTION & WATER WELL DRILLING CO. LTD.", style)
        date = 'ab'
        if self.to_date and self.from_date:
            # date = datetime.strptime(self.to_date, '%Y-%m-%d')
            worksheet.merge_range('A2:G2', "REPORT FORMAT FOR User Payment Report for Date OF ", style)
            worksheet.merge_range('H2:J2', str(self.from_date) + ' ' + 'To' + ' ' + str(self.to_date), style)
        else:
            worksheet.merge_range('A2:J2', " REPORT FORMAT FOR USER PAYMENT REPORT", style)
        worksheet.write('A4', "Date", style)
        worksheet.write('B4', "Payment Reference", style)
        worksheet.write('C4', "Payment Journal", style)
        worksheet.write('D4', "User", style)
        worksheet.write('E4', "Amount", style)
        total = 0.0
        for pay in payments:
            worksheet.write("A%s" % (new_row), str(pay.payment_date) or "Not Filled", align_value)
            worksheet.write("B%s" % (new_row), pay.name or "Not Filled", align_value)
            worksheet.write("C%s" % (new_row), pay.journal_id.name or "Not Filled", align_value)
            if pay.posted_id:
                worksheet.write("D%s" % (new_row), pay.posted_id.name or "Not Filled", align_value)
            else:
                worksheet.write("D%s" % (new_row), pay.create_uid.name or "Not Filled", align_value)
            worksheet.write("E%s" % (new_row), pay.amount or '-', align_value)
            total += pay.amount
            new_row += 1
        worksheet.merge_range("A%s:D%s" % (new_row, new_row), 'ToTal', style)
        worksheet.write("E%s" % (new_row), total, style)
        workbook.close()
        f = open(f_name, 'rb')
        data = f.read()
        f.close()
        name = 'Payment Report'
        dt = ' ' + 'From' + ' ' + str(self.from_date) + ' ' + 'To' + ' ' + str(self.to_date)
        if self.from_date and self.to_date:
            out_wizard = self.env['xlsx.output'].create({'name': name + dt + '.xlsx',
                                                         'xls_output': base64.encodebytes(data)})
        else:
            out_wizard = self.env['xlsx.output'].create({'name': name + '.xlsx',
                                                         'xls_output': base64.encodebytes(data)})
        view_id = self.env.ref('nuro_payment_report.xlsx_output_form').id
        return {
            'type': 'ir.actions.act_window',
            'name': _(name),
            'res_model': 'xlsx.output',
            'target': 'new',
            'view_mode': 'form',
            'res_id': out_wizard.id,
            'views': [[view_id, 'form']],
        }



