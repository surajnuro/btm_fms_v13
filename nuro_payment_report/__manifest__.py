# -*- coding: utf-8 -*-
# Copyright  Nuro Solution Pvt Ltd
{
    'name': 'Nuro Payment report',
    'category': 'Account',
    'summary': 'This module print daily payment entry',
    'description': """
        This module print daily payment entry
    """,
    'author': 'Nuro Solution Pvt Ltd',
    'website': 'http://www.nurosolution.com',
    'company': 'Nuro Solution Pvt Ltd',
    'license': 'Other proprietary',
    'depends': ['account'],
    'data': [
        'wizard/user_payment_report.xml',
        'wizard/xlsx_output_view.xml',
    ],
    'installable': True,
    'application': True,
}