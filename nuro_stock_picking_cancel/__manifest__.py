{
    'name': 'Stock Picking Cancel',
    'version': '13.0.0.1',
    'category': 'Warehouse',
    'sequence': 1,
    'summary': 'Cancel Done Picking or cancel done stock move',
    'description': """
        This module helps in reset stock move or cancel stock move or cancel stock picking.
        cancel Delivery order or cancel Receipt. Delivery order cancel / reverse .
        cancel done delivery order. 
        This module helps to reverse the done picking, allow to cancel picking and 
        set it to draft stage.
    """,
    'author': "Nurosolution Pvt Ltd",
    'website': "https://www.nurosolution.com",
    'depends': ['stock'],
    'data': [
        'security/security_view.xml',
        'wizard/cancel_move_wzard_view.xml',
        'views/stock_view.xml',
    ],
    'images': ['static/description/icon.png'],
    'license': 'Other proprietary',
    'installable': True,
    'application': True,
}
