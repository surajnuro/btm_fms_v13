from odoo import api, fields, models, _

class Team(models.Model):
    _name="tkt.team"

    name = fields.Text('Name')
    user_ids = fields.Many2many('res.users','res_team_rel','team_id','resp_id','Users')
    project_ids = fields.Many2many('project.project','project_team_rel','tesm_id','proj_id','Project')

    def write(self, vals):
        res = super(Team, self).write(vals)
        for rec in vals['project_ids']:
            self.team_ids = [(6,0, [self.id])]
        return res