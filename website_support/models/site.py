from odoo import api, fields, models, _

class Site(models.Model):
    _name="tkt.site"
    _inherit = ['mail.thread', 'mail.activity.mixin', 'portal.mixin']

    name = fields.Text('Site', track_visibility='onchange')
    active = fields.Boolean(default=True, help="If the active field is set to False, it will allow you to hide the payment terms without removing it.")


