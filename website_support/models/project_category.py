from odoo import api, fields, models, tools


class ProjectCategory(models.Model):
    _name = 'project.category'

    name = fields.Char('Project Category Name')
    user_id = fields.Many2one('res.users','Manager')
    report_id = fields.Many2one('ir.actions.report','Report')
    partner_id = fields.Many2one('res.partner', 'Client Project Representative')
    inv_prefix = fields.Char("Prefix")
