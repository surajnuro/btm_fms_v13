from odoo import api, fields, models, _

class Workcategories(models.Model):
    _name="work.categories"

    name = fields.Char('Categories Name',required=True)