# -*- coding: utf-8 -*-
from odoo import api, fields, models, _
from odoo import tools
from random import randint
from datetime import datetime
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT, DEFAULT_SERVER_DATE_FORMAT
import logging
_logger = logging.getLogger(__name__)
from odoo import SUPERUSER_ID
from odoo.exceptions import ValidationError

class WebsiteSupportTicket(models.Model):

    _name = "website.support.ticket"
    _description = "Website Support Ticket"
    _rec_name = "ticket_number"
    _order = 'id desc'
    _inherit = ['mail.thread']

    states = fields.Selection([('new','New'),('waiting_for_approval','Waiting For Approval'),('approved','Approved'),('inprogress','In progress'),
            ('staff_close','Closed By Staff'),('customer_close','Closed By Customer'),
                               ('rejected','Rejected')],default="new", string ="Status", copy=False, track_visibility='onchange')
    client_tic_no = fields.Char("Request ID")
    channel = fields.Char(string="Channel", default="Manual")
    create_user_id = fields.Many2one('res.users', "Creator", default=lambda self: self.env.user)
    priority_id = fields.Many2one('website.support.ticket.priority',  string="Priority")
    # priority_id = fields.Many2one('website.support.ticket.priority', default=_default_priority_id, string="Priority")
    partner_id = fields.Many2one('res.partner', string="Partner")
    user_id = fields.Many2one('res.users', string="Current Assignee",readonly=True)
    supervisor_id = fields.Many2one('res.users',string='Supervisor', track_visibility='onchange')
    person_name = fields.Char(string='Requester Name')
    email = fields.Char(string="Email")
    support_email = fields.Char(string="Support Email")
    # category = fields.Many2one('website.support.ticket.categories', string="Category", track_visibility='onchange')
    # sub_category_id = fields.Many2one('website.support.ticket.subcategory', string="Sub Category")
    subject = fields.Char(string="Category of Incident")
    description = fields.Text(string="Description")
    state = fields.Many2one('website.support.ticket.states', string="State")
    # conversation_history = fields.One2many('website.support.ticket.message', 'ticket_id', string="Conversation History")
    attachment = fields.Binary(string="Attachments")
    attachment_filename = fields.Char(string="Attachment Filename")
    attachment_ids = fields.One2many('ir.attachment', 'res_id', domain=[('res_model', '=', 'website.support.ticket')], string="Media Attachments")
    unattended = fields.Boolean(string="Unattended", store="True", help="In 'Open' state or 'Customer Replied' state taken into consideration name changes")
    portal_access_key = fields.Char(string="Portal Access Key")
    ticket_number = fields.Char(string="Ticket Number", track_visibility='onchange')
    # ticket_number_display = fields.Char(string="Ticket Number Display", compute="_compute_ticket_number_display")
    ticket_color = fields.Char(related="priority_id.color", string="Ticket Color")
    company_id = fields.Many2one('res.company', string="Company", default=lambda self: self.env['res.company']._company_default_get('website.support.ticket') )
    # support_rating = fields.Integer(string="Support Rating")
    # support_comment = fields.Text(string="Support Comment")
    close_comment = fields.Text(string="Close Comment")
    close_time = fields.Datetime(string="Close Time")
    close_date = fields.Date(string="Close Date")
    closed_by_id = fields.Many2one('res.users', string="Closed By")
    time_to_close = fields.Integer(string="Time to close (seconds)")
    # extra_field_ids = fields.One2many('website.support.ticket.field', 'wst_id', string="Extra Details")
    planned_time = fields.Datetime(string="Planned Time")
    # approval_id = fields.Many2one(string="Approval")
    approval_message = fields.Text(string="Approval Message")
    approve_url = fields.Char(compute="_compute_approve_url", string="Approve URL")
    disapprove_url = fields.Char(compute="_compute_disapprove_url", string="Disapprove URL")
    tag_ids = fields.Many2many('website.support.ticket.tag', string="Tags")
    state_name = fields.Char(related='state.name', string='State name')
    # task_count = fields.Integer(string="Tasks")
    task_count = fields.Integer(compute='_compute_task_count', string="Tasks")
    top_count = fields.Integer(compute='_compute_top_count', string="Tasks")
    active = fields.Boolean(default=True,
                            help="If the active field is set to False, it will allow you to hide the project without removing it.")
    doc_count = fields.Integer(compute='_compute_attached_docs_count', string="Number of documents attached")
    task_ids = fields.One2many('project.task', 'web_ticket_id',  string='Tasks',
                               domain=['|', ('stage_id.fold', '=', False), ('stage_id', '=', False)])
    work_type = fields.Selection([('regularmain','Routine Maintenance'), ('unschedulework','Unschedule Work')], string="Work Type", copy=False)
    branch_id = fields.Many2one('res.branch', string='Branch', tracking=True, copy=False)
    # project_task_id = fields.Many2one('project.task')
    related_person_name_id = fields.Many2one('res.partner','Related Person Name')
    quotation_id = fields.Many2one('project.task','Quotation',readonly=True,copy=False)
    quotation = fields.Char('Quotation', copy=False)
    location = fields.Text('Location', copy=False)
    team_id = fields.Many2one('tkt.team','Team', copy=False)
    site_id = fields.Many2one('tkt.site','Site', copy=False)
    receive_date = fields.Date('Date', default=fields.Date.context_today)
    start_date = fields.Datetime('Start Date', copy=False)
    close_date = fields.Date('Close Date', copy=False, readonly=True)

    location_comp = fields.Char('Location(Comp)')
    location_phase = fields.Char('Phase/Building')
    location_room = fields.Char('Room')
    contect_num = fields.Char('Contact No')
    mobile_num = fields.Char('Mobile No')
    beneficially_name = fields.Char('Beneficially')
    equipment_type =fields.Char('Type of Equipment')
    technician_contact = fields.Char('Technician Contact')
    request_type = fields.Many2one('request.type', string='Types of Request')
    date_due = fields.Datetime('Due Date')
    name_first = fields.Char('First Name')
    name_last = fields.Char('Last Name')
    work_categories_ids = fields.Many2many('work.categories', 'work_categ_rel', 'work_tkt_id', 'eork_categ_id',
                                           'Work Categories')
    project_id = fields.Many2one('project.project', string='Project', tracking=True, copy=False)
    approval_user_id = fields.Many2one('res.users','Approval By')
    approved_user_id = fields.Many2one('res.users','Approved by')
    trasnfer_reason = fields.Char('Transfer Reason', track_visibility='onchange')
    ticket_type = fields.Selection([('checker','Checker Team'),
                                    ('unticket','UN Ticket')], string="Ticket Type")


    _sql_constraints = [
        ('client_tic_no', 'unique (client_tic_no)', "Client Request ID can not be duplicated")
    ]

    def action_reset_to_draft(self):
        self.write({'states': 'new'})

    def unlink(self):
        for rec in self:
            if rec.states != 'new':
                raise ValidationError('You can delete only new stage ticket!!!')
        return super(WebsiteSupportTicket, self).unlink()
    

    @api.constrains('ticket_type')
    def check_input(self):
        if self.ticket_type == 'checker':
            if self.client_tic_no.isdigit():
                raise ValidationError('Request ID should start with character')
        if self.ticket_type == 'unticket':
            if not self.client_tic_no.isdigit():
                raise ValidationError('Request ID should start with number')

    def get_task(self):
        task_env = self.env['project.task']
        form_view_id = self.env.ref('nuro_project_task_management.task_regular_form_view').id
        tree_view_id = self.env.ref('nuro_project_task_management.task_regular_tree_view').id
        task_data = task_env.search([('ticket_id', 'in', self.ids), ('work_type', '=', 'regularmain')])
        if task_data:
            return {
                'name': "Task",
                'type': 'ir.actions.act_window',
                'view_mode': 'tree,form',
                'res_model': 'project.task',
                'res_id': task_data.ids,
                'domain':[('id','in',task_data.ids)],
                'views': [
                    (tree_view_id, 'tree'),
                    (form_view_id, 'form'),
                ],
            }


    def attachment_tree_view(self):
        self.ensure_one()
        domain = [
            '|',
            '&', ('res_model', '=', 'project.project'), ('res_id', 'in', self.ids),
            '&', ('res_model', '=', 'project.task'), ('res_id', 'in', self.task_ids.ids)]
        return {
            'name': _('Attachments'),
            'domain': domain,
            'res_model': 'ir.attachment',
            'type': 'ir.actions.act_window',
            'view_id': False,
            'view_mode': 'kanban,tree,form',
            'view_type': 'form',
            'help': _('''<p class="oe_view_nocontent_create">
                            Documents are attached to the tasks and issues of your project.</p><p>
                            Send messages or log internal notes with attachments to link
                            documents to your project.
                        </p>'''),
            'limit': 80,
            'context': "{'default_res_model': '%s','default_res_id': %d}" % (self._name, self.id)
        }

    def _compute_attached_docs_count(self):
        Attachment = self.env['ir.attachment']
        for project in self:
            project.doc_count = Attachment.search_count([
                '|',
                '&',
                ('res_model', '=', 'project.project'), ('res_id', '=', project.id),
                '&',
                ('res_model', '=', 'project.task'), ('res_id', 'in', project.task_ids.ids)
            ])

    def _compute_task_count(self):
        task_env = self.env['project.task']
        for project in self:
            task_data = task_env.search_count([('ticket_id', 'in', self.ids), ('work_type', '=', 'regularmain')])
            project.task_count = task_data

    def _compute_top_count(self):
        '''compute top '''
        task_env = self.env['project.task']
        for project in self:
            top_data = task_env.search_count([('ticket_id', 'in', self.ids),
                                               ('work_type', '=', 'unschedulework'),
                                               ('top_task', '=', True),
                                                ])
            project.top_count = top_data

    def action_approved(self):
        # state_type = self.env.ref('website_support.ticket_managment_approve')
        # self.write({'state': state_type.id})
        self.ensure_one()
        return {
            'name': _('Ticket Maintenance'),
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'ticket.maintenance',
            'view_id': self.env.ref('website_support.ticket_management_wizard').id,
            'type': 'ir.actions.act_window',
            'target': 'new',
        }

    def action_transfer_ticket(self):
        self.ensure_one()
        self = self.with_context(transfer=True)
        return {
            'name': _('Ticket Transfer'),
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'ticket.maintenance',
            'view_id': self.env.ref('website_support.ticket_management_wizard').id,
            'type': 'ir.actions.act_window',
            'target': 'new',
            'context': {'transfer': True},

        }

    @api.onchange('project_id')
    def onchange_branch_id(self):
        branch_lst = []
        if self.project_id:
            for b in self.project_id.branch_ids:
                branch_lst.append(b.id)
        domain = {'branch_id': [('id', 'in', branch_lst)]}
        return {'domain': domain}

    def action_progress(self):
        self.write({'states':'inprogress'})
        return True

    def action_customer_close(self):
        self.close_date = datetime.today()
        self.write({'states': 'customer_close'})
        return True

    def action_close(self):
        task_ids = self.env['project.task'].search([('ticket_id', '=', self.id)])
        for task in task_ids:
            if task.stage_name != 'Done':
                raise ValidationError(_('Task Stage is not Done'))
        picking_id = self.env['stock.picking'].search([('ticket_id', '=', self.id)])
        if picking_id:
            for pick in picking_id:
                if pick.state != 'done':
                    pick.sudo().action_cancel()

        self.write({'states': 'staff_close'})
        # template = self.env.ref('website_support.staff_close_mail1_template')
        # template.send_mail(self.id, force_send=True)
        return True

    def action_reject(self):
        # state_app_type = self.env.ref('website_support.ticket_managment_reject')
        self.write({'states': 'rejected'})
        return True

    def _compute_approve_url(self):
        self.approve_url = "/support/approve/" + str(self.id)

    def _compute_disapprove_url(self):
        self.disapprove_url = "/support/disapprove/" + str(self.id)

    @api.onchange('sub_category_id')
    def _onchange_sub_category_id(self):
        if self.sub_category_id:

            add_extra_fields = []

            for extra_field in self.sub_category_id.additional_field_ids:
                add_extra_fields.append((0, 0, {'name': extra_field.name}))

            self.update({
                'extra_field_ids': add_extra_fields,
            })

    @api.onchange('partner_id')
    def _onchange_partner_id(self):
        self.person_name = self.partner_id.name
        self.email = self.partner_id.email

    def message_new(self, msg, custom_values=None):
        """ Create new support ticket upon receiving new email"""

        defaults = {'support_email': msg.get('to'), 'subject': msg.get('subject')}
        fetchmail_server_id = self.env.context.get('fetchmail_server_id', False)
        incoming_mail = False
        if fetchmail_server_id:
            incoming_mail = self.env['fetchmail.server'].browse(fetchmail_server_id)

        #Extract the name from the from email if you can
        if "<" in msg.get('from') and ">" in msg.get('from'):
            start = msg.get('from').rindex( "<" ) + 1
            end = msg.get('from').rindex( ">", start )
            from_email = msg.get('from')[start:end]
            from_name = msg.get('from').split("<")[0].strip()
            defaults['branch_id'] = from_name
        else:
            from_email = msg.get('from')

        defaults['email'] = from_email
        defaults['channel'] = "Email"

        #Try to find the partner using the from email
        search_partner = self.env['res.partner'].sudo().search([('email','=', from_email)])
        if len(search_partner) > 0:
            defaults['partner_id'] = search_partner[0].id
            defaults['person_name'] = search_partner[0].name

        defaults['description'] = tools.html_sanitize(msg.get('body'))
        defaults['project_id'] = incoming_mail.project_id.id

        #condition to create ticket on new mail on specific subject line.
        if incoming_mail.project_id:
            sub =  msg.get('subject')
            if ('ticket' in sub) or ('Ticket' in sub) or ('TICKET' in sub):
                sub_line = sub.split()
                ticket_search = self.search([('client_tic_no', '=', sub_line[1])])
                if not ticket_search:
                    defaults['client_tic_no'] = sub_line[1]
                else:
                    return True
            else:
                return True

        #Assign to default category
        setting_email_default_category_id = self.env['ir.default'].get('website.support.settings', 'email_default_category_id')

        if setting_email_default_category_id:
            defaults['category'] = setting_email_default_category_id

        return super(WebsiteSupportTicket, self).message_new(msg, custom_values=defaults)

    def message_update(self, msg_dict, update_vals=None):
        """ Override to update the support ticket according to the email. """

        body_short = tools.html_sanitize(msg_dict['body'])
        #body_short = tools.html_email_clean(msg_dict['body'], shorten=True, remove=True)

        #Add to message history to keep HTML clean
        self.conversation_history.create({'ticket_id': self.id, 'by': 'customer', 'content': body_short })

        #If the to email address is to the customer then it must be a staff member
        if msg_dict.get('to') == self.email:
            change_state = self.env['ir.model.data'].get_object('website_support','website_ticket_state_staff_replied')
        else:
            change_state = self.env['ir.model.data'].get_object('website_support','website_ticket_state_customer_replied')

        self.state = change_state.id

        return super(WebsiteSupportTicket, self).message_update(msg_dict, update_vals=update_vals)


    @api.depends('states')
    def _compute_unattend(self):
        for rec in self:
            if rec.states != 'new':
                rec.unattended = True

    def request_approval(self):
        approval_email = self.env['ir.model.data'].get_object('website_support', 'support_ticket_approval')
        values = self.env['mail.compose.message'].generate_email_for_composer(approval_email.id, [self.id])[self.id]
        request_message = values['body']
        return {
            'name': "Request Approval",
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'website.support.ticket.compose',
            'context': {'default_ticket_id': self.id, 'default_email': self.email, 'default_subject': self.subject, 'default_approval': True, 'default_body': request_message},
            'target': 'new'
        }

    def open_close_ticket_wizard(self):

        return {
            'name': "Close Support Ticket",
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'website.support.ticket.close',
            'context': {'default_ticket_id': self.id},
            'target': 'new'
        }

    @api.model
    def _needaction_domain_get(self):
        open_state = self.env['ir.model.data'].get_object('website_support', 'website_ticket_state_open')
        custom_replied_state = self.env['ir.model.data'].get_object('website_support', 'website_ticket_state_customer_replied')
        return ['|',('state', '=', open_state.id ), ('state', '=', custom_replied_state.id)]

    @api.model
    def create(self, vals):
        new_id = super(WebsiteSupportTicket, self).create(vals)
        new_id.portal_access_key = randint(1000000000,2000000000)
        seq = self.env['ir.sequence'].next_by_code('website.support.ticket')
        new_id.ticket_number = seq
        return new_id


    def create_quotation_plan_usw(self):
        '''
        This method used for open project task quotation form view.
        '''
        top_id = self.env['project.task'].create({
             'ticket_id': self.id,
             'partner_id': self.related_person_name_id.id,
             'client_ticket': self.client_tic_no,
             'location': self.location,
             'tic_team_id': self.team_id.id,
             'site_id': self.site_id.id,
             'subject': self.subject,
             'supervisor_id': self.supervisor_id.id,
             'work_type': 'unschedulework',
             'top_task': True,
             'project_id': self.project_id.id,
             'branch_id': self.branch_id.id,
             'approved_by_id': self.approved_user_id.id
        })
        if top_id:
            self.quotation_id = top_id.id
            form_view = self.env.ref('nuro_project_task_management.task_top_work_form_view').id
            tree_view = self.env.ref('nuro_project_task_management.task_top_tree_view').id
            return {
                'name': _('TOP'),
                'res_model': 'project.task',
                'views': [
                    (tree_view, 'tree'),
                    (form_view, 'form'),
                ],
                'view_mode': 'tree,form',
                'type': 'ir.actions.act_window',
                'domain': [('id', '=', self.quotation_id.id)]
            }

    def get_top(self):
        '''open quotation '''
        if self.top_count == 1:
            action = {
                'name': _('TOP'),
                'type': 'ir.actions.act_window',
                'res_model': 'project.task',
                'context': {'create': False},
                'target': 'current',
            }
            top_id = self.quotation_id.id
            if self.quotation_id:
                top = top_id
                action['res_id'] = top
                action['view_mode'] = 'form'
                form_view = [(self.env.ref('nuro_project_task_management.task_top_work_form_view').id, 'form')]
                if 'views' in action:
                    action['views'] = form_view + [(state, view) for state, view in action['views'] if view != 'form']
                else:
                    action['views'] = form_view
            else:
                action['view_mode'] = 'tree,form'
                action['domain'] = [('id', '=', self.quotation_id.id)]
            return action
        else:
            top = self.env['project.task'].search([('ticket_id', '=', self.id)])
            form_view = self.env.ref('nuro_project_task_management.task_top_work_form_view').id
            tree_view = self.env.ref('nuro_project_task_management.task_top_tree_view').id
            return {
                'name': _('TOP'),
                'res_model': 'project.task',
                'views': [
                    (tree_view, 'tree'),
                    (form_view, 'form'),
                ],
                'view_mode': 'tree,form',
                'type': 'ir.actions.act_window',
                'domain': [('id', 'in', top)]
            }



    def action_transfer_ticket(self):
        self.ensure_one()
        self = self.with_context(transfer=True)
        return {
            'name': _('Ticket Transfer'),
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'ticket.maintenance',
            'view_id': self.env.ref('website_support.ticket_management_wizard').id,
            'type': 'ir.actions.act_window',
            'target': 'new',
            'context': {'transfer': True},

        }


    def create_quotation_plan_repair(self):
        '''
        This method used for open sale quotation form view.
        '''
        res = {
            'name': "Quotation Plan",
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'view_id': self.env.ref('gts_sale_contract.repair_form_view').id,
            'res_model': 'sale.order',
            'context': {'default_ticket_id': self.id,
                        'default_partner_id': self.related_person_name_id.id,
                        'default_client_ticket': self.client_tic_no,
                        'default_check_ticket': 'True',
                        'default_location': self.location,
                        'default_tkt_team_id': self.team_id.id,
                        'default_site_id': self.site_id.id,
                        'default_category_id': self.category.id,
                        'default_subject': self.subject,
                        'project_category_id': self.project_category_id.id,
                        'default_supervisor_id': self.supervisor_id.id,
                        'default_work_type': 'repair'}
        }
        return res

    def view_quotation(self):
        self.ensure_one()

        sale_order = self.env['sale.order']
        sale_data = sale_order.search([('ticket_id', 'in', self.ids)])
        if sale_data:
            return {
                'name': "Sale Order",
                'type': 'ir.actions.act_window',
                'view_type': 'form',
                'view_mode': 'tree,form',
                'res_model': 'sale.order',
                'res_id': sale_data.ids,
                'domain': [('id', 'in', sale_data.ids)]
            }

class WebsiteSupportTicketApproval(models.Model):

    _name = "website.support.ticket.approval"

    wst_id = fields.Many2one('website.support.ticket', string="Support Ticket")
    name = fields.Char(string="Name", translate=True)

class WebsiteSupportTicketField(models.Model):

    _name = "website.support.ticket.field"

    wst_id = fields.Many2one('website.support.ticket', string="Support Ticket")
    name = fields.Char(string="Label")
    value = fields.Char(string="Value")

class WebsiteSupportTicketMessage(models.Model):

    _name = "website.support.ticket.message"

    ticket_id = fields.Many2one('website.support.ticket', string='Ticket ID')
    by = fields.Selection([('staff','Staff'), ('customer','Customer')], string="By")
    content = fields.Html(string="Content")

class WebsiteSupportTicketCategories(models.Model):

    _name = "website.support.ticket.categories"
    _order = "sequence asc"

    sequence = fields.Integer(string="Sequence")
    name = fields.Char(required=True, translate=True, string='Category Name')
    cat_user_ids = fields.Many2many('res.users', string="Category Users")

    @api.model
    def create(self, values):
        sequence=self.env['ir.sequence'].next_by_code('website.support.ticket.categories')
        values['sequence']=sequence
        return super(WebsiteSupportTicketCategories, self).create(values)

class WebsiteSupportTicketSubCategories(models.Model):

    _name = "website.support.ticket.subcategory"
    _order = "sequence asc"

    sequence = fields.Integer(string="Sequence")
    name = fields.Char(required=True, translate=True, string='Sub Category Name')
    parent_category_id = fields.Many2one('website.support.ticket.categories', required=True, string="Parent Category")
    additional_field_ids = fields.One2many('website.support.ticket.subcategory.field', 'wsts_id', string="Additional Fields")

    @api.model
    def create(self, values):
        sequence=self.env['ir.sequence'].next_by_code('website.support.ticket.subcategory')
        values['sequence']=sequence
        return super(WebsiteSupportTicketSubCategories, self).create(values)

class WebsiteSupportTicketSubCategoryField(models.Model):

    _name = "website.support.ticket.subcategory.field"

    wsts_id = fields.Many2one('website.support.ticket.subcategory', string="Sub Category")
    name = fields.Char(string="Label", required="True")
    type = fields.Selection([('textbox','Textbox')], default="textbox", required="True", string="Type")

class WebsiteSupportTicketStates(models.Model):

    _name = "website.support.ticket.states"

    name = fields.Char(required=True, translate=True, string='State Name')
    mail_template_id = fields.Many2one('mail.template', domain="[('model_id','=','website.support.ticket')]", string="Mail Template")

class WebsiteSupportTicketPriority(models.Model):

    _name = "website.support.ticket.priority"
    _order = "sequence asc"

    sequence = fields.Integer(string="Sequence")
    name = fields.Char(required=True, translate=True, string="Priority Name")
    color = fields.Char(string="Color")

    @api.model
    def create(self, values):
        sequence=self.env['ir.sequence'].next_by_code('website.support.ticket.priority')
        values['sequence']=sequence
        return super(WebsiteSupportTicketPriority, self).create(values)

class WebsiteSupportTicketTag(models.Model):

    _name = "website.support.ticket.tag"

    name = fields.Char(required=True, translate=True, string="Tag Name")

class WebsiteSupportTicketUsers(models.Model):

    _inherit = "res.users"

    cat_user_ids = fields.Many2many('website.support.ticket.categories', string="Category Users")

class WebsiteSupportTicketCompose(models.Model):

    _name = "website.support.ticket.close"

    ticket_id = fields.Many2one('website.support.ticket', string="Ticket ID")
    message = fields.Text(string="Close Message")

class WebsiteSupportTicketCompose(models.Model):

    _name = "website.support.ticket.compose"

    ticket_id = fields.Many2one('website.support.ticket', string='Ticket ID')
    partner_id = fields.Many2one('res.partner', string="Partner", readonly="True")
    email = fields.Char(string="Email", readonly="True")
    subject = fields.Char(string="Subject", readonly="True")
    body = fields.Text(string="Message Body")
    template_id = fields.Many2one('mail.template', string="Mail Template", domain="[('model_id','=','website.support.ticket'), ('built_in','=',False)]")
    approval = fields.Boolean(string="Approval")
    planned_time = fields.Datetime(string="Planned Time")

    @api.onchange('template_id')
    def _onchange_template_id(self):
        if self.template_id:
            values = self.env['mail.compose.message'].generate_email_for_composer(self.template_id.id, [self.ticket_id.id])[self.ticket_id.id]
            self.body = values['body']

class ProjectTask(models.Model):
    _inherit = 'project.task'

    web_ticket_id = fields.Many2one('website.support.ticket')
    stage_name = fields.Char(related='stage_id.name', string='Stage')

class RequestType(models.Model):
    _name = 'request.type'
    _rec_name = 'request_name'

    request_name = fields.Char('')


