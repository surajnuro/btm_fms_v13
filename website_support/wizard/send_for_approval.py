from odoo import models, fields, api

class Assignapproval(models.TransientModel):
    _name="assign.wiz"

    @api.onchange('user_id')
    def get_user(self):
        duty_officer_group = self.env.ref('website_support.group_ticket_supervisor')
        return {'domain': {'user_id': [('id', 'in', duty_officer_group.users.ids)]}}

    user_id = fields.Many2one('res.users','Submit To')

    @api.model
    def default_get(self,fields):
        res = super(Assignapproval, self).default_get(fields)
        duty_officer_group = self.env.ref('website_support.group_ticket_supervisor')
        if duty_officer_group and duty_officer_group.users:
            users = duty_officer_group.users.ids
            if users and len(users)== 1:
                res['user_id'] = users[0]
        return res

    def assign(self):
        ''' Assign for approved ticket'''
        active_id = self.env.context.get('active_id')
        website_tkt_obj = self.env['website.support.ticket'].browse(active_id)
        website_tkt_obj.write({'states':'waiting_for_approval','approval_user_id':self.user_id.id})