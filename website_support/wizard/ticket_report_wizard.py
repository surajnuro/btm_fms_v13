from odoo import models, fields, api, _
from datetime import date
import base64
import xlsxwriter
from datetime import datetime
from odoo.exceptions import ValidationError

states = {'new':'New',
            'waiting_for_approval':'Waiting For Approval',
            'approved':'Approved',
            'inprogress':'In progress',
            'staff_close':'Closed By Staff',
            'customer_close':'Closed By Customer',
            'rejected':'Rejected'}

class ExcelDownload(models.TransientModel):
    _name = 'ticket.wizard'

    file_name = fields.Binary('File Name')
    name = fields.Char('Data')
    date_start = fields.Date('Start Date')
    date_end = fields.Date('Start End')
    project_category_id = fields.Many2one('project.category')
    ticket_type = fields.Selection([('checker', 'Checker Team'),
                                    ('unticket', 'UN Ticket')], string="Ticket Type")
    work_type = fields.Selection([('regularmain','Routine Maintenance'),
                                  ('unschedulework','Unschedule Work'),
                                  ('work_order', 'Work Order')], string="Work Type",)
    team_id = fields.Many2one('tkt.team', 'Team')

    @api.onchange('date_start', 'date_end')
    def on_change_date_start(self):
        current_date = datetime.today().date()
        if self.date_start:
            st_date = datetime.strptime(self.date_start, '%Y-%m-%d').date()
            if st_date >= current_date:
                raise ValidationError('Start date can not be more than current date')
        if self.date_end:
            st_end = datetime.strptime(self.date_end, '%Y-%m-%d').date()
            if st_end < st_date or st_end > current_date:
                raise ValidationError('End date can not be less than start date and greater than current date')

    def remove_html_tags(self, text):
        """Remove html tags from a string"""
        import re
        clean = re.compile('<.*?>')
        return re.sub(clean, '', text)

    # Function for printing excel report
    def print_ticket_report_excel(self):
        f_name = '/tmp/ticket_report.xlsx'
        workbook = xlsxwriter.Workbook(f_name)
        worksheet = workbook.add_worksheet('Report')
        worksheet.set_column('A:O', 12)
        style = workbook.add_format({
            'bold': 1,
            'border': 1,
            'align': 'center',
            'valign': 'vcenter'})
        style.set_font_size(8)
        align_value = workbook.add_format({
            'align': 'center',
            'valign': 'vcenter'})
        align_value.set_font_size(8)
        domain = ([('states', 'in', ('inprogress','staff_close','customer_close'))])
        if self.date_start:
            domain.append(('receive_date', '>=', self.date_start))
        if self.date_end:
            domain.append(('receive_date', '<=', self.date_end))
        if self.work_type:
            domain.append(('work_type', '=', self.work_type))
        if self.ticket_type:
            domain.append(('ticket_type', '=', self.ticket_type))
        if self.project_category_id:
            domain.append(('project_category_id', '=', self.project_category_id.id))
        if self.team_id:
            domain.append(('team_id', '=', self.team_id.id))
        ticket = self.env['website.support.ticket'].sudo().search(domain)


        row = 7
        new_row = row + 1
        worksheet.merge_range('A1:O1', "Report After " + str(self.date_start), style)
        worksheet.merge_range('A2:O2', "( FACILITY MAINTENANCE REPAIR AND GROUND MAINTENANCE SERVICES )" + ' for ' + str(self.project_category_id.name), align_value)
        worksheet.merge_range('A3:B5', "CONTACTS", style)
        worksheet.write('C3', "Name", style)
        worksheet.write('C4', "E-Mail", style)
        worksheet.write('C5', "Telephone", style)
        worksheet.merge_range('D3:D5', "UNSOS - FEMS Respresentative", style)
        worksheet.merge_range('E3:F3', self.project_category_id.partner_id.name or '', style)
        worksheet.merge_range('E4:F4', self.project_category_id.partner_id.phone or '', style)
        worksheet.merge_range('E5:F5', self.project_category_id.partner_id.email or '', style)
        worksheet.merge_range('G3:H5', "DEEQA CONST'N Representative", style)
        worksheet.write('I3', self.project_category_id.user_id.name or '', style)
        worksheet.write('I4', self.project_category_id.user_id.phone or '', style)
        worksheet.write('I5', self.project_category_id.user_id.login or '', style)
        worksheet.merge_range('J3:O4', str(date.today()), style)
        worksheet.merge_range('J5:K5', str(self.date_start), style)
        worksheet.merge_range('L5:M5', "TO", style)
        worksheet.merge_range('N5:O5', str(date.today()), style)
        worksheet.write("A6", 'TICKET/WO NO.', style)
        worksheet.merge_range("B6:C6", "DESCRIPTION OF WORK ORDER", style)
        worksheet.write("D6", "LOCATION/ AREA", style)
        worksheet.write("E6", "TEAM/ LEADER", style)
        worksheet.write("F6", "START", style)
        worksheet.write("G6", "FINISH", style)
        worksheet.write("H6", "STATUS", style)
        worksheet.write("I6", "MATERIAL DESCRIPTION", style)
        worksheet.write("J6", "QTY", style)
        worksheet.write("K6", "UNIT", style)
        worksheet.write("L6", "BOQ Rate", style)
        worksheet.write("M6", "Total", style)
        worksheet.write("N6", "", style)
        worksheet.write("O6", "", style)
        # worksheet.merge_range('A7:O7', "Regular Maintenance", style)
        for tkt in ticket:
            # if not task.ticket_id:
            #     continue

            project_task = self.env['project.task'].search([('ticket_id', '=', tkt.id)])
            if project_task:
                for task in project_task:
                    row_len = len(task.material_line)
                    cur_row = new_row
                    if row_len > 0:
                        new_row = new_row + row_len - 1
                    else:
                        new_row = new_row + 1
                    if new_row == cur_row:
                        worksheet.write("A%s" % (new_row), task.ticket_id.client_tic_no or "Not Filled", align_value)
                        worksheet.merge_range("B%s:C%s" % (new_row, new_row),self.remove_html_tags(task.ticket_id.description) or "Not Filled", align_value)
                        worksheet.write("D%s" % (new_row), task.ticket_id.location_phase or "Not Filled", align_value)
                        worksheet.write("E%s" % (new_row), task.ticket_id.team_id.name or "Not Filled", align_value)
                        worksheet.write("F%s" % (new_row), task.ticket_id.receive_date or "Not Filled", align_value)
                        worksheet.write("G%s" % (new_row), task.ticket_id.close_date or "Not Filled", align_value)
                        worksheet.write("H%s" % (new_row), states.get(task.ticket_id.states) or "Not Filled", align_value)
                    else:
                        worksheet.merge_range("A%s:A%s" % (cur_row, new_row), task.ticket_id.client_tic_no or "Not Filled", align_value)
                        worksheet.merge_range("B%s:C%s" % (cur_row, new_row), self.remove_html_tags(task.ticket_id.description) or "Not Filled", align_value)
                        worksheet.merge_range("D%s:D%s" % (cur_row, new_row), task.ticket_id.location_phase or "Not Filled", align_value)
                        worksheet.merge_range("E%s:E%s" % (cur_row, new_row), task.ticket_id.team_id.name or "Not Filled", align_value)
                        worksheet.merge_range("F%s:F%s" % (cur_row, new_row), task.ticket_id.receive_date or "Not Filled", align_value)
                        worksheet.merge_range("G%s:G%s" % (cur_row, new_row), task.ticket_id.close_date or "Not Filled", align_value)
                        worksheet.merge_range("H%s:H%s" % (cur_row, new_row), states.get(task.ticket_id.states) or "Not Filled", align_value)
                    if task.material_line:
                        for mat in task.material_line:
                            qty = 0.0
                            if mat.done_qty > 0.0:
                                qty = mat.done_qty
                            else:
                                if mat.transfered_qty > 0.0:
                                    qty = mat.transfered_qty
                            worksheet.write("I%s" % (cur_row), mat.product_id.name or "Not Filled", align_value)
                            worksheet.write("J%s" % (cur_row), qty or 0, align_value)
                            worksheet.write("K%s" % (cur_row), mat.product_uom or "Not Filled", align_value)
                            worksheet.write("L%s" % (cur_row), mat.product_id.product_boq_rate or mat.product_id.lst_price or 0.0, align_value)
                            worksheet.write("M%s" % (cur_row), float(mat.product_id.product_boq_rate)*float(mat.done_qty) or "Not Filled", align_value)
                            cur_row += 1
                        new_row += 1
                    # else:
                    #     worksheet.write("I%s" % (cur_row), "Ticket Have No Material", align_value)
                    #     worksheet.write("J%s" % (cur_row), 0, align_value)
                    #     worksheet.write("K%s" % (cur_row), "Not Filled", align_value)
                    #     worksheet.write("L%s" % (cur_row),
                    #                     0.0, align_value)
                    #     worksheet.write("M%s" % (cur_row),
                    #                      "Not Filled",
                    #                     align_value)
                    #     cur_row += 1
                    # new_row += 1
            if not project_task:
                worksheet.write("A%s" % (new_row), tkt.client_tic_no or "Not Filled", align_value)
                worksheet.merge_range("B%s:C%s" % (new_row, new_row),
                                      self.remove_html_tags(tkt.description) or "Not Filled", align_value)
                worksheet.write("D%s" % (new_row), tkt.location_phase or "Not Filled", align_value)
                worksheet.write("E%s" % (new_row), tkt.team_id.name or "Not Filled", align_value)
                worksheet.write("F%s" % (new_row), tkt.receive_date or "Not Filled", align_value)
                worksheet.write("G%s" % (new_row), tkt.close_date or "Not Filled", align_value)
                worksheet.write("H%s" % (new_row), tkt.states or "Not Filled", align_value)

                worksheet.write("I%s" % (new_row),  "Ticket Have No Material", align_value)
                worksheet.write("J%s" % (new_row),  0, align_value)
                worksheet.write("K%s" % (new_row), "Not Filled", align_value)
                worksheet.write("L%s" % (new_row), 0.0,
                                align_value)
                worksheet.write("M%s" % (new_row),
                                 "Not Filled",
                                align_value)
                new_row += 1

        workbook.close()
        f = open(f_name, 'rb')
        data = f.read()
        f.close()
        name = 'Ticket Report'

        out_wizard = self.env['ticket.wizard'].create({'name': name + '.xlsx', 'file_name': base64.b64encode(data)})
        view_id = self.env.ref('website_support.ticket_report_sheet_xls').id
        return {
            'type': 'ir.actions.act_window',
            'res_model': 'ticket.wizard',
            'target': 'new',
            'view_mode': 'form',
            'res_id': out_wizard.id,
            'views': [[view_id, 'form']],
        }
