from odoo import models, fields, api
from datetime import datetime

class CraeteTask(models.TransientModel):
    _name = 'create.task'

    deadline = fields.Datetime('Deadline Date')
    work_categ_lines = fields.One2many('work.categ','work_id','Work Categories')

    @api.model
    def default_get(self, fields):
        res = super(CraeteTask, self).default_get(fields)
        lst = []
        active_id = self.env.context.get('active_id')
        obj = self.env['website.support.ticket'].browse(active_id)
        for record in obj.work_categories_ids:
                lst.append((0, 0, {
                    'work_categ_name':record.name
                }))
        res['work_categ_lines'] = lst
        return res

    def create_task(self):
        active_id = self.env.context.get('active_id')
        support_tkt = self.env['website.support.ticket'].browse(active_id)

        address = ''
        if support_tkt.location_comp:
            address = address + support_tkt.location_comp + ' '
        if support_tkt.location_phase:
            address = address + support_tkt.location_phase + ' '
        if support_tkt.location_room:
            address = address + support_tkt.location_room
        today = datetime.today()
        support_tkt.start_date = today
        location_id = self.env.ref('nuro_project_task_management.regular_work_location_details')
        if not location_id:
            location = self.env['stock.location']
            location_id = location.sudo().create({
                'name': address,
                'usage': 'customer',
                'company_id': False
            })
        for line in self.work_categ_lines:
            user = line.assign_to_id.id
        for task in self.work_categ_lines:
            task_id = self.env['project.task'].create({
                'project_id': support_tkt.project_id.id,
                'branch_id': support_tkt.branch_id.id,
                'ticket_id' : support_tkt.id,
                'name': support_tkt.subject,
                'description': support_tkt.description,
                'email_from': support_tkt.email,
                'partner_id': support_tkt.partner_id.id,
                'site_id': support_tkt.site_id and support_tkt.site_id.id,
                'location':address,
                'client_ticket': support_tkt.client_tic_no,
                'tic_team_id': support_tkt.team_id.id,
                'work_categories_ids':support_tkt.work_categories_ids.ids,
                'work_type':support_tkt.work_type,
                'user_id': user or support_tkt.user_id.id,
                'consume_location_id':location_id.id,
            })
            support_tkt.write({'states': 'inprogress'})


class WorkCateg(models.TransientModel):
    _name="work.categ"

    work_id = fields.Many2one('create.task')
    work_categ_name = fields.Char('Work')
    assign_to_id = fields.Many2one('res.users','Assign To')


