from odoo import models, fields, api

class OrderReportWizard(models.TransientModel):
    _name = 'ticket.maintenance'
    _rec_name = 'maintenance_cat'


    maintenance_cat = fields.Selection([('regularmain','Routine Maintenance'),
                                        ('unschedulework','Unschedule Work')], string="Ticket Type")
    project_id = fields.Many2one('project.project',string='Project')
    branch_id = fields.Many2one('res.branch','Branch')
    supervisor_user_id = fields.Many2one('res.users', string='Supervisor')
    team_id = fields.Many2one('tkt.team','Team')
    work_categories_ids = fields.Many2many('work.categories', 'work_categories_rel', 'work_ticket_id', 'work_id',
                                           'Work Categories')
    is_ticket_transfer = fields.Boolean()
    transfer_reason = fields.Char()

    @api.onchange('project_id')
    def onchange_branch_id(self):
        branch_lst = []
        if self.project_id:
            for b in self.project_id.branch_ids:
                branch_lst.append(b.id)
        domain = {'branch_id': [('id', 'in', branch_lst)]}
        return {'domain': domain}

    @api.model
    def default_get(self, fields):
        res = super(OrderReportWizard, self).default_get(fields)
        ticket_id = self.env['website.support.ticket'].search([('id', 'in', self._context.get('active_ids'))])
        if self._context.get('transfer', False):
            res['maintenance_cat'] = False
            res['is_ticket_transfer'] = True
        else:
            res['maintenance_cat'] = ticket_id.work_type or False
        if ticket_id.project_id:
            res['project_id'] = ticket_id.project_id and ticket_id.project_id.id

            res['work_categories_ids'] = ticket_id.work_categories_ids.ids
            res['project_id'] = ticket_id.project_id and ticket_id.project_id.id or False
            res['branch_id'] = ticket_id.branch_id and ticket_id.branch_id.id or False
            res['team_id'] = ticket_id.team_id and ticket_id.team_id.id or False
            res['supervisor_user_id'] = ticket_id.supervisor_id and ticket_id.supervisor_id.id or False
        return res

    @api.onchange('team_id')
    def onchange_supervisor(self):
        for rec in self.team_id.user_ids:
            self.supervisor_user_id = rec.id

    def submit_wizard(self):
        state_type = self.env.ref('website_support.ticket_managment_approve')
        new_stage_id = self.env['website.support.ticket'].search([('id','in', self._context.get('active_ids'))])
        current_user_id = self.env.user
        new_stage_id.approved_user_id = current_user_id.id
        if self.maintenance_cat == 'regularmain':
            seg= 'RM' + new_stage_id.ticket_number
            new_stage_id.write({
                'states':'approved',
                'work_type': self.maintenance_cat,
                'supervisor_id': self.supervisor_user_id.id,
                'branch_id': self.branch_id.id,
                'team_id': self.team_id.id,
                'ticket_number': str(seg),
                'project_id': self.project_id.id,
                'work_categories_ids':[(6,0, self.work_categories_ids.ids)]
            })

        elif self.maintenance_cat == 'unschedulework':
            seg = 'UW' + new_stage_id.ticket_number

            new_stage_id.write({
                'states': 'approved',
                'work_type': self.maintenance_cat,
                'supervisor_id': self.supervisor_user_id.id,
                'ticket_number': str(seg),
                'project_id': self.project_id.id,
                'work_categories_ids': [(6,0, self.work_categories_ids.ids)]
            })
        else:
            seg = 'RW' + new_stage_id.ticket_number
            new_stage_id.write({
                'states': 'approved',
                'supervisor_id': self.supervisor_user_id.id,
                'work_type': self.maintenance_cat,
                'ticket_number': str(seg),
                'project_id': self.project_id.id,
                'work_categories_ids':[(6,0, self.work_categories_ids.ids)]
            })
        # template = self.env.ref('website_support.teicket_craete_mail1_template')
        # template.email_from = self.env.user.login
        # template.email_to = self.user_id.login
        # template.send_mail(self.id, force_send=True)
        return True

    def action_ticket_transfer(self):
            ticket = self.env['website.support.ticket'].search([('id','in', self._context.get('active_ids'))])
            current_user_id = self.env.user
            if ticket:
                number = ticket.ticket_number[-4:]
                if self.maintenance_cat == 'regularmain':
                    seq = 'RM' + number
                    ticket.write({
                        'states': 'approved',
                        'work_type': self.maintenance_cat,
                        'supervisor_id': self.supervisor_user_id.id,
                        'project_id': self.project_id.id,
                        'team_id': self.team_id.id,
                        'ticket_number': seq,
                        'branch_id': self.branch_id.id,
                        'work_categories_ids': [(6, 0, self.work_categories_ids.ids)]
                    })
                elif self.maintenance_cat == 'unschedulework':
                    seg = 'UW' + ticket.ticket_number[-4:]
                    ticket.write({
                        'states': 'approved',
                        'work_type': self.maintenance_cat,
                        'supervisor_id': self.supervisor_user_id.id,
                        'ticket_number': str(seg),
                        'project_id': self.project_id.id,
                        'branch_id': self.branch_id.id,
                        'work_categories_ids': [(6, 0, self.work_categories_ids.ids)]
                    })

