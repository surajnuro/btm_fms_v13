from odoo import api, fields, models,_
from odoo.exceptions import AccessError, UserError, ValidationError

class AccountPayment(models.Model):
    _inherit = 'account.payment'

    bill_exp_type = fields.Selection([('bill', 'Bill Payment'),
                                     ('expense', 'Expense')], default='bill', string='Bill-Expense Type')
    remark = fields.Text('Remark')
    expense_debit_id = fields.Many2one('account.account', 'Expense Debit A/C')

    def expense_payment(self):
        ml_list1 = []
        account_move = self.env['account.move']
        for rec in self:
            if rec.bill_exp_type == 'expense':
                ac_type = self.env['account.account.type'].search([('name', '=', 'Expenses')], limit=1)

                ml_list1.append((0, 0, {
                    'account_id': rec.expense_debit_id.id,
                    'name': ' ',
                    'debit': rec.amount,
                    'payment_id': rec.id
                }))
                ml_list1.append((0, 0, {
                    'account_id': rec.journal_id.default_credit_account_id.id,
                    'name': rec.name,
                    'credit': rec.amount,
                    'payment_id': rec.id
                }))
                move_id = account_move.create({'date': rec.payment_date,
                                               'journal_id': rec.journal_id.id,
                                               'ref': rec.name,
                                               'line_ids': ml_list1,
                                               'payment_id': rec.id,
                                               })
                move_id.post()
                rec.write({'state': 'posted', 'move_name': move_id.name})
                return True