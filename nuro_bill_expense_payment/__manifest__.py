# -*- coding: utf-8 -*-

{
    'name': 'Bill Expense Payment',
    'category': 'Payment',
    'summary': 'Bill Expense Payment',
    'version': '13.0.1.0.0',
    'website': 'http://www.nurosolution.com',
    'author': 'Nuro Software',
    'description': 'Bill Expense Payment',
    'license': "Other proprietary",

    'depends': [
        'account', 'nuro_payment_approval', 'nuro_multiple_invoice_payment'],

    'data': [
        'views/account_payment_view.xml'
    ],

    'installable': True,

}
