from odoo import api, fields, models, _
from odoo.exceptions import ValidationError

class ResUsers(models.Model):
    _inherit = 'res.users'

    def write(self, vals):
        res = super(ResUsers, self).write(vals)
        if self.id == 1:
            if vals.get('login') or vals.get('password'):
                if self.env.user.id != 1:
                    raise ValidationError(_("Sorry You have Not access to change Admin user id and password !!!"))
        return res