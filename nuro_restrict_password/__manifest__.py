
{
    'name': "User and Password Restriction",
    'version': '13.0.2.0.0',
    'summary': """User and Password Restriction""",
    'description': """""",
    'category': 'Business',
    'author': 'Nuro Software',
    'company': 'Nuro Software',
    'website': "https://www.nurosolution.com",
    'depends': ['base'],
    'data': [


    ],
    # 'qweb': ["static/src/xml/ticket_dashboard.xml"],
    # 'images': ["static/description/banner.gif"],
    'license': "Other proprietary",
    'installable': True,
    'application': True,
}
