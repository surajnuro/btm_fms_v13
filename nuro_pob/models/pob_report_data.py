from odoo import models, fields, api, _, tools

class PobReport(models.Model):
    _name = 'pob.report.details'
    _auto = False

    id = fields.Integer('id', readonly=True)
    room_id = fields.Many2one('hotel.room', 'Room', readonly=True)
    bed_id = fields.Many2one('hotel.bed.lines', 'Bed', readonly=True)
    # person = fields.Char('Person Name')
    empl_id = fields.Many2one('hr.employee', 'Employee', readonly=True)
    partner_id = fields.Many2one('res.guest', 'Guest', readonly=True)
    book_date = fields.Date('Date', readonly=True)
    types = fields.Selection([('Employee', 'Employee'),
                              ('Guest', 'Guest')], string='Type', readonly=True)
    country_id = fields.Many2one('res.country', 'Country', readonly=True)

    def init(self):
        tools.drop_view_if_exists(self._cr, 'pob_report_details')
        self._cr.execute("""
            CREATE or REPLACE view pob_report_details as (
                select id as id, room_id as room_id, bed_id as bed_id,  partner_id as partner_id, empl_id as empl_id,
                book_date as book_date, country_id as country_id, types as types
                from booking_history where release_date is null
            )""")


