from odoo import models, fields, api, _
from odoo.exceptions import ValidationError
from datetime import datetime

class RoomBooking(models.Model):
    _name = 'room.booking'
    _inherit = ['mail.thread', 'mail.activity.mixin', 'portal.mixin']
    _order = "id desc"

    name = fields.Char('Name', copy=False, index=True)
    is_emp = fields.Boolean('Employee')
    is_outside = fields.Boolean('Guest')
    booking_lines = fields.One2many('booking.line', 'booking_id')
    state = fields.Selection([('draft', 'Draft'),
                              ('reserve', 'Reserved'),
                              ],default='draft')
    date = fields.Date('Booking Date', default=fields.Datetime.now)

    def unlink(self):
        for rec in self:
            if rec.state == 'reserve':
                raise ValidationError(_('Sorry Can not delete'))
        return super(RoomBooking, self).unlink()

    def room_reserve(self):
        if self.booking_lines:
            history = self.env['booking.history']
            guest_lst = []
            emp_lst = []
            for line in self.booking_lines:
                if line.employee_id:
                    history = self.env['booking.history'].sudo().search([('empl_id', '=', line.employee_id.id), ('release_date', '=', False)], limit=1)
                    if history:
                        raise ValidationError(_('%s  already booked the Bed !!!' % line.employee_id.name))
                if line.partner_id:
                    history = self.env['booking.history'].search(
                        [('partner_id', '=', line.partner_id.id), ('release_date', '=', False)], limit=1)
                    if history:
                        raise ValidationError(_('%s  already booked the Bed !!!' % line.partner_id.name))
                line.bed_id.state = 'occupied'
                line.state = 'reserve'
                line.bed_id.guest_name = line.employee_id.name or line.partner_id.name
                if line.is_emp:
                    line.bed_id.write({'types' : 'Employee'})
                if line.is_outside:
                    line.bed_id.write({'types' : 'Guest'})
                history.create({'room_id': line.room_id.id,
                                'book_date': datetime.now().date(),
                                'empl_id': line.employee_id.id,
                                'partner_id': line.partner_id.id,
                                'booking_id': self.id,
                                'bed_id': line.bed_id.id,
                                'booking_line_id': line.id})
            seq = self.env['ir.sequence'].next_by_code('room.booking.sequence')
            self.name = seq
            self.write({'state': 'reserve'})

class BookingLine(models.Model):
    _name = 'booking.line'

    booking_id = fields.Many2one('room.booking')
    room_id = fields.Many2one('hotel.room', 'Room')
    bed_id = fields.Many2one('hotel.bed.lines', 'Bed')
    is_emp = fields.Boolean('Employee',store=True)
    is_outside = fields.Boolean('Guest', store=True)
    employee_id = fields.Many2one('hr.employee', 'Employee')
    partner_id = fields.Many2one('res.guest', 'Guest')
    booking_date = fields.Date('Booking Date', default=fields.Datetime.now)
    state = fields.Selection([('draft', 'Draft'),
                              ('reserve', 'Reserved'),
                              ('unreserve', 'UnReserve')], default='draft')

    def unlink(self):
        for rec in self:
            if rec.state == 'reserve':
                raise ValidationError(_('Sorry Can not delete'))
        return super(BookingLine, self).unlink()

    @api.onchange('is_emp')
    def onchange_is_emp(self):
        if self.is_emp:
            self.is_outside = False
            self.partner_id = False

    @api.onchange('is_outside')
    def onchange_is_outside(self):
        if self.is_outside:
            self.is_emp = False
            self.employee_id = False

    @api.constrains('is_emp', 'is_outside')
    def check_booleans(self):
        if not self.is_emp and not self.is_outside:
            raise ValidationError(_('Check Employee or Outside!!'))

    def room_reserve_single(self):
        if self.bed_id:
            self.bed_id.state = 'occupied'
            self.state = 'reserve'
            self.booking_id.state = 'reserve'
            self.bed_id.guest_name = self.employee_id.name or self.partner_id.name
            history = self.env['booking.history']
            if self.employee_id:
                history = self.env['booking.history'].sudo().search(
                    [('empl_id', '=', self.employee_id.id), ('release_date', '=', False)], limit=1)
                if history:
                    raise ValidationError(_('%s  already booked the Bed !!!' % self.employee_id.name))
            if self.partner_id:
                history = self.env['booking.history'].sudo().search(
                    [('partner_id', '=', self.partner_id.id), ('release_date', '=', False)], limit=1)
                if history:
                    raise ValidationError(_('%s  already booked the Bed !!!' % self.partner_id.name))
            history.create({'room_id': self.room_id.id,
                            'book_date': datetime.now().date(),
                            'empl_id': self.employee_id.id,
                            'partner_id': self.partner_id.id,
                            'booking_id': self.booking_id.id,
                            'bed_id': self.bed_id.id,
                            'types': self.bed_id.types,
                            'booking_line_id': self.id})
            if not self.booking_id.name:
                seq = self.env['ir.sequence'].next_by_code('room.booking.sequence')
                self.booking_id.name = seq



