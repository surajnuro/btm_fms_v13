from odoo import models, fields, api, _
from odoo.osv import expression

class ResGuest(models.Model):
    _name = 'res.guest'

    name = fields.Char('Name')
    email = fields.Char('Email')
    mobile = fields.Char('Phone')
    desc = fields.Text('Description')
    nationality = fields.Many2one('res.country', 'Nationality')
    gender = fields.Selection([('male', 'Male'),('female', 'Female')], string='Gender')

    @api.model
    def name_search(self, name, args=None, operator='ilike', limit=100):
        args = args or []
        domain = []
        if name:
            domain = ['|', '|', ('name', operator, name), ('email', operator, name), ('mobile', operator, name)]
        picks = self.search(domain + args, limit=limit)
        return picks.name_get()