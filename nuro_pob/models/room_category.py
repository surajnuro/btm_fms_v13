from odoo import models, fields, api, _

class RoomCategory(models.Model):
    _name = 'room.category'
    _inherits = {'product.category': 'category_id'}
    _inherit = ['mail.thread', 'mail.activity.mixin', 'portal.mixin']

    category_id = fields.Many2one('product.category', required=True)