from odoo import models, fields, api, _
from datetime import datetime
from odoo.exceptions import ValidationError


class HOtelRoom(models.Model):
    _name = 'hotel.room'
    _inherits = {'product.template': 'template_id'}
    _inherit = ['mail.thread', 'mail.activity.mixin', 'portal.mixin']

    template_id = fields.Many2one('product.template', ondelete='restrict', required=True)
    room_category_id = fields.Many2one('room.category', 'Category', required=True)
    state = fields.Selection([('available', 'Available'),
                              ('partial_available', 'Partial Available'),
                              ('occupied', 'Occupied')], default='available', compute='_room_status', store=True,track_visibility='onchange')
    bed_lines = fields.One2many('hotel.bed.lines', 'room_id')
    bed_history_line = fields.One2many('booking.history', 'room_id')
    count_beds = fields.Integer(compute='_calculate_bed')
    available_bed = fields.Integer(compute='_calculate_bed')
    occupied_bed = fields.Integer(compute='_calculate_bed')

    def _calculate_bed(self):
        count = 0
        total_aval = 0
        occupied_total = 0
        for rec in self:
            if rec.bed_lines:
                for line in rec.bed_lines:
                    count += 1
                    if line.state == 'available':
                        total_aval += 1
                    if line.state == 'occupied':
                        occupied_total += 1
            rec.count_beds = count
            rec.occupied_bed = occupied_total
            rec.available_bed = total_aval
            count = total_aval = occupied_total = 0

    @api.depends('bed_lines.state')
    def _room_status(self):
        for rec in self:
            if rec.bed_lines:
                occup = self.bed_lines.filtered(lambda line: line.state == 'occupied')
                available = self.bed_lines.filtered(lambda line: line.state == 'available')
                if occup and available:
                    rec.state = 'partial_available'
                if occup and not available:
                    rec.state = 'occupied'
                if available and not occup:
                    rec.state = 'available'
            else:
                rec.state = 'available'

    def unreserve_bed(self):
        print('asdasdasd', self.env.context)

    def get_action_unserve_bed(self):
        bed_lst = []
        if self.bed_lines:
            for line in self.bed_lines:
                if line.state == 'occupied':
                    bed_lst.append((0,0,{
                        'bed_id': line.id,
                        'guest_name': line.guest_name,
                        'room_id': line.room_id.id
                    }))
        res = {
            'name': "Unreserve",
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'unreserve.wiz',
            'target': 'new',
            # 'res_id': self.id,
            'context': {
                        'default_reserve_room_line': bed_lst,
                        'form_view_initial_mode': 'save',
                        }
        }
        return res


class BedLines(models.Model):
    _name = 'hotel.bed.lines'
    _rec_name = 'bed_name'

    room_status = fields.Selection(related='room_id.state', store=True, string="Room Status")
    room_id = fields.Many2one('hotel.room', 'Room')
    bed_name = fields.Char('Bed', ondelete='restrict')
    guest_name = fields.Char('Guest Name')
    types = fields.Selection([('Employee', 'Employee'),
                             ('Guest', 'Guest')], string='Type')
    state = fields.Selection([('available', 'Available'),
                              ('occupied', 'Occupied')], default='available', track_visibility='onchange')
    room_category_id = fields.Many2one('room.category', 'Category', related='room_id.room_category_id', store=True)
    number = fields.Char('Number', compute="_sequence_ref")

    @api.depends('room_id.bed_lines')
    def _sequence_ref(self):
        for line in self:
            no = 0
            for l in line.room_id.bed_lines:
                no += 1
                str_no = str(no)
                l.number = str_no[:1]

    def unlink(self):
        if self.state == 'occupied':
            raise ValidationError(_('Sorry Can not delete'))
        return super(BedLines, self).unlink()

    def unreserve_room(self):
        history = self.env['booking.history'].search([('release_date', '=', False),
                                                      ('room_id', '=', self.room_id.id),
                                                      ('bed_id','=', self.id)], limit=1)
        if history:
            history.write({'release_date': datetime.now().date()})
            self.write({'state': 'available'})
            self.guest_name = False

    def action_book_bed(self):
        booking_lst = []
        booking_lst.append((0,0,{'room_id': self.room_id.id,
                                 'bed_id': self.id}))
        res = {
            'name': "Booking",
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'view_id': self.env.ref('nuro_pob.room_book_form_view').id,
            'res_model': 'room.booking',
            'context': {'default_booking_lines': booking_lst,
                        }
        }
        return res

class BedHistory(models.Model):
    _name = 'booking.history'

    room_id = fields.Many2one('hotel.room', 'Room')
    book_date = fields.Date('Date')
    bed_id = fields.Many2one('hotel.bed.lines', 'Bed')
    empl_id = fields.Many2one('hr.employee', 'Employee')
    partner_id = fields.Many2one('res.guest', 'Guest')
    release_date = fields.Date('Unreserve Date')
    booking_id = fields.Many2one('room.booking', 'Booking Ref')
    booking_line_id = fields.Many2one('booking.line')
    types = fields.Selection([('Employee', 'Employee'),
                              ('Guest', 'Guest')], string='Type', store=True, compute='compute_country')
    country_id = fields.Many2one('res.country', 'Country', store=True, compute='compute_country')

    @api.depends('empl_id', 'partner_id')
    def compute_country(self):
        for rec in self:
            if rec.empl_id:
                rec.country_id = rec.empl_id.country_id.id
                rec.types = 'Employee'
            if rec.partner_id:
                rec.country_id = rec.partner_id.nationality.id
                rec.types = 'Guest'

    def unlink(self):
        raise ValidationError(_('Sorry Can not delete'))
        return super(BedHistory, self).unlink()


