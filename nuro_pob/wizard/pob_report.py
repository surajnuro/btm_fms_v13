from odoo import models, fields, api, _
from datetime import date, timedelta, datetime


class InOutWiz(models.TransientModel):
    _name = 'pob.report.wiz'

    from_date = fields.Date('Start Date')
    end_date = fields.Date('End Date')
    room_id = fields.Many2one('hotel.room', 'Room')

    def open_pob_table(self):
        '''method to find report data and create view where show data in tree view'''
        report_search = self.env['pob.report.data'].search([])
        if report_search:
            report_search.unlink()
        domain = []
        if self.from_date:
            domain += [('booking_date', '>=', self.from_date)]
        if self.end_date:
            domain += [('booking_date', '<=', self.end_date)]
        if self.room_id:
            domain += [('room_id', '=', self.room_id.id)]
        booking_data = self.env['booking.line'].search(domain)
        report_data = self.env['pob.report.data']
        if booking_data:
            for bl in booking_data:

                report_data.create({'room_id': bl.room_id.id,
                                    'bed_id': bl.bed_id.id,
                                    'person': bl.employee_id.name or bl.partner_id.name,
                                    'country_id': bl.partner_id.nationality.id,
                                    'date': bl.booking_date,
                                    'type': 'emp' if bl.employee_id else 'guest'})

        tree_view_id = self.env.ref('nuro_pob.pob_report_data_tree').id
        action = {
            'type': 'ir.actions.act_window',
            'views': [
                (tree_view_id, 'tree'),
            ],
            'view_mode': 'tree',
            'name': _('POB Report'),
            'res_model': 'pob.report.data',
        }
        return action