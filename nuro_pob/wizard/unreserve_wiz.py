from odoo import models, fields, api, _
from datetime import datetime

class UnreserveBed(models.TransientModel):
    _name = 'unreserve.wiz'

    reserve_room_line = fields.One2many('reserve.room.line', 'unreserve_id')

class ReserverRoomLine(models.TransientModel):
    _name = 'reserve.room.line'

    unreserve_id = fields.Many2one('unreserve.wiz')
    room_id = fields.Many2one('hotel.room', 'Room')
    bed_id = fields.Many2one('hotel.bed.lines', 'Bed')
    guest_name = fields.Char('Guest Name')

    def unreserve_bed(self):
        history = self.env['booking.history'].search([('release_date', '=', False),
                                                      ('room_id', '=', self.room_id.id),
                                                      ('bed_id', '=', self.id)], limit=1)
        if history:
            history.write({'release_date': datetime.now().date()})
        self.bed_id.write({'state': 'available',
                           'guest_name' : False})

