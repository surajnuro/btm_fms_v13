# -*- coding: utf-8 -*-
# Copyright  Nuro Solution Pvt Ltd
{
    'name': 'POB Management',
    'category': 'Custom',
    'summary': 'This module manage room, bed, and Type',
    'description': """
        This module manage room, bed, and Type
    """,
    'author': 'Nuro Solution Pvt Ltd',
    'website': 'http://www.nurosolution.com',
    'company': 'Nuro Solution Pvt Ltd',
    'license': 'OPL-1',
    'depends': ['product', 'hr'],
    'data': [
        'security/ir.model.access.csv',
        'security/security_view.xml',
        'views/room_category_view.xml',
        'views/room_view.xml',
        'views/room_booking_view.xml',
        'views/booking_sequence.xml',
        'views/bed_tree_views.xml',
        'views/history_details_tree.xml',
        'views/guest_view.xml',
        'views/dashboard_view.xml',
        'views/pob_report_data_view.xml',
        'wizard/pob_report.xml',
        'wizard/unreserve_wiz_view.xml',
        'menu/menu.xml',
    ],

    'installable': True,
    'application': True,
}