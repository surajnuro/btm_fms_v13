# -*- coding: utf-8 -*-
# Copyright  Nuro Solution Pvt Ltd
{
    'name': 'Currency Rate Conversion',
    'category': 'Custom',
    'summary': 'Currency Rate Conversion',
    'description': """
        Currency Rate Conversion
    """,
    'author': 'Nuro Solution Pvt Ltd',
    'website': 'http://www.nurosolution.com',
    'company': 'Nuro Solution Pvt Ltd',
    'license': 'OPL-1',
    'depends': ['account', 'base', 'nuro_expense_management', 'nuro_payment_approval', 'project', 'website_support', 'fms_expense'],
    'data': [
                'security/ir.model.access.csv',
                'views/move_view.xml',
                'views/account_view.xml',
                'views/hr_expense_sheet.xml',
                'views/account_payment.xml',
                'views/currency_transfer_view.xml',
                'views/currency_rate.xml',
    ],

    'installable': True,
    'application': True,
}