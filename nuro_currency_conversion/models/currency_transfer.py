from odoo import api,fields,models,_
from odoo.exceptions import UserError, ValidationError
from datetime import date, datetime

class CurrencyTransfer(models.Model):
    _name = 'currency.transfer'
    _inherit = ['mail.thread', 'mail.activity.mixin', 'portal.mixin']
    _order = "id desc"
    _rec_name = 'from_journal_id'
    _description = 'Inter-Bank/Cash Transfers'

    date = fields.Datetime('Date', default=fields.Datetime.now, track_visibility='onchange')
    from_journal_id = fields.Many2one('account.journal', 'From', track_visibility='onchange')
    to_journal_id = fields.Many2one('account.journal', 'To', track_visibility='onchange')
    rate = fields.Float('Conversion rate', track_visibility='onchange')
    currency_id = fields.Many2one('res.currency', string='Currency', readonly=False,
                                  default=lambda self: self.env.user.company_id.currency_id)
    note = fields.Char('Reference')
    state = fields.Selection([('draft', 'Draft'), ('post', 'Post')], default='draft')
    move_id = fields.Many2one('account.move', 'move')
    amount = fields.Float('Amount')
    converted_amt = fields.Float('Amount After Conversion', compute='_converted_amount', store=True)
    from_currency_id = fields.Many2one('res.currency')
    reference = fields.Text('Description')
    is_readonly = fields.Boolean()
    ir_attachment_ids = fields.Many2many('ir.attachment', 'attachment_currency_ref', 'currency_transfer_id', 'attachment_id')
    attachment_number = fields.Integer(compute='_compute_attachment_number', string='Number of Attachments')
    user_id = fields.Many2one('res.users', default=lambda self: self.env.user, readonly=True)
    company_id = fields.Many2one('res.company', 'Company', default=lambda self: self.env.user.company_id)

    @api.onchange('to_journal_id')
    def onchange_currency_to_account(self):
        if self.to_journal_id:
            self.currency_id = self.to_journal_id.currency_id.id or self.env.user.company_id.currency_id.id
        if self.from_journal_id and self.to_journal_id:
            if self.currency_id == self.from_currency_id:
                self.rate = 1.0
                self.is_readonly = True
            else:
                self.rate = 0.0
                self.is_readonly = False


    @api.onchange('from_journal_id')
    def onchange_currency(self):
        if self.from_journal_id:
            usd_currency = self.env['res.currency'].search([('name', 'ilike', 'USD')], limit=1)
            if self.from_journal_id.currency_id.name or self.env.user.company_id.currency_id.name != 'USD':
                self.rate = usd_currency.rate
            self.from_currency_id = self.from_journal_id.currency_id.id or self.env.user.company_id.currency_id.id
        if self.from_journal_id and self.to_journal_id:
            if self.currency_id == self.from_currency_id:
                self.rate = 1.0
                self.is_readonly = True
            else:
                self.rate = 0.0
                self.is_readonly = False

    @api.depends('amount', 'rate')
    def _converted_amount(self):
        for rec in self:
            if rec.from_currency_id.name != 'USD':
                if rec.rate > 0:
                    rec.converted_amt = rec.amount / rec.rate
            else:
                rec.converted_amt = rec.amount * rec.rate

    def action_post(self):
        '''create journal entry'''
        ml_lst = []  # ml list for append from and to account data so that we can add in move line
        account_move = self.env['account.move']
        if self.currency_id == self.from_currency_id:
            self.rate = 1.0
        if self.from_journal_id == self.to_journal_id:
            raise ValidationError(('From and To account should be different !!!'))
        if self.rate <= 0.0:
            raise ValidationError(('Rate should be greater than 0'))
        if self.amount <= 0.0:
            raise ValidationError(('Amount should be greater than 0'))
        if self.from_currency_id.id == self.env.user.company_id.currency_id.id:
            ml_lst.append((0,0, {
                'account_id': self.from_journal_id.default_credit_account_id.id,
                'name': 'Inter-Bank/Cash Transfers',
                'credit': self.converted_amt,
                'debit': 0.0,
                'amount_currency': -self.converted_amt,
                'conversion_rate': self.rate,
                'currency_id': self.currency_id.id,
            }))
            ml_lst.append((0, 0, {
                'account_id': self.to_journal_id.default_debit_account_id.id,
                'name': 'Inter-Bank/Cash Transfers',
                'credit': 0,
                'debit': self.converted_amt,
                'amount_currency': self.converted_amt,
                'conversion_rate': self.rate,
                'currency_id': self.currency_id.id,
            }))
            move_id = account_move.create({'date': date.today(),
                                           'journal_id': self.to_journal_id.id,
                                           'ref': 'Inter-Bank/Cash Transfers',
                                           'line_ids': ml_lst,
                                           'currency_transfer_id': self.id,
                                           'narration': self.reference
                                           })
            self.env['res.currency.rate'].create({'name': date.today(),
                                                  'date_time': datetime.now(),
                                                  'rate': self.rate,
                                                  'currency_id': self.currency_id.id})
            # ===============================================================================
            if move_id:
                self.move_id = move_id.id
                if move_id.line_ids:
                    for line in move_id.line_ids:
                        line._onchange_amount_currency()
                move_id.post()
        else:

            ml_lst.append((0, 0, {
                'account_id': self.to_journal_id.default_credit_account_id.id,
                'name': 'Inter-Bank/Cash Transfers',
                'credit': self.amount,
                'debit': 0.0,
                'amount_currency': -self.amount,
                'conversion_rate': self.rate,
                'currency_id': self.from_currency_id.id,
            }))
            ml_lst.append((0, 0, {
                'account_id': self.from_journal_id.default_debit_account_id.id,
                'name': 'Inter-Bank/Cash Transfers',
                'credit': 0,
                'debit': self.amount,
                'amount_currency': self.amount,
                'conversion_rate': self.rate,
                'currency_id': self.from_currency_id.id,
            }))

            move_id = account_move.create({'date': date.today(),
                                           'journal_id': self.from_journal_id.id,
                                           'ref': 'Inter-Bank/Cash Transfers',
                                           'line_ids': ml_lst,
                                           'currency_transfer_id': self.id,
                                           'narration': self.reference
                                           })
            if move_id:
                self.move_id = move_id.id
                if move_id.line_ids:
                    for line in move_id.line_ids:
                        line._onchange_amount_currency()
                move_id.post()
        self.write({'state': 'post'})

    def get_move(self):
        '''get account move ids'''
        action = self.env.ref('account.action_move_journal_line').read()[0]
        if len(self.move_id) == 1:
            action['views'] = [(self.env.ref('account.view_move_form').id, 'form')]
            action['res_id'] = self.move_id.id
        else:
            action = {'type': 'ir.actions.act_window_close'}
        return action

    def action_get_attachment_view(self):
        '''get attachment '''
        self.ensure_one()
        res = self.env['ir.actions.act_window'].for_xml_id('base', 'action_attachment')
        res['domain'] = [('res_model', '=', 'currency.transfer'), ('res_id', 'in', self.ids)]
        res['context'] = {'default_res_model': 'currency.transfer', 'default_res_id': self.id}
        return res

    def _compute_attachment_number(self):
        attachment_data = self.env['ir.attachment'].read_group(
            [('res_model', '=', 'currency.transfer'), ('res_id', 'in', self.ids)], ['res_id'], ['res_id'])
        attachment = dict((data['res_id'], data['res_id_count']) for data in attachment_data)
        for rec in self:
            rec.attachment_number = attachment.get(rec.id, 0)

class Move(models.Model):
    _inherit = 'account.move'

    currency_transfer_id = fields.Many2one('currency.transfer')