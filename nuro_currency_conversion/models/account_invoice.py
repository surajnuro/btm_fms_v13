from odoo import fields,models,api
from datetime import date
from odoo.exceptions import UserError

class Invoice(models.Model):
    _inherit = 'account.move'

    conversion_rate = fields.Float('Conversion Rate', invisible=True)

    @api.onchange('currency_id', 'invoice_date')
    def onchange_currency_rate(self):
        if self.currency_id:
            print(fields.Date.context_today(self))
            if self.currency_id != self.company_id.currency_id:
                currency_rate = self.currency_id.with_context(date=self.invoice_date or fields.Date.context_today(self)).rate
                if currency_rate:
                    self.conversion_rate = currency_rate
            else:
                self.conversion_rate = 0.0

    def action_post(self):
        # if self.currency_id != self.company_id.currency_id and self.conversion_rate <= 0.0:
        #     raise UserError(('if you are change currency, Conversion rate can not be 0.0  !!!'))
        # if self.conversion_rate > 0.0:
        #     if self.currency_id != self.company_id.currency_id:
        #         if self.conversion_rate > 0.0:
        #             currency = self.env['res.currency.rate'].search([('name', '=', self.date_invoice or date.today()),
        #                                                              ('currency_id', '=', self.currency_id.id)])
        #             if currency:
        #                 currency.sudo().write({'rate': self.conversion_rate})
        #             else:
        #                 self.env['res.currency.rate'].create({'name': self.date_invoice or date.today(),
        #                                                       'rate': self.conversion_rate,
        #                                                       'currency_id': self.currency_id.id, })
        res = super(Invoice, self).action_post()
        self.line_ids.sudo().write({'conversion_rate': self.conversion_rate})
        return res