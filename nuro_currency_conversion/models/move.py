from odoo import api,fields,models
from datetime import date

class MoveLine(models.Model):
    _inherit = 'account.move.line'

    conversion_rate = fields.Float('Conversion Rate')

    # @api.onchange('amount_currency', 'currency_id')
    # def _onchange_amount_currency(self):
    #     '''Recompute the debit/credit based on amount_currency/currency_id and date.
    #     However, date is a related field on account.move. Then, this onchange will not be triggered
    #     by the form view by changing the date on the account.move.
    #     To fix this problem, see _onchange_date method on account.move.
    #     '''
    #     for line in self:
    #         amount = line.amount_currency
    #         if line.currency_id and line.currency_id != line.company_currency_id:
    #             currency_rate = self.currency_id.with_context(date=line.date).rate
    #             if currency_rate:
    #                 line.conversion_rate = currency_rate
    #             amount = line.currency_id.with_context(date=line.date).compute(amount, line.company_currency_id)
    #             line.debit = amount > 0 and amount or 0.0
    #             line.credit = amount < 0 and -amount or 0.0

