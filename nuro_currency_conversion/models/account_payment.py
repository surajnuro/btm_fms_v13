from odoo import api,fields,models
from datetime import date

class AccountPayment(models.Model):
    _inherit = 'account.payment'

    conversion_rate = fields.Float('Conversion Rate' ,invisible=True)

    @api.onchange('currency_id', 'payment_date')
    def onchange_currency_rate(self):
        if self.currency_id:
            if self.currency_id != self.company_id.currency_id:
                currency_rate = self.currency_id.with_context(
                    date=self.payment_date or fields.Date.context_today(self)).rate
                if currency_rate:
                    self.conversion_rate = currency_rate
            else:
                self.conversion_rate = 0.0

    def post(self):
        res = super(AccountPayment, self).post()
        aml_ids = self.env['account.move.line'].search([('payment_id', '=', self.ids)])
        if aml_ids:
            aml_ids.sudo().write({'conversion_rate': self.conversion_rate})
        return res
