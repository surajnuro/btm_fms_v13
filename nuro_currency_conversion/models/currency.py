from odoo import fields,api,models,_
from datetime import date, datetime

class ResCurrency(models.Model):
    _inherit = 'res.currency'

    # @api.depends('rate_ids.rate')
    # def _compute_current_rate(self):
    #     '''Method ovverride for get latest currency rate from same date'''
    #     date = self._context.get('date') or fields.Date.today()
    #     company_id = self._context.get('company_id') or self.env.company.id
    #     # the subquery selects the last rate before 'date' for the given currency/company
    #     query = """SELECT c.id, (SELECT r.rate FROM res_currency_rate r
    #                                   WHERE r.currency_id = c.id AND r.name <= %s
    #                                     AND (r.company_id IS NULL OR r.company_id = %s)
    #                                ORDER BY r.company_id, r.id DESC
    #                                   LIMIT 1) AS rate
    #                    FROM res_currency c
    #                    WHERE c.id IN %s"""
    #     self._cr.execute(query, (date, company_id, tuple(self.ids)))
    #     currency_rates = dict(self._cr.fetchall())
    #     for currency in self:
    #         currency.rate = currency_rates.get(currency.id) or 1.0

class CurrencyRate(models.Model):
    _inherit = 'res.currency.rate'

    date_time = fields.Datetime()