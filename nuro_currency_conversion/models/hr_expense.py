from odoo import fields,models,api, _
from datetime import date
from datetime import datetime
from odoo.exceptions import UserError

class HrExpenseSheet(models.Model):
    _inherit = 'hr.expense.sheet'

    conversion_rate = fields.Float('Conversion Rate')
    currency_id = fields.Many2one('res.currency', string='Currency', readonly=False, default=lambda self: self.env.user.company_id.currency_id)

    @api.onchange('currency_id')
    def onchange_currency_account(self):
        if self.currency_id:
            if self.currency_id.name != 'USD':
                journal = self.env['account.journal'].search([('currency_id', '=', self.currency_id.id)])
            else:
                journal = self.env['account.journal'].search(['|',('currency_id', '=', self.currency_id.id),
                                                              ('currency_id', '=', False)])
            return {'domain': {'bank_journal_id': [('id', 'in', journal.ids)]}}


    @api.onchange('currency_id', 'accounting_date')
    def onchange_currency_rate(self):
        if self.currency_id and self.accounting_date:
            if self.currency_id != self.company_id.currency_id:
                currency_rate = self.currency_id.with_context(date=self.accounting_date).rate
                if currency_rate:
                    self.conversion_rate = currency_rate
            else:
                self.conversion_rate = 0.0

    def action_sheet_bill_create(self):
        invoice = self.env['account.invoice']
        inv_lst = []
        if self.expense_line_ids:
            for rec in self.expense_line_ids:
                if not rec.account_id:
                    raise UserError(_("Please select Account in Expense Line"))
                inv_lst.append((0, 0, {
                    'name': rec.name,
                    'account_id': self.account_id.id,
                    'account_analytic_id': rec.analytic_account_id.id or False,
                    'price_unit': rec.total_amount,
                    'branch_id': rec.branch_id.id,
                    'project_id': rec.project_id.id
                }))
        inv_id = invoice.create({
            'partner_id': self.vendor_id.id,
            'reference': self.expense_ref,
            'invoice_line_ids': inv_lst,
            'expense_id': self.id,
            'type': 'in_invoice',
            'currency_id': self.currency_id.id,
            'conversion_rate': self.conversion_rate
        })
        inv_id.action_invoice_open()
        # self.date = datetime.now()
        if inv_id:
            self.invoice_id = inv_id.id
            self.write({'state': 'post'})

    def action_sheet_journal_entry(self):
        ''' method to create account.move and move.line for expense'''
        account_move = self.env['account.move']
        ml_lst = []
        if any(sheet.state != 'approve' for sheet in self):
            raise UserError(_("You can only generate accounting entry for approved expense(s)."))
        if not self.accounting_date:
            raise UserError(_("Please fill Accounting Date"))
        if any(not sheet.journal_id for sheet in self):
            raise UserError(_("Expenses must have an expense journal specified to generate accounting entries."))
        if self.currency_id != self.company_id.currency_id and self.conversion_rate <= 0.0:
            raise UserError(('if you are change currency, Conversion rate can not be 0.0  !!!'))
        if self.currency_id != self.company_id.currency_id and self.conversion_rate > 0.0:
            if self.expense_line_ids:
                total_amt = total_convert_amt = 0.0
                for exp in self.expense_line_ids:
                    if not exp.account_id:
                        raise UserError(_("Please select Account in Expense Line"))
                    company_currency = exp.company_id.currency_id
                    # total, total_currency, move_lines = exp._compute_expense_totals(company_currency, move_lines,
                    total_amt += exp.unit_amount
                    total_convert_amt += (exp.unit_amount / self.conversion_rate)
                    ml_lst.append((0, 0, {
                        'name': exp.name,
                        'debit': exp.unit_amount,
                        # 'credit': 0,
                        'account_id': exp.account_id.id,
                        'amount_currency': exp.unit_amount,
                        'conversion_rate': self.conversion_rate,
                        'currency_id': self.currency_id.id,
                        'branch_id': exp.branch_id.id,
                        'project_id': exp.project_id.id,
                        'analytic_account_id': exp.analytic_account_id.id,
                        'expense_id': exp.id}))
                ml_lst.append((0, 0, {
                    'name': exp.name,
                    'debit': 0,
                    'credit': total_amt,
                    'amount_currency': -total_amt,
                    'conversion_rate': self.conversion_rate,
                    'account_id': self.bank_journal_id.default_credit_account_id.id,
                    'analytic_account_id': exp.analytic_account_id.id,
                    'currency_id': self.currency_id.id,
                    'expense_id': exp.id}))
            if ml_lst:
                move_id = account_move.create({'date': self.accounting_date,
                                               'journal_id': self.bank_journal_id.id,
                                               'ref': self.name,
                                               'line_ids': ml_lst,
                                               })
                if move_id:
                    if move_id.line_ids:
                        for line in move_id.line_ids:
                            line._onchange_amount_currency()
                            if line.credit > 0.0:
                                line.update({'credit': round(total_convert_amt,2)})
                    move_id.post()
                    self.account_move_id = move_id.id
            if not self.accounting_date:
                self.accounting_date = self.account_move_id.date
        else:
            if self.expense_line_ids:
                total_amt = 0.0
                for exp in self.expense_line_ids:
                    if not exp.account_id:
                        raise UserError(_("Please select Account in Expense Line"))
                    company_currency = exp.company_id.currency_id
                    # total, total_currency, move_lines = exp._compute_expense_totals(company_currency, move_lines,

                    total_amt += exp.unit_amount
                    ml_lst.append((0, 0, {
                        'name': exp.name,
                        'debit': exp.unit_amount,
                        'credit': 0,
                        'account_id': exp.account_id.id,
                        # 'amount_currency': ,
                        'currency_id': exp.currency_id.id,
                        'branch_id': exp.branch_id.id,
                        'project_id': exp.project_id.id,
                        'analytic_account_id': exp.analytic_account_id.id,
                        'expense_id': exp.id}))
                ml_lst.append((0, 0, {
                    'name': exp.name,
                    'debit': 0,
                    'credit': total_amt,
                    'account_id': self.bank_journal_id.default_credit_account_id.id,
                    'analytic_account_id': exp.analytic_account_id.id,
                    # 'amount_currency': line['price'] > 0 and abs(line.get('amount_currency')) or - abs(line.get('amount_currency')),
                    'currency_id': exp.currency_id.id,
                    'expense_id': exp.id}))

            if ml_lst:
                move_id = account_move.create({'date': self.accounting_date,
                                               'journal_id': self.bank_journal_id.id,
                                               'ref': self.name,
                                               'line_ids': ml_lst,
                                               })
                if move_id:
                    move_id.post()
                    self.account_move_id = move_id.id
            if not self.accounting_date:
                self.accounting_date = self.account_move_id.date
        self.write({'state': 'done'})