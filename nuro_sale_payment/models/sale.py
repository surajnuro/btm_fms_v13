from odoo import fields,models,api,_
from odoo.exceptions import ValidationError

class Sale(models.Model):
    _inherit = 'sale.order'

    payment_id = fields.Many2one('account.payment')

    def action_print_receipt(self):
        return self.env.ref('account.action_report_payment_receipt').report_action(self.payment_id)