# -*- coding: utf-8 -*-
# Copyright  Nuro Solution Pvt Ltd
{
    'name': 'Sale cash credit payment changes',
    'category': 'Custom',
    'summary': 'Sale payment changes',
    'description': """
        Sale payment changes
    """,
    'author': 'Nuro Solution Pvt Ltd',
    'website': 'http://www.nurosolution.com',
    'company': 'Nuro Solution Pvt Ltd',
    'license': 'Other proprietary',
    'depends': ['sale', 'account', 'nuro_sales_cash_credit', 'nuro_multiple_invoice_payment', 'nuro_project_task_management'],
    'data': [
        'views/sale_view.xml',
    ],

    'installable': True,
    'application': True,
}