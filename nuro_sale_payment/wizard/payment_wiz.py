from odoo import fields, models, api, _

class CreditCashWizard(models.TransientModel):
    _inherit = 'credit.cash.wizard'

    payment_method_id = fields.Many2one('account.journal',
                                        string='Payment Method',
                                        domain=[('type', 'in', ['cash', 'bank'])],
                                        readonly=False, required=True)

    def action_sale_cash_payment(self):
        # override
        '''
        method to create invoice from sale order if cash then paid automatic
        :return:
        '''
        for payment_wiz in self:
            payment_wiz.sale_id.action_confirm()
            if payment_wiz.sale_id.picking_ids:
                for picking in payment_wiz.sale_id.picking_ids:
                    picking.write({'project_id': payment_wiz.sale_id.project_id.id})
            payment_wiz.sale_id._create_invoices()
            if payment_wiz.sale_id.invoice_ids:
                payment_wiz.sale_id.invoice_ids.write({'project_id': payment_wiz.sale_id.project_id.id})
                payment_wiz.sale_id.invoice_ids.post()
            if payment_wiz.sale_id.invoice_ids:
                payment_wiz.sale_id.write({'payment_by': payment_wiz.payment_type})
                if payment_wiz.payment_type =='cash':
                    inv_ids = payment_wiz.sale_id.invoice_ids.filtered(lambda inv: inv.state != 'cancel')
                    inv_ids.post()
                    payment_lines = {
                        'payment_date':payment_wiz.sale_id.date_order,
                        'amount': abs(payment_wiz.amount),
                        'selected_inv_total': abs(payment_wiz.amount),
                        'discount': 0.0,
                        'journal_id': payment_wiz.payment_method_id.id,
                        'payment_method_id': payment_wiz.payment_method_id.inbound_payment_method_ids.id or
                                             payment_wiz.payment_method_id.outbound_payment_method_ids.id,
                        'payment_type': payment_wiz.amount > 0 and 'inbound' or 'outbound',
                        'partner_id': payment_wiz.sale_id.partner_id.id,
                        'partner_type': 'customer',
                        'invoice_payment_type': 'automated',
                        'communication': ' '.join([ref for ref in payment_wiz.sale_id.invoice_ids.mapped('ref') if ref]),
                        'invoice_ids': [(6, 0, inv_ids.ids)],
                    }
                    payments = self.env['account.payment'].create(payment_lines)
                    payments.onchange_payment_partner_id()
                    payments.amount = abs(payment_wiz.amount)
                    payments.onchange_payment_amount()
                    payments.post()
                    payment_wiz.sale_id.payment_id = payments.id
                    # return self.env.ref('account.action_report_payment_receipt').report_action(payments)
