# -*- coding: utf-8 -*-
# Copyright  Nuro Solution Pvt Ltd
{
    'name': 'Manage Multiple type of Task and Project',
    'category': 'Sales',
    'author': '',
    'summary': 'Manage Multiple type of Task and Project',
    "description":
        """
            manage regular and unschedule task
        """,
    'depends': [
        'base',
        'website_support',
        'project',
        'stock',
        'analytic',
        'website_support',
        'nuro_branch_management',
        'hr_timesheet',
        'nuro_product_boq',
        'account',
        'nuro_stock_xlsx_report'
    ],
    'data': [
        'security/ir.model.access.csv',
        'wizard/submit_for_review_view.xml',
        'wizard/assign_task.xml',
        'wizard/xlsx_output_view.xml',
        'wizard/excel_download.xml',
        'wizard/top_report_wiz_view.xml',
        'views/regular_task_form_view.xml',
        'views/location_view.xml',
        'views/project_view.xml',
        'views/picking_view.xml',
        'views/task_top_form.xml',
        'views/move_view.xml',
        'views/unschedule_task_view.xml',
        'views/to_views.xml',
        'views/purchase_view.xml',
        'views/hardwall_task_view.xml',
        'views/sale_view.xml',
        'views/company_view.xml',
        'data/data_view.xml',
        'report/picking_operation_view.xml',
        'report/fm_work_report.xml',
        'report/deeqa_invoice.xml',
        'report/task_order_report.xml',
        'sequence/sequence.xml',
        'template/send_review_template.xml',


    ],
    'qweb': [],
    'demo': [],
    "website": "https://www.nurosolution.com",
    "author": "Nuro Solution Pvt Ltd",
    'license': 'Other proprietary',
    'installable': True,
    'auto_install': True,
}
