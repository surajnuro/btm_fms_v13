from odoo import fields,models,api,_
from datetime import datetime,timedelta
from odoo.exceptions import ValidationError, Warning
from odoo.osv import expression
from itertools import groupby
from lxml import etree
import base64
import xlsxwriter
from odoo.tools import float_is_zero

class TOTracking(models.Model):
    _name = 'to.tracking'
    _inherit = ['mail.thread', 'mail.activity.mixin', 'portal.mixin']
    _order = "id desc"
    _rec_name = 'to_number'

    to_tracking_id = fields.Many2one('project.task', track_visibility='onchange', copy=True)
    un_to_number = fields.Char('TOP Number', track_visibility='onchange', copy=True)
    location_name = fields.Char('Location')
    description = fields.Char(related='to_tracking_id.project_scope', track_visibility='onchange',)
    to_description = fields.Char('TO Task Description', track_visibility='onchange',)
    to_start_date = fields.Date('TO Start Date', track_visibility='onchange', copy=True)
    to_end_date = fields.Date('TO  End Date', track_visibility='onchange', copy=True)
    previous_to_complition = fields.Float('Previous %', readonly=True, track_visibility='onchange',)
    to_complition_percentage = fields.Float('completion %', track_visibility='onchange',)
    tracking_line = fields.One2many('tracking.line', 'tracking_id', copy=True)
    last_update_date = fields.Date('Last Update Date', copy=True)
    total_to_amt = fields.Float('Total TO Amount', copy=True)
    to_number = fields.Char('TO Number', copy=True)
    company_id = fields.Many2one('res.company', 'Company', default=lambda self: self.env.user.company_id.id)
    total_profit_amt = fields.Float('ToTal Amount', compute='_calculate_profit_amt', store=True, track_visibility='onchange')
    overall_percentage = fields.Float('Overall Percentage', compute='_calculate_total_percentage', store=True, track_visibility='onchange')
    total_previous_amt = fields.Float('Total Previous Amt', compute='_calculate_profit_amt', store=True)
    date_to = fields.Date('Date To', copy=True)
    date_from = fields.Date('Date From', copy=True)
    remark = fields.Text('Remark', copy=True)
    history_line = fields.One2many('to.history.line', 'to_id')
    show_work_line = fields.One2many('show.work.line', 'tracking_id', copy=True)
    safety_line = fields.One2many('safety.line', 'tracking_id', copy=True)
    planning_line = fields.One2many('planing.line', 'tracking_id', copy=True)
    design_line = fields.One2many('design.line', 'tracking_id', copy=True)
    procurement_line = fields.One2many('procurement.line', 'tracking_id', copy=True)
    construction_line = fields.One2many('construction.line', 'tracking_id', copy=True)
    labor_line = fields.One2many('labor.line.report', 'tracking_id', copy=True)
    inspection_line = fields.One2many('inspection.line.report', 'tracking_id', copy=True)
    actual_start_date = fields.Date('Actual Start Date', copy=True)
    projected_finish_date = fields.Date('Projected Finished Date', copy=True)
    state = fields.Selection([('draft', 'Draft'),
                              ('confirm', 'Confirm')], default='draft')
    count_history = fields.Integer('History', compute='count_history_ids')
    un_to_reference = fields.Char('Reference Number')
    wpr_number = fields.Char('WPR Number')
    report_week_from = fields.Char('Report Week from')
    report_week_to = fields.Char('Report Week To')

    @api.depends('tracking_line.this_period_amt')
    def _calculate_total_percentage(self):
        for data in self:
            current_tot = 0.0
            tot_contract = 0.0
            p = 0
            for to in data.tracking_line:
                current_tot = current_tot + to.this_period_amt
                tot_contract = tot_contract + to.total_material_labor
                if current_tot > 0.0 or tot_contract > 0.0:
                    p = (current_tot * 100) / tot_contract
            data.overall_percentage = p


    def count_history_ids(self):
        for rec in self:
            count = self.search([('state', '=', 'confirm'), ('to_number', '=', self.to_number)])
            rec.count_history = len(count)

    def open_history(self):
        to_ids = self.search([('state', '=', 'confirm'), ('to_number', '=', self.to_number)])
        form_view = self.env.ref('nuro_project_task_management.task_order_tracking_form_confirm').id
        tree_view = self.env.ref('nuro_project_task_management.task_order_tree_confirm').id
        return {
            'name': _('History'),
            'res_model': 'to.tracking',
            'views': [
                (tree_view, 'tree'),
                (form_view, 'form'),
            ],
            'view_mode': 'tree,form',
            'type': 'ir.actions.act_window',
            'domain': [('id', 'in', to_ids.ids)]
        }

    def tracking_confirm(self):
        # self.copy()
        if self.show_work_line:
            for line in self.show_work_line:
                # if line.layout_category_id:
                #     tracking_line_id = self.env['tracking.line'].search(
                #         [('layout_category_id', '=', line.layout_category_id.id),
                #         ('tracking_id', '=', line.tracking_id.id)])
                #     if tracking_line_id:
                #          tracking_line_id.current = line.percentage
                tracking_line_id = self.env['tracking.line'].search([('tracking_id', '=', line.tracking_id.id)])
                if tracking_line_id:
                        tracking_line_id.current = line.percentage
        self.copy()
        self.write({'state': 'confirm'})

    def unlink(self):
        if self.state == 'confirm':
            raise ValidationError("Confirm Record can not be delete!!!")

    @api.depends('tracking_line.total_amt')
    def _calculate_profit_amt(self):
        for rec in self:
            total = previous_tot = 0.0
            for line in rec.tracking_line:
                total += line.total_amt
                previous_tot += line.previous_amt
            rec.total_profit_amt = total
            rec.total_previous_amt = previous_tot




    @api.onchange('to_tracking_id')
    def to_details(self):
        if self.to_tracking_id:
            to_ids = self.search([('to_tracking_id', '=', self.to_tracking_id.id)],order='id desc', limit=1)
            if to_ids:
                self.to_start_date = datetime.datetime.strptime(to_ids.to_end_date, '%Y-%m-%d') + timedelta(days=1)
                self.to_complition_percentage = to_ids.to_complition_percentage
                self.previous_to_complition = to_ids.to_complition_percentage
            self.un_to_number = self.to_tracking_id.un_to_reference
            self.location_name = self.to_tracking_id.sector_id.name

    def write(self, vals):
        if self.to_start_date:
            if vals.get('date_from'):
                if str(self.to_start_date) > vals.get('date_from'):
                    raise ValidationError("Tracking Date from Should be greater than TO Start Date")
        if vals.get('date_to') and vals.get('date_from'):
            if vals.get('date_from') > vals.get('date_to'):
                raise ValidationError("To Date should be Greater than From date")
        if vals.get('date_from'):
            if self.date_from:
                if str(self.date_from) > vals.get('date_from'):
                    raise ValidationError("Start Date Should be greater than Previous Start Date")
        if vals.get('date_to'):
            if self.date_to:
                if str(self.date_to) > vals.get('date_to'):
                    raise ValidationError("End Date should be Greater than Previous End date")
        res = super(TOTracking, self).write(vals)
        if self.remark:
            self.to_tracking_id.write({'remark':self.remark})
        return res

    def print_excel(self):
        f_name = '/tmp/progress_report.xlsx'
        workbook = xlsxwriter.Workbook(f_name)
        worksheet = workbook.add_worksheet('Tracking Report')
        worksheet2 = workbook.add_worksheet('Progress Report')
        worksheet.set_column('A:K', 12)
        style = workbook.add_format({
            'bold': 1,
            'border': 1,
            'align': 'center',
            'valign': 'vcenter'})
        style.set_font_size(8)
        align_value = workbook.add_format({
            'align': 'center',
            'valign': 'vcenter'})
        align_value.set_font_size(8)
        align_value_category = workbook.add_format({
            'align': 'left',
            'valign': 'vcenter'})
        align_value_category.set_font_size(8)
        align_value_float = workbook.add_format({
            'align': 'right',
            'valign': 'vcenter'})
        align_value_float.set_font_size(8)

        row = 6
        new_row = row + 1
        worksheet.merge_range('A1:J1', "DEEQA CONSTRUCTION & WATER WELL DRILLING CO. LTD.", style)
        date = 'ab'
        if self.date_to and self.date_from:
            date = datetime.datetime.strptime(self.date_to, '%Y-%m-%d')
            worksheet.merge_range('A2:G2', "REPORT FORMAT FOR PROGRESS REPORT DATE OF ", style)
            worksheet.merge_range('H2:J2', self.date_from + ' ' + 'To' + ' ' + self.date_to, style)
        else:
            worksheet.merge_range('A2:J2', " REPORT FORMAT FOR PROGRESS REPORT", style)
        worksheet.merge_range('D3:F3', self.to_number, style)
        worksheet.merge_range('A5:A6', "No", style)
        worksheet.merge_range('B5:B6', "Iteam", style)
        worksheet.merge_range('C5:C6', "Start Date", style)
        worksheet.merge_range('D5:D6', "Completion Date", style)
        worksheet.merge_range('E5:E6', "Material", style)
        worksheet.merge_range('F5:F6', "Labor", style)
        worksheet.merge_range('G5:G6', "Contract Amount", style)
        worksheet.merge_range('H5:J5', "% Of Accomplishment", style)
        worksheet.write('H%s' % (row), "Previous %", style)
        worksheet.write('I%s' % (row), "This Report %", style)
        worksheet.write('J%s' % (row), "Current %", style)
        worksheet.merge_range('K5:M5', "Value Of Accomplishment", style)
        worksheet.write('K%s' % (row), "Previous Value", style)
        worksheet.write('L%s' % (row), "This Report Value", style)
        worksheet.write('M%s' % (row), "To Date Value", style)
        count = 0
        material_tot = labor_tot = sub_toal = prevous_value = to_date_tot = to_report_val = 0.0
        for line in self.tracking_line:
            count += 1
            worksheet.write('A%s' % (new_row), count, align_value)
            worksheet.write('B%s' % (new_row), '', align_value)
            worksheet.write('C%s' % (new_row), line.start_date, align_value)
            worksheet.write('D%s' % (new_row), line.end_date, align_value)
            worksheet.write('E%s' % (new_row), line.material_amt, align_value)
            worksheet.write('F%s' % (new_row), line.labor_amt, align_value)
            worksheet.write('G%s' % (new_row), line.total_material_labor, align_value)
            worksheet.write('H%s' % (new_row), line.previous, align_value)
            worksheet.write('I%s' % (new_row), line.this_report, align_value)
            worksheet.write('J%s' % (new_row), line.current, align_value)
            worksheet.write('K%s' % (new_row), line.previous_amt, align_value)
            worksheet.write('L%s' % (new_row), line.this_period_amt, align_value)
            worksheet.write('M%s' % (new_row), line.total_amt, align_value)
            material_tot += line.material_amt
            labor_tot += line.labor_amt
            sub_toal += line.total_material_labor
            prevous_value += line.previous_amt
            to_date_tot += line.total_amt
            to_report_val += line.this_period_amt
            new_row += 1
        worksheet.merge_range('A%s:D%s' % (new_row,new_row), 'Total', style)
        worksheet.write('E%s' % (new_row), material_tot, style)
        worksheet.write('F%s'% (new_row), labor_tot, style)
        worksheet.write('G%s'% (new_row), sub_toal, style)
        worksheet.merge_range('H%s:J%s' % (new_row, new_row), ' ', style)
        worksheet.write('K%s' % (new_row), prevous_value, style)
        worksheet.write('L%s' % (new_row), to_report_val, style)
        worksheet.write('M%s'% (new_row), to_date_tot, style)

        row = 11
        new_row = row + 1
        worksheet2.merge_range('A1:K1', "DEEQA CONSTRUCTION & WATER WELL DRILLING CO. LTD.", style)
        worksheet2.merge_range('A2:K2', " REPORT FORMAT FOR PROGRESS REPORT", style)
        worksheet2.write('A4','Project File Reference', style)
        worksheet2.merge_range('B4:C4', "DESCRIPTION", style)
        worksheet2.merge_range('D4:K4', self.remark, style)
        worksheet2.write('A5', self.wpr_number, align_value)
        worksheet2.merge_range('B5:D5', "Contract No." + ' ' + self.un_to_reference or ' ', style)
        worksheet2.merge_range('E5:F5', "Task Order No", style)
        worksheet2.merge_range('G5:H5', self.to_number or  ' ', align_value)
        worksheet2.write('B6', "Start Date", style)
        worksheet2.write('C6', self.to_start_date or ' ', align_value)
        worksheet2.write('D6', "Finish Date", style)
        worksheet2.write('E6', self.to_end_date or ' ', align_value)
        worksheet2.write('F6', "Report Week", style)
        worksheet2.write('G6', self.report_week_from or ' ', align_value)
        worksheet2.write('H6', "Report Week To", style)
        worksheet2.write('I6', self.report_week_to or ' ', align_value)
        worksheet2.merge_range('A7:A9', "Contact", style)
        worksheet2.write('A%s' % (row), "ITEM", style)
        worksheet2.merge_range('B%s:H%s' % (row, row), "DESCRIPTION", style)

        if self.show_work_line:
            worksheet2.write('A%s' % (new_row), '1.0', style)
            worksheet2.merge_range('B%s:G%s' % (new_row, new_row), 'Show WOrk', style)
            worksheet2.write('H%s' % (new_row), "OVERALL % ACCOMPLISHMENT" + ' ' + str(round(self.overall_percentage, 2)), style)
            worksheet2.set_column('H10:H10', 30)
            new_row += 1
            for line in self.show_work_line:
                # if line.layout_category_id:
                #     worksheet2.write('A%s' % (new_row), line.number, align_value)
                #     worksheet2.merge_range('B%s:G%s' % (new_row, new_row), line.layout_category_id.name, align_value_category)
                #     worksheet2.write('H%s' % (new_row), line.percentage, align_value_float)
                #     new_row += 1
                # else:
                    worksheet2.merge_range('B%s:G%s' % (new_row, new_row), line.name, align_value)
                    worksheet2.write('H%s' % (new_row), line.percentage, align_value_float)
                    new_row += 1
        if self.safety_line:
            new_row += 1
            worksheet2.merge_range('A%s:H%s' % (new_row, new_row), 'Safety/Environmental', style)
            new_row += 1
            for line in self.safety_line:
                worksheet2.write('A%s' % (new_row), line.number, align_value)
                worksheet2.merge_range('B%s:F%s' % (new_row, new_row), line.desc, align_value)
                worksheet2.merge_range('G%s:H%s' % (new_row, new_row), line.percentage, align_value)
                new_row += 1

        if self.planning_line:
            new_row += 1
            worksheet2.merge_range('A%s:H%s' % (new_row, new_row), 'Planning', style)
            new_row += 1
            for line in self.planning_line:
                worksheet2.write('A%s' % (new_row), line.number, align_value)
                worksheet2.merge_range('B%s:F%s' % (new_row, new_row), line.desc, align_value)
                worksheet2.merge_range('G%s:H%s' % (new_row,new_row), line.percentage, align_value)
                new_row += 1

        if self.design_line:
            new_row += 1
            worksheet2.merge_range('A%s:H%s' % (new_row, new_row), 'Design', style)
            new_row += 1
            for line in self.design_line:
                worksheet2.write('A%s' % (new_row), line.number, align_value)
                worksheet2.merge_range('B%s:F%s' % (new_row, new_row), line.desc, align_value)
                worksheet2.merge_range('G%s:H%s' % (new_row, new_row), line.percentage, align_value)
                new_row += 1

        if self.procurement_line:
            new_row += 1
            worksheet2.merge_range('A%s:H%s' % (new_row, new_row), 'Procurement', style)
            new_row += 1
            for line in self.procurement_line:
                worksheet2.write('A%s' % (new_row), line.number, align_value)
                worksheet2.merge_range('B%s:F%s' % (new_row, new_row), line.desc, align_value)
                worksheet2.merge_range('G%s:H%s' % (new_row, new_row), line.percentage, align_value)
                new_row += 1

        if self.construction_line:
            new_row += 1
            worksheet2.merge_range('A%s:H%s' % (new_row, new_row), 'Construction', style)
            new_row += 1
            for line in self.construction_line:
                worksheet2.write('A%s' % (new_row), line.number, align_value)
                worksheet2.merge_range('B%s:F%s' % (new_row, new_row), line.desc, align_value)
                worksheet2.merge_range('G%s:H%s' % (new_row), line.percentage, align_value)
                new_row += 1

        if self.labor_line:
            new_row += 1
            worksheet2.merge_range('A%s:H%s' % (new_row, new_row), 'Labor', style)
            new_row += 1
            for line in self.labor_line:
                worksheet2.write('A%s' % (new_row), line.number, align_value)
                worksheet2.merge_range('B%s:F%s' % (new_row, new_row), line.desc, align_value)
                worksheet2.merge_range('G%s:H%s' % (new_row, new_row), line.percentage, align_value)
                new_row += 1

        if self.inspection_line:
            new_row += 1
            worksheet2.merge_range('A%s:H%s' % (new_row, new_row), 'Inspection', style)
            new_row += 1
            for line in self.inspection_line:
                worksheet2.write('A%s' % (new_row), line.number, align_value)
                worksheet2.merge_range('B%s:F%s' % (new_row, new_row), line.desc, align_value)
                worksheet2.merge_range('G%s:H%s' % (new_row), line.percentage, align_value)
                new_row += 1
        new_row += 1
        worksheet2.merge_range('A%s:B%s' % (new_row, new_row), 'Submitted By:', style)
        worksheet2.merge_range('C%s:E%s' % (new_row, new_row), ' ', style)

        worksheet2.merge_range('G%s:H%s' % (new_row, new_row), 'Approved By:', style)
        worksheet2.merge_range('I%s:K%s' % (new_row, new_row), ' ', style)

        new_row +=1
        worksheet2.merge_range('A%s:B%s' % (new_row, new_row), 'Contractor:', style)
        worksheet2.merge_range('C%s:E%s' % (new_row, new_row), 'DEEQA CONSTRRUCTION & WATER WELL DRILLING CO.LTD.:', style)

        worksheet2.merge_range('G%s:H%s' % (new_row, new_row), 'Client:', style)
        worksheet2.merge_range('I%s:K%s' % (new_row, new_row), ' ', style)

        new_row +=1
        worksheet2.merge_range('A%s:B%s' % (new_row, new_row), 'Project Manager:', style)
        worksheet2.merge_range('C%s:E%s' % (new_row, new_row), ' ', style)

        worksheet2.merge_range('G%s:H%s' % (new_row, new_row), 'FEMS Representative:', style)
        worksheet2.merge_range('I%s:K%s' % (new_row, new_row), ' ', style)

        new_row +=1
        worksheet2.merge_range('A%s:B%s' % (new_row, new_row), 'Signature:', style)
        worksheet2.merge_range('C%s:E%s' % (new_row, new_row), ' ', style)

        worksheet2.merge_range('G%s:H%s' % (new_row, new_row), 'Signature:', style)
        worksheet2.merge_range('I%s:K%s' % (new_row, new_row), ' ', style)
        workbook.close()
        f = open(f_name, 'rb')
        data = f.read()
        f.close()
        name = 'Progress Report'
        dt = ' ' + 'From' + ' ' + str(self.date_from) + ' ' + 'To' + ' ' + str(self.date_to)
        if self.date_from and self.date_to:
            out_wizard = self.env['xlsx.output'].create({'name': name + dt + '.xlsx',
                                                         'xls_output': base64.encodebytes(data)})
        else:
            out_wizard = self.env['xlsx.output'].create({'name': name + '.xlsx',
                                                         'xls_output': base64.encodebytes(data)})
        view_id = self.env.ref('nuro_project_task_management.xlsx_output_form').id
        return {
            'type': 'ir.actions.act_window',
            'name': _(name),
            'res_model': 'xlsx.output',
            'target': 'new',
            'view_mode': 'form',
            'res_id': out_wizard.id,
            'views': [[view_id, 'form']],
        }

class TrackingLine(models.Model):
    _name = 'tracking.line'

    @api.depends('material_amt', 'labor_amt')
    def _compute_total_material_labor(self):
        for rec in self:
            rec.total_material_labor = rec.material_amt + rec.labor_amt

    @api.depends('current')
    def _calculate_amt(self):
        for data in self:
            data.total_amt = 0
            if data.current:
                data.total_amt = (data.total_material_labor * data.current) / 100

    @api.depends('this_report')
    def _compute_this_report_amt(self):
        for rec in self:
            rec.this_period_amt = (rec.total_material_labor * rec.this_report) / 100

    @api.depends('previous')
    def _compute_previous_amt(self):
        for data in self:
            data.previous_amt = (data.total_material_labor * data.previous) / 100

    tracking_id = fields.Many2one('to.tracking', copy=False, ondelete='cascade')
    material_line_id = fields.Many2one('')
    # layout_category_id = fields.Many2one('sale.layout_category', string='Section')
    total_material_labor = fields.Float('Total Material & Labor', compute='_compute_total_material_labor', store=True)
    previous = fields.Float('Previous %', track_visibility='onchange')
    current = fields.Float('To Date %', track_visibility='onchange')
    start_date = fields.Date('Start Date')
    end_date = fields.Date('End Date', track_visibility='onchange')
    material_amt = fields.Float('Material Amount')
    labor_amt = fields.Float('Labor Amount')
    this_report = fields.Float('This Report %')
    previous_amt = fields.Float('Previous Value', compute='_compute_previous_amt', store=True)
    this_period_amt = fields.Float('This Period Value', compute='_compute_this_report_amt', store=True)
    total_amt = fields.Float('To Date', compute='_calculate_amt', store=True, track_visibility='onchange',)
    name = fields.Char('')
    sequence = fields.Char()
    display_type = fields.Selection([
        ('line_section', "Section"),
        ('line_note', "Note")], default=False)

    def write(self, vals):
        start_date = end_date = False
        history = self.env['to.history.line']
        if vals.get('current'):
            if self.this_report > vals.get('current'):
                raise ValidationError("Complition Percentage Should be Equal or greater then This Report Percentage!!!")
            if self.previous > vals.get('current'):
                raise ValidationError("Complition Percentage Should be Equal or greater then Previous Percentage!!!")
        self.tracking_id.last_update_date = datetime.now().date()
        if vals.get('current'):
            vals['previous'] = self.current
            this_report_per = vals.get('current') - vals['previous']
            vals['this_report'] = this_report_per
        res = super(TrackingLine, self).write(vals)
        return res

class HistoryLine(models.Model):
    _name = 'to.history.line'

    to_id = fields.Many2one('to.tracking')
    user_id = fields.Many2one('res.users', 'User')
    from_date = fields.Date('From Date')
    end_date = fields.Date('From End')
    previous = fields.Float('Previous %')
    current = fields.Float('Current %')
    previous_amt = fields.Float('Previous Amt')
    current_amt = fields.Float('Current Amt')
    to_detail_id = fields.Many2one('tracking.line')
    name = fields.Char()

class ShowWork(models.Model):
    _name = 'show.work.line'

    tracking_id = fields.Many2one('to.tracking')
    task_id = fields.Many2one('project.task', related='tracking_id.to_tracking_id', store=True)
    # layout_category_id = fields.Many2one('sale.layout_category', 'Description')
    percentage = fields.Float('Overall % Accomplishment')
    name = fields.Char('Activity')
    # number = fields.Char('Item No.', compute="_sequence_ref")
    number = fields.Char('Item No.', )
    sequence = fields.Char()
    display_type = fields.Selection([
        ('line_section', "Section"),
        ('line_note', "Note")], default=False)

    def write(self, vals):
        if vals.get('percentage'):
            if self.percentage > vals.get('percentage'):
                raise ValidationError("Current % is greater than previous % !!!")
            # if self.layout_category_id:
            #     tracking_line_id = self.env['tracking.line'].search(
            #         [('layout_category_id', '=', self.layout_category_id.id),
            #         ('tracking_id', '=', self.tracking_id.id)])
            #     if tracking_line_id:
            #          tracking_line_id.current = vals.get('percentage')
            # if not self.layout_category_id and not self.activity:
            #     tracking_line_id = self.env['tracking.line'].search([('tracking_id', '=', self.tracking_id.id)])
            #     if tracking_line_id:
            #             tracking_line_id.current = vals.get('percentage')
        res = super(ShowWork, self).write(vals)
        return res

    @api.depends('tracking_id.show_work_line')
    def _sequence_ref(self):
        for line in self._origin:
            no = 1.0
            for l in line.tracking_id.show_work_line:
                l.number = ''
                no += 0.1
                str_no = str(no)
                l.number = str_no[:3]

class SafetyLine(models.Model):
    _name = 'safety.line'

    tracking_id = fields.Many2one('to.tracking')
    task_id = fields.Many2one('project.task', related='tracking_id.to_tracking_id', store=True)
    # tracking_id = fields.Many2one('to.tracking')
    # layout_category_id = fields.Many2one('sale.layout_category', 'Description')
    percentage = fields.Float('Overall % Accomplishment')
    desc = fields.Char('Description')
    number = fields.Char('Item No.', compute="_sequence_ref")

    @api.depends('tracking_id.safety_line')
    def _sequence_ref(self):
        for line in self:
            no = 2.0
            for l in line.tracking_id.safety_line:
                no += 0.1
                str_no = str(no)
                l.number = str_no[:3]

class PlaningLine(models.Model):
    _name = 'planing.line'

    tracking_id = fields.Many2one('to.tracking')
    task_id = fields.Many2one('project.task', related='tracking_id.to_tracking_id', store=True)
    # tracking_id = fields.Many2one('to.tracking')
    desc = fields.Char('Description')
    # layout_category_id = fields.Many2one('sale.layout_category', 'Description')
    percentage = fields.Float('Overall % Accomplishment')
    number = fields.Char('Item No.', compute="_sequence_ref")

    @api.depends('tracking_id.planning_line')
    def _sequence_ref(self):
        for line in self:
            no = 3.0
            for l in line.tracking_id.planning_line:
                no += 0.1
                str_no = str(no)
                l.number = str_no[:3]


class DesignLine(models.Model):
    _name = 'design.line'

    tracking_id = fields.Many2one('to.tracking')
    task_id = fields.Many2one('project.task', related='tracking_id.to_tracking_id', store=True)
    # tracking_id = fields.Many2one('to.tracking')
    desc = fields.Char('Description')
    # layout_category_id = fields.Many2one('sale.layout_category', 'Description')
    percentage = fields.Float('Overall % Accomplishment')

    number = fields.Char('Item No.', compute="_sequence_ref")

    @api.depends('tracking_id.design_line')
    def _sequence_ref(self):
        for line in self:
            no = 4.1
            for l in line.tracking_id.design_line:
                no += 0.1
                str_no = str(no)
                l.number = str_no[:3]

class ProcurementLine(models.Model):
    _name = 'procurement.line'

    tracking_id = fields.Many2one('to.tracking')
    task_id = fields.Many2one('project.task', related='tracking_id.to_tracking_id', store=True)
    # tracking_id = fields.Many2one('to.tracking')
    desc = fields.Char('Description')
    # layout_category_id = fields.Many2one('sale.layout_category', 'Description')
    percentage = fields.Float('Overall % Accomplishment')
    number = fields.Char('Item No.', compute="_sequence_ref")

    @api.depends('tracking_id.procurement_line')
    def _sequence_ref(self):
        for line in self:
            no = 5.0
            for l in line.tracking_id.procurement_line:
                no += 0.1
                str_no = str(no)
                l.number = str_no[:3]

class ConstructionLine(models.Model):
    _name = 'construction.line'

    tracking_id = fields.Many2one('to.tracking')
    task_id = fields.Many2one('project.task', related='tracking_id.to_tracking_id', store=True)
    # tracking_id = fields.Many2one('to.tracking')
    desc = fields.Char('Activity')
    remark = fields.Char('Remark')
    # layout_category_id = fields.Many2one('sale.layout_category', 'Description')
    percentage = fields.Float('Overall % Accomplishment')
    number = fields.Char('Item No.', compute="_sequence_ref")

    @api.depends('tracking_id.construction_line')
    def _sequence_ref(self):
        for line in self:
            no = 6.0
            for l in line.tracking_id.construction_line:
                no += 0.1
                str_no = str(no)
                l.number = str_no[:3]

class LaborLine(models.Model):
    _name = 'labor.line.report'

    tracking_id = fields.Many2one('to.tracking')
    task_id = fields.Many2one('project.task', related='tracking_id.to_tracking_id', store=True)
    # tracking_id = fields.Many2one('to.tracking')
    desc = fields.Char('Description')
    # layout_category_id = fields.Many2one('sale.layout_category', 'Description')
    percentage = fields.Float('Overall % Accomplishment')
    number = fields.Char('Item No.', compute="_sequence_ref")

    @api.depends('tracking_id.labor_line')
    def _sequence_ref(self):
        for line in self:
            no = 7.0
            for l in line.tracking_id.labor_line:
                no += 0.1
                str_no = str(no)
                l.number = str_no[:3]

class InseptionReport(models.Model):
    _name = 'inspection.line.report'

    tracking_id = fields.Many2one('to.tracking')
    task_id = fields.Many2one('project.task', related='tracking_id.to_tracking_id', store=True)
    # tracking_id = fields.Many2one('to.tracking')
    desc = fields.Char('Description')
    # layout_category_id = fields.Many2one('sale.layout_category', 'Description')
    percentage = fields.Float('Overall % Accomplishment')
    number = fields.Char('Item No.', compute="_sequence_ref")

    @api.depends('tracking_id.inspection_line')
    def _sequence_ref(self):
        for line in self:
            no = 8.0
            for l in line.tracking_id.inspection_line:
                no += 0.1
                str_no = str(no)
                l.number = str_no[:3]