from odoo import fields,models,api,_

class StockLocation(models.Model):
    _inherit = 'stock.location'

    task_location = fields.Boolean('Task Location')

class Warehouse(models.Model):
    _inherit = 'stock.warehouse'

    is_un_warehouse = fields.Boolean()