from odoo import fields,models,_,api

class ProductTemplate(models.Model):
    _inherit = "product.template"

    product_boq_ref = fields.Char('BOQ Ref')
    product_boq_rate = fields.Float('BOQ Rate')
    skilled_ref = fields.Boolean('Skilled')
    unskilled_ref = fields.Boolean('Unskilled')
    minimum_stock_qty = fields.Integer("Minimum Stock Quantity")
    product_boq_ref = fields.Char('BOQ Ref')