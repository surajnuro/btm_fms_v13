from odoo import fields,models,api,_

class Project(models.Model):
    _inherit = 'project.project'

    warehouse_id = fields.Many2one('stock.warehouse', 'Warehouse')
    team_ids = fields.Many2many('tkt.team','team_proj_rel','proj_id','team_id','Team')
    sequence_no = fields.Char('Sequence')
    top_apply = fields.Boolean('Top Apply', default=True)