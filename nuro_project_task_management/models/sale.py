from odoo import fields,models,api,_
from odoo.exceptions import ValidationError, UserError, Warning

class SaleOrder(models.Model):
    _inherit = 'sale.order'

    project_id = fields.Many2one('project.project', 'Project')
