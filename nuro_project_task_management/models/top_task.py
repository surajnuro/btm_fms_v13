from odoo import fields,models,api,_
from datetime import datetime
from odoo.exceptions import ValidationError, Warning

class UnscheduleTask(models.Model):
    _inherit = 'project.task'
    _description = 'Unschedule task'

    material_planing_line = fields.One2many('material.planing.line', 'task_id')
    labor_planing_line = fields.One2many('labor.planing.line', 'task_id')
    un_to_reference = fields.Char('UN TO Reference', tracking=True)
    implementation_period = fields.Integer('Implementation Period', tracking=True)
    implementation_date = fields.Date('Implementation Date', tracking=True)
    completion_date = fields.Date('Completion Date',tracking=True)
    progess_status = fields.Char('Progress Status',tracking=True)
    # task_count = fields.Integer(compute='_compute_task_count', string="Tasks")
    approved_by_id = fields.Many2one('res.users', 'Approved By', tracking=True)
    project_scope = fields.Char('Scope of work', required=1,tracking=True)
    usw_state = fields.Selection([('draft', 'Draft'), ('under_eng_review', 'Under Engineer Review'),
                                  ('pmo_review', 'Under Pmo Review'), ('top_sent', 'TOP Sent'),
                                  ('ordered', 'Task Ordered'),
                                  ('closed', 'Closed'), ('cancel', 'Cancel')], default='draft', tracking=True)
    total_labour_hours = fields.Float(compute='get_labour_total', store=1,tracking=True)
    # invoiced_amount = fields.Float('Invoice Amount', compute='get_total_invoice_value', store=1)
    # invoiced_percentage = fields.Float('Invoice Percentage', compute='get_total_invoice_value', store=1)
    order_line = fields.One2many('task.order.line', 'task_id')
    task_order_date = fields.Date('Task Order Date', readonly=True, tracking=True)
    expected_complition = fields.Date('Expected Finish Date', tracking=True)
    actual_start_date = fields.Date('Actual Start Date', tracking=True)
    actual_end_date = fields.Date('Actual End Date', track_visibility="onchange")
    # previous_to_complition = fields.Float('Previous %', compute='calculate_previous_percentage', readonly=True,
    #                                       group_operator='avg')
    to_complition_percentage = fields.Float('Completed %', group_operator='avg')
    description = fields.Char('Remark')

    total_margin = fields.Float(compute='get_total_margin')
    total_margin_per = fields.Float(compute='get_total_margin')
    total_material_price = fields.Float(compute='get_material_total', store=1)
    total_labour_price = fields.Float(compute='get_labour_total', store=1)
    total_task_order_amount = fields.Float(compute='get_total_top_amount', store=1)
    sequence = fields.Char('Sequence', readonly=True)
    # status_tracket = fields.Selection([('fully_invoiced', 'Fully Invoiced'),
    #                                    ('partial_invoice', 'Partial Invoice'),
    #                                    ('not_invoiced', 'Not Invoiced')], compute='get_invoice_status', store=True)
    top_generated = fields.Boolean(default=False)
    is_closed = fields.Boolean(default=False)
    to_tracking_id = fields.Many2one('to.tracking')
    total_tracking_amt = fields.Float(related='to_tracking_id.total_profit_amt', store=True)
    total_previous_amt = fields.Float(related='to_tracking_id.total_previous_amt', store=True)
    progress_percentage = fields.Float('Progress %', compute='_compute_progress_percentage', store=True)
    un_submit_date = fields.Date('Un Submit Date')
    remark = fields.Text('Tracking Remark')
    section_required = fields.Boolean('Section Required', default=True)
    is_unmerge = fields.Boolean()
    partner_id = fields.Many2one('res.partner')
    approved_by_id = fields.Many2one('res.users', 'Approved By',tracking=True)
    client_order_ref = fields.Char('Client Order Ref',tracking=True)
    labour_breakdown_planing = fields.One2many('labour.breakdown.planing', 'task_id', 'Labour BreakDown')
    ticket_applicable = fields.Selection([('tke_apply', 'Applicable'), ('not_tkt_apply', 'Not Applicable')],
                                         string='Ticket Applicable', default='tke_apply')
    drawing_applicable = fields.Selection([('drawing_apply', 'Applicable'), ('not_drawing', 'Not Applicable')],
                                          string='Drawing Applicable', default='drawing_apply')
    other_applicable = fields.Selection([('other_apply', 'Applicable'), ('not_other_apply', 'Not Applicable')],
                                        string="Other Applicable", default='other_apply')
    ticket_attachment_1 = fields.Binary('Ticket Attachment', attachment=True)
    drawing_attachment_1 = fields.Binary('Drawing Attachment', attachment=True)
    other_attachment_1 = fields.Binary('Other Attachment', attachment=True)
    task_create_date = fields.Date(default=fields.Date.context_today)
    top_task = fields.Boolean()
    task_count = fields.Integer(compute='_compute_task_count', string="Tasks")
    task_id = fields.Many2one('project.task')
    supervisor_id = fields.Many2one('res.users')
    consume_location_id = fields.Many2one('stock.location')
    planned_hours = fields.Float()
    invoice_ids = fields.Many2many('account.move', 'task_inv_ref', 'task_id', 'move_id', 'Invoiced Lines')
    subject = fields.Char(string="Category of Incident")
    invoice_count = fields.Integer(compute='_calculate_invoice_count')
    previous_to_complition = fields.Float('Previous %', compute='calculate_previous_percentage', readonly=True, group_operator='avg')

    api.depends('total_previous_amt')
    def calculate_previous_percentage(self):
        for rec in self:
            if rec.total_previous_amt > 0:
                rec.previous_to_complition = (float(rec.total_previous_amt) / float(rec.total_task_order_amount)) * 100
            else:
                rec.previous_to_complition = 0.0

    def _compute_task_count(self):
        task_env = self.env['project.task']
        for task in self:
            task_data = task_env.search_count([('task_id', 'in', self.ids)])
            task.task_count = task_data

    def _calculate_invoice_count(self):
        inv = self.env['account.move']
        for task in self:
            inv_ids = inv.search_count([('task_id', 'in', self.ids),
                                        ('type', '=', 'out_invoice')])
            task.invoice_count = inv_ids


    def action_view_invoice(self):
        '''open customer invoices from TOP'''
        inv = self.env['account.move']
        inv_ids = inv.search([('task_id', 'in', self.ids),
                                    ('type', '=', 'out_invoice')])
        return {
            'name': _('Invoices'),
            'type': 'ir.actions.act_window',
            'res_model': 'account.move',
            'view_mode': 'tree,form',
            'domain': [('type', '=', 'out_invoice'), ('id', 'in', inv_ids.ids)],
        }

    @api.model
    def default_get(self, fields):
        """
        To get default values for the object.
        @param self: The object pointer.
        @param fields: List of fields for which we want default values
        @return: A dictionary which of fields with values.
        """
        labour_line = []
        skill_ref = self.env.ref('nuro_product_boq.skill_manpower')
        unskill_ref = self.env.ref('nuro_product_boq.unskill_manpower')
        if self._context is None:
            self._context = {}
        res = super(UnscheduleTask, self).default_get(fields)
        labour_line.append((0, 0, {'product_id': skill_ref.id, 'rate': skill_ref.lst_price}))
        labour_line.append((0, 0, {'product_id': unskill_ref.id, 'rate': unskill_ref.lst_price}))
        res.update({'labor_planing_line': labour_line})
        return res

    def get_task(self):
        task_data = self.search([('task_id', 'in', self.ids)])
        form_view = self.env.ref('nuro_project_task_management.unschedule_task_form_view').id
        tree_view = self.env.ref('nuro_project_task_management.unschedule_task_tree_view').id
        if task_data:
            return {
                'name': _('Unschedule Task'),
                'res_model': 'project.task',
                'views': [
                    (tree_view, 'tree'),
                    (form_view, 'form'),
                ],
                'view_mode': 'tree,form',
                'type': 'ir.actions.act_window',
                'domain': [('task_id', 'in', self.ids)]
            }

    @api.model
    def create(self, vals):
        seq = self.env['ir.sequence'].next_by_code('top.sequence')
        code = prefix = ' '
        if self.env.context.get('default_top_task') == True or vals.get('top_task') == True:
            # if self.env['project.project'].browse(vals.get('project_id')).sequence_no:
            #     code = str(self.env['project.project'].browse(vals.get('project_id')).sequence_no).upper() + '-'
            if vals.get('branch_id') or self.env.context.get('branch_id'):
                if self.env['res.branch'].browse(vals.get('branch_id')).prefix:
                    prefix = str(self.env['res.branch'].browse(vals.get('branch_id')).prefix).upper() + '-'
            name = 'TOP' + '-' + prefix + str(seq)
            vals['name'] = name
        res = super(UnscheduleTask, self).create(vals)
        return res

    @api.depends('total_tracking_amt')
    def _compute_progress_percentage(self):
        for rec in self:
            if rec.total_tracking_amt > 0:
                rec.progress_percentage = (float(rec.total_tracking_amt) / float(rec.total_task_order_amount)) * 100

    def submit_pmo_review(self):
        ordere_line=[]
        material_ref = self.env.ref('nuro_product_boq.material_top_product')
        for rec in self.labor_planing_line:
            if rec.manhour > 0.0:
                ordere_line.append((0, 0, {'product_id': rec.product_id.id,
                                           'name': rec.name,
                                           'product_uom': rec.product_id.uom_id.id,
                                           'price_unit': rec.labour_boq_rate or rec.rate,
                                           'product_uom_qty': rec.manhour}))
        ordere_line.append((0, 0, {'product_id': material_ref.id,
                                   'name': material_ref.name,
                                   'product_uom': material_ref.uom_id.id,
                                   'price_unit':self.total_material_price,
                                   'product_uom_qty': 1.0}))

        return self.write({'usw_state':'pmo_review','order_line':ordere_line})

    def send_to_un(self):
        return self.write({'usw_state': 'top_sent', 'un_submit_date': datetime.now().date()})

    def confirm(self):
        '''
        This function confirm task order proposal and create analytic account name as top number.
        and assign same analytic account to TOP.
        :return:
        '''
        if not self.un_to_reference:
            raise ValidationError(_('Please Enter The UN TO Number'))
        self.task_order_date = fields.Datetime.now()
        tracking = self.env['to.tracking']
        tracking_lst = []
        sec_list = []
        progress_lst = []
        skill = self.env['product.template'].search([('name', 'ilike', 'Skilled')], limit=1)
        unskill = self.env['product.template'].search([('name', 'ilike', 'UnSkilled')], limit=1)
        skill_amt = 30
        unskill_amt = 15
        if unskill and skill:
            skill_amt = skill.lst_price
            unskill_amt = unskill.lst_price
        material_tot = labor_tot = 0.0
        skill_tot = unskill_tot = 0.0
        for line in self.material_planing_line:
                material_tot += line.total_rate
        for lb in self.labour_breakdown_planing:
            labor_tot += (lb.man_hr * skill_amt + lb.unskill_man_hr * unskill_amt)
    # append all section name
        if self.material_planing_line:
            for line in self.material_planing_line:
                if line.display_type == 'line_section':
                    if line.name not in sec_list:
                        sec_list.append(line.name)
        # section wise create tracking line
        if sec_list:
            material_seq = labor_seq = 0
            for lst in sec_list:
                tracking_lst.append((0, 0, {
                    'name': lst,
                    'display_type': 'line_section'
                }))
                material_tot = labor_tot = 0.0
                skill_tot = unskill_tot = 0.0
                for line in self.material_planing_line:
                    if lst == line.name:
                        material_tot += line.total_rate
                for lb in self.labour_breakdown_planing:
                    if lst == lb.name:
                        labor_tot += (lb.man_hr * skill_amt + lb.unskill_man_hr * unskill_amt)

                tracking_lst.append((0,0, {
                    'material_amt': material_tot,
                    'labor_amt': labor_tot,
                    'start_date': str(datetime.now().date())
                }))
        # create tracking record
        tracking_id = tracking.create({'to_tracking_id': self.id,
                                       'to_number': self.name,
                                       'to_start_date': self.task_order_date,
                                       'to_end_date': self.completion_date,
                                       'tracking_line': tracking_lst,
                                       'un_to_reference': self.un_to_reference,
                                       })
        breakdown_section = []
        sow_work = self.env['show.work.line']
        # create show work line
        if tracking_id:
            if self.labour_breakdown_planing:
                for bdl in self.labour_breakdown_planing:
                    if bdl.display_type == 'line_section':
                        if bdl.name not in breakdown_section:
                            breakdown_section.append(bdl.name)
            if breakdown_section and self.labour_breakdown_planing:
                for sec in breakdown_section:
                    sow_work.create({'name': sec,
                                     'display_type': 'line_section',
                                     'tracking_id': tracking_id.id})
                    for line in self.labour_breakdown_planing:
                        if line.name == sec:
                            sow_work.create({'name': line.name,
                                             'tracking_id': tracking_id.id})

        return self.write({'usw_state': 'ordered',
                           'to_tracking_id': tracking_id.id or False})

    def cancel(self):
        return self.write({'usw_state': 'cancel'})

    def create_task_invoices(self):
        '''create invoice from task invoicing line'''
        invoice = self.env['account.move']
        invoice_lst = []
        for line in self.order_line:
            if line.invoiced == False:
                invoice_lst.append((0,0, {
                    'product_id': line.product_id.id,
                    'name': line.product_id.name,
                    'quantity': line.product_uom_qty,
                    'price_unit': line.price_unit,
                    'analytic_account_id': self.project_id.analytic_account_id.id,
                    'account_id': line.product_id.property_account_income_id.id or line.product_id.categ_id.property_account_income_categ_id.id
                }))
                line.invoiced = True
        if not invoice_lst:
            raise ValidationError(('Invoice Already created or not found invoicing line!!!'))
        move_id = invoice.create({'project_id': self.project_id.id,
                                  'branch_id': self.branch_id.id,
                                  'partner_id': self.partner_id.id,
                                  'invoice_line_ids': invoice_lst,
                                  'task_id': self.id,
                                  'type': 'out_invoice'})
        self.invoice_ids = [(4, move_id.id)]


    @api.depends('material_planing_line.product_margine_per', 'material_planing_line.product_margine_rate')
    def get_total_margin(self):
        for rec in self:
            rec.total_margin_per = 0.0
            rec.total_margin = 0.0
            for product in rec.material_planing_line:
                if product:
                    rec.total_margin += product.product_margine_rate
                    rec.total_margin_per += product.product_margine_per
            for labour in rec.labor_planing_line:
                if labour:
                    rec.total_margin += labour.labour_margin_rate
                    rec.total_margin_per += labour.labour_margin_per

    @api.depends('material_planing_line.total_rate')
    def get_material_total(self):
        for top in self:
            total = 0.0
            for m_line in top.material_planing_line:
                total = total + m_line.total_rate
            top.total_material_price = total

    @api.depends('total_material_price', 'total_labour_price')
    def get_total_top_amount(self):
        for top in self:
            top.total_task_order_amount = top.total_material_price + top.total_labour_price

    @api.depends('labor_planing_line.amount', 'labor_planing_line.manhour', 'labor_planing_line.rate')
    def get_labour_total(self):
        total = 0.0
        total_labour_hours = 0.0
        for top in self:
            for l_line in top.labor_planing_line:
                total = total + l_line.amount
                total_labour_hours = total_labour_hours + l_line.manhour
            top.total_labour_price = total
            top.total_labour_hours = total_labour_hours

class MaterialPlaning(models.Model):
    _name = 'material.planing.line'

    task_id = fields.Many2one('project.task')
    product_id = fields.Many2one('product.product', required=0)
    description = fields.Char('')
    product_quantity = fields.Float(required=0)
    product_rate = fields.Float(required=0)
    product_uom = fields.Char(compute='get_margine', string='UOM', readonly=True, store=True)
    total_rate = fields.Float(compute='_total_product_rate', store=True)
    product_boq_ref = fields.Char('BOQ Ref')
    product_boq_rate = fields.Float('BOQ Rate')
    product_cost = fields.Float(compute='get_margine', string='Product Cost', store=True)
    product_margine_per = fields.Float(compute='get_margine', string='Margin%', store=True)
    product_margine_rate = fields.Float(compute='get_margine', string='Margin', store=True)
    display_type = fields.Selection([
        ('line_section', "Section"),
        ('line_note', "Note")], default=False)
    sequence = fields.Integer(string='Sequence', default=10)
    name = fields.Char()

    @api.onchange('product_id')
    def product_onchange_name(self):
        if self.product_id:
            self.name = self.product_id.name

    @api.depends('product_id', 'total_rate', 'product_boq_rate')
    def get_margine(self):
        for material_line in self:
            material_line.product_uom = material_line.product_id.uom_id.name
            if material_line.product_id:
                if material_line.product_id.boq_line:
                    boq_line = material_line.product_id.boq_line.filtered(lambda line: line.partner_id.id == material_line.task_id.partner_id.id).limit=1
                    material_line.product_boq_rate = self.env['product.boq.line'].browse(boq_line).amount if boq_line else 0
                else:
                    material_line.product_boq_rate = 0.0
                material_line.product_boq_ref = material_line.product_id.product_boq_ref
                material_line.product_cost = material_line.product_id.standard_price

                if material_line.product_id.standard_price > 0.0:
                    material_line.product_margine_rate = material_line.total_rate - (
                                material_line.product_id.standard_price * material_line.product_quantity)
                    material_line.product_margine_per = ((material_line.product_rate - material_line.product_id.standard_price) / material_line.product_id.standard_price) * 100

    @api.depends('product_quantity', 'product_rate')
    def _total_product_rate(self):
        for material_line in self:
            material_line.total_rate = material_line.product_quantity * material_line.product_rate

class LaborLine(models.Model):
    _name = 'labor.planing.line'

    task_id = fields.Many2one('project.task')
    activities = fields.Char('Activities', required=0)
    duration = fields.Float('DURATION(DAYS)', required=0)
    skill = fields.Integer('SKILLED')
    man_hr = fields.Integer(compute="get_total_skill_man_hrs", string='MAN HR')
    unskill = fields.Integer('UNSKILLED')
    unskill_man_hr = fields.Integer(compute="get_total_unskill_man_hrs", string='UNSKILLED MAN HR')
    product_id = fields.Many2one('product.product')
    # manhour = fields.Float('Manhour')
    manhour = fields.Float('Manhour', compute='get_manhours')
    rate = fields.Float('Rate/manhour')
    amount = fields.Float('Amount', compute='get_amount')
    invoiced = fields.Boolean('Invoiced')
    labour_boq_ref = fields.Char(compute='get_labour_margin', string='BOQ Ref')
    labour_boq_rate = fields.Float(compute='get_labour_margin', string='BOQ Rate')
    labour_margin_per = fields.Float(compute='get_labour_margin', string='Margin%')
    labour_margin_rate = fields.Float(compute='get_labour_margin', string='Margin')
    labour_cost = fields.Float(compute='get_labour_margin', string='Cost')
    name = fields.Char()

    @api.depends('rate', 'manhour')
    def get_amount(self):
        for line in self:
            line.amount = line.manhour * line.rate

    @api.depends('task_id.labour_breakdown_planing')
    def get_manhours(self):
        skill_ref = self.env.ref('nuro_product_boq.skill_manpower')
        unskill_ref = self.env.ref('nuro_product_boq.unskill_manpower')
        for line in self:
            unskill_man_hours, skill_man_hours = 0.0, 0.0
            for breakdown_line in line.task_id.labour_breakdown_planing:
                skill_man_hours = skill_man_hours + breakdown_line.man_hr
                unskill_man_hours = unskill_man_hours + breakdown_line.unskill_man_hr
            if line.product_id.id == skill_ref.id:
                line.manhour = skill_man_hours
            if line.product_id.id == unskill_ref.id:
                line.manhour = unskill_man_hours

    @api.depends('manhour', 'rate', 'amount')
    def get_labour_margin(self):
        for labour_line in self:
            if labour_line.product_id:
                labour_line.labour_boq_ref = labour_line.product_id.product_boq_ref
                labour_line.labour_boq_rate = 0
                labour_line.labour_cost = labour_line.product_id.standard_price
                if labour_line.product_id.standard_price > 0.0:
                    labour_line.labour_margin_rate = labour_line.amount - (
                                labour_line.product_id.standard_price * labour_line.manhour)
                    labour_line.labour_margin_per = ((labour_line.rate - labour_line.product_id.standard_price) / labour_line.product_id.standard_price) * 100
                else:
                    labour_line.labour_margin_rate = 0
                    labour_line.labour_margin_per = 0

    @api.depends('duration', 'skill')
    def get_total_skill_man_hrs(self):
        total = 0.0
        for rec in self:
            rec.total = rec.duration * rec.skill * 8
            rec.man_hr = rec.total

    @api.depends('duration', 'unskill')
    def get_total_unskill_man_hrs(self):
        total = 0.0
        for rec in self:
            rec.total = rec.duration * rec.unskill * 8
            rec.unskill_man_hr = rec.total

class LabouBreakdownPlaning(models.Model):
    _name="labour.breakdown.planing"

    task_id = fields.Many2one('project.task')
    name = fields.Char('Activities', required=1)
    duration = fields.Float('DURATION(DAYS)', required=0)
    skill = fields.Integer('SKILLED')
    man_hr = fields.Integer(compute="get_total_skill_man_hrs",string='MAN HR')
    unskill = fields.Integer('UNSKILLED')
    unskill_man_hr = fields.Integer(compute="get_total_unskill_man_hrs",string='UNSKILLED MAN HR')
    display_type = fields.Selection([
        ('line_section', "Section"),
        ('line_note', "Note")], default=False)
    sequence = fields.Char()

    @api.depends('duration', 'skill')
    def get_total_skill_man_hrs(self):
        total = 0.0
        for rec in self:
            rec.total = rec.duration * rec.skill * 8
            rec.man_hr = rec.total

    @api.depends('duration', 'unskill')
    def get_total_unskill_man_hrs(self):
        total = 0.0
        for rec in self:
            rec.total = rec.duration * rec.unskill * 8
            rec.unskill_man_hr = rec.total

class TaskOrderLine(models.Model):
    _name = 'task.order.line'

    task_id = fields.Many2one('project.task')
    product_id = fields.Many2one('product.product')
    product_uom = fields.Many2one('uom.uom')
    price_unit = fields.Float('')
    product_uom_qty = fields.Float('')
    name = fields.Char()
    invoiced = fields.Boolean()
