from odoo import fields,models,api, _

from odoo import api, fields, models, _

class AccountAnalyticLine(models.Model):
    _inherit = 'account.analytic.line'

    skill = fields.Boolean('Skill')
    unskill = fields.Boolean('Un Skill')
    product_id = fields.Many2one('product.product', domain=['|',('skilled_ref', '=', True),('unskilled_ref', '=', True)])

    #onchange for skilled and unskilled service product
    @api.onchange('product_id')
    def onchange_product(self):
        if self.product_id.skilled_ref == True:
            self.skill = True
        elif self.product_id.unskilled_ref == True:
            self.unskill = True
        else:
            return False