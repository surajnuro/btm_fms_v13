from odoo import fields,models,api,_

class Picking(models.Model):
    _inherit = 'stock.picking'

    task_id = fields.Many2one('project.task')
    client_ticket = fields.Char("Request ID")
    ticket_id = fields.Many2one('website.support.ticket', 'Ticket')
    site_id = fields.Many2one('tkt.site', 'Site', copy=False)
    location = fields.Text('Location', copy=False)
    project_id = fields.Many2one('project.project')
    count = fields.Integer(default=0)
    analytic_account_id = fields.Many2one('account.analytic.account', 'Task Order', index=True)
    additional_transfer = fields.Boolean()
    return_order = fields.Boolean()
    warehouse_id = fields.Many2one('stock.warehouse', 'Warehouse To Request', domain=[('is_un_warehouse', '=', True)])

    def do_print_material_request(self):
        self.write({'count': self.count + 1})
        return self.env.ref('nuro_project_task_management.new_action_stock_picking').report_action(self)

    def action_done(self):
        res = super(Picking, self).action_done()
        for line in self.move_lines:
            if not self.additional_transfer:
                for rec in self.task_id.material_line:
                    if line.material_request_id and rec.id == line.material_request_id.id:
                        if not self.return_order:
                            rec.transfered_qty += line.quantity_done
                        else:
                            rec.transfered_qty -= line.quantity_done
            else:
                for rec in self.task_id.additional_material_line:
                    if line.material_request_id and rec.id == line.material_request_id.id:
                        if not self.return_order:
                            rec.state = 'material_rec'
                            rec.transfered_qty += line.quantity_done
                        else:
                            rec.transfered_qty -= line.quantity_done
        return res


class StockMove(models.Model):
    _inherit = 'stock.move'

    material_request_id = fields.Many2one('task.material.request')