from odoo import fields,models,_

class Company(models.Model):
    _inherit = 'res.company'

    mailing_address = fields.Char()
    mailing_city = fields.Char()
    mailing_state_id = fields.Many2one('res.country.state')
    mailing_country_id = fields.Many2one('res.country')
    mailing_phone = fields.Char()
    mailing_cell = fields.Char()
    mailing_email = fields.Char()
    mailing_zip = fields.Char()
    official_address = fields.Char()
    official_city = fields.Char()
    official_state_id = fields.Many2one('res.country.state')
    official_country_id = fields.Many2one('res.country')
    official_phone = fields.Char()
    official_cell = fields.Char()
    official_email = fields.Char()
    official_zip = fields.Char()
