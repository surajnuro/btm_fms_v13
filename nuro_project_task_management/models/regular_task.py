from odoo import fields,models,api,_
from odoo.exceptions import ValidationError, UserError, Warning
from datetime import timedelta

class ProjectTask(models.Model):
    _inherit = 'project.task'
    _description = 'Regular task'

    @api.depends('quotation_id')
    def _compute_material_line(self):
        for rec in self:
            if rec.quotation_id:
                rec.material_line_length = len(rec.quotation_id.material_order_line)

    quotation_id = fields.Many2one()
    branch_id = fields.Many2one('res.branch', 'Branch', copy=False, tracking=True)
    work_type = fields.Selection([('regularmain','Routine Maintenance'), ('unschedulework','Unschedule Work')],
                                 string="Work Type", copy=False,tracking=True)
    ticket_id = fields.Many2one('website.support.ticket', 'Ticket', tracking=True)
    partner_id = fields.Many2one('res.partner', string="Partner", tracking=True)
    location = fields.Text('Location', copy=False)
    tic_team_id = fields.Many2one('tkt.team', 'Team', copy=False)
    site_id = fields.Many2one('tkt.site', 'Site', copy=False)
    client_ticket = fields.Char("Request ID")
    work_categories_ids = fields.Many2many('work.categories', 'work_categ_task_rel', 'work_tkt_id', 'eork_categ_id',
                                           'Work Categories')
    consume_location_id = fields.Many2one('stock.location', 'Location Consume')

    state = fields.Selection([('draft', 'Draft'), ('submit', 'Submitted'),
                              ('approved', 'Approved'),
                              ('transfer', 'Transfer')], string='Status', default='draft', copy=False, compute='_find_additional_material_state')

    @api.depends('additional_material_line.state')
    def _find_additional_material_state(self):
        '''find  additional material line state'''
        for rec in self:
            if rec.additional_material_line:
                for line in rec.additional_material_line:
                    if line.state == 'draft':
                        rec.state = 'draft'
                    if line.state == 'submit':
                        rec.state = 'submit'
                    if line.state == 'approved':
                        rec.state = 'approved'
                    if line.state == 'transfer':
                        rec.state = 'transfer'
                    if line.state == 'material_rec':
                        rec.state = 'draft'
            else:
                rec.state == 'draft'

    supplier_id = fields.Many2one('res.partner', string='Contractor', domain="[('customer_rank','=', 1)]")
    contractor_work_ids = fields.One2many('contractor.work.line', 'task_id', 'Contractor Work Line')
    close_task = fields.Boolean('Task Closed', readonly=True)
    posted_timesheet_entry = fields.Boolean('Posted Timesheet Expense', copy=False)
    internal_work = fields.Boolean('Internal Work')
    un_to_reference = fields.Char('UN TO Number')
    stock_picking_number = fields.Char('Latest Delivery Number', readonly=True)
    material_line_length = fields.Integer(compute='_compute_material_line', store=True)
    additional_material_check = fields.Boolean()
    warehouse_id = fields.Many2one('stock.warehouse', 'Warehouse')
    picking_ids = fields.Many2many('stock.picking', 'picking_task_reference', 'task_id', 'pick_id')
    aditional_wh_id = fields.Many2one('stock.warehouse', 'Warehouse')
    task_cancel = fields.Boolean('Task Cancel')
    work_user_id = fields.Many2one('res.users')
    consume_location_id = fields.Many2one('stock.location', 'Location Consume')
    material_line = fields.One2many('task.material.request', 'task_id')
    additional_material_line = fields.One2many('task.material.request', 'additional_task_id')
    equipment_ids = fields.One2many('equipment.list', 'task_id', 'equipments')
    bill_id = fields.Many2one('account.move')
    picking_ids = fields.Many2many('stock.picking', 'picking_task_reference', 'task_id', 'pick_id')
    stage_name = fields.Char(related='stage_id.name', string='Stage', store=True)
    stock_picking_id = fields.Many2one('stock.picking', 'Picking')
    send_for_inventory_check = fields.Boolean('Send For Inventory Check', copy=False)
    count = fields.Integer(default=0)
    return_material_line = fields.One2many('task.material.request', 'return_task_id')
    start_date = fields.Date('Start Date')
    finish_date = fields.Date('Finish Date')
    task_duration = fields.Integer('Duration',store=True)
    task_sequence = fields.Char()
    task_done_date = fields.Date('')
    done_within_deadline = fields.Boolean(compute='_calculate_within_without_date', store=True)
    done_overdue_deadline = fields.Boolean(compute='_calculate_within_without_date', store=True)

    @api.depends('date_deadline', 'stage_id', 'task_done_date')
    def _calculate_within_without_date(self):
        '''calculate over ude and within date '''
        for rec in self:
            rec.done_within_deadline = False
            rec.done_overdue_deadline = False
            if rec.date_deadline and rec.task_done_date:
                if rec.date_deadline > rec.task_done_date:
                    rec.done_within_deadline = True
                if rec.task_done_date > rec.date_deadline:
                    rec.done_overdue_deadline = True

    @api.onchange('start_date', 'task_duration')
    def _fill_date_deadline(self):
        if self.start_date and self.task_duration > 0:
            deadline = self.start_date + timedelta(self.task_duration)
            self.date_deadline = deadline
        else:
            self.date_deadline = False

    @api.model
    def create(self, vals):
        seq = self.env['ir.sequence'].next_by_code('task.sequence')
        if self.env.context.get('default_hardwall_task') == True or vals.get('hardwall_task') == True or self.env.context.get('default_work_type') == 'regularmain' or \
                vals.get('work_type') == 'regularmain' or self.env.context.get('default_work_type') == 'unschedulework' or vals.get('work_type') == 'unschedulework':
            project = vals.get('project_id') or vals.get('default_project_id')
            vals['task_sequence'] = 'TASK-'+self.env['project.project'].browse(project).name.upper() + '-' + seq
        res = super(ProjectTask, self).create(vals)
        return res

    @api.constrains('start_date', 'finish_date')
    def task_date_validation(self):
        if self.finish_date and self.start_date:
            if self.start_date > self.finish_date:
                raise ValidationError(('Finish date should be grether than start date!!!'))

    @api.onchange('warehouse_id', 'aditional_wh_id')
    def onchange_wh(self):
        if self.warehouse_id:
            if self.material_line:
                for line in self.material_line:
                    line.update({'warehouse_id': self.warehouse_id.id})
        if self.aditional_wh_id:
            if self.additional_material_line:
                for line in self.additional_material_line:
                    line.update({'warehouse_id': self.aditional_wh_id.id})

    def action_task_cancel(self):
        '''during task cancel first check material is done or not if done then not allow to cancel'''
        stage_id = self.env['project.task.type'].search([('name', '=', 'Cancelled')], limit=1)
        if self.close_task == True:
            raise ValidationError(('Task Already Closed you can not Cancel!!!'))
        picking_ids = self.env['stock.picking'].search([('task_id', '=', self.id)])
        if picking_ids:
            for picking in picking_ids:
                if picking.state not in ['done', 'cancel']:
                    picking.sudo().do_unreserve()
                    picking.sudo().action_cancel()
                if picking.state == 'done':
                    raise ValidationError(('You can not cancel task beacuse some material already transfered'))
        if all(picking_ids for picking in picking_ids if picking.state != 'done'):
            self.task_cancel = True
            self.stage_id = stage_id.id or False
        if not picking_ids:
            self.task_cancel = True
            self.stage_id = stage_id.id or False

    def create_contractor_bill(self):
        inv_line_lst = []
        invoice_obj = self.env['account.move']
        if not self.supplier_id:
            raise UserError("Please fill Suplier !!!")
        if not self.contractor_work_ids:
            raise UserError("Please fill Details of contractor for generate bills !!!")
        search_id = self.env['account.journal'].search([
            ('type', '=', 'purchase')], limit=1)

        if self.work_type == 'regularmain':
            analytic_id = self.env.ref('nuro_project_task_management.analytic_all_regular_maintenance').id
        elif self.work_type in ('unschedulework'):
            analytic_id = self.quotation_id and self.quotation_id.analytic_account_id and self.quotation_id.analytic_account_id.id
        else:
            analytic_id = self.project_id.analytic_account_id and self.project_id.analytic_account_id.id or False

        for rec in self.contractor_work_ids:
            if not rec.invoiced:
                inv_line_lst.append((0, 0, {
                    'name': rec.item_name,
                    'quantity': rec.qty,
                    'price_unit': rec.rate,
                    'account_id': search_id.default_credit_account_id.id,
                    'analytic_account_id': analytic_id or False,
                }))
                rec.invoiced = True
        if not inv_line_lst:
            raise Warning(_('No remaing service for Bill !'))
        if inv_line_lst:
            inv = invoice_obj.sudo().create({
                'partner_id': self.supplier_id.id,
                'invoice_line_ids': inv_line_lst,
                'journal_id': search_id.id,
                'type': 'in_invoice',
                'project_id': self.project_id.id,
                'branch_id': self.branch_id.id,
                'task_id': self.id
            })
            self.bill_id = inv.id

    def request_all_inventory(self):
        '''all inventroy request from  task'''
        inventory_email = ''
        if not self.material_line:
            raise UserError("There is not any material to request for inventory !")

        lst = []
        inventory_group = self.env.ref('stock.group_stock_user')
        for user in inventory_group.users:
            if user.id == 1:
                continue
            inventory_email = inventory_email + user.login + ','
        internal_transfer = False
        wh = {}
        data_lst = []
        for rec in self.material_line:
            if not rec.warehouse_id:
                raise UserError("Please fill Warehouse!!!")
            if not self.consume_location_id:
                raise UserError("Please fill Consume Location!!!")
            wh[rec.warehouse_id] = wh.get(rec.warehouse_id, []) + [rec]
        if self.work_type == 'regularmain':
            analytic_id = self.env.ref('nuro_project_task_management.analytic_all_regular_maintenance').id
        elif self.work_type in ('unschedulework'):
            analytic_id = self.quotation_id and self.quotation_id.analytic_account_id and self.quotation_id.analytic_account_id.id
        else:
            analytic_id = self.project_id.analytic_account_id and self.project_id.analytic_account_id.id or False
        for key, value in wh.items():
            for rec in value:
                rec.requested_qty = rec.request_qty
                if rec.request_qty < rec.requested_qty:
                    raise UserError("Requested Quantity should not greater then Total Quantity!!!!")
                # if rec.request_qty < rec.requested_qty:
                #     raise UserError("There is not request qty for inventory !")
                # if rec.requested_qty > rec.request_qty - rec.total_requested:
                #     raise UserError(_("You have %s for inventory!!!!" % str(float(rec.request_qty) - float(rec.total_requested))))
                if rec.request_qty < rec.done_qty:
                    raise UserError("Done qty should not be greater than request qty!!!!")
                if rec.request_qty <= rec.total_requested:
                    raise UserError(_(
                        "Sorry You are exceeding the Total Quantity Limit, Please Request for Additional Material Approval"))
                data_lst.append((0, 0, {
                            'product_id': rec.product_id.id,
                            'product_uom': rec.product_id.uom_id.id,
                            'product_uom_qty': rec.requested_qty,
                            'name': 'Task Material Request',
                            'material_request_id': rec.id
                        }))
                rec.total_requested = rec.total_requested + rec.requested_qty
                rec.requested_qty = 0.0
                int_type = self.env['stock.picking.type'].search(
                    [('warehouse_id', '=', key.id), ('code', '=', 'internal')], limit=1)
            if not data_lst:
                raise UserError(_("There is no item to request"))
            task_dict = {
                'picking_type_id': int_type.id,
                'location_id': int_type.default_location_src_id.id,
                'location_dest_id': self.consume_location_id.id,
                'task_id': self.id,
                'warehouse_id': key.id,
                'client_ticket': self.client_ticket,
                'ticket_id': self.ticket_id.id,
                'site_id': self.site_id.id,
                'location': self.location,
                'origin': self.name,
                'move_ids_without_package': data_lst,
                'branch_id': self.branch_id.id,
                'project_id': self.project_id.id and self.project_id.id,
                'analytic_account_id': analytic_id or False,
            }
            internal_transfer = self.env['stock.picking'].create(task_dict)
            self.send_for_inventory_check = True
            if internal_transfer:
                self.stock_picking_number = internal_transfer.name
                self.picking_ids = [(4, internal_transfer.id)]
            data_lst = []
        picking = self.env['stock.picking'].search([('task_id', '=', self.id),('warehouse_id', '=', self.warehouse_id.id)])
        if picking:
            return internal_transfer.do_print_material_request()

    def request_for_inventory(self):
        '''request partial inventory'''
        inventory_email = ''
        if not self.material_line:
            raise UserError("There is not any material to request for inventory !")

        lst=[]
        inventory_group = self.env.ref('stock.group_stock_user')
        for user in inventory_group.users:
            if user.id == 1:
                continue
            inventory_email = inventory_email + user.login + ','
        internal_transfer = False
        wh = {}
        data_lst = []
        for rec in self.material_line:
            if not rec.warehouse_id:
                raise UserError("Please fill Warehouse!!!")
            if not self.consume_location_id:
                raise UserError("Please fill Consume Location!!!")
            wh[rec.warehouse_id] = wh.get(rec.warehouse_id, []) + [rec]
        if self.work_type == 'regularmain':
            analytic_id = self.env.ref('nuro_project_task_management.analytic_all_regular_maintenance').id
        elif self.work_type in ('repair_upgrade', 'unschedulework'):
            analytic_id = self.quotation_id and self.quotation_id.analytic_account_id and self.quotation_id.analytic_account_id.id
        else:
            analytic_id = self.project_id.analytic_account_id and self.project_id.analytic_account_id.id or False
        check_qty = ''
        for key, value in wh.items():
            for rec in value:
                if rec.requested_qty == 0.0:
                    continue
                check_qty = 'Greater Value'
                if rec.request_qty < rec.requested_qty:
                    raise UserError("Requested Quantity should not greater then Total Quantity!!!!")
                # if rec.request_qty < rec.requested_qty:
                #     raise UserError("There is not request qty for inventory !")
                if rec.requested_qty > rec.request_qty - rec.total_requested:
                    rem_qty = float(rec.request_qty) - float(rec.total_requested)
                    raise UserError(_("You have %s Quantity for inventory for %s !!!" % (str(rem_qty), rec.product_id.name)))
                if rec.request_qty < rec.done_qty:
                    raise UserError("Done qty should not be greater than request qty!!!!")
                if rec.request_qty <= rec.total_requested:
                    raise UserError(_(
                        "Sorry You are exceeding the Total Quantity Limit, Please Request for Additional Material Approval"))
                data_lst.append((0, 0, {
                    'product_id': rec.product_id.id,
                    'product_uom': rec.product_id.uom_id.id,
                    'product_uom_qty': rec.requested_qty,
                    'name': 'Task Material Request',
                    'material_request_id': rec.id
                }))
                rec.total_requested = rec.total_requested + rec.requested_qty
                rec.requested_qty = 0.0
                int_type = self.env['stock.picking.type'].search(
                    [('warehouse_id', '=', key.id), ('code', '=', 'internal')], limit=1)
            if not data_lst:
                raise UserError(_("There is no item to request"))
            task_dict = {
                'picking_type_id': int_type.id,
                'location_id': int_type.default_location_src_id.id,
                'location_dest_id': self.consume_location_id.id,
                'task_id': self.id,
                'warehouse_id': key.id,
                'client_ticket': self.client_ticket,
                'ticket_id': self.ticket_id.id,
                'site_id': self.site_id.id,
                'location': self.location,
                'origin': self.name,
                'move_ids_without_package': data_lst,
                'branch_id': self.branch_id.id,
                'project_id': self.project_id.id and self.project_id.id,
                'analytic_account_id': analytic_id,
            }
            if not check_qty:
                raise UserError("You can not process please fill Requested Quantity !!!!")
            internal_transfer = self.env['stock.picking'].create(task_dict)
            self.send_for_inventory_check = True
            data_lst = []
            if internal_transfer:
                self.stock_picking_number = internal_transfer.name
                self.picking_ids = [(4, internal_transfer.id)]
        picking = self.env['stock.picking'].search(
            [('task_id', '=', self.id), ('warehouse_id', '=', self.warehouse_id.id)])
        if picking:
            return internal_transfer.do_print_material_request()

    def task_done(self):
        '''
        code create return picking from work location to stock location for remaining qty
        :return:
        '''
        picking = self.env['stock.picking']
        for data in self:
            # picking_ids = picking.search([('task_id', '=', data.id), ('state', 'not in', ('done', 'cancel'))])
            # if picking_ids:
            #     picking_ids.write({'note': 'Picking cancel because TOP or task has been closed'})
            #     picking_ids.do_unreserve()
            #     picking_ids.action_cancel()
            if not data.return_material_line:
                return self.write({'close_task': True, 'task_done_date': fields.Date.context_today(self)})
            # if data.stage_id.name != 'Done':
            #     raise Warning(_('You can not close task which is not in Done state'))
            lst = []
            for rec in data.return_material_line:
                if rec.transfered_qty > 0 and (rec.returned_qty < 0 or rec.done_qty < 0):
                    # if (not rec.done_qty or not rec.returned_qty) and rec.transfered_qty:
                    raise UserError("Please fill consumed done quantity in material line !")
                remaning_qty = rec.transfered_qty - (rec.done_qty + rec.returned_qty)
                if remaning_qty > 0.0:
                    lst.append((0, 0, {
                        'product_id': rec.product_id.id,
                        'product_uom': rec.product_id.uom_id.id,
                        'product_uom_qty': remaning_qty,
                        'name': 'Task Material Return Request',
                        'material_request_id': rec.id
                    }))
                    rec.returned_qty += remaning_qty
            if data.work_type == 'regularmain':
                analytic_id = self.env.ref('nuro_project_task_management.analytic_all_regular_maintenance').id
            elif data.work_type in ('unschedulework'):
                analytic_id = self.project_id.analytic_account_id and self.project_id.analytic_account_id.id or False
            int_type = self.env['stock.picking.type'].search(
                [('warehouse_id', '=', self.branch_id.warehouse_id.id), ('code', '=', 'internal')], limit=1)
            if lst:
                task_dict = {
                    'picking_type_id': int_type.id,
                    'location_id': data.consume_location_id.id,
                    'location_dest_id': int_type.default_location_dest_id.id,
                    'task_id': data.id,
                    'client_ticket': data.client_ticket,
                    'ticket_id': data.ticket_id.id,
                    'site_id': data.site_id.id,
                    'location': data.location,
                    'origin': data.task_id.name,
                    'move_ids_without_package': lst,
                    'branch_id': data.branch_id.id,
                    'project_id': data.project_id.id and self.project_id.id,
                    'analytic_account_id': analytic_id or False,
                    'return_order': True
                }
                rec.is_return_transfer = True
                if task_dict:
                    picking_id = self.env['stock.picking'].create(task_dict)
                    self.picking_ids = [(4, picking_id.id)]
            stage_id = self.env['project.task.type'].search([('name', '=', 'Done')], limit=1)
            return self.write({'close_task': True, 'stage_id': stage_id.id or False, 'task_done_date': fields.Date.context_today(self)})

    def print_pdf(self):
        return self.env.ref('nuro_project_task_management.report_accompplishment').report_action(self)

    def send_for_approval(self):
        # material_request = self.env['task.material.request']
        if not self.additional_material_line:
            raise Warning(_('Please add the material lines'))
        self.write({'state': 'submit'})
        for line in self.additional_material_line:
            if line.state == 'draft':
                line.write({'state': 'submit'})
        return True

    def approved_additional_request(self):
        ''' Approved Additional Material Request button for Additional material request'''
        self.write({'state': 'approved'})
        for line in self.additional_material_line:
            if line.state == 'submit':
                line.write({'state': 'approved'})
        return True

    def request_for_additional_inventory(self):
        ''' Create inventory for additional material request'''
        inventory_email = ''
        if not self.additional_material_line:
            raise UserError("There is not any material to request for inventory !")
        if not self.consume_location_id:
            raise UserError("Please fill Consume Location!!!")
        inventory_group = self.env.ref('stock.group_stock_user')
        for user in inventory_group.users:
            if user.id == 1:
                continue
            inventory_email = inventory_email + user.login + ','
        internal_transfer = False
        wh = {}
        data_lst = []
        for rec in self.additional_material_line:
            if not rec.warehouse_id:
                raise UserError("Please fill Warehouse!!!")
            wh[rec.warehouse_id] = wh.get(rec.warehouse_id, []) + [rec]
        if self.work_type == 'regularmain':
            analytic_id = self.env.ref('nuro_project_task_management.analytic_all_regular_maintenance').id
        elif self.work_type in ('unschedulework'):
            analytic_id = self.quotation_id and self.quotation_id.analytic_account_id and self.quotation_id.analytic_account_id.id
        else:
            analytic_id = self.project_id.analytic_account_id and self.project_id.analytic_account_id.id or False

        for key, value in wh.items():
            for rec in value:
                if rec.requested_qty == 0.0:
                    continue
                if rec.request_qty < rec.requested_qty:
                    raise UserError("Requested Quantity should not greater then Total Quantity!!!!")
                if rec.requested_qty > rec.request_qty - rec.total_requested:
                    rem_qty = float(rec.request_qty) - float(rec.total_requested)
                    raise UserError(
                        _("You have %s for inventory for %s !!!" % (str(rem_qty), rec.product_id.name)))
                if rec.request_qty < rec.done_qty:
                    raise UserError("Done qty should not be greater than request qty!!!!")
                if rec.request_qty <= rec.total_requested:
                    raise UserError(_(
                        "Sorry You are exceeding the Total Quantity Limit!!!"))
                if rec.state == 'approved':
                    data_lst.append((0, 0, {
                        'product_id': rec.product_id.id,
                        'product_uom': rec.product_id.uom_id.id,
                        'product_uom_qty': rec.requested_qty,
                        'name': 'Task Material Request',
                        'material_request_id': rec.id
                    }))
                    rec.total_requested = rec.total_requested + rec.requested_qty
                    rec.requested_qty = 0.0
                    int_type = self.env['stock.picking.type'].search(
                        [('warehouse_id', '=', key.id), ('code', '=', 'internal')], limit=1)
                    rec.total_requested = rec.total_requested + rec.requested_qty
                    rec.requested_qty = 0.0
            if not data_lst:
                raise UserError(_("There is no item to request"))
            task_dict = {
                'picking_type_id': int_type.id,
                'location_id': int_type.default_location_src_id.id,
                'location_dest_id': self.consume_location_id.id,
                'task_id': self.id,
                'client_ticket': self.client_ticket,
                'ticket_id': self.ticket_id.id,
                'site_id': self.site_id.id,
                'location': self.location,
                'origin': self.name,
                'additional_transfer': True,
                'move_ids_without_package': data_lst,
                'warehouse_id': key.id,
                'branch_id': self.branch_id.id,
                'project_id': self.project_id.id and self.project_id.id,
                'analytic_account_id': analytic_id or False,
            }
            internal_transfer = self.env['stock.picking'].create(task_dict)
            self.send_for_inventory_check = True
            self.picking_ids = [(4, internal_transfer.id)]
            data_lst = []
            if internal_transfer:
                self.stock_picking_number = internal_transfer.name
                internal_transfer.do_print_material_request()
        picking = self.env['stock.picking'].search(
            [('task_id', '=', self.id), ('warehouse_id', '=', self.aditional_wh_id.id)])
        if picking:
            return internal_transfer.do_print_material_request()

    def download_picking_print(self):
        material_requested = self.env['stock.picking'].sudo().search([('name', '=', self.stock_picking_number)])
        if not self.stock_picking_number:
            raise ValidationError(
                _("You Can not Generate the Picking slip for this Record as Picking number is not available"))
        else:
            return material_requested.do_print_material_request()

    def return_extra_material(self):

        ''' return material form return line '''

        for material in self:
            if not self.return_material_line:
                raise ValidationError(('Please add return line'))
            if material.return_material_line:
                material_lst = []
                for record in material.return_material_line:
                    if not record.is_return_transfer:
                        # ===========check product exist or not in transfer==========
                        material_lines = self.material_line.filtered(lambda p: p.product_id.id == record.product_id.id)
                        add_lines = self.additional_material_line.filtered(
                            lambda add: add.product_id.id == record.product_id.id)
                        if not material_lines and not add_lines:
                            raise ValidationError(('Product %s not found from material line or additional material line' % (
                                record.product_id.name)))
                        # ==============================================================
                        if record.transfered_qty <= 0:
                            continue
                        # if not record.transfered_qty:
                        #     raise UserError("You Can not return the material as material is not transfer yet !")
                        if record.returned_qty >= record.transfered_qty - record.done_qty:
                            raise UserError("You Can not return more than transfered qty!")
                        if record.return_qty > 0.0:
                            material_lst.append((0, 0, {
                                'product_id': record.product_id.id,
                                'product_uom': record.product_id.uom_id.id,
                                'product_uom_qty': record.return_qty,
                                'name': 'Task Material Return Request',
                                'material_request_id': record.id
                            }))
                            record.returned_qty += record.return_qty
                            qty = record.return_qty
                            if self.additional_material_line:
                                for additional in self.additional_material_line:
                                    if record.product_id == additional.product_id:
                                        if qty > 0.0:
                                            avl_qty = (additional.transfered_qty - additional.returned_qty)
                                            if (additional.transfered_qty - additional.returned_qty) > 0.0:
                                                if avl_qty >= qty:
                                                    additional.returned_qty += record.return_qty
                                                    qty = 0.0
                                                else:
                                                    additional.returned_qty += additional.transfered_qty
                                                    qty -= additional.transfered_qty
                            for mat in self.material_line:
                                if abs(qty) > 0.0:
                                    if record.product_id == mat.product_id:
                                        avl_qty = (mat.transfered_qty - mat.returned_qty)
                                        if (mat.transfered_qty - mat.returned_qty) > 0.0:
                                            if avl_qty >= qty:
                                                mat.returned_qty += qty
                                                qty = 0.0
                                            else:
                                                mat.returned_qty += mat.transfered_qty
                                                qty -= mat.transfered_qty
                            record.is_return_transfer = True
                            record.return_qty = 0.0
                            qty = 0.0

                if material.work_type == 'regularmain':
                    analytic_id = self.env.ref('nuro_project_task_management.analytic_all_regular_maintenance').id
                elif material.work_type in ('unschedulework'):
                    analytic_id = material.quotation_id and material.quotation_id.analytic_account_id and material.quotation_id.analytic_account_id.id
                else:
                    analytic_id = self.project_id.analytic_account_id and self.project_id.analytic_account_id.id or False
                int_type = self.env['stock.picking.type'].search(
                    [('warehouse_id', '=', self.branch_id.warehouse_id.id), ('code', '=', 'internal')], limit=1)
                if not material.consume_location_id:
                    raise ValidationError(('Please fill Consume Material Line!!!'))
                if material_lst:
                    task_dict = {
                        'picking_type_id': int_type.id,
                        'location_id': material.consume_location_id.id,
                        'location_dest_id': int_type.default_location_dest_id.id,
                        'task_id': material.id,
                        'client_ticket': material.client_ticket,
                        'ticket_id': material.ticket_id.id,
                        'site_id': material.site_id.id,
                        'location': material.location,
                        'warehouse_id': self.branch_id.warehouse_id.id,
                        'origin': material.name,
                        'move_ids_without_package': material_lst,
                        'branch_id': material.branch_id.id,
                        'project_id': material.project_id.id and self.project_id.id,
                        'analytic_account_id': analytic_id or False,
                        'return_order': True
                    }

                    if task_dict:
                        picking = self.env['stock.picking'].create(task_dict)
                        if picking:
                            self.picking_ids = [(4, picking.id)]
                            return picking.do_print_material_request()


    def post_expense_entry(self):
        '''
        Function post timesheet hours accounting entry for
        :return:
        '''
        total=0.0
        dr_line_dict =[]
        if self.work_type=='regularmain':
            analytic_id = self.env.ref('nuro_project_task_management.analytic_all_regular_maintenance').id
        elif self.work_type in ('unschedulework'):
            analytic_id = self.quotation_id and self.quotation_id.analytic_account_id and self.quotation_id.analytic_account_id.id
        else:
            analytic_id = self.project_id.analytic_account_id and self.project_id.analytic_account_id.id or False

        for timesheet in self.timesheet_ids:
            debit_account_id = self.env.ref('nuro_project_task_management.salary_expense')
            cedit_account_id = self.env.ref('nuro_project_task_management.salary_payable')
            total = total + (timesheet.unit_amount*timesheet.product_id.standard_price)
            dr_line_dict.append ((0, 0,{
                'name': _('labour Cost'),
                'debit': (timesheet.unit_amount*timesheet.product_id.standard_price),
                'account_id': debit_account_id.id,
                'partner_id': timesheet.user_id.partner_id.id,
                'analytic_account_id': analytic_id,
                'project_id': self.project_id.id and self.project_id.id,
                'branch_id': self.branch_id.id,
                'quantity': timesheet.unit_amount,

            }))
            timesheet.amount = -(timesheet.unit_amount*timesheet.product_id.standard_price)

        dr_line_dict.append ((0, 0,{
            'name': _('labour Cost'),
            'credit': total,
            'account_id': cedit_account_id.id,
            'partner_id': timesheet.user_id.partner_id.id,
            'project_id': self.project_id.id and self.project_id.id,
            'branch_id': self.branch_id.id,
        }))

        journal_id = self.env.ref('nuro_project_task_management.labour_expense')
        writeoff_move = self.env['account.move'].create({
            'journal_id': journal_id.id,
            'date': fields.datetime.now().date(),
            'state': 'draft',
            'branch_id': self.branch_id.id,
            'line_ids': dr_line_dict,
        })
        writeoff_move.post()
        self.posted_timesheet_entry = True
        return True

    def get_unskill_men(self):
        total_unskill_hr = 0.0
        for rec in self.timesheet_ids:
            if rec.unskill:
                total_unskill_hr = total_unskill_hr + rec.unit_amount
        return total_unskill_hr

    def get_labour_used(self):
        labour_count = 0.0
        for rec in self.timesheet_ids:
            labour_count = labour_count + 1
        return labour_count

    def get_skill_men(self):
        total_skill_hr = 0.0
        for rec in self.timesheet_ids:
            if rec.skill:
                total_skill_hr = total_skill_hr+rec.unit_amount
        return total_skill_hr


class EquipmentList(models.Model):
    _name = 'equipment.list'

    task_id = fields.Many2one('project.task')
    qty = fields.Char('Quantity')
    unit = fields.Char('Unit')
    item = fields.Char('Item Description')
    remark = fields.Char('Remarks')

class ContractorWorkLine(models.Model):
    _name = 'contractor.work.line'


    @api.depends('qty','rate')
    def get_total(self):
        for line in self:
            line.total = line.qty * line.rate

    item_name = fields.Char('Service/Material')
    qty = fields.Float('Quantity/Hours')
    rate = fields.Float('Rate')
    total = fields.Float('Total', compute='get_total', store=True)
    invoiced = fields.Boolean('Invoiced',readonly=True)
    task_id = fields.Many2one('project.task')

class TaskMaterialRequest(models.Model):
    _name="task.material.request"

    return_task_id = fields.Many2one('project.task')
    product_id = fields.Many2one('product.product','Product')
    source_location = fields.Many2one('stock.location','Source Location')
    request_qty = fields.Float('Total Qty')
    requested_qty = fields.Float('Requested Qty')
    total_requested = fields.Float(string='Total Requested')
    transfered_qty = fields.Float(string='Transferred Qty')
    done_qty = fields.Float(string='Done Qty')
    product_uom = fields.Char(compute='product_onchange', string='UOM', readonly=True, store=True)
    task_id = fields.Many2one('project.task')
    is_return_transfer = fields.Boolean('Return')
    check_type = fields.Boolean()
    work_type = fields.Selection(related='task_id.work_type')
    remark = fields.Char('Remarks')
    additional_task_id = fields.Many2one('project.task')
    state = fields.Selection([('draft', 'Draft'), ('submit', 'Submitted'),
                              ('approved', 'Approved'),
                              ('transfer', 'Requested'),
                              ('material_rec', 'Transferred')], string='Status', default='draft')
    is_return_transfer_aditional = fields.Boolean('Return')
    remaining_qty = fields.Float('Remaining', readonly=True, compute='_compute_remaining_qty')
    return_qty = fields.Float('Return')
    returned_qty = fields.Float('Returned', readonly=True)
    warehouse_id = fields.Many2one('stock.warehouse', store=True, readonly=False)
    return_domain = fields.Boolean(default=True)
    additional_id = fields.Many2one('task.material.request')
    material_line_id = fields.Many2one('task.material.request')

    @api.onchange('done_qty', 'return_qty')
    def _onchange_done_qty(self):
        for rec in self:
            if rec.done_qty > rec.remaining_qty:
                warning_message = 'Hey Listen Please!! Done quantity can not be greater then Remaining qty which is ' + str(
                    rec.remaining_qty)
                rec.done_qty = False
                return ({'warning': {'title': 'Done Qty Error Error', "message": warning_message}})
            if rec.return_qty > rec.remaining_qty:
                warning_message2 = 'Hey Listen Please!! Return quantity can not be greater then Remaining qty which is ' + str(
                    rec.remaining_qty)
                rec.return_qty = False
                return ({'warning': {'title': 'Return Qty Error Error', "message": warning_message2}})
            if rec.return_qty + rec.done_qty > rec.remaining_qty:
                warning_message3 = 'Hey Listen Please!! Addition of Return quantity and done qty can not be greater then Remaining qty which is ' + str(
                    rec.remaining_qty)
                rec.return_qty = False
                rec.done_qty = False
                return ({'warning': {'title': 'Return Qty Error Error', "message": warning_message3}})

    def unlink(self):
        for line in self:
            if line.is_return_transfer == True:
                raise UserError(_('Can not delete line which is returned'))
        return super(TaskMaterialRequest, self).unlink()

    @api.onchange('return_domain')
    def onchange_return_product(self):
        ''' show only transfered material'''
        prod_lst = []
        if self.return_task_id:
            if self.return_task_id.additional_material_line:
                for ad_line in self.return_task_id.additional_material_line:
                    if (ad_line.transfered_qty - ad_line.returned_qty) > 0.0:
                        prod_lst.append(ad_line.product_id.id)
            if self.return_task_id.material_line:
                for line in self.return_task_id.material_line:
                    if (line.transfered_qty - line.returned_qty) > 0.0:
                        prod_lst.append(line.product_id.id)
            return {'domain': {'product_id': [('id', 'in', prod_lst)]}}

    @api.depends('transfered_qty', 'returned_qty', 'done_qty', 'return_qty')
    def _compute_remaining_qty(self):
        for rec in self:
            if rec.transfered_qty and not rec.returned_qty and not rec.done_qty:
                rec.remaining_qty = rec.transfered_qty
            else:
                rec.remaining_qty = rec.transfered_qty - rec.returned_qty

    @api.depends('product_id')
    def product_onchange(self):
        for val in self:
            val.product_uom = val.product_id.uom_id.name

    @api.onchange('product_id')
    def product_onchange_name(self):
        if self.product_id:
            total_transfer = 0
            self.name = self.product_id.name
            if self.return_task_id:
                if self.return_task_id.additional_material_line:
                    for ad_line in self.return_task_id.additional_material_line:
                        if self.product_id == ad_line.product_id:
                            self.additional_id = ad_line._origin.id
                            total_transfer += (ad_line.transfered_qty - ad_line.returned_qty)
                if self.return_task_id.material_line:
                    for line in self.return_task_id.material_line:
                        if self.product_id == line.product_id:
                            self.material_line_id = line._origin.id
                            total_transfer += (line.transfered_qty - line.returned_qty)
                self.transfered_qty = total_transfer


