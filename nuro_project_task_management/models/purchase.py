from odoo import fields,models,api,_

class Purchase(models.Model):
    _inherit = 'purchase.order'

    project_id = fields.Many2one('project.project')
    purchase_type = fields.Selection([('construction', 'Construction FM'), ('maintenance', 'Regular maintenance'),
                                      ('life_support', 'Life support'), ('it', 'IT'), ('project', 'Other Projects'),
                                      ('task_order', 'Task Order'), ('vehicle_maintenance', 'Vehicle Maintenance'),
                                      ('warehouse_re_stocking', 'Warehouse Re-Stocking')], string="Purchase Type")