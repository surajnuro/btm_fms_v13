from odoo import fields,models,api,_

class AccountMove(models.Model):
    _inherit = 'account.move'

    task_id = fields.Many2one('project.task')
    project_id = fields.Many2one('project.project')
    cash = fields.Boolean('Cash')
    invoice_number = fields.Char('Invoice Number')
    invoice_subject = fields.Char('Invoice Subject')
    contract_ref_number = fields.Char('Contract Reference Number')
    res_bank_id = fields.Many2one('res.partner.bank', 'Bank')

    @api.model
    def create(self, vals):
        self.clear_caches()
        res = super(AccountMove, self).create(vals)
        if res.stock_move_id:
            res.branch_id = res.stock_move_id.branch_id.id
            res.project_id = res.stock_move_id.picking_id.project_id.id
        return res


class AccountMoveLine(models.Model):
    _inherit = 'account.move.line'

    project_id = fields.Many2one('project.project', related='move_id.project_id', store=True)