from odoo import fields,models,api,_


class ProjectTaskNormal(models.Model):
    _inherit = 'project.task'
    _description = 'Normal task'

    hardwall_task = fields.Boolean()