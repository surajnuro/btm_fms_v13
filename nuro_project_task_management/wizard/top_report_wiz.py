from odoo import models, fields, api, _
import base64
import xlsxwriter
from datetime import datetime
from odoo.exceptions import ValidationError


class TOPExcel(models.TransientModel):
    _name = 'top.excel.wiz'

    date_start = fields.Date('Start Date')
    date_end = fields.Date('Start End')
    project_id = fields.Many2one('project.project')
    top_id = fields.Many2one('project.task', string='TO Number')
    file_name = fields.Binary('File Name')
    name = fields.Char('Data')

    @api.onchange('date_start', 'date_end')
    def on_change_date_start(self):
        current_date = datetime.today().date()
        if self.date_start:
            st_date = self.date_start
            if st_date >= current_date:
                raise ValidationError('Start date can not be more than current date')
        if self.date_end:
            st_end = self.date_end
            if st_end < st_date or st_end > current_date:
                raise ValidationError('End date can not be less than start date and greater than current date')

    def print_top_excel(self):
        f_name = '/tmp/top_proposal_report.xlsx'
        workbook = xlsxwriter.Workbook(f_name)
        worksheet = workbook.add_worksheet('Report')
        worksheet.set_column('A:N', 12)
        style = workbook.add_format({
            'bold': 1,
            'border': 1,
            'align': 'center',
            'valign': 'vcenter'})
        style.set_font_size(8)
        align_value = workbook.add_format({
            'align': 'center',
            'valign': 'vcenter'})
        align_value.set_font_size(8)

        domain = ([('work_type', '=', 'unschedulework'),('top_task', '=', True)])

        if self.date_start:
            domain.append(('task_create_date', '>=', self.date_start))
        if self.date_end:
            domain.append(('task_create_date', '<=', self.date_end))
        if self.project_id:
            domain.append(('project_id', '=', self.project_id.id))
        if self.top_id:
            domain.append(('id', '=', self.top_id.id))
        top_ids = self.env['project.task'].sudo().search(domain)
        row = 4
        new_row = row + 1
        worksheet.merge_range('A1:J1', "DEEQA CONSTRUCTION & WATER WELL DRILLING CO. LTD.", style)
        if self.date_start and self.project_id:
            worksheet.merge_range('A2:J2', "SUMMARY OF TASK ORDERS PROPOSAL AS OF:" + str(self.date_end) + ' for ' + str(
                self.project_id.name), align_value)
        else:
            worksheet.merge_range('A2:J2', "ALL SUMMARY OF TASK ORDERS PROPOSAL", align_value)
        worksheet.write('A%s' % (row), "TOP No.", style)
        worksheet.write('B%s' % (row), "Ticket Ref No.", style)
        worksheet.write('C%s' % (row), "Location", style)
        worksheet.write('D%s' % (row), "Description", style)
        worksheet.write('E%s' % (row), "Date Submitted To FEMS/IDIS", style)
        worksheet.write('F%s' % (row), "TOP Amount", style)
        worksheet.write('G%s' % (row), "Status", style)
        worksheet.write('H%s' % (row), "Remarks", style)
        worksheet.write('I%s' % (row), "TO Issued", style)
        for top in top_ids:
            worksheet.write('A%s' % (new_row), top.name or '-', align_value)
            worksheet.write('B%s' % (new_row), top.ticket_id.ticket_number, align_value)
            worksheet.write('C%s' % (new_row), top.branch_id.name or 'Not Filled', align_value)
            worksheet.write('D%s' % (new_row), top.project_scope or 'Not Filled', align_value)
            worksheet.write('E%s' % (new_row), str(top.un_submit_date) or 'Not Filled', align_value)
            worksheet.write('F%s' % (new_row), top.total_task_order_amount or 'Not Filled', align_value)
            worksheet.write('G%s' % (new_row), top.usw_state, align_value)
            worksheet.write('H%s' % (new_row), '-' or 'Not Filled', align_value)
            worksheet.write('I%s' % (new_row), top.un_to_reference or 'Not Filled', align_value)
            new_row += 1

        workbook.close()
        f = open(f_name, 'rb')
        data = f.read()
        f.close()
        name = 'TOP Report Excel'

        out_wizard = self.env['top.excel.wiz'].create({'name': name + '.xlsx', 'file_name': base64.b64encode(data)})
        view_id = self.env.ref('nuro_project_task_management.top_excel_report').id
        return {
            'type': 'ir.actions.act_window',
            'res_model': 'top.excel.wiz',
            'target': 'new',
            'view_mode': 'form',
            'res_id': out_wizard.id,
            'views': [[view_id, 'form']],
        }


