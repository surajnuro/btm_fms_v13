from odoo import models, fields, api, _
import base64
import xlsxwriter

class ExcelDownload(models.TransientModel):
    _name = 'sale.order.export'

    file_name = fields.Binary('File Name')
    name = fields.Char('Data')

class SaleOrder(models.Model):
    _inherit = 'sale.order'

    @api.multi
    def print_top_report_excel(self):
        f_name = '/tmp/to_report.xlsx'
        workbook = xlsxwriter.Workbook(f_name)
        worksheet = workbook.add_worksheet('Report')
        worksheet.set_column('A:G', 12)

        logo_data = base64.b64decode(self.env.user.company_id.logo)
        file = "/tmp/" + str(self.env.user.company_id.id) + ".png"
        with open(file, "wb") as imgFile:
            imgFile.write(logo_data)

        style = workbook.add_format({
            'bold': 1,
            'border': 1,
            'align': 'center',
            'valign': 'vcenter'
        })
        style.set_font_size(8)

        new_style = workbook.add_format({
            'align': 'right',
            'valign': 'vcenter',
            'bold': 1,
            'border': 1,
        })
        new_style.set_font_size(8)

        align_value = workbook.add_format({
            'align': 'left',
            'border': 1,
            'valign': 'vcenter'})
        align_value.set_font_size(8)

        signature_style = workbook.add_format({
            'bold': 1,
            'border': 1,
            'align': 'left',
            'valign': 'vcenter'})
        signature_style.set_font_size(8)
        # Code for printing material_order_line
        index = 1
        row = 17

        worksheet.insert_image('A1:G5', file, {'x_offset': 15, 'y_offset': 5})
        worksheet.merge_range('A6:G6', "Task Order Proposal", style)
        worksheet.merge_range('A7:F7', "Task Order Proposal No", new_style)
        worksheet.write('G7', self.sequence or self.un_to_reference or '', style)
        worksheet.merge_range('A8:F8', 'Sector', new_style)
        worksheet.write('G8', self.sector_id.name or '', style)
        worksheet.merge_range('A9:F9', 'Task Location', new_style)
        worksheet.write('G9', self.sector_id.name or '', style)
        worksheet.merge_range('A10:F10', 'Site', new_style)
        worksheet.write('G10', self.site_id.name or '', style)
        worksheet.merge_range('A11:F11', 'To Implementation Period', new_style)
        worksheet.write('G11', self.implementation_period or '', style)
        worksheet.merge_range('A12:F12', 'To Commencement Date', new_style)
        worksheet.write('G12', self.task_order_date or '', style)
        worksheet.merge_range('A13:F13', 'To Completion Date', new_style)
        worksheet.write('G13', self.completion_date or '', style)
        worksheet.merge_range('A14:F14', 'Progress Status', new_style)
        worksheet.write('G14', self.progess_status or '', style)
        worksheet.write('A15', "Scope", align_value)
        worksheet.merge_range('B15:G15', self.project_scope, new_style)
        worksheet.write('A16', 'Cont Ref.', style)
        worksheet.write('B16', 'Decription.', style)
        worksheet.write('C16', 'Unite Of Measure', style)
        worksheet.write('D16', 'Task Order Qty', style)
        worksheet.write('E16', 'Unit Price(USD)', style)
        worksheet.write('F16', 'Task Order Value(USD)', style)
        worksheet.write('G16', 'Reference In Boq', style)
        for sale in self.material_order_line:
            worksheet.write('A%s' % (row), 'A.' + str(index), align_value)
            worksheet.write('B%s' % (row), sale.product_name_id.name or '', align_value)
            worksheet.write('C%s' % (row), sale.product_uom or '', align_value)
            worksheet.write('D%s' % (row), sale.product_quantity or '', align_value)
            worksheet.write('E%s' % (row), '$' + str(sale.product_rate) or '', align_value)
            worksheet.write('F%s' % (row), '$' + str(sale.product_rate * sale.product_quantity) or '',
                            align_value)
            worksheet.write('G%s' % (row), sale.product_name_id.product_boq_ref or '', align_value)
            row += 1
            index += 1
        # for printing TOTAL COST OF MATERIALS
        worksheet.merge_range('A%s:E%s' % (row, row), 'TOTAL COST OF MATERIALS', style)
        worksheet.write('F%s' % (row), '$' + str(self.total_material_price), align_value)

        #  Code for inserting blank line between material_order_line and labour_line
        row = row + 1
        worksheet.merge_range('A%s:G%s' % (row, row), '', align_value)

        #  Code for printing labour_line
        index = 1
        row = row + 1
        worksheet.merge_range('A%s:B%s' % (row, row), 'B.Labour', style)
        worksheet.merge_range('C%s:D%s' % (row, row), 'Manhour', style)
        worksheet.write('E%s' % (row), 'Rate/Manhour', style)
        for labour in self.labour_line:
            row += 1
            worksheet.write('A%s' % (row), 'B.' + str(index), align_value)
            worksheet.write('B%s' % (row), labour.name.name or '', align_value)
            worksheet.merge_range('C%s:D%s' % (row, row), labour.manhour or '', align_value)
            worksheet.write('E%s' % (row), '$' + str(labour.rate) or '', align_value)
            worksheet.write('F%s' % (row), '$' + str(labour.manhour * labour.rate) or '', align_value)
            worksheet.write('G%s' % (row), labour.name.product_boq_ref or '', align_value)
            index += 1
        # for printing TOTAL COST OF LABOUR
        row += 1
        worksheet.merge_range('A%s:E%s' % (row, row), 'TOTAL COST OF LABOUR', style)
        worksheet.write('F%s' % (row), '$' + str(self.total_labour_price), align_value)

        #  Code for inserting blank line between labour_line and signature
        row = row + 1
        worksheet.merge_range('A%s:G%s' % (row, row), '', align_value)

        # Code for printing signature
        row = row + 1
        worksheet.merge_range('A%s:C%s' % (row, row), 'TOTAL AMOUNT OF TASK ORDER', signature_style)
        worksheet.merge_range('D%s:G%s' % (row, row), '$' + str(self.total_task_order_amount), signature_style)
        column_first = ['SIGN AND DATE:', 'PREPARED BY:', 'DEEQA CONSTRUCTION AUTHORIZED \n REPRESENTATIVE:',
                        'SIGN AND DATE:', 'APPROVED BY:', 'UNSOS PM AUTHORIZED REPRESENTATIVE:']
        column_second = ['SIGN AND DATE:', 'CHECKED BY:', 'DEEQA CONSTRUCTION AUTHORIZED REPRESENTATIVE:',
                         'SIGN AND DATE:', 'APPROVED BY:', 'UNSOS-CHIEF PM AUTHORIZED REPRESENTATIVE:']
        for num in range(0, len(column_first)):
            row += 1
            worksheet.merge_range('A%s:C%s' % (row, row), column_first[num], align_value)
            worksheet.merge_range('D%s:G%s' % (row, row), column_second[num], align_value)

        # Code for blank line between signature and labour_breakdown_line
        row = row + 1
        worksheet.merge_range('A%s:G%s' % (row, row), '', align_value)

        #  Code for printing labour_breakdown_line
        row = row + 1
        worksheet.merge_range('A%s:G%s' % (row, row), 'LABOUR BREAKDOWN', style)

        duration = 0
        skill_man_hr = 0
        unskill_man_hr = 0
        row += 1
        worksheet.merge_range('A%s:B%s' % (row, row), 'Activities', style)
        worksheet.write('C%s' % (row), 'Duration(Days)', style)
        worksheet.write('D%s' % (row), 'Skilled', style)
        worksheet.write('E%s' % (row), 'Man Hrs', style)
        worksheet.write('F%s' % (row), 'Unskilled', style)
        worksheet.write('G%s' % (row), 'Man Hrs', style)
        for labour_breakdown in self.labour_breakdown_line:
            row += 1
            worksheet.merge_range('A%s:B%s' % (row, row), labour_breakdown.activities, align_value)
            worksheet.write('C%s' % (row), labour_breakdown.duration, align_value)
            duration += labour_breakdown.duration
            worksheet.write('D%s' % (row), labour_breakdown.skill, align_value)
            worksheet.write('E%s' % (row), labour_breakdown.man_hr, align_value)
            skill_man_hr += labour_breakdown.man_hr
            worksheet.write('F%s' % (row), labour_breakdown.unskill, align_value)
            worksheet.write('G%s' % (row), labour_breakdown.unskill_man_hr, align_value)
            unskill_man_hr += labour_breakdown.unskill_man_hr
        #  for printing total
        row += 1
        worksheet.merge_range('A%s:B%s' % (row, row), 'Total ', style)
        worksheet.write('C%s' % (row), duration, align_value)
        worksheet.write('D%s' % (row), '', align_value)
        worksheet.write('E%s' % (row), skill_man_hr, align_value)
        worksheet.write('F%s' % (row), '', align_value)
        worksheet.write('G%s' % (row), unskill_man_hr, align_value)

        workbook.close()
        f = open(f_name, 'rb')
        data = f.read()
        f.close()
        name = 'Task Order'

        out_wizard = self.env['sale.order.export'].create({'name': name + '.xlsx', 'file_name': base64.b64encode(data)})
        view_id = self.env.ref('gts_sale_contract.export_sale_order_material').id
        return {
            'type': 'ir.actions.act_window',
            'res_model': 'sale.order.export',
            'target': 'new',
            'view_mode': 'form',
            'res_id': out_wizard.id,
            'views': [[view_id, 'form']],
        }