from odoo import api, fields, models, _
from odoo.exceptions import ValidationError, Warning

class SubmitReview(models.TransientModel):
    _name="submit.review.wiz"

    user_id = fields.Many2one('res.users','Submit to')

    @api.model
    def default_get(self, fields):
        res = super(SubmitReview, self).default_get(fields)
        active_id = self.env.context.get('active_id')
        task_id = self.env['project.task'].browse(active_id)
        if task_id.work_type == 'unschedulework':
            if not task_id.material_planing_line and not task_id.total_labour_price:
                raise Warning(_('Please fill required material or Labour Details before submit for review !'))
            if task_id.ticket_applicable == 'tke_apply' and not task_id.ticket_attachment_1:
                raise ValidationError("Please Add Ticket Attachment")
            if task_id.drawing_applicable == 'drawing_apply' and not task_id.drawing_attachment_1:
                raise ValidationError("Please Add Drawing Attachment")
            if task_id.other_applicable == 'other_apply' and not task_id.other_attachment_1:
                raise ValidationError("Please Add Other Attachment ")
            if not task_id.ticket_attachment_1 and not task_id.drawing_attachment_1 and not  task_id.other_attachment_1:
                raise ValidationError(_('You need to attach atleast one document !'))
        res['user_id'] = task_id.approved_by_id.id
        # template = self.env.ref('gts_sale_contract.create_top_mail1_template')
        # template.email_from = self.env.user.login
        # template.send_mail(sale_order.id, force_send=True)
        return res

    def send_for_review(self):
        ''' send for under review and send mail to how is approve tkt and assign top '''
        mail_message = self.env['mail.message']
        channel_obj = self.env['mail.channel']
        active_id = self.env.context.get('active_id')
        task_id = self.env['project.task'].browse(active_id)
        task_id.write({'usw_state':'under_eng_review', 'approved_by_id': self.user_id.id})
        template = self.env.ref('nuro_project_task_management.send_for_review_mail_template')
        template.email_from = self.env.user.login
        template.email_to = task_id.approved_by_id.partner_id.email
        template.send_mail(task_id.id, force_send=True)
        approval_name = self.user_id.name + ',' + ' ' + self.env.user.name
        channel_id = self.env['mail.channel'].search([('name', '=', approval_name)], limit=1)
        # if not channel_id:
        #     data = channel_obj.channel_get_and_minimize(partners_to=[self.user_id.partner_id.id])
        #     mail_message.create({'author_id': self.env.user.partner_id.id,
        #                                    'body': '<p>Hello Please Approve TOP</p>',
        #                                    'model': 'mail.channel',
        #                                    'res_id': data.get('id'),
        #                                    'subtype_id': 1,
        #                                    'message_type': 'comment',
        #                                    'channel_ids': [(6,0,[data.get('id')])]})
        # else:
        #     message = mail_message.create({'author_id': self.env.user.partner_id.id,
        #                                    'body': '<p>Hello Please Approve TOP</p>',
        #                                    'model': 'mail.channel',
        #                                    'res_id': channel_id.id,
        #                                    'subtype_id': 1,
        #                                    'message_type': 'comment',
        #                                    'channel_ids': [(6, 0, [channel_id.id])]})
        #     channel_id._notify(message=message)
        return True