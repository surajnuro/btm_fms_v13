from odoo import api, fields, models, _
from odoo.exceptions import ValidationError, Warning

class SectionMateril(models.Model):
    _name = 'section.material.wiz'

    layout_category_id = fields.Many2one('sale.layout_category', string='Section')
    section_material_line = fields.One2many('section.material.line', 'section_wiz_id')
    section_labor_line = fields.One2many('section.labor.line', 'section_wiz_id')
    type = fields.Selection([('material', 'Material Line'),
                             ('labor', 'Labor Line')], default='material')
    section_required = fields.Boolean('Section Require', default=True)

    @api.multi
    def submit(self):
        material_line = self.env['top.material.line']
        labor = self.env['labour.breakdown']
        active_id = self.env.context.get('active_id')
        if self.type == 'material':
            if active_id:
                if not self.section_material_line:
                    raise ValidationError(_('Please Add Material Line!!!'))
                for line in self.section_material_line:
                    material_id = material_line.search([('top_order_id', '=', active_id),
                                                        ('product_name_id', '=', line.product_id.id)])
                    if material_id:
                        material_id.write({'product_quantity': material_id.product_quantity + line.quantity,
                                           'product_rate': line.price})
                        continue

                    material_line.create({'layout_category_id': self.layout_category_id.id or False,
                                          'product_name_id': line.product_id.id,
                                          'product_quantity': line.quantity,
                                          'product_rate': line.price,
                                          'top_order_id': active_id})
        if self.type == 'labor':
            if active_id:
                if not self.section_labor_line:
                    raise ValidationError(_('Please Add Labor Line!!!'))
                for line in self.section_labor_line:
                    labor.create({'layout_category_id': self.layout_category_id.id or False,
                                          'activities': line.activities,
                                          'duration': line.duration,
                                          'skill': line.skill,
                                          'unskill': line.unskill,
                                          'labour_id': active_id,
                                            'man_hr': line.man_hr,
                                            'unskill_man_hr': line.unskill_man_hr})
                sale_id = self.env['sale.order'].browse(active_id)
                if sale_id:
                    sale_id.get_labour_total()

class SectionMaterialLine(models.TransientModel):
    _name = 'section.material.line'

    section_wiz_id = fields.Many2one('section.material.wiz')
    product_id = fields.Many2one('product.product', 'Product')
    quantity = fields.Float('Quantity')
    price = fields.Float('Price')

    @api.onchange('product_id')
    def onchange_prodssss(self):
        material_line = self.env['top.material.line']
        context = dict(self._context or {})
        active_id = context.get('active_id', False)
        if active_id:
            sale_id = self.env['sale.order'].browse(active_id)
            material_id = material_line.search([('top_order_id', '=', sale_id.id),
                                                ('product_name_id', '=', self.product_id.id)], limit=1)
            if material_id:
                material_id.write({'product_quantity': material_id.product_quantity + self.quantity})
                message = _('%s product already added in Materil Line so it will not add in material line, product quantity will be increase in material line!!!') % \
                          (self.product_id.name)
                warning_mess = {
                    'title': _('Duplicate Product!'),
                    'message': message
                }
                return {'warning': warning_mess}
        return {}

class LaborLine(models.Model):
    _name = 'section.labor.line'

    section_wiz_id = fields.Many2one('section.material.wiz')
    activities = fields.Char('Activities', required=1)
    duration = fields.Float('DURATION(DAYS)', required=1)
    skill = fields.Integer('SKILLED')
    man_hr = fields.Integer(compute="get_total_skill_man_hrs", string='MAN HR')
    unskill = fields.Integer('UNSKILLED')
    unskill_man_hr = fields.Integer(compute="get_total_unskill_man_hrs", string='UNSKILLED MAN HR')

    @api.depends('duration', 'skill')
    def get_total_skill_man_hrs(self):
        total = 0.0
        for rec in self:
            rec.total = rec.duration * rec.skill * 8
            rec.man_hr = rec.total

    @api.depends('duration', 'unskill')
    def get_total_unskill_man_hrs(self):
        total = 0.0
        for rec in self:
            rec.total = rec.duration * rec.unskill * 8
            rec.unskill_man_hr = rec.total