from odoo import models, fields, api, _
import base64
import xlsxwriter
from datetime import datetime
from odoo.exceptions import ValidationError


class ExcelDownload(models.TransientModel):
    _name = 'excel.download'

    file_name = fields.Binary('File Name')
    name = fields.Char('Data')
    date_start = fields.Date('Start Date')
    date_end = fields.Date('Start End')
    project_id = fields.Many2one('project.project', 'Project')
    top_id = fields.Many2one('project.task', string='TO Number')

    @api.onchange('date_start', 'date_end')
    def on_change_date_start(self):
        current_date = datetime.today().date()
        if self.date_start:
            st_date = self.date_start
            if st_date >= current_date:
                raise ValidationError('Start date can not be more than current date')
        if self.date_end:
            st_end = self.date_end
            if st_end < st_date or st_end > current_date:
                raise ValidationError('End date can not be less than start date and greater than current date')

    def print_top_report_excel(self):
        f_name = '/tmp/summary_top_report.xlsx'
        workbook = xlsxwriter.Workbook(f_name)
        worksheet = workbook.add_worksheet('Report')
        worksheet.set_column('A:N', 12)
        style = workbook.add_format({
            'bold': 1,
            'border': 1,
            'align': 'center',
            'valign': 'vcenter'})
        style.set_font_size(8)
        align_value = workbook.add_format({
            'align': 'center',
            'valign': 'vcenter'})
        align_value.set_font_size(8)

        domain = ([('usw_state', '=', 'ordered'), ('work_type', '=', 'unschedulework'),('top_task', '=', True)])

        if self.date_start:
            domain.append(('task_create_date', '>=', self.date_start))
        if self.date_end:
            domain.append(('task_create_date', '<=', self.date_end))
        if self.project_id:
            domain.append(('project_id', '=', self.project_id.id))
        if self.top_id:
            domain.append(('id', '=', self.top_id.id))
        top_ids = self.env['project.task'].sudo().search(domain)

        row = 4
        new_row = row + 1
        worksheet.merge_range('A1:N1', "DEEQA CONSTRUCTION & WATER WELL DRILLING CO. LTD.", style)
        if self.date_start and self.project_id:
            worksheet.merge_range('A2:N2', "SUMMARY OF TASK ORDERS AS OF:" + str(self.date_end) + ' for ' + str(
                self.project_id.name), align_value)
        else:
            worksheet.merge_range('A2:N2', "ALL SUMMARY OF TASK ORDERS",  align_value)
        worksheet.merge_range('A3:A4', "Task Order", style)
        worksheet.merge_range('B3:B4', "Location", style)
        worksheet.merge_range('C3:C4', "Description", style)
        worksheet.merge_range('D3:D4', "Total", style)
        worksheet.merge_range('E3:F3', "Planned", style)
        worksheet.write('E4', 'Start Date', style)
        worksheet.write('F4', 'Completion Date', style)
        worksheet.merge_range('G3:G4', "Task Order Approval Date", style)
        worksheet.merge_range('H3:H4', "Task Order Received & Accepted", style)
        worksheet.merge_range('I3:J3', "Actual", style)
        worksheet.write('I4', 'Start Date', style)
        worksheet.write('J4', 'Completion Date', style)
        worksheet.merge_range('K3:L3', "Percent Accomplishment", style)
        worksheet.write('K4', 'Previous', style)
        worksheet.write('L4', 'To Date', style)
        worksheet.merge_range('M3:M4', "Remark", style)
        for top in top_ids:
            worksheet.write('A%s' % (new_row), top.un_to_reference or 'Not Filled', align_value)
            worksheet.write('B%s' % (new_row), top.branch_id.name or 'Not Filled', align_value)
            worksheet.write('C%s' % (new_row), top.project_scope or 'Not Filled', align_value)
            worksheet.write('D%s' % (new_row), top.total_task_order_amount or 'Not Filled', align_value)
            worksheet.write('E%s' % (new_row), str(top.implementation_date) or 'Not Filled', align_value)
            worksheet.write('F%s' % (new_row), str(top.completion_date) or 'Not Filled', align_value)
            worksheet.write('G%s' % (new_row), str(top.un_submit_date) or 'Not Filled', align_value)
            worksheet.write('H%s' % (new_row), str(top.task_order_date) or 'Not Filled', align_value)
            worksheet.write('I%s' % (new_row), str(top.actual_start_date) or 'Not Filled', align_value)
            worksheet.write('J%s' % (new_row), str(top.actual_end_date) or 'Not Filled', align_value)
            worksheet.write('K%s' % (new_row), top.previous_to_complition or 'Not Filled', align_value)
            worksheet.write('L%s' % (new_row), top.progress_percentage or 'Not Filled', align_value)
            worksheet.write('M%s' % (new_row), top.to_tracking_id.remark or 'Not Filled', align_value)
            new_row += 1

        workbook.close()
        f = open(f_name, 'rb')
        data = f.read()
        f.close()
        name = 'TOP Report'

        out_wizard = self.env['excel.download'].create({'name': name + '.xlsx', 'file_name': base64.b64encode(data)})
        view_id = self.env.ref('nuro_project_task_management.report_sheet_xls').id
        return {
            'type': 'ir.actions.act_window',
            'res_model': 'excel.download',
            'target': 'new',
            'view_mode': 'form',
            'res_id': out_wizard.id,
            'views': [[view_id, 'form']],
        }