from odoo import api, fields, models, _
from odoo.exceptions import UserError

class AssignTask(models.TransientModel):
    _name="assign.task"

    supervisor_name = fields.Many2one('res.users','Technician')
    ticket_team_id = fields.Many2one('tkt.team', 'Team')
    project_id = fields.Many2one('project.project','Sub Project')
    proj_cat_id = fields.Many2one('project.category','Category')

    @api.model
    def default_get(self, fields):
        res = super(AssignTask, self).default_get(fields)
        sale_id = self.env['sale.order'].search([('id', 'in', self._context.get('active_ids'))])
        if sale_id.project_category_id:
            res['proj_cat_id'] = sale_id.project_category_id.id
        if sale_id.sector_id:
            res['project_id'] = sale_id.sector_id.id
        return res

    @api.multi
    def submit(self):
        """ method to create task, create location"""

        active_id = self.env.context.get('active_id')
        sale_order = self.env['sale.order'].browse(active_id)
        address = ''
        if sale_order.ticket_id.location_comp:
            address = address+sale_order.ticket_id.location_comp + ' '
        if sale_order.ticket_id.location_phase:
            address = address + sale_order.ticket_id.location_phase + ' '
        if sale_order.ticket_id.location_room:
            address = address + sale_order.ticket_id.location_room
        if not address:
            address = 'work location for TOP ' +sale_order.un_to_reference

        location_id=self.env.ref('gts_sale_contract.work_consume_location_details')

        if not location_id:
            location = self.env['stock.location']
            location_id = location.create({
                'name': address,
                'usage': 'customer',
                'comapany_id': False
            })
        section_lst = []
        task_id = []
        for order_line in sale_order.material_order_line:
            if order_line.layout_category_id:
                if not order_line.layout_category_id in section_lst:
                    section_lst.append(order_line.layout_category_id)
        if section_lst:
            for rec in section_lst:
                line_lst = []
                for line in sale_order.material_order_line:
                    if rec == line.layout_category_id:
                        line_lst.append((0,0,{'product_id': line.product_name_id.id,
                                                'request_qty':line.product_quantity,
                                                'check_type':True,
                                                }))
                task_dictonary1 = {
                    'user_id':self.supervisor_name.id,
                    'project_id': self.project_id.id,
                    'name':  'task for top'+' '+sale_order.un_to_reference + ' ' + rec.name or ' ',
                    'ticket_id' : sale_order.ticket_id.id,
                    'quotation_id' : sale_order.id,
                    'date_deadline': sale_order.completion_date,
                    'client_ticket':sale_order.client_ticket,
                    'un_to_reference':sale_order.un_to_reference,
                    'tic_team_id': self.ticket_team_id.id,
                    'location':address,
                    'site_id':sale_order.site_id.id,
                    'supervisor_id':sale_order.supervisor_id.id,
                    'consume_location_id':location_id.id,
                    'work_type': 'unschedulework',
                    'planned_hours': sale_order.total_labour_hours,
                    'description':sale_order.project_scope,
                    'material_line': line_lst
                }
                task = self.env['project.task'].create(task_dictonary1)
                task_id.append(task.id)
                line_lst = []
        else:
            material_line = []
            if sale_order.material_order_line:
                for line in sale_order.material_order_line:
                    material_line.append((0, 0, {'product_id': line.product_name_id.id,
                                            'request_qty': line.product_quantity,
                                            'check_type': True,
                                            }))
            task_dictonary1 = {
                'user_id': self.supervisor_name.id,
                'project_id': self.project_id.id,
                'name': 'task for top' + ' ' + sale_order.un_to_reference + ' ' + sale_order.name or ' ',
                'ticket_id': sale_order.ticket_id.id,
                'quotation_id': sale_order.id,
                'client_ticket': sale_order.client_ticket,
                'un_to_reference': sale_order.un_to_reference,
                'tic_team_id': self.ticket_team_id.id,
                'location': address,
                'site_id': sale_order.site_id.id,
                'supervisor_id': sale_order.supervisor_id.id,
                'consume_location_id': location_id.id,
                'work_type': 'unschedulework',
                'planned_hours': sale_order.total_labour_hours,
                'description': sale_order.project_scope,
                'material_line': material_line
            }
            task = self.env['project.task'].create(task_dictonary1)
            task_id.append(task.id)

        # template = self.env.ref('website_support.create_task_mail1_template')
        # template.send_mail(task.id, force_send=True)
        if sale_order.ticket_id:
            sale_order.ticket_id.states='inprogress'
        form_view = self.env.ref('project.view_task_form2').id
        tree_view = self.env.ref('project.view_task_tree2').id
        return {
            'name': _('Task'),
            'res_model': 'project.task',
            'views': [
                (tree_view, 'tree'),
                (form_view, 'form'),
            ],
            'view_mode': 'tree,form',
            'type': 'ir.actions.act_window',
            'domain': [('id', 'in', task_id)]
        }


