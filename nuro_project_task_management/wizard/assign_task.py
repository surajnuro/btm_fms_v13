from odoo import api, fields, models, _
from odoo.exceptions import UserError

class AssignTask(models.TransientModel):
    _name="assign.task.wiz"

    supervisor_name_id = fields.Many2one('res.users','Technician')
    ticket_team_id = fields.Many2one('tkt.team', 'Team')
    project_id = fields.Many2one('project.project','Sub Project')
    branch_id = fields.Many2one('res.branch','Sector')

    @api.model
    def default_get(self, fields):
        res = super(AssignTask, self).default_get(fields)
        task_id = self.env['project.task'].search([('id', 'in', self._context.get('active_ids'))])
        if task_id.project_id:
            res['project_id'] = task_id.project_id.id
        if task_id.branch_id:
            res['branch_id'] = task_id.branch_id.id
        return res

    def submit(self):
        """ method to create task, create location"""
        active_id = self.env.context.get('active_id')
        task_id = self.env['project.task'].browse(active_id)
        address = ''
        if task_id.ticket_id.location_comp:
            address = address+task_id.ticket_id.location_comp + ' '
        if task_id.ticket_id.location_phase:
            address = address + task_id.ticket_id.location_phase + ' '
        if task_id.ticket_id.location_room:
            address = address + task_id.ticket_id.location_room
        if not address:
            address = 'work location for TOP ' +task_id.un_to_reference

        location_id=self.env.ref('nuro_project_task_management.work_consume_location_details')

        if not location_id:
            location = self.env['stock.location']
            location_id = location.create({
                'name': address,
                'usage': 'customer',
                'comapany_id': False
            })
        section_lst = []
        new_task_id = []
        material_line = []
        if task_id.material_planing_line:
            for line in task_id.material_planing_line:
                if line.display_type == False:
                    material_line.append((0, 0, {'product_id': line.product_id.id,
                                            'request_qty': line.product_quantity,
                                            }))
        task_dictonary1 = {
            'user_id': self.supervisor_name_id.id,
            'project_id': self.project_id.id,
            'branch_id': self.branch_id.id,
            'name': 'task for top' + ' ' + task_id.un_to_reference + ' ' + task_id.name or ' ',
            # 'ticket_id': task_id.ticket_id.id,
            'client_ticket': task_id.client_ticket,
            'un_to_reference': task_id.un_to_reference,
            'tic_team_id': self.ticket_team_id.id,
            'location': address,
            'site_id': task_id.site_id.id,
            'supervisor_id': self.supervisor_name_id.id,
            'consume_location_id': location_id.id,
            'work_type': 'unschedulework',
            'date_deadline': task_id.completion_date,
            'planned_hours': task_id.total_labour_hours,
            'description': task_id.project_scope,
            'material_line': material_line,
            # 'top_task': True,
            # 'top_generated': True,
            'task_id': task_id.id,
        }
        task = self.env['project.task'].create(task_dictonary1)
        new_task_id.append(task.id)
        form_view = self.env.ref('nuro_project_task_management.unschedule_task_form_view').id
        tree_view = self.env.ref('nuro_project_task_management.unschedule_task_tree_view').id
        return {
            'name': _('Unschedule Task'),
            'res_model': 'project.task',
            'views': [
                (tree_view, 'tree'),
                (form_view, 'form'),
            ],
            'view_mode': 'tree,form',
            'type': 'ir.actions.act_window',
            'domain': [('id', 'in', new_task_id)]
        }
