from datetime import datetime
from odoo import api, fields, models, _
from odoo.addons import decimal_precision as dp
from odoo.exceptions import UserError, ValidationError
from odoo.tools import float_compare, float_round

class MrpProductProduce(models.TransientModel):
    _inherit = "mrp.product.produce"
    _description = "Record Production"

    def _record_production(self):
        # Check all the product_produce line have a move id (the user can add product
        # to consume directly in the wizard)
        for line in self._workorder_line_ids():
            if not line.move_id:
                # Find move_id that would match
                if line.raw_product_produce_id:
                    moves = self.move_raw_ids
                else:
                    moves = self.move_finished_ids
                move_id = moves.filtered(lambda m: m.product_id == line.product_id and m.state not in ('done', 'cancel'))
                if not move_id:
                    # create a move to assign it to the line
                    if line.raw_product_produce_id:
                        values = {
                            'name': self.production_id.name,
                            'reference': self.production_id.name,
                            'product_id': line.product_id.id,
                            'product_uom': line.product_uom_id.id,
                            'location_id': self.production_id.location_src_id.id,
                            'location_dest_id': line.product_id.property_stock_production.id,
                            'raw_material_production_id': self.production_id.id,
                            'group_id': self.production_id.procurement_group_id.id,
                            'origin': self.production_id.name,
                            'state': 'confirmed',
                            'company_id': self.production_id.company_id.id,
                        }
                    else:
                        values = self.production_id._get_finished_move_value(line.product_id.id, 0, line.product_uom_id.id)
                    move_id = self.env['stock.move'].create(values)
                line.move_id = move_id.id

        # because of an ORM limitation (fields on transient models are not
        # recomputed by updates in non-transient models), the related fields on
        # this model are not recomputed by the creations above
        self.invalidate_cache(['move_raw_ids', 'move_finished_ids'])

        # Save product produce lines data into stock moves/move lines
        quantity = self.qty_producing
        if float_compare(quantity, 0, precision_rounding=self.product_uom_id.rounding) <= 0:
            raise UserError(_("The production order for '%s' has no quantity specified.") % self.product_id.display_name)
        self._update_finished_move()
        self._update_moves()
        if self.production_id.state == 'confirmed':
            self.production_id.write({
                'date_start': datetime.now(),
            })

    # def do_produce(self):
    #     # override for change product_uom_qty from bom line
    #     # Nothing to do for lots since values are created using default data (stock.move.lots)
    #     quantity = self.qty_producing
    #     if float_compare(quantity, 0, precision_rounding=self.product_uom_id.rounding) <= 0:
    #         raise UserError(_("The production order for '%s' has no quantity specified") % self.product_id.display_name)
    #     for move in self.production_id.move_raw_ids:
    #         # TODO currently not possible to guess if the user updated quantity by hand or automatically by the produce wizard.
    #         if move.product_id.tracking == 'none' and move.state not in ('done', 'cancel') and move.unit_factor:
    #             rounding = move.product_uom.rounding
    #             if self.product_id.tracking != 'none':
    #                 # change qty * material product_uom_qty
    #                 qty_to_add = float_round(quantity * move.product_uom_qty, precision_rounding=rounding)
    #                 move._generate_consumed_move_line(qty_to_add, self.lot_id)
    #             elif len(move._get_move_lines()) < 2:
    #                 # change qty * material product_uom_qty
    #                 move.quantity_done += float_round(quantity * move.product_uom_qty, precision_rounding=rounding)
    #             else:
    #                 # change qty * material product_uom_qty
    #                 move._set_quantity_done(quantity * move.product_uom_qty)
    #     for move in self.production_id.move_finished_ids:
    #         if move.product_id.tracking == 'none' and move.state not in ('done', 'cancel'):
    #             rounding = move.product_uom.rounding
    #             if move.product_id.id == self.production_id.product_id.id:
    #                 move.quantity_done += float_round(quantity, precision_rounding=rounding)
    #             elif move.unit_factor:
    #                 # byproducts handling
    #                 move.quantity_done += float_round(quantity * move.unit_factor, precision_rounding=rounding)
    #     self.check_finished_move_lots()
    #     if self.production_id.state == 'confirmed':
    #         self.production_id.write({
    #             'state': 'progress',
    #             'date_start': datetime.now(),
    #         })
    #     return {'type': 'ir.actions.act_window_close'}