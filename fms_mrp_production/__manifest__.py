# -*- coding: utf-8 -*-
# Copyright  Nuro Solution Pvt Ltd
{
    'name': 'MRP Production',
    'category': 'Custom',
    'summary': 'This module will create internal transfer from production',
    'description': """
        This module will create internal transfer from production
    """,
    'author': 'Nuro Solution Pvt Ltd',
    'website': 'http://www.nurosolution.com',
    'company': 'Nuro Solution Pvt Ltd',
    'license': 'Other proprietary',
    'depends': ['product', 'stock','mrp', 'account','stock_account', 'bi_odoo_process_costing_manufacturing'],
    'data': [
        'views/mrp_production.xml',
    ],

    'installable': True,
    'application': True,
}