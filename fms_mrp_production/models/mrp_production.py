from odoo import api, fields, models,_
from odoo.exceptions import ValidationError

class MrpProduction(models.Model):
    _inherit = 'mrp.production'

    is_hide = fields.Boolean()
    picking_count = fields.Integer()
    picking_ids = fields.Many2many('stock.picking', 'mrp_pickings_ref', 'mrp_id', 'picking_id')
    move_raw_ids = fields.One2many(
        'stock.move', 'raw_material_production_id', 'Raw Materials', oldname='move_lines',
        copy=False, states={'done': [('readonly', True)], 'cancel': [('readonly', True)]},
        domain=[('scrapped', '=', False)])

    # def button_mark_done(self):
    #     '''inherit for update cast price in product'''
    #     component_amt = 0
    #     if self.move_raw_ids:
    #         for bom_line in self.move_raw_ids:
    #             component_amt =  component_amt + (((bom_line.product_id.standard_price * bom_line.product_uom_qty) * self.bom_id.casting_percentage)) / 100
    #     sql = '''select sum(debit - credit) from account_move_line where product_id = %s
    #                                                     and account_id = %s''' % (
    #         self.product_id.id, self.product_id.categ_id.property_stock_valuation_account_id.id)
    #     self.env.cr.execute(sql)
    #     result = self.env.cr.fetchone()
    #     balance = (result and result[0] or 0.0)
    #     new_value = ((balance + component_amt) / (self.product_id.qty_available + self.product_qty))
    #     print(new_value)
    #     self.product_id.write({'standard_price': new_value})
    #     # finished product standard price update
    #
    #     if self.bom_id.sub_products:
    #         for sub_product in self.bom_id.sub_products:
    #             amount = 0
    #             amount = amount + ((sub_product.product_id.standard_price * sub_product.product_qty ) * sub_product.casting_percentage) / 100
    #             sql = '''select sum(debit - credit) from account_move_line where product_id = %s
    #                                                                     and account_id = %s''' % (
    #                 sub_product.product_id.id, sub_product.product_id.categ_id.property_stock_valuation_account_id.id)
    #             self.env.cr.execute(sql)
    #             result = self.env.cr.fetchone()
    #             balance = (result and result[0] or 0.0)
    #             new_value = ((balance + amount) / (sub_product.product_id.qty_available + sub_product.product_qty))
    #             sub_product.product_id.write({'standard_price': new_value})
    #     return super(MrpProduction, self).button_mark_done()

    def create_internal_transfer(self):
        '''create material transfer from MRP'''
        picking = self.env['stock.picking']
        self.action_assign()
        mv_lst = []
        if not self.move_raw_ids:
            raise ValidationError(('Please fill Material lines!!'))
        if all(row.product_uom_qty - row.reserved_availability == 0.0 for row in self.move_raw_ids):
            return True
        for row in self.move_raw_ids:
            if row.product_uom_qty - row.reserved_availability == 0.0:
                continue
            mv_lst.append((0,0, {
                'product_id': row.product_id.id,
                'product_uom': row.product_uom.id,
                'product_uom_qty': row.product_uom_qty - row.reserved_availability,
                'name': row.product_id.name,
                'quantity_done': row.product_uom_qty - row.reserved_availability,
                'reserved_availability': row.product_uom_qty - row.reserved_availability,
            }))
        if self.location_src_id == self.location_dest_id:
            raise ValidationError(('Source location and destination location should not be same!!!'))
        picking_data = {'picking_type_id': self.picking_type_id.id,
                        'location_id': self.location_dest_id.id,
                        'location_dest_id': self.location_src_id.id,
                        'origin': self.name,
                        'mrp_production_id': self.id,
                        'move_lines': mv_lst}
        picking_id = picking.create(picking_data)
        picking_id.action_assign()
        self.is_hide = True
        # picking_id.button_validate()
        self.action_assign()
        self.picking_ids = [(4, picking_id.id)]

