from odoo import api, fields, models,_

class StockPicking(models.Model):
    _inherit = 'stock.picking'

    mrp_production_id = fields.Many2one('mrp.production', 'MRP')