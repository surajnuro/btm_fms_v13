from odoo import api, fields, models,_
from odoo.exceptions import ValidationError

class Bom(models.Model):
    _inherit = 'mrp.bom'

    casting_percentage = fields.Float('Casting Percentage')

    @api.constrains('casting_percentage', 'byproduct_ids.casting_percentage')
    def casting_perentage_validation(self):
        total = 0.0
        for line in self.byproduct_ids:
            total += line.casting_percentage
        total += self.casting_percentage
        if total > 100:
            raise ValidationError(('Sum of casting percentage should not greater than 100!!!'))


class BomLine(models.Model):
    _inherit = 'mrp.bom.byproduct'

    casting_percentage = fields.Float('Casting Percentage')